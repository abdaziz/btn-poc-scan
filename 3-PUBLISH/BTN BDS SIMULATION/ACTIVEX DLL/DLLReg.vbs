'Register an ActiveX DLL or OCX.
'
'RUN THIS AS AN ADMIN USER (on Vista or later you will
'be prompted for elevation).
'
'Drag the DLL's icon onto the icon for this file, or
'execute it from a command prompt as in:
'
'          DLLReg.vbs fullpathtoDLL
'
Option Explicit

Private Const ssfSYSTEMx86 = &H29
Private System, WinVer

If WScript.Arguments.Count < 1 Then
  WScript.Echo "Missing DLL parameter." & vbNewLine _
             & "Use DLLReg.vbs fullpathtoDLL" & vbNewLine _
             & "or drag DLL's icon onto this script's icon."
Else
  System = CreateObject("Shell.Application").NameSpace(ssfSYSTEMx86).Self.Path
  With CreateObject("WScript.Shell")
    WinVer = .RegRead("HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\CurrentVersion")
    If Fix(CSng(WinVer)) < 6 then
      'Win2K or XP (run by admin user).
      .Run """" & System & "\regsvr32"" """ & WScript.Arguments(0) & """"
    Else
      'Vista or later, request elevation.
      With CreateObject("Shell.Application")
        .ShellExecute System & "\regsvr32", """" & WScript.Arguments(0) & """", , "runas"
      End With
    End If
  End With
End If
