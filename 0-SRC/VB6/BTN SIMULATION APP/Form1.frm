VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "BTN BDS APP SIMULATION"
   ClientHeight    =   3624
   ClientLeft      =   60
   ClientTop       =   456
   ClientWidth     =   4560
   LinkTopic       =   "Form1"
   ScaleHeight     =   3624
   ScaleWidth      =   4560
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton btnVerifyFinger 
      Caption         =   "Verify"
      Height          =   735
      Left            =   1560
      TabIndex        =   7
      Top             =   2640
      Width           =   1695
   End
   Begin VB.TextBox txtBdsId 
      Height          =   495
      Left            =   1560
      TabIndex        =   0
      Top             =   360
      Width           =   2775
   End
   Begin VB.Label lblMsg 
      Caption         =   "Message:"
      Height          =   375
      Left            =   240
      TabIndex        =   8
      Top             =   1080
      Width           =   855
   End
   Begin VB.Label lblMessage 
      Height          =   495
      Left            =   1200
      TabIndex        =   6
      Top             =   1080
      Width           =   3255
   End
   Begin VB.Label Label6 
      Caption         =   "BDS ID:"
      Height          =   255
      Left            =   240
      TabIndex        =   5
      Top             =   480
      Width           =   1215
   End
   Begin VB.Label lblReceive 
      Caption         =   "receive"
      Height          =   255
      Left            =   1320
      TabIndex        =   4
      Top             =   2160
      Width           =   1215
   End
   Begin VB.Label Label3 
      Caption         =   "fs_receive:"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   2160
      Width           =   1095
   End
   Begin VB.Label Label2 
      Caption         =   "fs_send:"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   1800
      Width           =   1215
   End
   Begin VB.Label lblSend 
      Caption         =   "send"
      Height          =   255
      Left            =   1320
      TabIndex        =   1
      Top             =   1800
      Width           =   1215
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim finger As New FINGER_BTN_VB.Fingerprint
Private Sub btnVerifyFinger_Click()
  Dim sendResult As Integer
    Dim userId As String
    Dim responseCode As Integer
    Dim err As Integer
    Dim workingDir As String
    workingDir = App.Path
    
    userId = txtBdsId.Text
    lblMessage.Caption = ""
    
    If Len(txtBdsId.Text) < 7 Then
        MsgBox ("Bdsid is invalid length character (7 Digit only)")
        Exit Sub
    End If
    
    
     err = finger.Send(userId)
     lblSend.Caption = CStr(err)
     If err <> 0 Then
        If err = 3 Then
            lblMessage.Caption = "Verify failed (BDSID is Mandatory)"
        Else
            MsgBox ("Error fs_send")
        End If
        Exit Sub
     End If
     
     err = finger.Receive(responseCode)
     lblReceive.Caption = CStr(err)
     If err <> 0 Then
            MsgBox ("Error fs_receive")
        Exit Sub
     End If
    
     If responseCode = 0 Then
        lblMessage.Caption = "Verify success (Finger matched)"
     ElseIf responseCode = 1 Then
        lblMessage.Caption = "Verify failed (Finger not matched)"
     ElseIf responseCode = 2 Then
        lblMessage.Caption = "Verify failed (License Error)"
     ElseIf responseCode = 4 Then
        lblMessage.Caption = "Verify failed (BDSID not found)"
     ElseIf responseCode = 5 Then
        lblMessage.Caption = "Verify failed (Timeout)"
     ElseIf responseCode = 6 Then
        lblMessage.Caption = "Verify failed (Cannot open scanner)"
     ElseIf responseCode = 7 Then
        lblMessage.Caption = "Verify failed (Unspecified error)"
     ElseIf responseCode = 8 Then
        lblMessage.Caption = "Verify failed (Connection database central error)"
     ElseIf responseCode = 9 Then
        lblMessage.Caption = "Verify failed (Device busy)"
     ElseIf responseCode = 10 Then
        lblMessage.Caption = "Verify failed (Config file not found)"
     ElseIf responseCode = 11 Then
        lblMessage.Caption = "Verify failed (Central service problem)"
     Else
        lblMessage.Caption = "Verify failed (Unspecified error)"
     End If

End Sub

Private Sub Form_Load()
    txtBdsId.Text = ""
    lblSend.Caption = ""
    lblReceive.Caption = ""
    txtBdsId.MaxLength = 7
End Sub

Private Sub txtBdsId_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
  Case Is < 32
  Case 46
    If InStr(txtBdsId.Text, ".") <> 0 Then KeyAscii = 0
  Case 48 To 57
  Case Else
       KeyAscii = 0
End Select
End Sub
