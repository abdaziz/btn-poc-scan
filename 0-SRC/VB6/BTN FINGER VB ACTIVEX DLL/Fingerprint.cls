VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Fingerprint"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private Declare Function fs_send Lib ".\FINGER_BTN.dll" (ByVal userId As String) As Integer
Private Declare Function fs_receive Lib ".\FINGER_BTN.dll" (ByRef respCode As Integer) As Integer


Public Function Send(ByVal bdsId As String) As Integer

    Send = fs_send(bdsId)
    
End Function


Public Function Receive(ByRef respCode As Integer) As Integer

    Receive = fs_receive(respCode)
    
End Function

