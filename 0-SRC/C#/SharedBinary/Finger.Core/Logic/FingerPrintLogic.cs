﻿using Finger.Core.Data;
using Finger.Core.Entity;
using Finger.Core.Models;
using System;


namespace Finger.Core.Logic
{
    public class FingerPrintLogic
    {
        public UserInfo GetUserInfoByCentralId(int centralId)
        {
            return new FingerCoreProvider().GetUserInfoByCentralId(centralId);

        }

        public FingerTemplateInfo GetTemplatesByUserId(string userId)
        {
            return new FingerCoreProvider().GetTemplateByUserId(userId);
        }

        
    }
}
