﻿using Finger.Core.Base.Interface;
using Finger.Core.Models;
using System;


namespace Finger.Core
{
    public class CoreLicensing
    { 
        #region Private Member(s)
        private IFingerPrintLicensing _license;
        #endregion

        #region Constructor
        public CoreLicensing(IFingerPrintLicensing license)
        {
            _license = license;
        }
        #endregion

        #region Public Method(s)
        public bool Configure()
        {

            return this._license.Configure();
        }

        public bool Configure(bool isWriteLog)
        {

            return this._license.Configure(isWriteLog);
        }

        public Exception GetExceptionDetail()
        {
            return this._license.GetExceptionDetail();
        }
        #endregion
    }
}
