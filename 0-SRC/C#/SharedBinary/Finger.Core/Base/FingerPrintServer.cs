﻿using Finger.Core.Base.Interface;
using Finger.Core.Data;
using Finger.Core.Entity;
using Finger.Core.Logic;
using Finger.Core.Models;
using Finger.Framework.Common;
using Finger.Framework.Entity;
using Neurotec.Biometrics;
using Neurotec.Biometrics.Client;
using Neurotec.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;


namespace Finger.Core.Base
{
    public class FingerPrintServer : IFingerPrintServer
    {
        #region Private Field(s)
        private NBiometricClient _biometricClient;
        private string _guidThreadId = string.Empty;
        private string _appName = string.Empty;
        private string _appVersion = string.Empty;
        private string _logName = string.Empty;
        private FingerCoreProvider _fingerCoreProvider;
        private NBiometricStatus _biometricStatus;
        static int requestCounter;
        #endregion

        #region Properties
        public List<cfgitems> configItems { get; set; }
        public NBiometricClient BiometricClient
        {
            get { return _biometricClient; }
            set { _biometricClient = value; }
        }
        public bool IsVerified { get; set; }

        public double MinimumFAR
        {
            get
            {

                return Convert.ToDouble(configItems.Where(x => x.name == Constant.KEY_MIN_FAR).First().value);

            }
        }

        public string MatcherServerName
        {
            get
            {

                return configItems.Where(x => x.name == Constant.KEY_MATCHER_SERVER_NAME).First().value;

            }
        }

        public Int32 MatcherServerPort
        {
            get
            {

                return Convert.ToInt32(configItems.Where(x => x.name == Constant.KEY_MATCHER_SERVER_PORT).First().value);

            }
        }

        public Int32 MatcherServerAdminPort
        {
            get
            {

                return Convert.ToInt32(configItems.Where(x => x.name == Constant.KEY_MATCHER_ADMIN_SERVER_PORT).First().value);

            }
        }

        public FingerCoreProvider FingerCoreProvider
        {
            get
            {
                if (_fingerCoreProvider == null)
                {
                    _fingerCoreProvider = new FingerCoreProvider();
                }
                return _fingerCoreProvider;
            }
        }

        #endregion

        #region Constructor
        public FingerPrintServer(string guidThreadId, string appName, string appVersion)
        {
            if (configItems == null)
            {
                ErrorInfo errorInfo = null;
                string[] arrKey = { Constant.KEY_MIN_FAR, Constant.KEY_MATCHER_SERVER_NAME, Constant.KEY_MATCHER_SERVER_PORT, Constant.KEY_MATCHER_ADMIN_SERVER_PORT };
                configItems = new FingerCoreProvider().GetFingerConfigItems(out errorInfo, arrKey);
            }

            _guidThreadId = guidThreadId;
            _appName = appName;
            _appVersion = appVersion;
        }

        #endregion

        #region Public Method(s)

        public FingerPrintResult EnrollmentOnServer(List<byte[]> bufferTemplate, int fsCentralId)
        {
            FingerPrintResult fingerPrintResult = null;
            UserInfo userInfo = null;
            bool result = false;
            string logMessage = string.Empty;
            try
            {
                if (configItems == null)
                {
                    Log("Configuration items is null", Constant.LEVEL_ERROR);
                    return fingerPrintResult;
                }
                Log("Starting enrollment finger on server", Constant.LEVEL_DEBUG);

                fingerPrintResult = new FingerPrintResult();
                if (bufferTemplate.Count <= 0)
                    return fingerPrintResult;

                NBuffer buffer = CreateMultiTemplate(bufferTemplate);
                byte[] template = buffer.ToArray();
                result = FingerServerCommand(fsCentralId, template, MinimumFAR, NBiometricOperations.Enroll, MatcherServerName, MatcherServerPort, MatcherServerAdminPort);
                if (result)
                {
                    userInfo = FingerCoreProvider.GetUserInfoByCentralId(fsCentralId);
                    logMessage = string.Format("Enrollment finger on server ({0}) has been succeeded", GetEnrollmentSuccessMessage(userInfo.UserId, userInfo.Name, (short)userInfo.UserStatusId));
                    MappingFingerResultForEnrollment(fsCentralId, userInfo, true, ref fingerPrintResult);

                }
            }
            catch (Exception ex)
            {

                logMessage = string.Format("Enrollment finger on server failed (error: {0}-{1}).", Constant.ERROR_CODE_SYSTEM, ex.Message);
                throw ex;
            }
            finally
            {

                Log(logMessage, Constant.LEVEL_DEBUG);
                Log("End enrollment finger on server", Constant.LEVEL_DEBUG);
            }


            return fingerPrintResult;

        }

        public FingerPrintResult EnrollmentOnServer(List<byte[]> bufferTemplate, int fsCentralId, string branchCode)
        {
            FingerPrintResult fingerPrintResult = null;
            UserInfo userInfo = null;
            bool result = false;
            string logMessage = string.Empty;
            try
            {
                if (configItems == null)
                {
                    Log("Configuration items is null", Constant.LEVEL_ERROR);
                    return fingerPrintResult;
                }
                Log("Get nserver location", Constant.LEVEL_DEBUG);
                string zoneMatcherServerName = FingerCoreProvider.GetInfoNserverLocation(branchCode);

                Log("Starting enrollment finger on server", Constant.LEVEL_DEBUG);
                fingerPrintResult = new FingerPrintResult();
                if (bufferTemplate.Count <= 0)
                    return fingerPrintResult;

                NBuffer buffer = CreateMultiTemplate(bufferTemplate);
                byte[] template = buffer.ToArray();

                result = FingerServerCommand(fsCentralId, template, MinimumFAR, NBiometricOperations.Enroll, string.IsNullOrEmpty(zoneMatcherServerName) ? MatcherServerName : zoneMatcherServerName, MatcherServerPort, MatcherServerAdminPort, branchCode);
                if (result)
                {
                    userInfo = FingerCoreProvider.GetUserInfoByCentralId(fsCentralId);
                    logMessage = string.Format("Enrollment finger on server ({0}) has been succeeded", GetEnrollmentSuccessMessage(userInfo.UserId, userInfo.Name, (short)userInfo.UserStatusId));
                    MappingFingerResultForEnrollment(fsCentralId, userInfo, true, ref fingerPrintResult);

                }
            }
            catch (Exception ex)
            {

                logMessage = string.Format("Enrollment finger on server failed (error: {0}-{1}).", Constant.ERROR_CODE_SYSTEM, ex.Message);
                throw ex;
            }
            finally
            {

                Log(logMessage, Constant.LEVEL_DEBUG);
                Log("End enrollment finger on server", Constant.LEVEL_DEBUG);
            }


            return fingerPrintResult;

        }

        public FingerPrintResult IdentifyFingerOnServer(byte[] template)
        {
            FingerPrintResult fingerResult = null;
            List<FingerMatchingInfo> matchInfo = null;
            string logMessage = string.Empty;
            int matchCount = 0;
            bool result = false;

            try
            {
                if (configItems == null)
                {
                    Log("Configuration items is null", Constant.LEVEL_ERROR);
                    return fingerResult;
                }
                Log("Starting identify finger on server", Constant.LEVEL_DEBUG);
                fingerResult = new FingerPrintResult();
                result = AnalyzedFinger(MatcherServerName, MatcherServerPort, MatcherServerAdminPort, MinimumFAR, NBiometricOperations.Identify, 99999, template, ref matchInfo, ref matchCount);
                if (result & matchCount > 0)
                {
                    fingerResult = MatcherIndexAndScore(matchInfo, matchCount);
                }
            }
            catch (Exception ex)
            {
                logMessage = string.Format("Identify finger on server failed (error: {0}-{1}).", Constant.ERROR_CODE_SYSTEM, ex.Message);
                throw ex;
            }
            finally
            {

                if (fingerResult != null)
                {
                    if (fingerResult.FingerPrintStatus)
                    {
                        logMessage = string.Format("Identify finger on server found (user id:{0} name:{1} score:{2} index:{3})", fingerResult.UserId, fingerResult.Name, fingerResult.Score, fingerResult.Index);
                    }
                    else
                    {
                        logMessage = "Identify finger on server not found";
                    }
                }

                Log(logMessage, Constant.LEVEL_DEBUG);
                Log("End identify finger on server", Constant.LEVEL_DEBUG);

            }

            return fingerResult;

        }

        public FingerPrintResult IdentifyFingerOnServer(byte[] template, string branchCode)
        {
            FingerPrintResult fingerResult = null;
            List<FingerMatchingInfo> matchInfo = null;
            string logMessage = string.Empty;
            int matchCount = 0;
            bool result = false;

            try
            {
                if (configItems == null)
                {
                    Log("Configuration items is null", Constant.LEVEL_ERROR);
                    return fingerResult;
                }
                Log("Get nserver location", Constant.LEVEL_DEBUG);
                string zoneMatcherServerName = FingerCoreProvider.GetInfoNserverLocation(branchCode);

                Log("Starting identify finger on server", Constant.LEVEL_DEBUG);
                fingerResult = new FingerPrintResult();
                result = AnalyzedFinger(string.IsNullOrEmpty(zoneMatcherServerName) ? MatcherServerName : zoneMatcherServerName, MatcherServerPort, MatcherServerAdminPort, MinimumFAR, NBiometricOperations.Identify, template, branchCode, ref matchInfo, ref matchCount);
                if (result & matchCount > 0)
                {
                    fingerResult = MatcherIndexAndScore(matchInfo, matchCount);
                }
            }
            catch (Exception ex)
            {
                logMessage = string.Format("Identify finger on server failed (error: {0}-{1}).", Constant.ERROR_CODE_SYSTEM, ex.Message);
                throw ex;
            }
            finally
            {

                if (fingerResult != null)
                {
                    if (fingerResult.FingerPrintStatus)
                    {
                        logMessage = string.Format("Identify finger on server found (user id:{0} name:{1} score:{2} index:{3})", fingerResult.UserId, fingerResult.Name, fingerResult.Score, fingerResult.Index);
                    }
                    else
                    {
                        logMessage = "Identify finger on server not found";
                    }
                }

                Log(logMessage, Constant.LEVEL_DEBUG);
                Log("End identify finger on server", Constant.LEVEL_DEBUG);

            }

            return fingerResult;

        }

        public FingerPrintResult VerifyFingerOnServer(byte[] scanTemplate, string userId)
        {
            FingerPrintResult fingerResult = new FingerPrintResult();
            string logMessage = string.Empty;
            List<FingerMatchingInfo> matchInfo = null;
            int matchCount = 0;
            bool result = false;
            try
            {
                if (configItems == null)
                {
                    Log("Configuration items is null", Constant.LEVEL_ERROR);
                    return fingerResult;
                }
                Log("Starting verify finger on server", Constant.LEVEL_DEBUG);

                //Get FP Template By Nip
                FingerPrintLogic logic = new FingerPrintLogic();
                Log("Get template user", Constant.LEVEL_DEBUG);
                FingerTemplateInfo templateInfo = logic.GetTemplatesByUserId(userId);

                if (templateInfo != null)
                {
                    result = AnalyzeVerifyFingerByTemplate(MatcherServerName,
                                   MatcherServerPort,
                                   MatcherServerAdminPort,
                                   MinimumFAR,
                                   scanTemplate,
                                   templateInfo.Template,
                                   templateInfo.CentralId,
                                   ref matchInfo,
                                   ref matchCount);

                    if (result & matchCount > 0)
                    {
                        fingerResult = MatcherIndexAndScore(matchInfo, matchCount);
                    }
                }

            }
            catch (Exception ex)
            {
                logMessage = string.Format("Verify finger on server failed (error: {0}-{1}).", Constant.ERROR_CODE_SYSTEM, ex.Message);
                throw ex;
            }
            finally
            {
                if (fingerResult != null)
                {
                    if (fingerResult.FingerPrintStatus)
                    {
                        logMessage = string.Format("Verify finger on server found (user id:{0} name:{1} score:{2} index:{3})", fingerResult.UserId, fingerResult.Name, fingerResult.Score, fingerResult.Index);
                    }
                    else
                    {
                        logMessage = "Verify finger on server not found";
                    }
                }
                Log(logMessage, Constant.LEVEL_DEBUG);
                Log("End verify finger on server", Constant.LEVEL_DEBUG);

            }
            return fingerResult;
        }

        public bool DeleteFingerOnServer(int fscentralId)
        {
            bool result = false;
            if (configItems == null)
            {
                Log("Configuration items is null", Constant.LEVEL_ERROR);
                return result;
            }

            List<FingerMatchingInfo> matchInfo = null;
            int matchCount = 0;
            result = AnalyzedFinger(MatcherServerName, MatcherServerPort, MatcherServerAdminPort, MinimumFAR, fscentralId, null, ref matchInfo, ref matchCount, Constant.FINGER_OPERATION_DELETE);
            return result;
        }

        public FingerPrintResult EnrollmentOnServerAsync(List<byte[]> bufferTemplate, int fsCentralId)
        {
            FingerPrintResult fingerPrintResult = null;
            UserInfo userInfo = null;
            bool result = false;
            string logMessage = string.Empty;
            try
            {
                if (configItems == null)
                {
                    Log("Configuration items is null", Constant.LEVEL_ERROR);
                    return fingerPrintResult;
                }
                Log("Starting enrollment finger on server", Constant.LEVEL_DEBUG);

                fingerPrintResult = new FingerPrintResult();
                if (bufferTemplate.Count <= 0)
                    return fingerPrintResult;

                NBuffer buffer = CreateMultiTemplate(bufferTemplate);
                byte[] template = buffer.ToArray();
                result = FingerServerCommandAsync(fsCentralId, template, MinimumFAR, NBiometricOperations.Enroll, MatcherServerName, MatcherServerPort, MatcherServerAdminPort);
                if (result)
                {
                    userInfo = FingerCoreProvider.GetUserInfoByCentralId(fsCentralId);
                    logMessage = string.Format("Enrollment finger on server ({0}) has been succeeded", GetEnrollmentSuccessMessage(userInfo.UserId, userInfo.Name, (short)userInfo.UserStatusId));
                    MappingFingerResultForEnrollment(fsCentralId, userInfo, true, ref fingerPrintResult);

                }
            }
            catch (Exception ex)
            {

                logMessage = string.Format("Enrollment finger on server failed (error: {0}-{1}).", Constant.ERROR_CODE_SYSTEM, ex.Message);
                throw ex;
            }
            finally
            {

                Log(logMessage, Constant.LEVEL_DEBUG);
                Log("End enrollment finger on server", Constant.LEVEL_DEBUG);
            }


            return fingerPrintResult;

        }

        public FingerPrintResult IdentifyFingerOnServerAsync(byte[] template)
        {
            FingerPrintResult fingerResult = null;
            List<FingerMatchingInfo> matchInfo = null;
            string logMessage = string.Empty;
            int matchCount = 0;
            bool result = false;

            try
            {
                if (configItems == null)
                {
                    Log("Configuration items is null", Constant.LEVEL_ERROR);
                    return fingerResult;
                }
                Log("Starting identify finger on server", Constant.LEVEL_DEBUG);
                fingerResult = new FingerPrintResult();
                result = AnalyzedFingerAsync(MatcherServerName, MatcherServerPort, MatcherServerAdminPort, MinimumFAR, NBiometricOperations.Identify, 99999, template, ref matchInfo, ref matchCount);
                if (result & matchCount > 0)
                {
                    fingerResult = MatcherIndexAndScore(matchInfo, matchCount);
                }
            }
            catch (Exception ex)
            {
                logMessage = string.Format("Identify finger on server failed (error: {0}-{1}).", Constant.ERROR_CODE_SYSTEM, ex.Message);
                throw ex;
            }
            finally
            {

                if (fingerResult != null)
                {
                    if (fingerResult.FingerPrintStatus)
                    {
                        logMessage = string.Format("Identify finger on server found (user id:{0} name:{1} score:{2} index:{3})", fingerResult.UserId, fingerResult.Name, fingerResult.Score, fingerResult.Index);
                    }
                    else
                    {
                        logMessage = "Identify finger on server not found";
                    }
                }

                Log(logMessage, Constant.LEVEL_DEBUG);
                Log("End identify finger on server", Constant.LEVEL_DEBUG);

            }

            return fingerResult;

        }

        public bool DeleteFingerOnServerAsync(int fscentralId)
        {
            bool result = false;
            if (configItems == null)
            {
                Log("Configuration items is null", Constant.LEVEL_ERROR);
                return result;
            }

            List<FingerMatchingInfo> matchInfo = null;
            int matchCount = 0;
            result = AnalyzedFingerAsync(MatcherServerName, MatcherServerPort, MatcherServerAdminPort, MinimumFAR, fscentralId, null, ref matchInfo, ref matchCount, Constant.FINGER_OPERATION_DELETE);
            return result;
        }

        //public bool DeleteFingerOnServer(int fscentralId, bool isWriteLog)
        //{
        //    FingerPrintResult fingerResult = new FingerPrintResult();
        //    string message = string.Empty;
        //    Utility.WriteLog("Begin delete finger", _guidThreadId, _appName, _appVersion, _logName);

        //    List<FingerMatchingInfo> matchInfo = null;
        //    int matchCount = 0;

        //    bool result = AnalyzedFinger(ConfigItems.matcherSvrName, ConfigItems.matcherSvrPort, ConfigItems.matcherSvrAdminPort, ConfigItems.minFar, fscentralId, null, ref matchInfo, ref matchCount, Constant.FINGER_OPERATION_DELETE, false);
        //    return result;
        //}


        #endregion

        #region Private Method(s)

        private NBuffer CreateMultiTemplate(List<byte[]> listOfTemplate)
        {
            NBuffer[] hBuffer = new NBuffer[listOfTemplate.Count];
            int numFinger = 0;

            NBuffer result = null;
            for (int i = 0; i < listOfTemplate.Count; i++)
            {
                NBuffer bufferTemplate = new NBuffer(listOfTemplate[i]);
                hBuffer[i] = bufferTemplate;
                numFinger++;
            }

            if (numFinger > 0)
                result = fsCreateTemplates(hBuffer, numFinger);

            return result;
        }

        private NBuffer fsCreateTemplates(NBuffer[] hBuffer, int numTempl)
        {
            NBuffer result = null;
            var nfTemplate = new NFTemplate();
            for (int i = 0; i < numTempl; i++)
            {
                var template = new NTemplate(hBuffer[i]);
                if (template.Fingers != null)
                {
                    foreach (NFRecord record in template.Fingers.Records)
                    {
                        nfTemplate.Records.Add((NFRecord)record.Clone());
                    }
                }
            }

            result = nfTemplate.Save();

            return result;

        }

        private bool FingerServerCommand(ref NSubject subject, int centralId, byte[] template, double minFAR, NBiometricOperations operation, string serverName, int port, int adminPort, ref string statusResult)
        {

            bool result = false;

            try
            {
                using (var biometricClient = new NBiometricClient())
                {
                    subject = CreateSubject(template, centralId.ToString());

                    var connection = new NClusterBiometricConnection(serverName, port, adminPort);
                    biometricClient.RemoteConnections.Add(connection);
                    if (operation == NBiometricOperations.EnrollWithDuplicateCheck || operation == NBiometricOperations.Identify)
                    {
                        biometricClient.MatchingThreshold = Utility.MatchingThresholdFromString(minFAR.ToString());
                        biometricClient.MatchingWithDetails = true;
                    }

                    NBiometricTask operationTask = biometricClient.CreateTask(operation, subject);
                    biometricClient.PerformTask(operationTask);
                    NBiometricStatus status = operationTask.Status;
                    statusResult = status.ToString();

                    if (status != NBiometricStatus.Ok)
                    {
                        return result;
                    }

                    result = true;
                }
            }
            catch (Exception ex)
            {

                throw new Exception(string.Format("(NServer Error:{0})", ex.Message));
            }

            return result;

        }

        private bool FingerServerCommand(ref NSubject subject, string branchCode, byte[] template, double minFAR, NBiometricOperations operation, string serverName, int port, int adminPort, ref string statusResult)
        {

            bool result = false;

            try
            {
                using (var biometricClient = new NBiometricClient())
                {
                    subject = CreateSubjectByBranch(template, branchCode);

                    var connection = new NClusterBiometricConnection(serverName, port, adminPort);
                    biometricClient.RemoteConnections.Add(connection);
                    if (operation == NBiometricOperations.EnrollWithDuplicateCheck || operation == NBiometricOperations.Identify)
                    {
                        biometricClient.MatchingThreshold = Utility.MatchingThresholdFromString(minFAR.ToString());
                        biometricClient.MatchingWithDetails = true;
                        biometricClient.FingersMatchingSpeed = NMatchingSpeed.High;
                    }

                    NBiometricTask operationTask = biometricClient.CreateTask(operation, subject);
                    biometricClient.PerformTask(operationTask);
                    NBiometricStatus status = operationTask.Status;
                    statusResult = status.ToString();

                    if (status != NBiometricStatus.Ok)
                    {
                        return result;
                    }

                    result = true;
                }
            }
            catch (Exception ex)
            {

                throw new Exception(string.Format("(NServer Error:{0})", ex.Message));
            }

            return result;

        }

        private bool FingerServerCommand(int centralId, byte[] template, double minFAR, NBiometricOperations operation, string serverName, int port, int adminPort)
        {

            bool result = false;
            NSubject subject = null;

            try
            {
                using (var biometricClient = new NBiometricClient())
                {
                    subject = CreateSubject(template, centralId.ToString());

                    var connection = new NClusterBiometricConnection(serverName, port, adminPort);
                    biometricClient.RemoteConnections.Add(connection);
                    if (operation == NBiometricOperations.EnrollWithDuplicateCheck || operation == NBiometricOperations.Identify)
                    {
                        biometricClient.MatchingThreshold = Utility.MatchingThresholdFromString(minFAR.ToString());
                        biometricClient.MatchingWithDetails = true;
                    }

                    NBiometricTask operationTask = biometricClient.CreateTask(operation, subject);
                    biometricClient.PerformTask(operationTask);
                    NBiometricStatus status = operationTask.Status;

                    if (status != NBiometricStatus.Ok)
                    {
                        return result;
                    }



                    result = true;
                }
            }
            catch (Exception ex)
            {

                throw new Exception(string.Format("(NServer Error:{0})", ex.Message));
            }

            return result;

        }

        private bool FingerServerCommand(int centralId, byte[] template, double minFAR, NBiometricOperations operation, string serverName, int port, int adminPort, string branchCode)
        {

            bool result = false;
            NSubject subject = null;

            try
            {
                using (var biometricClient = new NBiometricClient())
                {
                    subject = CreateSubject(template, centralId.ToString());

                    var connection = new NClusterBiometricConnection(serverName, port, adminPort);
                    biometricClient.RemoteConnections.Add(connection);
                    if (operation == NBiometricOperations.EnrollWithDuplicateCheck || operation == NBiometricOperations.Identify)
                    {
                        biometricClient.MatchingThreshold = Utility.MatchingThresholdFromString(minFAR.ToString());
                        biometricClient.MatchingWithDetails = true;
                    }

                    NBiometricTask operationTask = biometricClient.CreateTask(operation, subject);
                    biometricClient.PerformTask(operationTask);
                    NBiometricStatus status = operationTask.Status;

                    if (status != NBiometricStatus.Ok)
                    {
                        return result;
                    }

                    if (operation == NBiometricOperations.Enroll && status == NBiometricStatus.Ok)
                    {
                        result = FingerCoreProvider.UpdateFsTemplate(centralId, branchCode);
                        if (!result)
                        {
                            return result;
                        }
                    }

                    result = true;
                }
            }
            catch (Exception ex)
            {

                throw new Exception(string.Format("(NServer Error:{0})", ex.Message));
            }

            return result;

        }


        private bool FingerServerCommandAsync(ref NSubject subject, int centralId, byte[] template, double minFAR, NBiometricOperations operation, string serverName, int port, int adminPort, ref string statusResult)
        {

            bool result = false;

            try
            {
                using (var biometricClient = new NBiometricClient())
                {

                    subject = CreateSubject(template, centralId.ToString());

                    var connection = new NClusterBiometricConnection(serverName, port, adminPort);
                    biometricClient.RemoteConnections.Add(connection);
                    if (operation == NBiometricOperations.EnrollWithDuplicateCheck || operation == NBiometricOperations.Identify)
                    {
                        biometricClient.MatchingThreshold = Utility.MatchingThresholdFromString(minFAR.ToString());
                        biometricClient.MatchingWithDetails = true;
                    }

                    NBiometricTask operationTask = biometricClient.CreateTask(operation, subject);
                    AsyncCallback fingerAsyncCallBack = new AsyncCallback(OnFingerServerCompleted);
                    Interlocked.Increment(ref requestCounter);
                    biometricClient.BeginPerformTask(operationTask, fingerAsyncCallBack, biometricClient);
                    NBiometricStatus status = GetFingerStatusAsync();
                    statusResult = status.ToString();

                    if (status != NBiometricStatus.Ok)
                    {
                        return result;
                    }

                    result = true;
                }
            }
            catch (Exception ex)
            {

                throw new Exception(string.Format("(NServer Error:{0})", ex.Message));
            }


            return result;

        }

        private bool FingerServerCommandAsync(int centralId, byte[] template, double minFAR, NBiometricOperations operation, string serverName, int port, int adminPort)
        {

            bool result = false;
            NSubject subject = null;

            try
            {
                using (var biometricClient = new NBiometricClient())
                {
                    subject = CreateSubject(template, centralId.ToString());

                    var connection = new NClusterBiometricConnection(serverName, port, adminPort);
                    biometricClient.RemoteConnections.Add(connection);
                    if (operation == NBiometricOperations.EnrollWithDuplicateCheck || operation == NBiometricOperations.Identify)
                    {
                        biometricClient.MatchingThreshold = Utility.MatchingThresholdFromString(minFAR.ToString());
                        biometricClient.MatchingWithDetails = true;
                    }

                    NBiometricTask operationTask = biometricClient.CreateTask(operation, subject);
                    AsyncCallback fingerAsyncCallBack = new AsyncCallback(OnFingerServerCompleted);
                    Interlocked.Increment(ref requestCounter);
                    biometricClient.BeginPerformTask(operationTask, fingerAsyncCallBack, biometricClient);
                    NBiometricStatus status = GetFingerStatusAsync();

                    if (status != NBiometricStatus.Ok)
                    {
                        return result;
                    }

                    result = true;
                }
            }
            catch (Exception ex)
            {

                throw new Exception(string.Format("(NServer Error:{0})", ex.Message));
            }


            return result;

        }




        private void OnFingerServerCompleted(IAsyncResult result)
        {
            try
            {
                using (var biometricClient = (NBiometricClient)result.AsyncState)
                {
                    NBiometricTask task = biometricClient.EndPerformTask(result);
                    _biometricStatus = task.Status;

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Interlocked.Decrement(ref requestCounter);

            }

        }

        private NBiometricStatus GetFingerStatusAsync()
        {

            return _biometricStatus;
        }

        private NSubject CreateSubject(byte[] nbuffer, string subjectId)
        {
            NBuffer actualTemplate = new NBuffer(nbuffer);
            var subject = new NSubject();
            subject.SetTemplateBuffer(actualTemplate);
            subject.Id = subjectId;
            return subject;
        }

        private NSubject CreateSubjectByBranch(byte[] nbuffer, string branchCode)
        {
            NBuffer actualTemplate = new NBuffer(nbuffer);
            var subject = new NSubject();
            //subject.QueryString = string.Format("branchcode = '{0}'", branchCode);
            subject.SetTemplateBuffer(actualTemplate);
            return subject;
        }

        private bool AnalyzedFinger(string serverName, int port, int adminPort, double minFAR, NBiometricOperations operations, int centralId, byte[] template, ref List<FingerMatchingInfo> matchInfo, ref int matchCount)
        {
            bool result = false;
            NSubject subject = null;
            string matchResultSubjectId = null;
            string statusOut = string.Empty;
            try
            {

                result = FingerServerCommand(ref subject, centralId, template, minFAR, operations, serverName, port, adminPort, ref statusOut);
                if (result)
                {
                    Neurotec.Biometrics.NSubject.MatchingResultCollection matchResult = subject.MatchingResults;
                    if (matchResult != null)
                    {
                        List<FingerMatchingInfo> listOfMatchingInfo = new List<FingerMatchingInfo>();
                        for (int i = 0; i < matchResult.Count; i++)
                        {
                            matchResultSubjectId = matchResult[i].Id;
                            NMatchingDetails matchingDetails = matchResult[i].MatchingDetails;
                            int matchFingerCount = matchingDetails.Fingers.Count;
                            for (int fi = 0; fi < matchFingerCount; fi++)
                            {
                                int fingerIndex = matchingDetails.Fingers[fi].MatchedIndex;
                                int fingerScore = matchingDetails.Fingers[fi].Score;
                                listOfMatchingInfo.Add(new FingerMatchingInfo() { SubjectId = Convert.ToInt32(matchResultSubjectId), Index = fingerIndex, Score = fingerScore });
                                matchCount++;
                            }
                        }

                        if (subject.Id != matchResultSubjectId && operations == NBiometricOperations.Verify)
                            matchCount = 0;

                        //if (subject.Id != matchResultSubjectId && operations == NBiometricOperations.Verify)
                        //{
                        //    Utility.WriteLog(logInfo = new LogInfo("Finger Status = MatchNotFound", _guidThreadId, _appName, _appVersion, Constant.LEVEL_DEBUG, Constant.FINGER_SERVICE));
                        //    matchCount = 0;
                        //}
                        //else
                        //{
                        //    Utility.WriteLog(logInfo = new LogInfo("Finger Status = OK", _guidThreadId, _appName, _appVersion, Constant.LEVEL_DEBUG, Constant.FINGER_SERVICE));
                        //}


                        if (listOfMatchingInfo != null)
                        {
                            matchInfo = listOfMatchingInfo;
                            return true;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return false;
        }

        private bool AnalyzedFinger(string serverName, int port, int adminPort, double minFAR, NBiometricOperations operations, byte[] template, string branchCode, ref List<FingerMatchingInfo> matchInfo, ref int matchCount)
        {
            bool result = false;
            NSubject subject = null;
            string matchResultSubjectId = null;
            string statusOut = string.Empty;
            try
            {

                result = FingerServerCommand(ref subject, branchCode, template, minFAR, operations, serverName, port, adminPort, ref statusOut);
                if (result)
                {
                    Neurotec.Biometrics.NSubject.MatchingResultCollection matchResult = subject.MatchingResults;
                    if (matchResult != null)
                    {
                        List<FingerMatchingInfo> listOfMatchingInfo = new List<FingerMatchingInfo>();
                        for (int i = 0; i < matchResult.Count; i++)
                        {
                            matchResultSubjectId = matchResult[i].Id;
                            NMatchingDetails matchingDetails = matchResult[i].MatchingDetails;
                            int matchFingerCount = matchingDetails.Fingers.Count;
                            for (int fi = 0; fi < matchFingerCount; fi++)
                            {
                                int fingerIndex = matchingDetails.Fingers[fi].MatchedIndex;
                                int fingerScore = matchingDetails.Fingers[fi].Score;
                                listOfMatchingInfo.Add(new FingerMatchingInfo() { SubjectId = Convert.ToInt32(matchResultSubjectId), Index = fingerIndex, Score = fingerScore });
                                matchCount++;
                            }
                        }

                        if (subject.Id != matchResultSubjectId && operations == NBiometricOperations.Verify)
                            matchCount = 0;

                        if (listOfMatchingInfo != null)
                        {
                            matchInfo = listOfMatchingInfo;
                            return true;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return false;
        }

        private bool AnalyzedFinger(string serverName, int port, int adminPort, double minFAR, int fpId, byte[] template, ref List<FingerMatchingInfo> matchInfo, ref int matchCount, string fingerAction)
        {
            LogInfo logInfo = null;
            bool result = false;
            NSubject subject = null;
            string matchResultSubjectId = null;
            string statusOut = string.Empty;

            NBiometricOperations operation = NBiometricOperations.None;
            switch (fingerAction)
            {
                case Constant.FINGER_OPERATION_VERIFY:
                    operation = NBiometricOperations.Verify;
                    break;
                case Constant.FINGER_OPERATION_IDENTIFY:
                    operation = NBiometricOperations.Identify;
                    break;
                case Constant.FINGER_OPERATION_DELETE:
                    operation = NBiometricOperations.Delete;
                    break;
            }

            try
            {

                result = FingerServerCommand(ref subject, fpId, template, minFAR, operation, serverName, port, adminPort, ref statusOut);
                if (result)
                {
                    if (operation != NBiometricOperations.Delete)
                    {
                        Neurotec.Biometrics.NSubject.MatchingResultCollection matchResult = subject.MatchingResults;
                        if (matchResult != null)
                        {
                            List<FingerMatchingInfo> listOfMatchingInfo = new List<FingerMatchingInfo>();
                            for (int i = 0; i < matchResult.Count; i++)
                            {
                                matchResultSubjectId = matchResult[i].Id;
                                NMatchingDetails matchingDetails = matchResult[i].MatchingDetails;
                                int matchFingerCount = matchingDetails.Fingers.Count;
                                for (int fi = 0; fi < matchFingerCount; fi++)
                                {
                                    int fingerIndex = matchingDetails.Fingers[fi].MatchedIndex;
                                    int fingerScore = matchingDetails.Fingers[fi].Score;
                                    listOfMatchingInfo.Add(new FingerMatchingInfo() { SubjectId = Convert.ToInt32(matchResultSubjectId), Index = fingerIndex, Score = fingerScore });
                                    matchCount++;
                                }
                            }

                            if (subject.Id != matchResultSubjectId && fingerAction == Constant.FINGER_OPERATION_VERIFY)
                            {
                                Utility.WriteLog(logInfo = new LogInfo("Finger Status = MatchNotFound", _guidThreadId, _appName, _appVersion, Constant.LEVEL_DEBUG, Constant.FINGER_SERVICE));
                                matchCount = 0;
                            }
                            else
                            {
                                Utility.WriteLog(logInfo = new LogInfo("Finger Status = OK", _guidThreadId, _appName, _appVersion, Constant.LEVEL_DEBUG, Constant.FINGER_SERVICE));
                            }

                            if (listOfMatchingInfo != null)
                            {
                                matchInfo = listOfMatchingInfo;
                                return true;
                            }
                        }
                    }
                    else
                    {
                        return result;
                    }
                }
                else
                {
                    Utility.WriteLog(logInfo = new LogInfo("Finger Status = MatchNotFound", _guidThreadId, _appName, _appVersion, Constant.LEVEL_DEBUG, Constant.FINGER_SERVICE));
                }

            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return false;
        }

        private bool AnalyzedFinger(string serverName, int port, int adminPort, double minFAR, int fpId, byte[] template, ref List<FingerMatchingInfo> matchInfo, ref int matchCount, string fingerAction, bool isWriteLog)
        {
            LogInfo logInfo = null;
            bool result = false;
            NSubject subject = null;
            string matchResultSubjectId = null;
            string statusOut = string.Empty;

            NBiometricOperations operation = NBiometricOperations.None;
            switch (fingerAction)
            {
                case Constant.FINGER_OPERATION_VERIFY:
                    operation = NBiometricOperations.Verify;
                    break;
                case Constant.FINGER_OPERATION_IDENTIFY:
                    operation = NBiometricOperations.Identify;
                    break;
                case Constant.FINGER_OPERATION_DELETE:
                    operation = NBiometricOperations.Delete;
                    break;
            }

            try
            {

                result = FingerServerCommand(ref subject, fpId, template, minFAR, operation, serverName, port, adminPort, ref statusOut);
                if (result)
                {
                    if (operation != NBiometricOperations.Delete)
                    {
                        Neurotec.Biometrics.NSubject.MatchingResultCollection matchResult = subject.MatchingResults;
                        if (matchResult != null)
                        {
                            List<FingerMatchingInfo> listOfMatchingInfo = new List<FingerMatchingInfo>();
                            for (int i = 0; i < matchResult.Count; i++)
                            {
                                matchResultSubjectId = matchResult[i].Id;
                                NMatchingDetails matchingDetails = matchResult[i].MatchingDetails;
                                int matchFingerCount = matchingDetails.Fingers.Count;
                                for (int fi = 0; fi < matchFingerCount; fi++)
                                {
                                    int fingerIndex = matchingDetails.Fingers[fi].MatchedIndex;
                                    int fingerScore = matchingDetails.Fingers[fi].Score;
                                    listOfMatchingInfo.Add(new FingerMatchingInfo() { SubjectId = Convert.ToInt32(matchResultSubjectId), Index = fingerIndex, Score = fingerScore });
                                    matchCount++;
                                }
                            }

                            if (subject.Id != matchResultSubjectId && fingerAction == Constant.FINGER_OPERATION_VERIFY)
                            {
                                Utility.WriteLog(logInfo = new LogInfo("Finger Status = MatchNotFound", _guidThreadId, _appName, _appVersion, Constant.LEVEL_DEBUG, Constant.FINGER_SERVICE));
                                matchCount = 0;
                            }
                            else
                            {
                                Utility.WriteLog(logInfo = new LogInfo("Finger Status = OK", _guidThreadId, _appName, _appVersion, Constant.LEVEL_DEBUG, Constant.FINGER_SERVICE));
                            }

                            if (listOfMatchingInfo != null)
                            {
                                matchInfo = listOfMatchingInfo;
                                return true;
                            }
                        }
                    }
                    else
                    {
                        return result;
                    }
                }
                else
                {
                    Utility.WriteLog(logInfo = new LogInfo("Finger Status = MatchNotFound", _guidThreadId, _appName, _appVersion, Constant.LEVEL_DEBUG, Constant.FINGER_SERVICE));
                }

            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return false;
        }

        private bool AnalyzeVerifyFingerByTemplate(string serverName, int port, int adminPort, double minFAR, byte[] scanTemplate, byte[] dbTemplate, int transId, ref List<FingerMatchingInfo> matchInfo, ref int matchCount)
        {
            return VerifyServerCommandByTemplate(serverName, port, adminPort, minFAR, scanTemplate, dbTemplate, transId, out matchInfo, out matchCount);
        }

        private bool VerifyServerCommandByTemplate(string serverName, int port, int adminPort, double minFAR, byte[] scanTemplate, byte[] dbTemplate, int transId, out List<FingerMatchingInfo> matchInfo, out int matchCount)
        {
            LogInfo logInfo = null;
            bool result = false;
            string matchResultSubjectId = null;
            matchCount = 0;
            matchInfo = null;
            try
            {

                _biometricClient = new NBiometricClient();

                using (NSubject probeSubject = CreateSubject(scanTemplate, transId.ToString()))
                using (NSubject candidateSubject = CreateSubject(dbTemplate, transId.ToString()))
                {
                    _biometricClient.MatchingThreshold = Utility.MatchingThresholdFromString(minFAR.ToString());
                    _biometricClient.MatchingWithDetails = true;
                    NBiometricStatus status = _biometricClient.Verify(probeSubject, candidateSubject);
                    IsVerified = (status == NBiometricStatus.Ok);
                    _biometricClient.Dispose(); // added issue BCA performance

                    if (IsVerified)
                    {

                        Neurotec.Biometrics.NSubject.MatchingResultCollection matchResult = probeSubject.MatchingResults;
                        if (matchResult != null)
                        {
                            List<FingerMatchingInfo> listOfMatchingInfo = new List<FingerMatchingInfo>();
                            for (int i = 0; i < matchResult.Count; i++)
                            {
                                matchResultSubjectId = matchResult[i].Id;
                                NMatchingDetails matchingDetails = matchResult[i].MatchingDetails;
                                int matchFingerCount = matchingDetails.Fingers.Count;
                                for (int fi = 0; fi < matchFingerCount; fi++)
                                {
                                    int fingerIndex = matchingDetails.Fingers[fi].MatchedIndex;
                                    int fingerScore = matchingDetails.Fingers[fi].Score;
                                    listOfMatchingInfo.Add(new FingerMatchingInfo() { SubjectId = Convert.ToInt32(matchResultSubjectId), Index = fingerIndex, Score = fingerScore });
                                    matchCount++;
                                }
                            }

                            if (probeSubject.Id != matchResultSubjectId)
                            {
                                Utility.WriteLog(logInfo = new LogInfo("Finger Status = MatchNotFound", _guidThreadId, _appName, _appVersion, Constant.LEVEL_DEBUG, Constant.FINGER_SERVICE));
                                matchCount = 0;
                            }
                            else
                            {
                                Utility.WriteLog(logInfo = new LogInfo("Finger Status = OK", _guidThreadId, _appName, _appVersion, Constant.LEVEL_DEBUG, Constant.FINGER_SERVICE));
                            }

                            if (listOfMatchingInfo != null)
                            {
                                matchInfo = listOfMatchingInfo;
                                return true;
                            }
                        }

                        result = IsVerified;
                    }



                }

            }
            catch (Exception ex)
            {

                throw ex;
            }


            return result;

        }



        private bool AnalyzedFingerAsync(string serverName, int port, int adminPort, double minFAR, NBiometricOperations operations, int centralId, byte[] template, ref List<FingerMatchingInfo> matchInfo, ref int matchCount)
        {
            bool result = false;
            NSubject subject = null;
            string matchResultSubjectId = null;
            string statusOut = string.Empty;
            try
            {

                result = FingerServerCommandAsync(ref subject, centralId, template, minFAR, operations, serverName, port, adminPort, ref statusOut);
                if (result)
                {
                    Neurotec.Biometrics.NSubject.MatchingResultCollection matchResult = subject.MatchingResults;
                    if (matchResult != null)
                    {
                        List<FingerMatchingInfo> listOfMatchingInfo = new List<FingerMatchingInfo>();
                        for (int i = 0; i < matchResult.Count; i++)
                        {
                            matchResultSubjectId = matchResult[i].Id;
                            NMatchingDetails matchingDetails = matchResult[i].MatchingDetails;
                            int matchFingerCount = matchingDetails.Fingers.Count;
                            for (int fi = 0; fi < matchFingerCount; fi++)
                            {
                                int fingerIndex = matchingDetails.Fingers[fi].MatchedIndex;
                                int fingerScore = matchingDetails.Fingers[fi].Score;
                                listOfMatchingInfo.Add(new FingerMatchingInfo() { SubjectId = Convert.ToInt32(matchResultSubjectId), Index = fingerIndex, Score = fingerScore });
                                matchCount++;
                            }
                        }

                        if (subject.Id != matchResultSubjectId && operations == NBiometricOperations.Verify)
                            matchCount = 0;



                        if (listOfMatchingInfo != null)
                        {
                            matchInfo = listOfMatchingInfo;
                            return true;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return false;
        }

        private bool AnalyzedFingerAsync(string serverName, int port, int adminPort, double minFAR, int fpId, byte[] template, ref List<FingerMatchingInfo> matchInfo, ref int matchCount, string fingerAction)
        {
            LogInfo logInfo = null;
            bool result = false;
            NSubject subject = null;
            string matchResultSubjectId = null;
            string statusOut = string.Empty;

            NBiometricOperations operation = NBiometricOperations.None;
            switch (fingerAction)
            {
                case Constant.FINGER_OPERATION_VERIFY:
                    operation = NBiometricOperations.Verify;
                    break;
                case Constant.FINGER_OPERATION_IDENTIFY:
                    operation = NBiometricOperations.Identify;
                    break;
                case Constant.FINGER_OPERATION_DELETE:
                    operation = NBiometricOperations.Delete;
                    break;
            }

            try
            {

                result = FingerServerCommandAsync(ref subject, fpId, template, minFAR, operation, serverName, port, adminPort, ref statusOut);
                if (result)
                {
                    if (operation != NBiometricOperations.Delete)
                    {
                        Neurotec.Biometrics.NSubject.MatchingResultCollection matchResult = subject.MatchingResults;
                        if (matchResult != null)
                        {
                            List<FingerMatchingInfo> listOfMatchingInfo = new List<FingerMatchingInfo>();
                            for (int i = 0; i < matchResult.Count; i++)
                            {
                                matchResultSubjectId = matchResult[i].Id;
                                NMatchingDetails matchingDetails = matchResult[i].MatchingDetails;
                                int matchFingerCount = matchingDetails.Fingers.Count;
                                for (int fi = 0; fi < matchFingerCount; fi++)
                                {
                                    int fingerIndex = matchingDetails.Fingers[fi].MatchedIndex;
                                    int fingerScore = matchingDetails.Fingers[fi].Score;
                                    listOfMatchingInfo.Add(new FingerMatchingInfo() { SubjectId = Convert.ToInt32(matchResultSubjectId), Index = fingerIndex, Score = fingerScore });
                                    matchCount++;
                                }
                            }

                            if (subject.Id != matchResultSubjectId && fingerAction == Constant.FINGER_OPERATION_VERIFY)
                            {
                                Utility.WriteLog(logInfo = new LogInfo("Finger Status = MatchNotFound", _guidThreadId, _appName, _appVersion, Constant.LEVEL_DEBUG, Constant.FINGER_SERVICE));
                                matchCount = 0;
                            }
                            else
                            {
                                Utility.WriteLog(logInfo = new LogInfo("Finger Status = OK", _guidThreadId, _appName, _appVersion, Constant.LEVEL_DEBUG, Constant.FINGER_SERVICE));
                            }

                            if (listOfMatchingInfo != null)
                            {
                                matchInfo = listOfMatchingInfo;
                                return true;
                            }
                        }
                    }
                    else
                    {
                        return result;
                    }
                }
                else
                {
                    Utility.WriteLog(logInfo = new LogInfo("Finger Status = MatchNotFound", _guidThreadId, _appName, _appVersion, Constant.LEVEL_DEBUG, Constant.FINGER_SERVICE));
                }

            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return false;
        }

        private FingerPrintResult MatcherIndexAndScore(List<FingerMatchingInfo> matchInfo, int matchCount)
        {
            //LogInfo logInfo = null;
            int bestIndex = 0;
            int bestScore = 0;
            int userIdType = 0;
            for (int i = 0; i < matchCount; i++)
            {
                if (bestScore < matchInfo[i].Score)
                {
                    bestIndex = i;
                    bestScore = matchInfo[i].Score;
                }
            }

            FingerPrintResult result = new FingerPrintResult();
            if (bestScore != 0)
            {
                FingerMatchingInfo resultInfo = matchInfo.Find(a => a.Score == bestScore);

                if (resultInfo != null)
                {
                    FingerPrintLogic fingerPrintLogic = new FingerPrintLogic();
                    UserInfo userInfo = fingerPrintLogic.GetUserInfoByCentralId(resultInfo.SubjectId);
                    if (userInfo != null)
                    {
                        string strUserId = string.Empty;
                        switch (userInfo.UserStatusId)
                        {
                            case 2:
                            case 3:
                                userIdType = (int)Framework.Common.Enumeration.EnumOfUserIdType.NIP;
                                break;
                            case 4:
                                userIdType = (int)Framework.Common.Enumeration.EnumOfUserIdType.VENDOR_NIP;
                                break;
                            case 5:
                                userIdType = (int)Framework.Common.Enumeration.EnumOfUserIdType.NIK;
                                break;
                            default:
                                userIdType = (int)Framework.Common.Enumeration.EnumOfUserIdType.NONE;
                                break;
                        }

                        string fingerName = GetFingerNameByIndex(resultInfo, userInfo);
                        result = new FingerPrintResult(true, resultInfo.SubjectId, userInfo.UserId, GetUserIdType(userIdType), userInfo.Name, resultInfo.Score, resultInfo.Index, matchCount, userInfo.UserStatusId, fingerName, userInfo.GroupId, userInfo.BranchCode, userInfo.OfficeCode);
                    }
                }

            }

            return result;
        }

        /// <summary>
        /// finger v3
        /// </summary>
        /// <param name="resultInfo"></param>
        /// <param name="fsMain"></param>
        /// <returns></returns>
        private static string GetFingerNameByIndex(FingerMatchingInfo resultInfo, UserInfo userInfo)
        {
            string fingerName = string.Empty;

            char[] charArr = userInfo.TemplateLayout.ToCharArray();
            int fingerIndex = 99;
            if (resultInfo.Index < userInfo.TemplateLayout.Length)
            {
                fingerIndex = Convert.ToInt32(charArr[resultInfo.Index].ToString());
            }

            switch (fingerIndex)
            {
                case 0:
                    fingerName = "Kelingking Kiri";
                    break;
                case 1:
                    fingerName = "Manis Kiri";
                    break;
                case 2:
                    fingerName = "Tengah Kiri";
                    break;
                case 3:
                    fingerName = "Telunjuk Kiri";
                    break;
                case 4:
                    fingerName = "Jempol Kiri";
                    break;
                case 5:
                    fingerName = "Jempol Kanan";
                    break;
                case 6:
                    fingerName = "Telunjuk Kanan";
                    break;
                case 7:
                    fingerName = "Tengah Kanan";
                    break;
                case 8:
                    fingerName = "Manis Kanan";
                    break;
                case 9:
                    fingerName = "Kelingking Kanan";
                    break;
                default:
                    fingerName = string.Empty;
                    break;
            }
            return fingerName;
        }

        private string GetUserIdType(int userIdType)
        {
            if (userIdType == (int)Framework.Common.Enumeration.EnumOfUserIdType.NIP)
            {
                return Utility.GetEnumDescription(Framework.Common.Enumeration.EnumOfUserIdType.NIP);
            }
            else if (userIdType == (int)Framework.Common.Enumeration.EnumOfUserIdType.NIK)
            {
                return Utility.GetEnumDescription(Framework.Common.Enumeration.EnumOfUserIdType.NIK);

            }
            else if (userIdType == (int)Framework.Common.Enumeration.EnumOfUserIdType.VENDOR_NIP)
            {
                return Utility.GetEnumDescription(Framework.Common.Enumeration.EnumOfUserIdType.VENDOR_NIP);

            }
            else
            {

                return Utility.GetEnumDescription(Framework.Common.Enumeration.EnumOfUserIdType.NONE);

            }

        }


        private void Log(string message, string logLevel)
        {
            try
            {
                LogInfo logInfo = new LogInfo();
                logInfo.AppName = !string.IsNullOrEmpty(_appName) ? _appName : string.Empty;
                logInfo.AppVersion = !string.IsNullOrEmpty(_appVersion) ? _appVersion : string.Empty;
                logInfo.LogName = Constant.FINGER_SERVICE;
                logInfo.Level = logLevel;
                logInfo.GuidThreadId = _guidThreadId;
                logInfo.Message = message;

                Utility.WriteLog(logInfo);
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(_guidThreadId, ex.Message);
            }

        }

        private void MappingFingerResultForEnrollment(int fsCentralId, UserInfo userInfo, bool fingerPrintStatus, ref FingerPrintResult fingerPrintResult)
        {
            fingerPrintResult.CentralId = fsCentralId;
            fingerPrintResult.UserId = userInfo.UserId;
            fingerPrintResult.Name = userInfo.Name;
            fingerPrintResult.UserStatus = userInfo.UserStatusId;
            fingerPrintResult.GroupId = userInfo.GroupId;
            fingerPrintResult.FingerPrintStatus = fingerPrintStatus;
        }

        private string GetEnrollmentSuccessMessage(string userId, string name, Int16 userType)
        {
            string message = string.Empty;

            if (userType == (Int16)Enumeration.EnumOfUserStatus.USER_STATUS_TETAP || userType == (Int16)Enumeration.EnumOfUserStatus.USER_STATUS_BAKTI)
            {
                message = string.Format("NIP: {0}, Name: {1}", userId, name);
            }
            else if (userType == (Int16)Enumeration.EnumOfUserStatus.USER_STATUS_ALIH_DAYA)
            {

                message = string.Format("Vendor NIP: {0}, Name: {1}", userId, name);
            }
            else if (userType == (Int16)Enumeration.EnumOfUserStatus.USER_STATUS_TANPA_NIP)
            {

                message = string.Format("NIK: {0}, Name: {1}", userId, name);
            }

            return message;
        }

        #endregion

    }
}
