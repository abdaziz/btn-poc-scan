﻿using Finger.Framework.Common;
using Finger.Framework.Security;
using System;

namespace Finger.Core.Data
{
    public class CoreBaseProvider
    {
        public string GetConnectionString()
        {
            return Cryptography.Decode(Utility.GetConfigurationValue(Constant.KEY_SQL_CONNECTION_STRING));
        }

        public int GetCommandTimeOut()
        {
            return Convert.ToInt32(Utility.GetConfigurationValue(Constant.KEY_COMMAND_TIMEOUT));
        }
    }
}
