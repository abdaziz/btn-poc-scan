﻿using Finger.Core.Entity;
using Finger.Core.Models;
using Finger.Framework.Common;
using Finger.Framework.Entity;
using Finger.Framework.Data;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finger.Core.Data
{
    public class FingerCoreProvider : CoreBaseProvider
    {

        public UserInfo GetUserInfoByCentralId(int centralId)
        {
            if (centralId == 0)
                return null;

            UserInfo userInfo = null;
            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "[dbo].[usp_GetUserInfoByCentralId]";
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@centralId", centralId);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                userInfo = new UserInfo();
                                userInfo.UserId = reader["UserId"] != DBNull.Value ? reader["UserId"].ToString() : string.Empty;
                                userInfo.Name = reader["Name"] != DBNull.Value ? reader["Name"].ToString() : string.Empty;
                                userInfo.UserStatusId = reader["UserStatusId"] != DBNull.Value ? Convert.ToInt32(reader["UserStatusId"]) : 0;
                                userInfo.TemplateLayout = reader["TemplateLayout"] != DBNull.Value ? reader["TemplateLayout"].ToString() : string.Empty;
                                userInfo.GroupId = reader["GroupId"] != DBNull.Value ? Convert.ToInt32(reader["GroupId"]) : 0;
                                userInfo.BranchCode = reader["MainBranchCode"] != DBNull.Value ? reader["MainBranchCode"].ToString() : string.Empty;
                                userInfo.OfficeCode = reader["MainOfficeCode"] != DBNull.Value ? reader["MainOfficeCode"].ToString() : string.Empty;

                            }
                        }

                    }

                }
                return userInfo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public FingerTemplateInfo GetTemplateByUserId(string userId)
        {
            if (string.IsNullOrEmpty(userId))
                return null;

            FingerTemplateInfo result = null;
            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "[dbo].[usp_GetTemplateByUserId]";
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@UserId", userId);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                result = new FingerTemplateInfo();
                                result.CentralId = reader["CentralId"] != DBNull.Value ? (int)reader["CentralId"] : 0;
                                result.Template = reader["Templates"] != DBNull.Value ? (byte[])reader["Templates"] : new byte[0];
                                result.MinFAR = reader["Quality"] != DBNull.Value ? Convert.ToInt32(reader["Quality"]) : 0;
                                result.BranchCode = reader["BranchCode"] != DBNull.Value ? reader["BranchCode"].ToString() : string.Empty;
                                result.OfficeCode = reader["OfficeCode"] != DBNull.Value ? reader["OfficeCode"].ToString() : string.Empty;
                                result.GroupId = reader["GroupId"] != DBNull.Value ? Convert.ToInt32(reader["GroupId"]) : 0;
                            }
                        }

                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<cfgitems> GetFingerConfigItems(out ErrorInfo oErrorInfo, string[] arrKey)
        {
            oErrorInfo = new ErrorInfo();
            List<cfgitems> oCfgitems = null;
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(GetConnectionString()))
                {
                    oSqlConnection.Open();
                    oCfgitems = SQLProvider.Instance.GetCentralCfgitems(oSqlConnection, GetCommandTimeOut(), arrKey);
                }
            }
            catch (Framework.Exception.CKVException ex)
            {
                oErrorInfo.Code = Constant.ERR_CODE_5051;
                oErrorInfo.Message = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return oCfgitems;
        }



        public string GetInfoNserverLocation(string branchCode)
        {
            if (string.IsNullOrEmpty(branchCode))
                return null;

            string result = string.Empty;
            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "[dbo].[usp_GetInfoNserverLocation]";
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@BranchCode", branchCode);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                result = reader["ZoneMatcherSvrName"] != DBNull.Value ? reader["ZoneMatcherSvrName"].ToString() : string.Empty;
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public bool UpdateFsTemplate(int fsCentralid, string branchCode)
        {
            bool result = false;    
            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "[dbo].[usp_UpdateFsTemplateByFsCentralId]";
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@FsCentralId", fsCentralid);
                        command.Parameters.AddWithValue("@BranchCode", branchCode);
                        result = Convert.ToBoolean(command.ExecuteNonQuery());
                    }
                }
            }
            catch (Exception)
            {
                
                throw;
            }
            return result;
        }
    }
}
