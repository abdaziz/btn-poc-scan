﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finger.Core.Models
{
    public class UserInfo
    {
        public string UniqueId { get; set; }
        public int CentralId { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public DateTime? StartPeriod { get; set; }
        public DateTime? EndPeriod { get; set; }
        public int UserStatusId { get; set; }
        public string UserStatusName { get; set; }
        public string Shift { get; set; }
        public string BranchCode { get; set; }
        public string OfficeCode { get; set; }
        public string BdsId1 { get; set; }
        public string BdsId2 { get; set; }
        public string BdsId3 { get; set; }
        public string BdsId4 { get; set; }
        public string BdsId5 { get; set; }
        public string BdsId6 { get; set; }
        public string BdsId7 { get; set; }
        public string BdsId8 { get; set; }
        public string BdsId9 { get; set; }
        public string BdsId10 { get; set; }
        public string KodeAlihDaya { get; set; }
        public int Quality { get; set; }
        public double UserFAR { get; set; }
        public int QualitySpecialUser { get; set; }
        public int ImageMap { get; set; }
        public string TemplateLayout { get; set; }
        public string ApprovedBy { get; set; }
        public string Reason { get; set; }


        public UserInfo()
        {
            UniqueId = string.Empty;
            CentralId = 0;
            UserId = string.Empty;
            Name = string.Empty;
            CategoryId = 0;
            CategoryName = string.Empty;
            GroupId = 0;
            GroupName = string.Empty;
            StartPeriod = null;
            EndPeriod = null;
            UserStatusId = 0;
            UserStatusName = string.Empty;
            Shift = string.Empty;
            BranchCode = string.Empty;
            OfficeCode = string.Empty;
            BdsId1 = string.Empty;
            BdsId2 = string.Empty;
            BdsId3 = string.Empty;
            BdsId4 = string.Empty;
            BdsId5 = string.Empty;
            BdsId6 = string.Empty;
            BdsId7 = string.Empty;
            BdsId8 = string.Empty;
            BdsId9 = string.Empty;
            BdsId10 = string.Empty;
            KodeAlihDaya = string.Empty;
            Quality = 0;
            UserFAR = 0;
            QualitySpecialUser = 0;
            ImageMap = 0;
            TemplateLayout = string.Empty;
            ApprovedBy = string.Empty;
            Reason = string.Empty;

        }
    }
}
