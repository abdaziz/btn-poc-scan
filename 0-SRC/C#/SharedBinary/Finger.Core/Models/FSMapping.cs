﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finger.Core.Models
{
    public class FSMapping
    {
        public int CentralId { get; set; }
        public int CategoryId { get; set; }
        public string UserName { get; set; }
        public short UserCategory { get; set; }
        public short UserType { get; set; }
        public short SubType { get; set; }
        public string BranchCode { get; set; }
        public string OfficeCode { get; set; }
    }
}
