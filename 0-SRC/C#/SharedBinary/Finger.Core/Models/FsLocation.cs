﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finger.Core.Models
{
    public class FSLocation
    {
        public int FsLocationId { get; set; }

        public string UserId { get; set; }

        public string BranchCode { get; set; }

        public string OfficeCode { get; set; }

        public int CategoryId { get; set; }

    }
}
