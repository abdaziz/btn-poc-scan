﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finger.Core.Models
{
    public class FSImages
    {
        public int FSImagesIndex { get; set; }
        public string FSImages0 { get; set; }
        public string FSImages1 { get; set; }
        public string FSImages2 { get; set; }
        public string FSImages3 { get; set; }
        public string FSImages4 { get; set; }
        public string FSImages5 { get; set; }
        public string FSImages6 { get; set; }
        public string FSImages7 { get; set; }
        public string FSImages8 { get; set; }
        public string FSImages9 { get; set; }
    }
}
