﻿using System;

namespace Finger.Core.Entity
{
    public class ComponentLicensing
    {
        public Int16 LicenseType { get; set; }

        public string Address { get; set; }

        public Int32 Port { get; set; }

        public string Component { get; set; }
    }
}
