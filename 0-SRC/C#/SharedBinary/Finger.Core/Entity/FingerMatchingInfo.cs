﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finger.Core.Entity
{
    public class FingerMatchingInfo
    {
        public int SubjectId { get; set; }
        public int Score { get; set; }
        public int Index { get; set; }
    }
}
