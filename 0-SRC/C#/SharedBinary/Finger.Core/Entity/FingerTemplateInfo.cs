﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finger.Core.Entity
{
    public class FingerTemplateInfo
    {
        public int CentralId { get; set; }

        public byte[] Template { get; set; }

        public double MinFAR { get; set; }

        public string BranchCode { get; set; }

        public string OfficeCode { get; set; }

        public int GroupId { get; set; }
    }
}
