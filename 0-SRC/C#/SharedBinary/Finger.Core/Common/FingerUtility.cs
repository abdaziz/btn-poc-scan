﻿using Neurotec.Biometrics;
using Neurotec.IO;
using System;
using System.Collections.Generic;


namespace Finger.Core.Common
{
    public static class FingerUtility
    {
        #region Public Method(s)

        public static NBuffer CreateMultiTemplate(List<byte[]> listOfTemplate)
        {
            NBuffer[] hBuffer = new NBuffer[listOfTemplate.Count];
            int numFinger = 0;

            NBuffer result = null;
            for (int i = 0; i < listOfTemplate.Count; i++)
            {
                NBuffer bufferTemplate = new NBuffer(listOfTemplate[i]);
                hBuffer[i] = bufferTemplate;
                numFinger++;
            }

            if (numFinger > 0)
                result = fsCreateTemplates(hBuffer, numFinger);

            return result;
        }

        public static bool IsValidTemplate(List<byte[]> listOfTemplate)
        {
            bool isValid = false;
            try
            {
                using (NBuffer NBufferTemplate = CreateMultiTemplate(listOfTemplate))
                {
                    isValid = true;

                }

            }
            catch
            {

                isValid = false;
            }

            return isValid;

        }

        public static bool IsValidTemplate(byte[] Template)
        {
            bool isValid = false;
            try
            {
                List<byte[]> listOfTemplate = new List<byte[]>();
                listOfTemplate.Add(Template);

                using (NBuffer NBufferTemplate = CreateMultiTemplate(listOfTemplate))
                {
                    isValid = true;
                }
            }
            catch
            {
                isValid = false;
            }

            return isValid;
        }

        #endregion




        #region Private Method(s)
        private static NBuffer fsCreateTemplates(NBuffer[] hBuffer, int numTempl)
        {
            NBuffer result = null;
            var nfTemplate = new NFTemplate();
            for (int i = 0; i < numTempl; i++)
            {
                var template = new NTemplate(hBuffer[i]);
                if (template.Fingers != null)
                {
                    foreach (NFRecord record in template.Fingers.Records)
                    {
                        nfTemplate.Records.Add((NFRecord)record.Clone());
                    }
                }
            }

            result = nfTemplate.Save();

            return result;

        }

        #endregion


    }
}
