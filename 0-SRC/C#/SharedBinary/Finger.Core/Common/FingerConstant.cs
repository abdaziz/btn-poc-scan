﻿using System;

namespace Finger.Core.Common
{
    internal static class FingerConstant
    {
        /// <summary>
        /// Constant defining the Configuration Category for Neurotech
        /// </summary>
        internal const string NEUROTECH_CATEGORY = "Neurotech";

        /// <summary>
        /// Constant defining the Configuration Items for neurotech license type 
        /// </summary>
        internal const string LICENSE_TYPE_KEY = "LicenseType";

        /// <summary>
        /// Constant defining the Configuration Items for neurotech license server address
        /// </summary>
        internal const string LICENSE_SERVER_ADDRESS_KEY = "LicenseServerAddress";

        /// <summary>
        /// Constant defining the Configuration Items for neurotech license server port
        /// </summary>
        internal const string LICENSE_SERVER_PORT_KEY = "LicenseServerPort";


        /// <summary>
        /// Constant defining the Configuration Items for neurotech matcher server address
        /// </summary>
        internal const string MATCHER_SERVER_ADDRESS_KEY = "MatcherServerAddress";

        /// <summary>
        /// Constant defining the Configuration Items for neurotech matcher server port
        /// </summary>
        internal const string MATCHER_SERVER_PORT_KEY = "MatcherServerPort";

        /// <summary>
        /// Constant defining the Configuration Items for neurotech matcher server admin port
        /// </summary>
        internal const string MATCHER_SERVER_ADMIN_PORT_KEY = "MatcherServerAdminPort";


        /// <summary>
        /// Constant defining the Configuration Items for neurotech minimum FAR Threshold
        /// </summary>
        internal const string MIN_FAR_KEY = "MinFAR";


        /// <summary>
        /// Constant defining local address for Neurotech
        /// </summary>
        internal const string LOCAL_ADDRESS = "/local";

        /// <summary>
        /// Constant defining local component for Neurotech
        /// </summary>
        //internal const string LOCAL_COMPONENT = "Biometrics.FingerExtraction,Devices.FingerScanners";
        internal const string LOCAL_COMPONENT = "Biometrics.FingerMatching";


        /// <summary>
        /// Constant defining server component for Neurotech
        /// </summary>
        internal const string SERVER_COMPONENT = "Biometrics.FingerExtraction,Devices.FingerScanners";

        /// <summary>
        /// Constant defining signed component for Neurotech
        /// </summary>
        //internal const string SIGNED_COMPONENT = "SingleComputerLicense:FingerExtractor,Biometrics.FingerExtraction,Biometrics.FingerMatching";

        internal const string SIGNED_COMPONENT = "Biometrics.FingerExtraction,Biometrics.FingerMatching";

        /// <summary>
        /// Constant defining minimum quality threshold for Neurotech
        /// </summary>
        internal const string MINIMUM_QUALITY_THRESHOLD_KEY = "MinQualityThreshold";

        /// <summary>
        /// Constant defining time out for current biometric completed
        /// </summary>
        internal const int BIOMETRIC_COMPLETED_TIME_OUT = (30 * 1000);
    }
}
