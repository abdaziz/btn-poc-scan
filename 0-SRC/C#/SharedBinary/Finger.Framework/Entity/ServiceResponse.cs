﻿using System;

namespace Finger.Framework.Entity
{
    public class ServiceResponse
    {
        public int statusCode;
        public string statusMessage;
        public string responseData;

        public ServiceResponse()
        {
            this.statusCode = 0;
            this.statusMessage = string.Empty;
            this.responseData = string.Empty;
        }

        public ServiceResponse(int statusCode, string statusMessage, string responseData)
        {
            this.statusCode = statusCode;
            this.statusMessage = statusMessage;
            this.responseData = responseData;
        }
    }
}
