﻿using Finger.Framework.Common;
using System.Data;

namespace Finger.Framework.Entity
{
    public class cfgitems : CalculateCKV
    {
        public string name { get; set; }
        public string value { get; set; }
        public int ckv { get; set; }
        public int? dbckv { get; set; }

        public override void GenerateCKV()
        {
            string[] objArray = new string[2];
            objArray[0] = this.name;
            objArray[1] = this.value;

            this.ckv = Utility.GenerateCKV(objArray);
        }

        public override string CheckCKV(string fieldName)
        {
            string msg = string.Empty;
            if (this.dbckv != this.ckv)
            {
                msg = Utility.GetErrorMessage(Constant.ERR_CODE_5051);
                throw new Exception.CKVException(msg);
            }
            return msg;
        }

        public override void ToDataTable(DataTable oDataTable)
        {
            DataRow dataRow = oDataTable.NewRow();
            dataRow[oDataTable.Columns[0].ColumnName] = this.name;
            dataRow[oDataTable.Columns[1].ColumnName] = this.value;
            dataRow[oDataTable.Columns[2].ColumnName] = this.ckv;
            oDataTable.Rows.Add(dataRow);
        }
    }
}
