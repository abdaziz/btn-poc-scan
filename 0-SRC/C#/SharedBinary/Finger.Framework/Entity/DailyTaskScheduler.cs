﻿using System;


namespace Finger.Framework.Entity
{
    /// <summary>
    /// Daily Task Scheduler Entity.
    /// </summary>
    public class DailyTaskScheduler
    {

        #region Constructor

        /// <param name="folderName">Folder name created in windows task scheduler.</param>
        /// <param name="taskName">Task name created in windows task scheduler.</param>
        /// <param name="taskDescription">Task description created in windows task scheduler.</param>
        /// <param name="applicationPath">the application file path location.</param>
        /// <param name="startTask">Start task running in windows task scheduler.</param>
        /// <param name="recurEvery"> the selected task will be repeated every number of days that you indicate.</param>
        public DailyTaskScheduler(string folderName, string taskName, string taskDescription, string applicationPath, DateTime startTask, short recurEvery = 1)
        {
            this.FolderName = folderName;
            this.TaskName = taskName;
            this.TaskDescription = taskDescription;
            this.ApplicationPath = applicationPath;
            this.StartTask = startTask;
            this.RecurEvery = recurEvery;

        }

        /// <param name="folderName">Folder name created in windows task scheduler.</param>
        /// <param name="taskName">Task name created in windows task scheduler.</param>
        /// <param name="taskDescription">Task description created in windows task scheduler.</param>
        /// <param name="applicationPath">the application file path location.</param>
        /// <param name="applicationArgument">the application command argument.</param>
        /// <param name="startTask">Start task running in windows task scheduler.</param>
        /// <param name="recurEvery"> the selected task will be repeated every number of days that you indicate.</param>
        public DailyTaskScheduler(string folderName, string taskName, string taskDescription, string applicationPath, string applicationArgument, DateTime startTask, short recurEvery = 1)
        {
            this.FolderName = folderName;
            this.TaskName = taskName;
            this.TaskDescription = taskDescription;
            this.ApplicationPath = applicationPath;
            this.ApplicationArgument = applicationArgument;
            this.StartTask = startTask;
            this.RecurEvery = recurEvery;

        }
        #endregion


        #region Properties

        /// <summary>
        /// Folder name created in windows task scheduler.
        /// </summary>
        public string FolderName { get; set; }

        /// <summary>
        /// Task name created in windows task scheduler.
        /// </summary>
        public string TaskName { get; set; }

        /// <summary>
        /// Task description created in windows task scheduler.
        /// </summary>
        public string TaskDescription { get; set; }

        /// <summary>
        /// Start task running in windows task scheduler.
        /// </summary>
        public DateTime StartTask { get; set; }

        /// <summary>
        /// the selected task will be repeated every number of days that you indicate.
        /// </summary>
        public short RecurEvery { get; set; }

        /// <summary>
        /// the application file path location.
        /// </summary>
        public string ApplicationPath { get; set; }


        /// <summary>
        /// the application command argument.
        /// </summary>
        public string ApplicationArgument { get; set; }

        #endregion
    }
}
