﻿using Finger.Framework.Common;
using Finger.Framework.Entity;
using Finger.Framework.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finger.Framework.Data
{
    public sealed class SQLProvider
    {
        SQLProvider()
        {
        }
        private static readonly object padlock = new object();
        private static SQLProvider instance = null;
        public static SQLProvider Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (padlock)
                    {
                        if (instance == null)
                        {
                            instance = new SQLProvider();
                        }
                    }
                }
                return instance;
            }
        }

        public bool CheckSQLConnection(SqlConnection oSqlConnection, string databaseName, int retryConnection, int delayMilliseconds)
        {
            bool connected = false;
            int retry = 1;
            while ((!connected) && retry <= retryConnection)
            {
                retry++;
                try
                {
                    if (oSqlConnection.State != ConnectionState.Open)
                        oSqlConnection.Open();

                    string statusDatabase = string.Empty;
                    string query = string.Format("use Master Select state_desc from Sys.Databases where name = '{0}'", databaseName);
                    using (SqlCommand oSqlCommand = new SqlCommand(query, oSqlConnection))
                    {
                        using (SqlDataReader oSqlDataReader = oSqlCommand.ExecuteReader())
                        {
                            if (oSqlDataReader.Read())
                            {
                                statusDatabase = oSqlDataReader["state_desc"] != DBNull.Value ? oSqlDataReader["state_desc"].ToString().ToLower() : string.Empty;
                                connected = statusDatabase == "online";
                            }
                        }
                    }
                    oSqlConnection.Close();
                    if (!connected) System.Threading.Thread.Sleep(delayMilliseconds);
                }
                catch
                {
                    System.Threading.Thread.Sleep(delayMilliseconds);
                }
            }
            return connected;
        }

        public List<cfgitems> GetCentralCfgitems(SqlConnection oSqlConnection, int SqlCommandTimeout, params string[] param)
        {
            List<cfgitems> oCfgitems = new List<cfgitems>();
            using (SqlCommand oSqlCommand = new SqlCommand())
            {
                if (oSqlConnection.State != ConnectionState.Open)
                    oSqlConnection.Open();

                oSqlCommand.Connection = oSqlConnection;
                oSqlCommand.CommandTimeout = SqlCommandTimeout;
                oSqlCommand.CommandText = "[dbo].[usp_Getcfgitems]";
                oSqlCommand.CommandType = CommandType.StoredProcedure;
                oSqlCommand.Parameters.AddWithValue("@KeyItem", param.Length == 0 ? null : string.Join(";", param));

                using (SqlDataReader oSqlDataReader = oSqlCommand.ExecuteReader())
                {
                    while (oSqlDataReader.Read())
                    {
                        cfgitems oCfgitem = new cfgitems();
                        oCfgitem.name = oSqlDataReader["name"] == DBNull.Value ? null : oSqlDataReader["name"].ToString();
                        oCfgitem.value = oSqlDataReader["value"] == DBNull.Value ? null : oSqlDataReader["value"].ToString();
                        oCfgitem.dbckv = oSqlDataReader["ckv"] == DBNull.Value ? (int?)null : Convert.ToInt32(oSqlDataReader["ckv"]);
                        oCfgitem.GenerateCKV();
                        oCfgitem.CheckCKV(oCfgitem.name);
                        oCfgitems.Add(oCfgitem);
                    }
                }
            }
            return oCfgitems;
        }

        public ServiceResponse GetCentralCfgitems(SqlConnection oSqlConnection, int SqlCommandTimeout, out List<cfgitems> oCfgitems, params string[] param)
        {
            ServiceResponse response = new ServiceResponse();
            oCfgitems = new List<cfgitems>();
            try
            {
                using (SqlCommand oSqlCommand = new SqlCommand())
                {
                    if (oSqlConnection.State != ConnectionState.Open)
                        oSqlConnection.Open();

                    oSqlCommand.Connection = oSqlConnection;
                    oSqlCommand.CommandTimeout = SqlCommandTimeout;
                    oSqlCommand.CommandText = "[dbo].[usp_Getcfgitems]";
                    oSqlCommand.CommandType = CommandType.StoredProcedure;
                    oSqlCommand.Parameters.AddWithValue("@KeyItem", param.Length == 0 ? null : string.Join(";", param));

                    using (SqlDataReader oSqlDataReader = oSqlCommand.ExecuteReader())
                    {
                        while (oSqlDataReader.Read())
                        {
                            cfgitems oCfgitem = new cfgitems();
                            oCfgitem.name = oSqlDataReader["name"] == DBNull.Value ? null : oSqlDataReader["name"].ToString();
                            oCfgitem.value = oSqlDataReader["value"] == DBNull.Value ? null : oSqlDataReader["value"].ToString();
                            oCfgitem.dbckv = oSqlDataReader["ckv"] == DBNull.Value ? (int?)null : Convert.ToInt32(oSqlDataReader["ckv"]);
                            oCfgitem.GenerateCKV();
                            oCfgitem.CheckCKV(oCfgitem.name);
                            oCfgitems.Add(oCfgitem);
                        }
                    }
                }
            }
            catch (SqlException sqlex)
            {
                if (sqlex.Number == Constant.ERROR_CODE_CONNECTION)
                {
                    response.statusCode = Constant.ERROR_CODE_CONNECTION;
                    response.statusMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5050);
                }
                else
                {
                    response.statusCode = Constant.ERROR_CODE_SYSTEM;
                    response.statusMessage = sqlex.Message;
                }
            }

            return response;
        }

        public List<cfgitems> GetCentralCfgitems(string branchCode, string officeCode, SqlConnection oSqlConnection, int SqlCommandTimeout, params string[] param)
        {
            List<cfgitems> oCfgitems = new List<cfgitems>();
            using (SqlCommand oSqlCommand = new SqlCommand())
            {
                if (oSqlConnection.State != ConnectionState.Open)
                    oSqlConnection.Open();

                oSqlCommand.Connection = oSqlConnection;
                oSqlCommand.CommandTimeout = SqlCommandTimeout;
                oSqlCommand.CommandText = "[dbo].[usp_Getcfgitems]";
                oSqlCommand.CommandType = CommandType.StoredProcedure;
                oSqlCommand.Parameters.AddWithValue("@KeyItem", param.Length == 0 ? null : string.Join(";", param));
                oSqlCommand.Parameters.AddWithValue("@BranchCode", branchCode);
                oSqlCommand.Parameters.AddWithValue("@OfficeCode", officeCode);

                using (SqlDataReader oSqlDataReader = oSqlCommand.ExecuteReader())
                {
                    while (oSqlDataReader.Read())
                    {
                        cfgitems oCfgitem = new cfgitems();
                        oCfgitem.name = oSqlDataReader["name"] == DBNull.Value ? null : oSqlDataReader["name"].ToString();
                        oCfgitem.value = oSqlDataReader["value"] == DBNull.Value ? null : oSqlDataReader["value"].ToString();
                        oCfgitem.dbckv = oSqlDataReader["ckv"] == DBNull.Value ? (int?)null : Convert.ToInt32(oSqlDataReader["ckv"]);
                        oCfgitem.GenerateCKV();
                        oCfgitem.CheckCKV(oCfgitem.name);
                        oCfgitems.Add(oCfgitem);
                    }
                }
            }
            return oCfgitems;
        }

        public cfgitems GetCentralCfgitem(string KeyItem, SqlConnection oSqlConnection, int SqlCommandTimeout)
        {
            cfgitems oCfgitem = new cfgitems();
            using (SqlCommand oSqlCommand = new SqlCommand())
            {
                if (oSqlConnection.State != ConnectionState.Open)
                    oSqlConnection.Open();

                oSqlCommand.Connection = oSqlConnection;
                oSqlCommand.CommandTimeout = SqlCommandTimeout;
                oSqlCommand.CommandText = "[dbo].[usp_Getcfgitems]";
                oSqlCommand.CommandType = CommandType.StoredProcedure;
                oSqlCommand.Parameters.AddWithValue("@KeyItem", KeyItem);

                using (SqlDataReader oSqlDataReader = oSqlCommand.ExecuteReader())
                {
                    if (oSqlDataReader.Read())
                    {
                        oCfgitem.name = oSqlDataReader["name"] == DBNull.Value ? null : oSqlDataReader["name"].ToString();
                        oCfgitem.value = oSqlDataReader["value"] == DBNull.Value ? null : oSqlDataReader["value"].ToString();
                        oCfgitem.dbckv = oSqlDataReader["ckv"] == DBNull.Value ? (int?)null : Convert.ToInt32(oSqlDataReader["ckv"]);
                        oCfgitem.GenerateCKV();
                        oCfgitem.CheckCKV(oCfgitem.name);
                    }
                }
            }
            return oCfgitem;
        }

        public ErrorInfo SaveActivity(FSActivity activity, SqlConnection oSqlConnection)
        {
            try
            {
                SqlDataReader reader = null;
                using (SqlCommand oSqlCommand = new SqlCommand())
                {
                    oSqlCommand.Connection = oSqlConnection;
                    oSqlCommand.CommandText = "[dbo].[usp_SaveFsActivity]";
                    oSqlCommand.CommandType = CommandType.StoredProcedure;
                    oSqlCommand.Parameters.AddWithValue("@ApplicationRequestName", activity.ApplicationRequestName);
                    oSqlCommand.Parameters.AddWithValue("@ApplicationRequestVersion", activity.ApplicationRequestVersion);
                    oSqlCommand.Parameters.AddWithValue("@ApplicationLoginId", activity.ApplicationLoginId);
                    oSqlCommand.Parameters.AddWithValue("@ApplicationTransactionId", activity.ApplicationTransactionId);
                    oSqlCommand.Parameters.AddWithValue("@ActivityActionName", activity.ActivityActionName);
                    oSqlCommand.Parameters.AddWithValue("@ActivityDate", activity.ActivityDate);
                    oSqlCommand.Parameters.AddWithValue("@ActivityName", activity.ActivityName);
                    oSqlCommand.Parameters.AddWithValue("@ActivityMessage", activity.ActivityMessage);
                    oSqlCommand.Parameters.AddWithValue("@ActivityUserAddress", activity.ActivityUserAddress);
                    oSqlCommand.Parameters.AddWithValue("@ActivityUserAddressType", activity.ActivityUserAddressType);
                    oSqlCommand.Parameters.AddWithValue("@ActivityHostName", activity.ActivityHostName);
                    oSqlCommand.Parameters.AddWithValue("@ActivityUserName", activity.ActivityUserName);
                    oSqlCommand.Parameters.AddWithValue("@ActivityBranchCode", activity.ActivityBranchCode);
                    oSqlCommand.Parameters.AddWithValue("@ActivityOfficeCode", activity.ActivityOfficeCode);
                    reader = oSqlCommand.ExecuteReader();
                    return GetErrorInfoMapper(reader);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }

        private ErrorInfo GetErrorInfoMapper(SqlDataReader reader)
        {
            ErrorInfo erroInfo = new ErrorInfo();
            while (reader.Read())
            {
                erroInfo.Code = reader["ErrorNumber"] != DBNull.Value ? Convert.ToInt32(reader["ErrorNumber"]) : 0;
                erroInfo.Message = reader["ErrorMessage"] != DBNull.Value ? reader["ErrorMessage"].ToString() : string.Empty;
            }
            return erroInfo;
        }


    }
}
