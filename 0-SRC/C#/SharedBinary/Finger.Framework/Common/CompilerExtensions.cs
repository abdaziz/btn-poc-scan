﻿using System;
using System.Linq.Expressions;


namespace Finger.Framework.Common
{
    public static class CompilerExtensions
    {
        public static string GetNameOf<T>(Expression<Func<T>> expression)
        {
            var body = (MemberExpression)expression.Body;

            return body.Member.Name;
        }
    }
}
