﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finger.Framework.Common
{
    public static class FingerPrintSystemCode
    {
        public const int BASE_ERROR_CODE = 8001;

        public const int SUCCESS_CODE = 0;

        public const int CONNECTION_ERROR_CODE = BASE_ERROR_CODE - 1;
        public const int BIOMETRIC_LICENSE_ERROR_CODE = BASE_ERROR_CODE + 1;
        public const int BIOMETRIC_ENROLLMENT_ERROR_CODE = BASE_ERROR_CODE + 2;
        public const int BIOMETRIC_IDENTIFY_ERROR_CODE = BASE_ERROR_CODE + 3;
        public const int BIOMETRIC_VERIFY_ERROR_CODE = BASE_ERROR_CODE + 4;
        public const int GET_SYS_ADMIN_COUNT_ERROR_CODE = BASE_ERROR_CODE + 5;
        public const int SYS_ADMIN_ERROR_CODE = BASE_ERROR_CODE + 6;
        public const int WEEKEND_BANKING_ERROR_CODE = BASE_ERROR_CODE + 7;
        public const int DELETE_WEEKEND_BANKING_ERROR_CODE = BASE_ERROR_CODE + 8;
        public const int ENROLLMENT_VALIDATION_ERROR_CODE = BASE_ERROR_CODE + 9;
        public const int WEEKEND_BANKING_VALIDATION_ERROR_CODE = BASE_ERROR_CODE + 10;
        public const int IDENTIFY_VALIDATION_ERROR_CODE = BASE_ERROR_CODE + 11;
        public const int VERIFY_VALIDATION_ERROR_CODE = BASE_ERROR_CODE + 12;
        public const int DELETE_RESIGN_USER_ERROR_CODE = BASE_ERROR_CODE + 13;
        public const int BDS_ID_NOT_FOUND_ERROR_CODE = BASE_ERROR_CODE + 14;
        public const int SAVE_JOB_DELETE_VALIDATION_ERROR_CODE = BASE_ERROR_CODE + 15;
        public const int SAVE_JOB_DELETE_ERROR_CODE = BASE_ERROR_CODE + 16;
        public const int CLIENT_INFO_ERROR_CODE = BASE_ERROR_CODE + 17;
        public const int SAVE_ACTIVITY_ERROR_CODE = BASE_ERROR_CODE + 18;
        public const int CLEANUP_MIGRATION_DATA_ERROR_CODE = BASE_ERROR_CODE + 19;
        public const int SAVE_SA_ERROR_CODE = BASE_ERROR_CODE + 20;
        public const int SAVE_SKES_ERROR_CODE = BASE_ERROR_CODE + 21;
        public const int MIGRATIONLOG_NOT_FOUND_ERROR_CODE = BASE_ERROR_CODE + 22;
    }
}
