﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finger.Framework.Models
{
    public class FSActivity
    {
        public Int32 FsActivityId { get; set; }
        public DateTime ActivityDate { get; set; }
        public string ActivityActionName { get; set; }
        public string ActivityName { get; set; }
        public string ActivityMessage { get; set; }
        public string ActivityUserAddress { get; set; }
        public short ActivityUserAddressType { get; set; }
        public string ActivityHostName { get; set; }
        public string ActivityUserName { get; set; }
        public string ActivityBranchCode { get; set; }
        public string ActivityOfficeCode { get; set; }
        public string ApplicationTransactionId { get; set; }
        public string ApplicationRequestName { get; set; }
        public string ApplicationRequestVersion { get; set; }
        public string ApplicationLoginId { get; set; }
    }
}
