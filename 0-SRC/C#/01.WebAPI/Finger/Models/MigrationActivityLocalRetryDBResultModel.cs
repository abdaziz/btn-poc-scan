﻿using System;


namespace Finger.Models
{
    public class MigrationActivityLocalRetryDBResultModel
    {
        public int ErrorCode { get; set; }
        public string DBName { get; set; }
    }
}