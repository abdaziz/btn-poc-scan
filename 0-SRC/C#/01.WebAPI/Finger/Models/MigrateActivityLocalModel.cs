﻿using Finger.Core.Entity;
using System;
using System.Collections.Generic;

namespace Finger.Models
{
    public class MigrateActivityLocalModel
    {
        public ClientInfo ClientInfo { get; set; }
        public string File { get; set; }
        public string FileName { get; set; }
        public List<string> ListFile { get; set; }
    }
}