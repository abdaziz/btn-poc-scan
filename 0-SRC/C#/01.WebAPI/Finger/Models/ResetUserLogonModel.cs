﻿using Finger.Core.Entity;
using Finger.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Models
{
    public class ResetInfo {
        public int FsCentralId { get; set; }
        public string UserId { get; set; }
        public string UserLogTime { get; set; }
        public string BranchCode { get; set; }
        public string OfficeCode { get; set; }
        

    }
    public class ResetUserLogonModel
    {
        public ResetUserLogonModel()
        {
        }

        public ResetInfo ResetUserInfo { get; set; }
        public ClientInfo ClientInfo { get; set; }
        public PagingInfo PagingInfo { get; set; }
        public List<ResetUserLogon> Users { get; set; }
    }
}