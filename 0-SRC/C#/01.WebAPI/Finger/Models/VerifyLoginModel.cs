﻿using Finger.Core.Entity;
using Finger.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Models
{
    public class VerifyLoginModel
    {
        public ClientInfo ClientInfo { get; set; }
        public VerifyInfo VerifyInfo { get; set; }
        public string UserLogTime { get; set; }
    }
}