﻿using Finger.Core.Entity;
using System;

namespace Finger.Models
{
    public class DocumentInfoModel
    {
        public ClientInfo ClientInfo { get; set; }

        public string DocumentName { get; set; }
    }
}