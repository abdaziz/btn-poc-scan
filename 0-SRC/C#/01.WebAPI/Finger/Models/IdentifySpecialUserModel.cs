﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Models
{
    public class IdentifySpecialUserModel
    {
        public string UserId { get; set; }
        public string Template { get; set; }
    }
}