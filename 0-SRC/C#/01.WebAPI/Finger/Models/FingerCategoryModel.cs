﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Models
{
    public class FingerCategoryModel
    {
        public FingerCategoryModel()
        {
            CategoryId = -1;
            CategoryStartPeriod = null;
            CategoryEndPeriod = null;
        }

        public Int16 CategoryId { get; set; }

        public DateTime? CategoryStartPeriod { get; set; }

        public DateTime? CategoryEndPeriod { get; set; }

    }
}