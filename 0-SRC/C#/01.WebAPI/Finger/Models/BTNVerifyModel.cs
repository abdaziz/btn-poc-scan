﻿using Finger.Core.Entity;
using System;


namespace Finger.Models
{
    public class BTNVerifyModel
    {
        public string ApplicationGuid { get; set; }
        public string ApplicationName { get; set; }
        public string ApplicationVersion { get; set; }
        public string BdsId { get; set; }
        public string Template { get; set; }
        public string VerifyDate { get; set; }
    }
}