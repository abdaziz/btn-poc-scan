﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Models
{
    public class IdentifyInfoModel
    {
        public IdentifySpecialUserModel IdentifySpecialUserInfo { get; set; }
        public string Template { get; set; }
    }
}