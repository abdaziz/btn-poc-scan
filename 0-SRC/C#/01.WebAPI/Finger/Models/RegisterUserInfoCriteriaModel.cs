﻿using Finger.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Models
{
    public class RegisterUserInfoCriteriaModel
    {

        public ClientInfo ClientInfo { get; set; }
        public string UserId { get; set; }
    }
}