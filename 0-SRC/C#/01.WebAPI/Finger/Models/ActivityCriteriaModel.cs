﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Models
{
    public class ActivityCriteriaModel
    {
        public string FilterByApplicationValue { get; set; }
        public string FilterByActivityValue { get; set; }
        public string FilterByPeriodStart { get; set; }
        public string FilterByPeriodEnd { get; set; }
        public string BranchCode { get; set; }
        public string OfficeCode { get; set; }
        public int BranchType { get; set; }

    }
}