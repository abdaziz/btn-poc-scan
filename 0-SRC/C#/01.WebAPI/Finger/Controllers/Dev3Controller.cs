﻿using Finger.Core.Entity;
using Finger.Entity;
using Finger.Exception;
using Finger.Framework.Common;
using Finger.Framework.Entity;
using Finger.Models;
using Finger.Validation;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Description;

namespace Finger.Controllers
{
    public partial class FingerController : ApiController
    {
        #region Constant
        private const string FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_PAGING_INFO = "Validate paging info";
        private const string FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_USER_ACCESS_MENU = "Validate user access menu";
        private const string FINGER_SERVICE_METHOD_USER_INQUERY = "User Inquery";
        private const string FINGER_SERVICE_METHOD_REGISTER_USER_MUTATION = "Register Mutation";
        private const string FINGER_SERVICE_METHOD_INQUERY_USER_MUTATION = "Inquery User Mutation";
        private const string FINGER_SERVICE_METHOD_CANCEL_USER_MUTATION = "Cancel Mutation";
        private const string FINGER_SERVICE_METHOD_APPROVE_USER_MUTATION = "Approve User Mutation";
        private const string FINGER_SERVICE_METHOD_FIND_USER_MUTATION = "Find User Mutation";
        private const string FINGER_SERVICE_METHOD_DELETE_USER = "Delete";
        private const string FINGER_ACTIVITY_RESET_USER = "Reset User Logon (User ID: {0};Login time: {1})";
        private const string WEBADMIN_SERVICE_METHOD_LOGIN = "Login";
        private const string WEBADMIN_SERVICE_METHOD_USERMANAGEMENT_ADD = "Add User";
        private const string WEBADMIN_SERVICE_METHOD_USERMANAGEMENT_INQUERY = "Inquery User";
        private const string WEBADMIN_SERVICE_METHOD_USERMANAGEMENT_DELETE = "Delete User";
        private const string WEBADMIN_SERVICE_METHOD_USERMANAGEMENT_GET_ROLES = "Get Roles";
        #endregion

        #region Public Method(s)
        /// <summary>
        /// Get user inquery
        /// </summary>
        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/get_user_inquery")]
        public HttpResponseMessage UserInquery([FromBody]UserInqueryModel oUserInqueryModel)
        {
            string guidThreadId = string.Empty;
            ServiceResponse oServiceResponse = new ServiceResponse();

            try
            {
                if (oUserInqueryModel == null) LogErrorNullParameter("User info", FINGER_SERVICE_METHOD_USER_INQUERY);
                if (oUserInqueryModel.ClientInfo == null) LogErrorNullParameter("Client info", FINGER_SERVICE_METHOD_USER_INQUERY);
                if (oUserInqueryModel.PagingInfo == null) LogErrorNullParameter("Paging info", FINGER_SERVICE_METHOD_USER_INQUERY);
                if (oUserInqueryModel.BranchCode == null) LogErrorNullParameter("Branch code", FINGER_SERVICE_METHOD_USER_INQUERY);
                if (oUserInqueryModel.OfficeCode == null) LogErrorNullParameter("Office code", FINGER_SERVICE_METHOD_USER_INQUERY);

                guidThreadId = GetGuidThreadId(oUserInqueryModel.ClientInfo);
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_METHOD_USER_INQUERY), Constant.LEVEL_ALL, oUserInqueryModel.ClientInfo);
                if (ValidateClientInfo(guidThreadId, oUserInqueryModel.ClientInfo, ref oServiceResponse))
                {
                    if (ValidatePagingInfo(guidThreadId, oUserInqueryModel.PagingInfo, oUserInqueryModel.ClientInfo, ref oServiceResponse))
                    {
                        if (ValidateUserAccessMenu(guidThreadId, oUserInqueryModel.ClientInfo, Enumeration.EnumOfFSMGMTMenu.ListOfUser, ref oServiceResponse))
                        {

                            if (oUserInqueryModel.BranchType == (int)Enumeration.EnumOfBranchHierarchyType.BRANCH_HIERARCHY) //When BranchType = Branch Hierarchy
                            {
                                oServiceResponse = fingerLogic.ValidateBranchHierarchy(oUserInqueryModel.ClientInfo.LogonID, oUserInqueryModel.BranchCode);
                            }

                            if (oServiceResponse.statusCode == Constant.NO_ERROR)
                            {
                                DataTable exportDataTable = new DataTable();
                                ErrorInfo oErrorInfo = fingerLogic.getUserInquery(oUserInqueryModel, out exportDataTable);
                                oServiceResponse.statusCode = oErrorInfo.Code;
                                oServiceResponse.statusMessage = oErrorInfo.Message;
                                //oServiceResponse.responseData = JsonConvert.SerializeObject(new { oUserInqueryModel.Users, oUserInqueryModel.PagingInfo });

                                if (oErrorInfo.Code != Constant.NO_ERROR)
                                {
                                    oServiceResponse.responseData = string.Empty;
                                }
                                else
                                {
                                    if (oUserInqueryModel.PagingInfo.Index != null)
                                    {
                                        oServiceResponse.responseData = JsonConvert.SerializeObject(new { oUserInqueryModel.Users, oUserInqueryModel.PagingInfo });
                                    }
                                    else
                                    {

                                        Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, "Export user"), Constant.LEVEL_ALL, oUserInqueryModel.ClientInfo);
                                        string fileNameFullPath = null;
                                        if (oUserInqueryModel.PagingInfo.ExportFileType.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfExportType.PDF_File)))
                                        {
                                            List<Columns> columns = this.ConfigureReportUserColumn(Enumeration.EnumOfExportType.PDF_File);
                                            fileNameFullPath = GetExportFileName(oUserInqueryModel.ClientInfo.BranchCode, oUserInqueryModel.ClientInfo.OfficeCode, Enumeration.EnumOfApplicationExport.LIST_OF_USERS, oUserInqueryModel.PagingInfo.ExportFileType);
                                            Utility.GeneratePDF(exportDataTable, columns, fileNameFullPath);
                                            Log(guidThreadId, string.Format("Export user as pdf file ({0}) success", fileNameFullPath), Constant.LEVEL_ALL, oUserInqueryModel.ClientInfo);
                                        }
                                        else if (oUserInqueryModel.PagingInfo.ExportFileType.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfExportType.CSV_File)))
                                        {
                                            List<Columns> columns = this.ConfigureReportUserColumn(Enumeration.EnumOfExportType.CSV_File);
                                            fileNameFullPath = GetExportFileName(oUserInqueryModel.ClientInfo.BranchCode, oUserInqueryModel.ClientInfo.OfficeCode, Enumeration.EnumOfApplicationExport.LIST_OF_USERS, oUserInqueryModel.PagingInfo.ExportFileType);
                                            Utility.GenerateCSV(exportDataTable, columns, fileNameFullPath);
                                            Log(guidThreadId, string.Format("Export user as csv file ({0}) success", fileNameFullPath), Constant.LEVEL_ALL, oUserInqueryModel.ClientInfo);
                                            Utility.CreateSingleZip(fileNameFullPath, Path.GetDirectoryName(fileNameFullPath), string.Empty, Path.GetFileNameWithoutExtension(fileNameFullPath));
                                            string zipFullPath = Path.Combine(Path.GetDirectoryName(fileNameFullPath), string.Concat(Path.GetFileNameWithoutExtension(fileNameFullPath), ".zip"));
                                            Log(guidThreadId, string.Format("Export user as zip file ({0}) success", zipFullPath), Constant.LEVEL_ALL, oUserInqueryModel.ClientInfo);
                                            File.Delete(fileNameFullPath);
                                            Log(guidThreadId, string.Format("Delete user csv file ({0}) success", fileNameFullPath), Constant.LEVEL_ALL, oUserInqueryModel.ClientInfo);
                                        }
                                        Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, "Export user"), Constant.LEVEL_ALL, oUserInqueryModel.ClientInfo);
                                        exportDataTable = null;
                                        string fileName = Path.GetFileName(fileNameFullPath);
                                        oServiceResponse.responseData = JsonConvert.SerializeObject(new { fileName });

                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (FingerServiceException ex)
            {
                oServiceResponse = ex.ServiceResponse;
            }
            catch (System.Exception ex)
            {
                oServiceResponse.statusCode = Constant.ERROR_CODE_SYSTEM;
                oServiceResponse.statusMessage = ex.Message;
            }
            finally
            {
                if (oUserInqueryModel == null || oUserInqueryModel.ClientInfo == null)
                {
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_USER_INQUERY), Constant.LEVEL_ALL);
                }
                else
                {
                    SetMethodResult(guidThreadId, FINGER_SERVICE_METHOD_USER_INQUERY, oServiceResponse, oUserInqueryModel.ClientInfo);
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_USER_INQUERY), Constant.LEVEL_ALL, oUserInqueryModel.ClientInfo);
                }
            }

            return GetResponseMessage(oServiceResponse);
        }

        /// <summary>
        /// Delete user
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/delete_user")]
        public HttpResponseMessage DeleteUser([FromBody]DeleteUserModel oDeleteUserModel)
        {
            string guidThreadId = string.Empty;
            ServiceResponse oServiceResponse = new ServiceResponse();
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                if (oDeleteUserModel == null) LogErrorNullParameter("User info", FINGER_SERVICE_METHOD_DELETE_USER);
                if (oDeleteUserModel.ClientInfo == null) LogErrorNullParameter("Client info", FINGER_SERVICE_METHOD_DELETE_USER);
                if (oDeleteUserModel.Users == null || oDeleteUserModel.Users.Count == 0) LogErrorNullParameter("Selected users", FINGER_SERVICE_METHOD_DELETE_USER);

                guidThreadId = GetGuidThreadId(oDeleteUserModel.ClientInfo);
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_METHOD_DELETE_USER), Constant.LEVEL_ALL, oDeleteUserModel.ClientInfo);
                if (ValidateClientInfo(guidThreadId, oDeleteUserModel.ClientInfo, ref oServiceResponse))
                {
                    if (ValidateUserAccessMenu(guidThreadId, oDeleteUserModel.ClientInfo, Enumeration.EnumOfFSMGMTMenu.ListOfUser, ref oServiceResponse))
                    {
                        oErrorInfo = fingerLogic.deleteUser(oDeleteUserModel);
                        oServiceResponse.statusCode = oErrorInfo.Code;
                        oServiceResponse.statusMessage = oErrorInfo.Message;
                    }
                    else
                    {
                        oErrorInfo.Code = oServiceResponse.statusCode;
                        oErrorInfo.Message = oServiceResponse.statusMessage;
                    }
                }
                else
                {
                    oErrorInfo.Code = oServiceResponse.statusCode;
                    oErrorInfo.Message = oServiceResponse.statusMessage;
                }
            }
            catch (FingerServiceException ex)
            {
                oErrorInfo.Code = Constant.ERROR_CODE_SYSTEM;
                oServiceResponse = ex.ServiceResponse;
            }
            catch (System.Exception ex)
            {
                oErrorInfo.Code = Constant.ERROR_CODE_SYSTEM;
                oServiceResponse.statusCode = Constant.ERROR_CODE_SYSTEM;
                oServiceResponse.statusMessage = ex.Message;
            }
            finally
            {
                if (oDeleteUserModel == null || oDeleteUserModel.ClientInfo == null)
                {
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_DELETE_USER), Constant.LEVEL_ALL);
                }
                else
                {
                    SetMethodResult(guidThreadId, FINGER_SERVICE_METHOD_DELETE_USER, oServiceResponse, oDeleteUserModel.ClientInfo);
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_DELETE_USER), Constant.LEVEL_ALL, oDeleteUserModel.ClientInfo);
                    if (oErrorInfo.Code == Constant.NO_ERROR)
                    {
                        for (int i = 0; i < oDeleteUserModel.Users.Count; i++)
                        {
                            SaveActivity(FINGER_SERVICE_METHOD_DELETE_USER, FINGER_SERVICE_METHOD_DELETE_USER, GetActivitySuccessMessageForDeleteUser(oDeleteUserModel.Users[i]), guidThreadId, oDeleteUserModel.ClientInfo, ref oServiceResponse);
                        }
                    }
                }
            }

            return GetResponseMessage(oServiceResponse);
        }

        /// <summary>
        /// Reset user logon
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/reset_user_logon")]
        public HttpResponseMessage ResetUserLogon([FromBody]ResetUserLogonModel oResetUserLogonModel)
        {
            string guidThreadId = string.Empty;
            ServiceResponse oServiceResponse = new ServiceResponse();
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                if (oResetUserLogonModel == null) LogErrorNullParameter("User info", FS_METHOD_RESET_USER_LOGON);
                if (oResetUserLogonModel.ClientInfo == null) LogErrorNullParameter("Client info", FS_METHOD_RESET_USER_LOGON);
                if (oResetUserLogonModel.ResetUserInfo == null) LogErrorNullParameter("Reset info", FS_METHOD_RESET_USER_LOGON);

                guidThreadId = GetGuidThreadId(oResetUserLogonModel.ClientInfo);
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FS_METHOD_RESET_USER_LOGON), Constant.LEVEL_ALL, oResetUserLogonModel.ClientInfo);

                if (ValidateClientInfo(guidThreadId, oResetUserLogonModel.ClientInfo, ref oServiceResponse))
                {
                    if (ValidateUserAccessMenu(guidThreadId, oResetUserLogonModel.ClientInfo, Enumeration.EnumOfFSMGMTMenu.ListOfUser, ref oServiceResponse))
                    {
                        oErrorInfo = fingerLogic.resetUserLogon(oResetUserLogonModel);
                        oServiceResponse.statusCode = oErrorInfo.Code;
                        oServiceResponse.statusMessage = oErrorInfo.Message;
                    }
                }
            }
            catch (FingerServiceException ex)
            {
                oErrorInfo.Code = Constant.ERROR_CODE_SYSTEM;
                oServiceResponse = ex.ServiceResponse;
            }
            catch (System.Exception ex)
            {
                oErrorInfo.Code = Constant.ERROR_CODE_SYSTEM;
                oServiceResponse.statusCode = Constant.ERROR_CODE_SYSTEM;
                oServiceResponse.statusMessage = ex.Message;
            }
            finally
            {
                if (oResetUserLogonModel == null || oResetUserLogonModel.ClientInfo == null)
                {
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FS_METHOD_RESET_USER_LOGON), Constant.LEVEL_ALL);
                }
                else
                {
                    SetMethodResult(guidThreadId, FS_METHOD_RESET_USER_LOGON, oServiceResponse, oResetUserLogonModel.ClientInfo);
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FS_METHOD_RESET_USER_LOGON), Constant.LEVEL_ALL, oResetUserLogonModel.ClientInfo);

                    if (oErrorInfo.Code == Constant.NO_ERROR)
                    {
                        string message = FINGER_ACTIVITY_RESET_USER.Replace("{0}", oResetUserLogonModel.ResetUserInfo.UserId);
                        message = message.Replace("{1}", Convert.ToDateTime(oResetUserLogonModel.ResetUserInfo.UserLogTime).ToString(Constant.ACTIVITY_USER_LOG_TIME_FORMAT));
                        SaveActivity(FS_METHOD_RESET_USER_LOGON, FS_METHOD_RESET_USER_LOGON, string.Format(message, FS_METHOD_RESET_USER_LOGON)
                            , guidThreadId, oResetUserLogonModel.ClientInfo, ref oServiceResponse);
                    }
                }
            }
            return GetResponseMessage(oServiceResponse);
        }

        /// <summary>
        /// Reg user mutation
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/reg_user_mutation")]
        public HttpResponseMessage RegUserMutation([FromBody]UserMutationModel oUserMutationModel)
        {
            string guidThreadId = string.Empty;
            ServiceResponse oServiceResponse = new ServiceResponse();
            ErrorInfo oErrorInfo = new ErrorInfo();

            try
            {
                if (oUserMutationModel == null) LogErrorNullParameter("User info", FINGER_SERVICE_METHOD_REGISTER_USER_MUTATION);
                if (oUserMutationModel.ClientInfo == null) LogErrorNullParameter("Client info", FINGER_SERVICE_METHOD_REGISTER_USER_MUTATION);
                if (oUserMutationModel.UserMutation == null) LogErrorNullParameter("User mutation info", FINGER_SERVICE_METHOD_REGISTER_USER_MUTATION);

                guidThreadId = GetGuidThreadId(oUserMutationModel.ClientInfo);
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_METHOD_REGISTER_USER_MUTATION), Constant.LEVEL_ALL, oUserMutationModel.ClientInfo);
                if (ValidateClientInfo(guidThreadId, oUserMutationModel.ClientInfo, ref oServiceResponse))
                {
                    if (ValidateUserAccessMenu(guidThreadId, oUserMutationModel.ClientInfo, Enumeration.EnumOfFSMGMTMenu.ListOfUser, ref oServiceResponse))
                    {
                        if (ValidateUserMutation(guidThreadId, oUserMutationModel.UserMutation, oUserMutationModel.ClientInfo, (Int16)Enumeration.EnumOfMutationAction.REGISTER_MUTATION, ref oServiceResponse))
                        {
                            oErrorInfo = fingerLogic.regUserMutation(oUserMutationModel);
                            oServiceResponse.statusCode = oErrorInfo.Code;
                            oServiceResponse.statusMessage = oErrorInfo.Message;
                        }
                        else
                        {
                            oErrorInfo.Code = oServiceResponse.statusCode;
                            oErrorInfo.Message = oServiceResponse.statusMessage;

                        }
                    }
                    else
                    {
                        oErrorInfo.Code = oServiceResponse.statusCode;
                        oErrorInfo.Message = oServiceResponse.statusMessage;

                    }
                }
                else
                {
                    oErrorInfo.Code = oServiceResponse.statusCode;
                    oErrorInfo.Message = oServiceResponse.statusMessage;

                }
            }
            catch (FingerServiceException ex)
            {
                oErrorInfo.Code = Constant.ERROR_CODE_SYSTEM;
                oServiceResponse = ex.ServiceResponse;
            }
            catch (System.Exception ex)
            {
                oErrorInfo.Code = Constant.ERROR_CODE_SYSTEM;
                oServiceResponse.statusCode = Constant.ERROR_CODE_SYSTEM;
                oServiceResponse.statusMessage = ex.Message;
            }
            finally
            {
                if (oUserMutationModel == null || oUserMutationModel.ClientInfo == null)
                {
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_REGISTER_USER_MUTATION), Constant.LEVEL_ALL);
                }
                else
                {
                    SetMethodResult(guidThreadId, FINGER_SERVICE_METHOD_REGISTER_USER_MUTATION, oServiceResponse, oUserMutationModel.ClientInfo);
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_REGISTER_USER_MUTATION), Constant.LEVEL_ALL, oUserMutationModel.ClientInfo);
                    if (oErrorInfo.Code == Constant.NO_ERROR)
                        SaveActivity(FINGER_SERVICE_METHOD_REGISTER_USER_MUTATION, FINGER_SERVICE_METHOD_REGISTER_USER_MUTATION, GetActivitySuccessMessageForMutationUser(oUserMutationModel.UserMutation, "Registered")
                            , guidThreadId, oUserMutationModel.ClientInfo, ref oServiceResponse);
                }
            }

            return GetResponseMessage(oServiceResponse);
        }

        /// <summary>
        /// Get user mutation inquery
        /// </summary>
        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/get_user_mutation_inquery")]
        public HttpResponseMessage InqueryUserMutation([FromBody]UserMutationModel oUserMutationModel)
        {
            string guidThreadId = string.Empty;
            ServiceResponse oServiceResponse = new ServiceResponse();
            try
            {
                if (oUserMutationModel == null) LogErrorNullParameter("User info", FINGER_SERVICE_METHOD_INQUERY_USER_MUTATION);
                if (oUserMutationModel.ClientInfo == null) LogErrorNullParameter("Client info", FINGER_SERVICE_METHOD_INQUERY_USER_MUTATION);

                guidThreadId = GetGuidThreadId(oUserMutationModel.ClientInfo);
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_METHOD_INQUERY_USER_MUTATION), Constant.LEVEL_ALL, oUserMutationModel.ClientInfo);
                if (ValidateClientInfo(guidThreadId, oUserMutationModel.ClientInfo, ref oServiceResponse))
                {
                    if (ValidatePagingInfo(guidThreadId, oUserMutationModel.PagingInfo, oUserMutationModel.ClientInfo, ref oServiceResponse))
                    {
                        if (ValidateUserAccessMenu(guidThreadId, oUserMutationModel.ClientInfo, Enumeration.EnumOfFSMGMTMenu.ListOfUserMutation, ref oServiceResponse))
                        {

                            //oServiceResponse = fingerLogic.ValidateBranchHierarchy(oUserMutationModel.ClientInfo.LogonID, oUserMutationModel.BranchCode);

                            if (oServiceResponse.statusCode == Constant.NO_ERROR)
                            {
                                var exportDataTable = new System.Data.DataTable();
                                ErrorInfo oErrorInfo = fingerLogic.getUserMutation(oUserMutationModel, out exportDataTable, oUserMutationModel.PagingInfo);
                                oServiceResponse.statusCode = oErrorInfo.Code;
                                oServiceResponse.statusMessage = oErrorInfo.Message;
                                //before changes
                                //oServiceResponse.responseData = JsonConvert.SerializeObject(new { oUserMutationModel.Users, oUserMutationModel.PagingInfo });

                                if (oErrorInfo.Code != Constant.NO_ERROR)
                                {
                                    oServiceResponse.responseData = string.Empty;
                                }
                                else
                                {
                                    //after changes export
                                    if (oUserMutationModel.PagingInfo.Index != null)
                                    {
                                        oServiceResponse.responseData = JsonConvert.SerializeObject(new { oUserMutationModel.Users, oUserMutationModel.PagingInfo });
                                    }
                                    else
                                    {
                                        Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, "Export mutation"), Constant.LEVEL_ALL, oUserMutationModel.ClientInfo);
                                        string fileNameFullPath = null;
                                        if (oUserMutationModel.PagingInfo.ExportFileType.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfExportType.PDF_File)))
                                        {
                                            List<Columns> columns = this.ConfigureReportMutationColumn(Enumeration.EnumOfExportType.PDF_File);
                                            fileNameFullPath = GetExportFileName(oUserMutationModel.ClientInfo.BranchCode, oUserMutationModel.ClientInfo.OfficeCode, Enumeration.EnumOfApplicationExport.LIST_OF_MUTATION, oUserMutationModel.PagingInfo.ExportFileType);
                                            Utility.GeneratePDF(exportDataTable, columns, fileNameFullPath);
                                            Log(guidThreadId, string.Format("Export list of mutation as pdf file ({0}) success", fileNameFullPath), Constant.LEVEL_ALL, oUserMutationModel.ClientInfo);
                                        }
                                        else if (oUserMutationModel.PagingInfo.ExportFileType.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfExportType.CSV_File)))
                                        {
                                            List<Columns> columns = this.ConfigureReportMutationColumn(Enumeration.EnumOfExportType.CSV_File);
                                            fileNameFullPath = GetExportFileName(oUserMutationModel.ClientInfo.BranchCode, oUserMutationModel.ClientInfo.OfficeCode, Enumeration.EnumOfApplicationExport.LIST_OF_MUTATION, oUserMutationModel.PagingInfo.ExportFileType);
                                            Utility.GenerateCSV(exportDataTable, columns, fileNameFullPath);
                                            Log(guidThreadId, string.Format("Export list of mutation as csv file ({0}) success", fileNameFullPath), Constant.LEVEL_ALL, oUserMutationModel.ClientInfo);
                                            Utility.CreateSingleZip(fileNameFullPath, Path.GetDirectoryName(fileNameFullPath), string.Empty, Path.GetFileNameWithoutExtension(fileNameFullPath));
                                            string zipFullPath = Path.Combine(Path.GetDirectoryName(fileNameFullPath), string.Concat(Path.GetFileNameWithoutExtension(fileNameFullPath), ".zip"));
                                            Log(guidThreadId, string.Format("Export list of mutation as zip file ({0}) success", zipFullPath), Constant.LEVEL_ALL, oUserMutationModel.ClientInfo);
                                            File.Delete(fileNameFullPath);
                                            Log(guidThreadId, string.Format("Delete list of mutation csv file ({0}) success", fileNameFullPath), Constant.LEVEL_ALL, oUserMutationModel.ClientInfo);
                                        }
                                        exportDataTable = null;
                                        string fileName = System.IO.Path.GetFileName(fileNameFullPath);
                                        oServiceResponse.responseData = JsonConvert.SerializeObject(new { fileName });
                                    }
                                }

                            }

                        }
                    }
                }
            }
            catch (Framework.Exception.CKVException ex)
            {
                oServiceResponse.statusCode = Constant.ERROR_CODE_SYSTEM;
                oServiceResponse.statusMessage = ex.Message;
            }
            catch (FingerServiceException ex)
            {
                oServiceResponse = ex.ServiceResponse;
            }
            catch (System.Exception ex)
            {
                oServiceResponse.statusCode = Constant.ERROR_CODE_SYSTEM;
                oServiceResponse.statusMessage = ex.Message;
            }
            finally
            {
                if (oUserMutationModel == null || oUserMutationModel.ClientInfo == null)
                {
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_INQUERY_USER_MUTATION), Constant.LEVEL_ALL);
                }
                else
                {
                    SetMethodResult(guidThreadId, FINGER_SERVICE_METHOD_INQUERY_USER_MUTATION, oServiceResponse, oUserMutationModel.ClientInfo);
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_INQUERY_USER_MUTATION), Constant.LEVEL_ALL, oUserMutationModel.ClientInfo);
                }
            }
            return GetResponseMessage(oServiceResponse);
        }

        /// <summary>
        /// Cancel user mutation
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/cancel_user_mutation")]
        public HttpResponseMessage CancelUserMutation([FromBody]UserMutationModel oUserMutationModel)
        {
            string guidThreadId = string.Empty;
            ServiceResponse oServiceResponse = new ServiceResponse();
            ErrorInfo oErrorInfo = new ErrorInfo();
            List<UserMutation> listWantToCancel = oUserMutationModel.Users;

            try
            {
                if (oUserMutationModel == null) LogErrorNullParameter("User info", FINGER_SERVICE_METHOD_CANCEL_USER_MUTATION);
                if (oUserMutationModel.ClientInfo == null) LogErrorNullParameter("Client info", FINGER_SERVICE_METHOD_CANCEL_USER_MUTATION);
                if (oUserMutationModel.Users == null || oUserMutationModel.Users.Count == 0) LogErrorNullParameter("User collection info", FINGER_SERVICE_METHOD_CANCEL_USER_MUTATION);



                guidThreadId = GetGuidThreadId(oUserMutationModel.ClientInfo);
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_METHOD_CANCEL_USER_MUTATION), Constant.LEVEL_ALL, oUserMutationModel.ClientInfo);
                if (ValidateClientInfo(guidThreadId, oUserMutationModel.ClientInfo, ref oServiceResponse))
                {
                    if (ValidateUserAccessMenu(guidThreadId, oUserMutationModel.ClientInfo, Enumeration.EnumOfFSMGMTMenu.ListOfUserMutation, ref oServiceResponse))
                    {
                        if (ValidateUserMutation(guidThreadId, oUserMutationModel.UserMutation, oUserMutationModel.ClientInfo, (Int16)Enumeration.EnumOfMutationAction.CANCEL_MUTATION, ref oServiceResponse))
                        {
                            oErrorInfo = fingerLogic.CancelUserMutation(oUserMutationModel);
                            oServiceResponse.statusCode = oErrorInfo.Code;
                            oServiceResponse.statusMessage = oErrorInfo.Message;
                        }
                        else
                        {
                            oErrorInfo.Code = oServiceResponse.statusCode;
                            oErrorInfo.Message = oServiceResponse.statusMessage;

                        }
                    }
                    else
                    {
                        oErrorInfo.Code = oServiceResponse.statusCode;
                        oErrorInfo.Message = oServiceResponse.statusMessage;

                    }
                }
                else
                {
                    oErrorInfo.Code = oServiceResponse.statusCode;
                    oErrorInfo.Message = oServiceResponse.statusMessage;

                }
            }
            catch (FingerServiceException ex)
            {
                oErrorInfo.Code = Constant.ERROR_CODE_SYSTEM;
                oServiceResponse = ex.ServiceResponse;
            }
            catch (System.Exception ex)
            {
                oErrorInfo.Code = Constant.ERROR_CODE_SYSTEM;
                oServiceResponse.statusCode = Constant.ERROR_CODE_SYSTEM;
                oServiceResponse.statusMessage = ex.Message;
            }
            finally
            {
                if (oUserMutationModel == null || oUserMutationModel.ClientInfo == null)
                {
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_CANCEL_USER_MUTATION), Constant.LEVEL_ALL);
                }
                else
                {
                    SetMethodResult(guidThreadId, FINGER_SERVICE_METHOD_CANCEL_USER_MUTATION, oServiceResponse, oUserMutationModel.ClientInfo);
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_CANCEL_USER_MUTATION), Constant.LEVEL_ALL, oUserMutationModel.ClientInfo);
                    if (oErrorInfo.Code == Constant.NO_ERROR)
                    {
                        for (int i = 0; i < listWantToCancel.Count; i++)
                        {
                            SaveActivity(FINGER_SERVICE_METHOD_CANCEL_USER_MUTATION, FINGER_SERVICE_METHOD_CANCEL_USER_MUTATION, GetActivitySuccessMessageForMutationUser(oUserMutationModel.Users[i], "Cancelled")
                                , guidThreadId, oUserMutationModel.ClientInfo, ref oServiceResponse);
                        }
                    }
                }
            }

            return GetResponseMessage(oServiceResponse);
        }

        /// <summary>
        /// Approval user mutation
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/approve_user_mutation")]
        public HttpResponseMessage ApproveUserMutation([FromBody]UserMutationModel oUserMutationModel)
        {
            string guidThreadId = string.Empty;
            ServiceResponse oServiceResponse = new ServiceResponse();
            try
            {
                if (oUserMutationModel == null) LogErrorNullParameter("User info", FINGER_SERVICE_METHOD_APPROVE_USER_MUTATION);
                if (oUserMutationModel.ClientInfo == null) LogErrorNullParameter("Client info", FINGER_SERVICE_METHOD_APPROVE_USER_MUTATION);
                if (oUserMutationModel.Users == null || oUserMutationModel.Users.Count == 0) LogErrorNullParameter("User collection info", FINGER_SERVICE_METHOD_APPROVE_USER_MUTATION);

                guidThreadId = GetGuidThreadId(oUserMutationModel.ClientInfo);
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_METHOD_APPROVE_USER_MUTATION), Constant.LEVEL_ALL, oUserMutationModel.ClientInfo);
                if (ValidateClientInfo(guidThreadId, oUserMutationModel.ClientInfo, ref oServiceResponse))
                {
                    if (ValidateUserAccessMenu(guidThreadId, oUserMutationModel.ClientInfo, Enumeration.EnumOfFSMGMTMenu.ListOfUserMutation, ref oServiceResponse))
                    {
                        ErrorInfo oErrorInfo = fingerLogic.ApproveUserMutation(oUserMutationModel);
                        oServiceResponse.statusCode = oErrorInfo.Code;
                        oServiceResponse.statusMessage = oErrorInfo.Message;
                    }
                }
            }
            catch (FingerServiceException ex)
            {
                oServiceResponse = ex.ServiceResponse;
            }
            catch (System.Exception ex)
            {
                oServiceResponse.statusCode = Constant.ERROR_CODE_SYSTEM;
                oServiceResponse.statusMessage = ex.Message;
            }
            finally
            {
                if (oUserMutationModel == null || oUserMutationModel.ClientInfo == null)
                {
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_APPROVE_USER_MUTATION), Constant.LEVEL_ALL);
                }
                else
                {
                    SetMethodResult(guidThreadId, FINGER_SERVICE_METHOD_APPROVE_USER_MUTATION, oServiceResponse, oUserMutationModel.ClientInfo);
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_APPROVE_USER_MUTATION), Constant.LEVEL_ALL, oUserMutationModel.ClientInfo);
                }
            }

            return GetResponseMessage(oServiceResponse);
        }

        /// <summary>
        /// Find user mutation
        /// </summary>
        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/find_user_mutation")]
        public HttpResponseMessage FindUserMutation([FromBody]UserMutationModel oUserMutationModel)
        {
            string guidThreadId = string.Empty;
            ServiceResponse oServiceResponse = new ServiceResponse();
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                if (oUserMutationModel == null) LogErrorNullParameter("User info", FINGER_SERVICE_METHOD_FIND_USER_MUTATION);
                if (oUserMutationModel.ClientInfo == null) LogErrorNullParameter("Client info", FINGER_SERVICE_METHOD_FIND_USER_MUTATION);
                if (string.IsNullOrEmpty(oUserMutationModel.UserMutation.UserId)) LogErrorNullParameter("UserId", FINGER_SERVICE_METHOD_FIND_USER_MUTATION);

                guidThreadId = GetGuidThreadId(oUserMutationModel.ClientInfo);
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_METHOD_FIND_USER_MUTATION), Constant.LEVEL_ALL, oUserMutationModel.ClientInfo);
                if (ValidateClientInfo(guidThreadId, oUserMutationModel.ClientInfo, ref oServiceResponse))
                {
                    if (ValidateUserAccessMenu(guidThreadId, oUserMutationModel.ClientInfo, Enumeration.EnumOfFSMGMTMenu.ListOfUser, ref oServiceResponse))
                    {
                        oErrorInfo = fingerLogic.FindUserMutation(oUserMutationModel);
                        oServiceResponse.statusCode = oErrorInfo.Code;
                        oServiceResponse.statusMessage = oErrorInfo.Message;
                        oServiceResponse.responseData = JsonConvert.SerializeObject(new { oUserMutationModel.UserMutation });
                    }
                }
            }
            catch (FingerServiceException ex)
            {
                oErrorInfo.Code = Constant.ERROR_CODE_SYSTEM;
                oServiceResponse = ex.ServiceResponse;
            }
            catch (System.Exception ex)
            {
                oErrorInfo.Code = Constant.ERROR_CODE_SYSTEM;
                oServiceResponse.statusCode = Constant.ERROR_CODE_SYSTEM;
                oServiceResponse.statusMessage = ex.Message;
            }
            finally
            {
                if (oUserMutationModel == null || oUserMutationModel.ClientInfo == null)
                {
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_FIND_USER_MUTATION), Constant.LEVEL_ALL);
                }
                else
                {
                    SetMethodResult(guidThreadId, FINGER_SERVICE_METHOD_FIND_USER_MUTATION, oServiceResponse, oUserMutationModel.ClientInfo);
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_FIND_USER_MUTATION), Constant.LEVEL_ALL, oUserMutationModel.ClientInfo);
                }
            }
            return GetResponseMessage(oServiceResponse);
        }


        #region Web Admin
        /// <summary>
        /// Validation web admin login
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("admin/login")]
        public HttpResponseMessage WebAdminValidationLogin([FromBody]BCAUserModel oBCAUserModel)
        {
            string guidThreadId = Utility.GetGuidThreadId(Guid.NewGuid().ToString());
            ServiceResponse oServiceResponse = new ServiceResponse();
            ErrorInfo oErrorInfo = new ErrorInfo();

            try
            {
                if (oBCAUserModel == null) LogErrorNullParameter("Login info", WEBADMIN_SERVICE_METHOD_LOGIN);

                WebAdminLog(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, WEBADMIN_SERVICE_METHOD_LOGIN), Constant.LEVEL_ALL);
                oErrorInfo = fingerLogic.WebAdminLogin(oBCAUserModel, Enumeration.EnumOfWebAdminMenu.Login
                    , Enumeration.EnumOfWebAdminRole.SuperAdmin, Enumeration.EnumOfWebAdminRole.Admin, Enumeration.EnumOfWebAdminRole.PICApps);
                oServiceResponse.statusCode = oErrorInfo.Code;
                oServiceResponse.statusMessage = oErrorInfo.Message;
                oServiceResponse.responseData = JsonConvert.SerializeObject(new
                {
                    oBCAUserModel
                });
            }
            catch (FingerServiceException ex)
            {
                oServiceResponse = ex.ServiceResponse;
            }
            catch (System.Exception ex)
            {
                oErrorInfo.Code = Constant.ERROR_CODE_SYSTEM;
                oServiceResponse.statusCode = Constant.ERROR_CODE_SYSTEM;
                oServiceResponse.statusMessage = ex.Message;

            }
            finally
            {
                if (oErrorInfo != null)
                {
                    if (oErrorInfo.Code != Constant.NO_ERROR)
                    {
                        WebAdminLog(guidThreadId, string.Format(LOG_METHOD_NAME_FAILED, WEBADMIN_SERVICE_METHOD_LOGIN, oErrorInfo.Code, oErrorInfo.Message), Constant.LEVEL_ERROR);
                    }
                    else
                    {
                        WebAdminLog(guidThreadId, string.Format(LOG_METHOD_NAME_SUCCESS, WEBADMIN_SERVICE_METHOD_LOGIN), Constant.LEVEL_ALL);
                    }
                }

                WebAdminLog(guidThreadId, string.Format(LOG_END_METHOD_NAME, WEBADMIN_SERVICE_METHOD_LOGIN), Constant.LEVEL_ALL);
            }
            return GetResponseMessage(oServiceResponse);
        }

        /// <summary>
        /// Add Users
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("admin/add_user")]
        public HttpResponseMessage WebAdminAddUser([FromBody]BCAUserModel oBCAUserModel)
        {
            string guidThreadId = Utility.GetGuidThreadId(Guid.NewGuid().ToString());
            ServiceResponse oServiceResponse = new ServiceResponse();
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                if (oBCAUserModel == null) LogErrorNullParameter("User info", WEBADMIN_SERVICE_METHOD_USERMANAGEMENT_ADD);

                WebAdminLog(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, WEBADMIN_SERVICE_METHOD_USERMANAGEMENT_ADD), Constant.LEVEL_ALL);
                oErrorInfo = fingerLogic.WebAdminAddUser(oBCAUserModel, Enumeration.EnumOfWebAdminMenu.UserManagement
                    , Enumeration.EnumOfWebAdminRole.SuperAdmin, Enumeration.EnumOfWebAdminRole.Admin);
                oServiceResponse.statusCode = oErrorInfo.Code;
                oServiceResponse.statusMessage = oErrorInfo.Message;
            }
            catch (FingerServiceException ex)
            {
                oServiceResponse = ex.ServiceResponse;
            }
            catch (System.Exception ex)
            {
                oErrorInfo.Code = Constant.ERROR_CODE_SYSTEM;
                oServiceResponse.statusCode = Constant.ERROR_CODE_SYSTEM;
                oServiceResponse.statusMessage = ex.Message;

            }
            finally
            {
                if (oErrorInfo != null)
                {
                    if (oErrorInfo.Code != Constant.NO_ERROR)
                    {
                        WebAdminLog(guidThreadId, string.Format(LOG_METHOD_NAME_FAILED, WEBADMIN_SERVICE_METHOD_USERMANAGEMENT_ADD, oErrorInfo.Code, oErrorInfo.Message), Constant.LEVEL_ERROR);
                    }
                    else
                    {
                        WebAdminLog(guidThreadId, string.Format(LOG_METHOD_NAME_SUCCESS, WEBADMIN_SERVICE_METHOD_USERMANAGEMENT_ADD), Constant.LEVEL_ALL);
                    }
                }

                WebAdminLog(guidThreadId, string.Format(LOG_END_METHOD_NAME, WEBADMIN_SERVICE_METHOD_USERMANAGEMENT_ADD), Constant.LEVEL_ALL);
            }
            return GetResponseMessage(oServiceResponse);
        }

        /// <summary>
        /// Inquery Users Admin
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("admin/inquery_user_admin")]
        public HttpResponseMessage WebAdminInqueryUser([FromBody]UsersManagementModel oUsersManagementModel)
        {
            string guidThreadId = Utility.GetGuidThreadId(Guid.NewGuid().ToString());
            ServiceResponse oServiceResponse = new ServiceResponse();
            ErrorInfo oErrorInfo = new ErrorInfo();

            try
            {
                if (oUsersManagementModel == null) LogErrorNullParameter("User Management info", WEBADMIN_SERVICE_METHOD_USERMANAGEMENT_INQUERY);
                if (oUsersManagementModel.BCAUserModel == null) LogErrorNullParameter("User info", WEBADMIN_SERVICE_METHOD_USERMANAGEMENT_INQUERY);

                WebAdminLog(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, WEBADMIN_SERVICE_METHOD_USERMANAGEMENT_INQUERY), Constant.LEVEL_ALL);

                List<WebAdminUsers> Users = new List<WebAdminUsers>();
                int TotalRows = 0;
                oErrorInfo = fingerLogic.WebAdminInqueryUser(oUsersManagementModel, Users, ref TotalRows, Enumeration.EnumOfWebAdminMenu.UserManagement
                    , Enumeration.EnumOfWebAdminRole.SuperAdmin, Enumeration.EnumOfWebAdminRole.Admin);
                oServiceResponse.statusCode = oErrorInfo.Code;
                oServiceResponse.statusMessage = oErrorInfo.Message;
                oServiceResponse.responseData = JsonConvert.SerializeObject(new
                {
                    TotalRows,
                    Users
                });
            }
            catch (FingerServiceException ex)
            {
                oServiceResponse = ex.ServiceResponse;
            }
            catch (System.Exception ex)
            {
                oErrorInfo.Code = Constant.ERROR_CODE_SYSTEM;
                oServiceResponse.statusCode = Constant.ERROR_CODE_SYSTEM;
                oServiceResponse.statusMessage = ex.Message;

            }
            finally
            {

                if (oErrorInfo != null)
                {
                    if (oErrorInfo.Code != Constant.NO_ERROR)
                    {
                        WebAdminLog(guidThreadId, string.Format(LOG_METHOD_NAME_FAILED, WEBADMIN_SERVICE_METHOD_USERMANAGEMENT_INQUERY, oErrorInfo.Code, oErrorInfo.Message), Constant.LEVEL_ERROR);
                    }
                    else
                    {
                        WebAdminLog(guidThreadId, string.Format(LOG_METHOD_NAME_SUCCESS, WEBADMIN_SERVICE_METHOD_USERMANAGEMENT_INQUERY), Constant.LEVEL_ALL);
                    }
                }

                WebAdminLog(guidThreadId, string.Format(LOG_END_METHOD_NAME, WEBADMIN_SERVICE_METHOD_USERMANAGEMENT_INQUERY), Constant.LEVEL_ALL);
            }
            return GetResponseMessage(oServiceResponse);
        }

        /// <summary>
        /// Delete Users Admin
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("admin/delete_user_admin")]
        public HttpResponseMessage WebAdminDeleteUser([FromBody]BCAUserModel oBCAUserModel)
        {
            string guidThreadId = Utility.GetGuidThreadId(Guid.NewGuid().ToString());
            ServiceResponse oServiceResponse = new ServiceResponse();
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                if (oBCAUserModel == null) LogErrorNullParameter("User info", WEBADMIN_SERVICE_METHOD_USERMANAGEMENT_DELETE);

                WebAdminLog(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, WEBADMIN_SERVICE_METHOD_USERMANAGEMENT_DELETE), Constant.LEVEL_ALL);
                oErrorInfo = fingerLogic.WebAdminDeleteUser(oBCAUserModel, Enumeration.EnumOfWebAdminMenu.UserManagement
                    , Enumeration.EnumOfWebAdminRole.SuperAdmin, Enumeration.EnumOfWebAdminRole.Admin);
                oServiceResponse.statusCode = oErrorInfo.Code;
                oServiceResponse.statusMessage = oErrorInfo.Message;
            }
            catch (FingerServiceException ex)
            {
                oServiceResponse = ex.ServiceResponse;
            }
            catch (System.Exception ex)
            {
                oErrorInfo.Code = Constant.ERROR_CODE_SYSTEM;
                oServiceResponse.statusCode = Constant.ERROR_CODE_SYSTEM;
                oServiceResponse.statusMessage = ex.Message;

            }
            finally
            {
                if (oErrorInfo != null)
                {
                    if (oErrorInfo.Code != Constant.NO_ERROR)
                    {
                        WebAdminLog(guidThreadId, string.Format(LOG_METHOD_NAME_FAILED, WEBADMIN_SERVICE_METHOD_USERMANAGEMENT_DELETE, oErrorInfo.Code, oErrorInfo.Message), Constant.LEVEL_ERROR);
                    }
                    else
                    {
                        WebAdminLog(guidThreadId, string.Format(LOG_METHOD_NAME_SUCCESS, WEBADMIN_SERVICE_METHOD_USERMANAGEMENT_DELETE), Constant.LEVEL_ALL);
                    }
                }

                WebAdminLog(guidThreadId, string.Format(LOG_END_METHOD_NAME, WEBADMIN_SERVICE_METHOD_USERMANAGEMENT_DELETE), Constant.LEVEL_ALL);
            }
            return GetResponseMessage(oServiceResponse);
        }

        /// <summary>
        /// Get Role
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("admin/get_roles")]
        public HttpResponseMessage WebAdminGetRole([FromBody]BCAUserModel oBCAUserModel)
        {
            string guidThreadId = Utility.GetGuidThreadId(Guid.NewGuid().ToString());
            ServiceResponse oServiceResponse = new ServiceResponse();
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                if (oBCAUserModel == null) LogErrorNullParameter("User info", WEBADMIN_SERVICE_METHOD_USERMANAGEMENT_GET_ROLES);

                WebAdminLog(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, WEBADMIN_SERVICE_METHOD_USERMANAGEMENT_GET_ROLES), Constant.LEVEL_ALL);

                List<FsRoles> Roles = new List<FsRoles>();
                oErrorInfo = fingerLogic.WebAdminGetRoles(oBCAUserModel, Roles, Enumeration.EnumOfWebAdminMenu.UserManagement
                    , Enumeration.EnumOfWebAdminRole.SuperAdmin, Enumeration.EnumOfWebAdminRole.Admin);
                oServiceResponse.statusCode = oErrorInfo.Code;
                oServiceResponse.statusMessage = oErrorInfo.Message;
                oServiceResponse.responseData = JsonConvert.SerializeObject(new
                {
                    Roles
                });
            }
            catch (FingerServiceException ex)
            {
                oServiceResponse = ex.ServiceResponse;
            }
            catch (System.Exception ex)
            {
                oErrorInfo.Code = Constant.ERROR_CODE_SYSTEM;
                oServiceResponse.statusCode = Constant.ERROR_CODE_SYSTEM;
                oServiceResponse.statusMessage = ex.Message;

            }
            finally
            {
                if (oErrorInfo != null)
                {
                    if (oErrorInfo.Code != Constant.NO_ERROR)
                    {
                        WebAdminLog(guidThreadId, string.Format(LOG_METHOD_NAME_FAILED, WEBADMIN_SERVICE_METHOD_USERMANAGEMENT_GET_ROLES, oErrorInfo.Code, oErrorInfo.Message), Constant.LEVEL_ERROR);
                    }
                    else
                    {
                        WebAdminLog(guidThreadId, string.Format(LOG_METHOD_NAME_SUCCESS, WEBADMIN_SERVICE_METHOD_USERMANAGEMENT_GET_ROLES), Constant.LEVEL_ALL);
                    }
                }
                WebAdminLog(guidThreadId, string.Format(LOG_END_METHOD_NAME, WEBADMIN_SERVICE_METHOD_USERMANAGEMENT_GET_ROLES), Constant.LEVEL_ALL);
            }
            return GetResponseMessage(oServiceResponse);
        }
        #endregion

        #endregion

        #region Private Method(s)
        private string GetActivitySuccessMessageForDeleteUser(UserInquery user)
        {
            StringBuilder activityMessage = new StringBuilder();
            activityMessage.Append("Delete User (");
            activityMessage.AppendFormat("Branch code: {0};Office code: {1};User status: {2};", user.BranchCode, user.OfficeCode, user.UserStatus);

            if (user.UserStatus.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfUserStatus.USER_STATUS_TETAP).ToLower()))
            {
                activityMessage.AppendFormat("Shift: {0};", user.Shift);
            }
            activityMessage.AppendFormat("User ID: {0}; Name:{1};User group: {2};", user.UserId, user.Name, user.UserGroup);
            activityMessage.AppendFormat("User category: {0};", user.UserCategory);
            if (user.CategoryId != 0)
                activityMessage.AppendFormat("Start periode: {0};End periode: {1};", user.StartPeriodString, user.EndPeriodString);

            activityMessage.AppendFormat("User Type: {0};", user.UserType);
            if (user.Quality != 0)
                activityMessage.AppendFormat("Quality: {0};Approved By: {1};Reason: {2};", user.Quality, user.ApprovedBy, user.Reason);

            activityMessage.AppendFormat("BDS1: {0};BDS2: {1};BDS3: {2};BDS4: {3};BDS5: {4}; ", user.BDS1, user.BDS2, user.BDS3, user.BDS4, user.BDS5);
            activityMessage.AppendFormat("BDS6: {0};BDS7: {1};BDS8: {2};BDS9: {3};BDS10: {4})", user.BDS6, user.BDS7, user.BDS8, user.BDS9, user.BDS10);
            return activityMessage.ToString();
        }
        private string GetActivitySuccessMessageForMutationUser(UserMutation user, string actionStatus)
        {
            if (string.IsNullOrEmpty(user.UserStatus) || string.IsNullOrEmpty(user.UserGroup))
            {
                UserInfoModel userInfo = fingerLogic.GetUserInfoByUserId(user.UserId);
                if (userInfo != null)
                {
                    user.UserStatus = fingerLogic.GetUserStatus(userInfo.FingerBDSID.UserType);
                    user.UserGroup = fingerLogic.GetUserGroup(userInfo.FingerMain.GroupId);
                }
            }
            StringBuilder activityMessage = new StringBuilder();
            activityMessage.Append("Mutation User (");
            activityMessage.AppendFormat("User ID:{0}; Name:{1}; User Status:{2}; User Group:{3}; ", user.UserId, user.UserName, user.UserStatus, user.UserGroup);
            activityMessage.AppendFormat("Current Branch Code:{0}; Current Office Code:{1}; ", user.BranchSourceCode, user.OfficeSourceCode);
            activityMessage.AppendFormat("Destination Branch Code:{0}; Destination Office Code:{1}; ", user.DestBranchCode, user.DestOfficeCode);
            activityMessage.AppendFormat("Effective Date:{0}; Status:{1})", user.EffectiveDateString, actionStatus);
            return activityMessage.ToString();
        }
        private bool ValidatePagingInfo(string guidThreadId, PagingInfo oPagingInfo, ClientInfo oClientInfo, ref ServiceResponse oServiceResponse)
        {
            bool isValid = false;
            string logMessage = null;

            try
            {
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_PAGING_INFO), Constant.LEVEL_DEBUG, oClientInfo);
                ValidationContext context = new ValidationContext(new ValidationPagingInfo(oPagingInfo));
                if (!context.Execute())
                {
                    logMessage = string.Format(LOG_METHOD_NAME_FAILED, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_PAGING_INFO, context.GetErrorCode(), context.GetErrorMessage());
                    SetResponse(context.GetErrorCode(), context.GetErrorMessage(), ref oServiceResponse);
                    return isValid;
                }
                SetResponse(Constant.NO_ERROR, string.Empty, ref oServiceResponse);
                logMessage = string.Format(LOG_METHOD_NAME_SUCCESS, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_PAGING_INFO);
                isValid = true;
            }
            catch (System.Exception ex)
            {
                logMessage = string.Format(LOG_METHOD_NAME_FAILED, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_PAGING_INFO, Constant.ERROR_CODE_SYSTEM, ex.Message);
                throw ex;
            }
            finally
            {
                Log(guidThreadId, logMessage, Constant.LEVEL_DEBUG, oClientInfo);
                Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_PAGING_INFO), Constant.LEVEL_DEBUG, oClientInfo);
            }
            return isValid;
        }

        private bool ValidateUserAccessMenu(string guidThreadId, ClientInfo oClientInfo, Enumeration.EnumOfFSMGMTMenu FSMGMTMenu, ref ServiceResponse oServiceResponse)
        {
            bool isValid = false;
            string logMessage = null;

            try
            {
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_USER_ACCESS_MENU), Constant.LEVEL_DEBUG, oClientInfo);
                ValidationContext context = new ValidationContext(new ValidationUserAccessMenu(oClientInfo, FSMGMTMenu));
                if (!context.Execute())
                {
                    logMessage = string.Format(LOG_METHOD_NAME_FAILED, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_USER_ACCESS_MENU, context.GetErrorCode(), context.GetErrorMessage());
                    SetResponse(context.GetErrorCode(), context.GetErrorMessage(), ref oServiceResponse);
                    return isValid;
                }
                SetResponse(Constant.NO_ERROR, string.Empty, ref oServiceResponse);
                logMessage = string.Format(LOG_METHOD_NAME_SUCCESS, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_USER_ACCESS_MENU);
                isValid = true;
            }
            catch (System.Exception ex)
            {
                logMessage = string.Format(LOG_METHOD_NAME_FAILED, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_USER_ACCESS_MENU, Constant.ERROR_CODE_SYSTEM, ex.Message);
                throw ex;
            }
            finally
            {
                Log(guidThreadId, logMessage, Constant.LEVEL_DEBUG, oClientInfo);
                Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_USER_ACCESS_MENU), Constant.LEVEL_DEBUG, oClientInfo);
            }
            return isValid;
        }
        private bool ValidateUserMutation(string guidThreadId, UserMutation oUserMutation, ClientInfo oClientInfo, Int16 mutationAction, ref ServiceResponse oServiceResponse)
        {
            bool isValid = false;
            string logMessage = null;

            try
            {
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_METHOD_REGISTER_USER_MUTATION), Constant.LEVEL_DEBUG, oClientInfo);
                ValidationContext context = new ValidationContext(new ValidationUserMutation(oUserMutation, oClientInfo, mutationAction));
                if (!context.Execute())
                {
                    logMessage = string.Format(LOG_METHOD_NAME_FAILED, FINGER_SERVICE_METHOD_REGISTER_USER_MUTATION, context.GetErrorCode(), context.GetErrorMessage());
                    SetResponse(context.GetErrorCode(), context.GetErrorMessage(), ref oServiceResponse);
                    return isValid;
                }
                SetResponse(Constant.NO_ERROR, string.Empty, ref oServiceResponse);
                logMessage = string.Format(LOG_METHOD_NAME_SUCCESS, FINGER_SERVICE_METHOD_REGISTER_USER_MUTATION);
                isValid = true;
            }
            catch (System.Exception ex)
            {
                logMessage = string.Format(LOG_METHOD_NAME_FAILED, FINGER_SERVICE_METHOD_REGISTER_USER_MUTATION, Constant.ERROR_CODE_SYSTEM, ex.Message);
                throw ex;
            }
            finally
            {
                Log(guidThreadId, logMessage, Constant.LEVEL_DEBUG, oClientInfo);
                Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_REGISTER_USER_MUTATION), Constant.LEVEL_DEBUG, oClientInfo);
            }
            return isValid;
        }

        private List<Columns> ConfigureReportUserColumn(Enumeration.EnumOfExportType exportType)
        {
            List<Columns> columns = new List<Columns>();
            string[] colBranchCode = null;
            string[] colOfficeCode = null;
            string[] colUserStatus = null;
            string[] colShift = null;
            string[] colUserId = null;
            string[] colName = null;
            string[] colUserGroup = null;
            string[] colUserCategory = null;
            string[] colStartPeriod = null;
            string[] colEndPeriod = null;
            string[] colUserType = null;
            string[] colQuality = null;
            string[] colApprovedBy = null;
            string[] colReason = null;
            string[] colBDS1 = null;
            string[] colBDS2 = null;
            string[] colBDS3 = null;
            string[] colBDS4 = null;
            string[] colBDS5 = null;
            string[] colBDS6 = null;
            string[] colBDS7 = null;
            string[] colBDS8 = null;
            string[] colBDS9 = null;
            string[] colBDS10 = null;
            if (exportType == Enumeration.EnumOfExportType.CSV_File)
            {
                colBranchCode = new string[1];
                colBranchCode[0] = "Branch Code";

                colOfficeCode = new string[1];
                colOfficeCode[0] = "Office Code";

                colUserStatus = new string[1];
                colUserStatus[0] = "User Status";

                colShift = new string[1];
                colShift[0] = "Shift";

                colUserId = new string[1];
                colUserId[0] = "NIP/NIK/VendorNIP";

                colName = new string[1];
                colName[0] = "Name";

                colUserGroup = new string[1];
                colUserGroup[0] = "User Group";

                colUserCategory = new string[1];
                colUserCategory[0] = "User Category";

                colStartPeriod = new string[1];
                colStartPeriod[0] = "Start Period";

                colEndPeriod = new string[1];
                colEndPeriod[0] = "End Period";

                colUserType = new string[1];
                colUserType[0] = "User Type";

                colQuality = new string[1];
                colQuality[0] = "Quality";

                colApprovedBy = new string[1];
                colApprovedBy[0] = "Approved By";

                colReason = new string[1];
                colReason[0] = "Reason";

                colBDS1 = new string[1];
                colBDS1[0] = "BDS ID 1";

                colBDS2 = new string[1];
                colBDS2[0] = "BDS ID 2";

                colBDS3 = new string[1];
                colBDS3[0] = "BDS ID 3";

                colBDS4 = new string[1];
                colBDS4[0] = "BDS ID 4";

                colBDS5 = new string[1];
                colBDS5[0] = "BDS ID 5";

                colBDS6 = new string[1];
                colBDS6[0] = "BDS ID 6";

                colBDS7 = new string[1];
                colBDS7[0] = "BDS ID 7";

                colBDS8 = new string[1];
                colBDS8[0] = "BDS ID 8";

                colBDS9 = new string[1];
                colBDS9[0] = "BDS ID 9";

                colBDS10 = new string[1];
                colBDS10[0] = "BDS ID 10";

            }
            else if (exportType == Enumeration.EnumOfExportType.PDF_File)
            {

                colBranchCode = new string[3];
                colBranchCode[0] = "Branch";
                colBranchCode[1] = "Code";
                colBranchCode[2] = "=";

                colOfficeCode = new string[3];
                colOfficeCode[0] = "Office";
                colOfficeCode[1] = "Code";
                colOfficeCode[2] = "=";

                colUserStatus = new string[3];
                colUserStatus[0] = "User";
                colUserStatus[1] = "Status";
                colUserStatus[2] = "=";

                colShift = new string[3];
                colShift[0] = "Shift";
                colShift[1] = "";
                colShift[2] = "=";

                colUserId = new string[3];
                colUserId[0] = "NIP/NIK/";
                colUserId[1] = "Vendor NIP";
                colUserId[2] = "=";

                colName = new string[3];
                colName[0] = "Name";
                colName[1] = "";
                colName[2] = "=";

                colUserGroup = new string[3];
                colUserGroup[0] = "User";
                colUserGroup[1] = "Group";
                colUserGroup[2] = "=";

                colBDS1 = new string[3];
                colBDS1[0] = "BDS ID 1";
                colBDS1[1] = "";
                colBDS1[2] = "=";

                colBDS2 = new string[3];
                colBDS2[0] = "BDS ID 2";
                colBDS2[1] = "";
                colBDS2[2] = "=";

                colBDS3 = new string[3];
                colBDS3[0] = "BDS ID 3";
                colBDS3[1] = "";
                colBDS3[2] = "=";

                colBDS4 = new string[3];
                colBDS4[0] = "BDS ID 4";
                colBDS4[1] = "";
                colBDS4[2] = "=";

                colBDS5 = new string[3];
                colBDS5[0] = "BDS ID 5";
                colBDS5[1] = "";
                colBDS5[2] = "=";

                colBDS6 = new string[3];
                colBDS6[0] = "BDS ID 6";
                colBDS6[1] = "";
                colBDS6[2] = "=";

                colBDS7 = new string[3];
                colBDS7[0] = "BDS ID 7";
                colBDS7[1] = "";
                colBDS7[2] = "=";

                colBDS8 = new string[3];
                colBDS8[0] = "BDS ID 8";
                colBDS8[1] = "";
                colBDS8[2] = "=";

                colBDS9 = new string[3];
                colBDS9[0] = "BDS ID 9";
                colBDS9[1] = "";
                colBDS9[2] = "=";

                colBDS10 = new string[3];
                colBDS10[0] = "BDS ID 10";
                colBDS10[1] = "";
                colBDS10[2] = "=";


            }


            columns.Add(new Columns() { ColumnName = colBranchCode, ColumnWidth = 6 });
            columns.Add(new Columns() { ColumnName = colOfficeCode, ColumnWidth = 6 });
            columns.Add(new Columns() { ColumnName = colUserStatus, ColumnWidth = 9 });
            columns.Add(new Columns() { ColumnName = colShift, ColumnWidth = 5 });
            columns.Add(new Columns() { ColumnName = colUserId, ColumnWidth = 16 });
            columns.Add(new Columns() { ColumnName = colName, ColumnWidth = 42 });
            columns.Add(new Columns() { ColumnName = colUserGroup, ColumnWidth = 8 });


            if (exportType == Enumeration.EnumOfExportType.CSV_File)
            {
                columns.Add(new Columns() { ColumnName = colUserCategory });
                columns.Add(new Columns() { ColumnName = colStartPeriod });
                columns.Add(new Columns() { ColumnName = colEndPeriod });
                columns.Add(new Columns() { ColumnName = colUserType });
                columns.Add(new Columns() { ColumnName = colQuality });
                columns.Add(new Columns() { ColumnName = colApprovedBy });
                columns.Add(new Columns() { ColumnName = colReason });
            }

            columns.Add(new Columns() { ColumnName = colBDS1, ColumnWidth = 8 });
            columns.Add(new Columns() { ColumnName = colBDS2, ColumnWidth = 8 });
            columns.Add(new Columns() { ColumnName = colBDS3, ColumnWidth = 8 });
            columns.Add(new Columns() { ColumnName = colBDS4, ColumnWidth = 8 });
            columns.Add(new Columns() { ColumnName = colBDS5, ColumnWidth = 8 });
            columns.Add(new Columns() { ColumnName = colBDS6, ColumnWidth = 8 });
            columns.Add(new Columns() { ColumnName = colBDS7, ColumnWidth = 8 });
            columns.Add(new Columns() { ColumnName = colBDS8, ColumnWidth = 8 });
            columns.Add(new Columns() { ColumnName = colBDS9, ColumnWidth = 8 });
            columns.Add(new Columns() { ColumnName = colBDS10, ColumnWidth = 8 });


            return columns;
        }

        private List<Columns> ConfigureReportMutationColumn(Enumeration.EnumOfExportType exportType)
        {
            List<Columns> columns = new List<Columns>();
            string[] colMutation = null;
            string[] colUserId = null;
            string[] colName = null;
            string[] colCurrentBranchCode = null;
            string[] colCurrentOfficeCode = null;
            string[] colDestinationBranchCode = null;
            string[] colDestinationOfficeCode = null;
            string[] colEffectiveDate = null;
            string[] colStatus = null;
            if (exportType == Enumeration.EnumOfExportType.CSV_File)
            {
                colMutation = new string[1];
                colMutation[0] = "Mutation";

                colUserId = new string[1];
                colUserId[0] = "NIP/NIK/VendorNIP";

                colName = new string[1];
                colName[0] = "Name";

                colCurrentBranchCode = new string[1];
                colCurrentBranchCode[0] = "Current Branch Code";

                colCurrentOfficeCode = new string[1];
                colCurrentOfficeCode[0] = "Current Office Code";

                colDestinationBranchCode = new string[1];
                colDestinationBranchCode[0] = "Destination BranchCode";

                colDestinationOfficeCode = new string[1];
                colDestinationOfficeCode[0] = "Destination Office Code";

                colEffectiveDate = new string[1];
                colEffectiveDate[0] = "Effective Date";

                colStatus = new string[1];
                colStatus[0] = "Status";

            }
            else if (exportType == Enumeration.EnumOfExportType.PDF_File)
            {

                colMutation = new string[3];
                colMutation[0] = "Mutation";
                colMutation[1] = "";
                colMutation[2] = "=";

                colUserId = new string[3];
                colUserId[0] = "NIP/NIK/";
                colUserId[1] = "Vendor NIP";
                colUserId[2] = "=";

                colName = new string[3];
                colName[0] = "Name";
                colName[1] = "";
                colName[2] = "=";

                colCurrentBranchCode = new string[3];
                colCurrentBranchCode[0] = "Current";
                colCurrentBranchCode[1] = "Branch Code";
                colCurrentBranchCode[2] = "=";

                colCurrentOfficeCode = new string[3];
                colCurrentOfficeCode[0] = "Current";
                colCurrentOfficeCode[1] = "Office Code";
                colCurrentOfficeCode[2] = "=";

                colDestinationBranchCode = new string[3];
                colDestinationBranchCode[0] = "Destination";
                colDestinationBranchCode[1] = "Branch Code";
                colDestinationBranchCode[2] = "=";

                colDestinationOfficeCode = new string[3];
                colDestinationOfficeCode[0] = "Destination";
                colDestinationOfficeCode[1] = "Office Code";
                colDestinationOfficeCode[2] = "=";

                colEffectiveDate = new string[3];
                colEffectiveDate[0] = "Effective";
                colEffectiveDate[1] = "Date";
                colEffectiveDate[2] = "=";

                colStatus = new string[3];
                colStatus[0] = "Status";
                colStatus[1] = "";
                colStatus[2] = "=";
            }

            columns.Add(new Columns() { ColumnName = colMutation, ColumnWidth = 8 });
            columns.Add(new Columns() { ColumnName = colUserId, ColumnWidth = 13 });
            columns.Add(new Columns() { ColumnName = colName, ColumnWidth = 40 });
            columns.Add(new Columns() { ColumnName = colCurrentBranchCode, ColumnWidth = 13 });
            columns.Add(new Columns() { ColumnName = colCurrentOfficeCode, ColumnWidth = 13 });
            columns.Add(new Columns() { ColumnName = colDestinationBranchCode, ColumnWidth = 13 });
            columns.Add(new Columns() { ColumnName = colDestinationOfficeCode, ColumnWidth = 13 });
            columns.Add(new Columns() { ColumnName = colEffectiveDate, ColumnWidth = 15 });
            columns.Add(new Columns() { ColumnName = colStatus, ColumnWidth = 16 });

            return columns;
        }
        #endregion
    }
}
