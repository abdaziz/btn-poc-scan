﻿
using Finger.Core.Entity;
using Finger.Core.Models;
using Finger.Entity;
using Finger.Exception;
using Finger.Framework.Common;
using Finger.Framework.Entity;
using Finger.Logic;
using Finger.Models;
using Finger.Validation;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Threading;

namespace Finger.Controllers
{


    public partial class FingerController : ApiController
    {
        #region public method(s)

        #region finger web service
        /// <summary>
        /// Connection service
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = false)]
        [Route("finger/connect")] //route alias
        public HttpResponseMessage Connect([FromBody]ClientInfo clientInfo)
        {
            string guidThreadId = string.Empty;
            ServiceResponse response = new ServiceResponse();
            try
            {
                if (clientInfo == null) return LogsErrorNullParameter("Client info", response);
                ValidationContext context = new ValidationContext(new ValidationClientInfo(clientInfo));
                if (!context.Execute())
                {
                    Logs(string.Format("Connect validation failed : {0}", context.GetErrorMessage()), guidThreadId, string.Empty, string.Empty, Constant.LEVEL_ERROR);
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }
                guidThreadId = Utility.GetGuidThreadId(clientInfo.NewGuid);
                Logs("Starting connect to central server", guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ALL);

                response = new FingerLogic().CheckConnection();
                if (response.statusCode != Constant.NO_ERROR)
                {
                    Logs(response.statusMessage, guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ERROR);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, response);
                }
                Logs(response.statusMessage, guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ALL);
                //new FingerLogic().SaveActivity("Check Connection", response.statusMessage, guidThreadId, clientInfo);
            }
            catch (System.Exception ex)
            {
                return CreateResponseException(string.Format("Connect error exception : {0}", ex.Message), clientInfo, guidThreadId, response);
            }
            finally
            {
                if (clientInfo == null)
                {
                    Logs("End connect to central server", string.Empty, string.Empty, string.Empty, Constant.LEVEL_ALL);
                }
                else
                {
                    Logs("End connect to central server", guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ALL);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        /// <summary>
        /// Gets Configuration Items From Central Database
        /// </summary>
        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/getconfigitems")] //route alias   
        public HttpResponseMessage GetConfigItems([FromBody]ClientInfo clientInfo)
        {
            FingerConfigItems configItems = new FingerConfigItems();
            string guidThreadId = string.Empty;
            ServiceResponse response = new ServiceResponse();
            try
            {
                if (clientInfo == null) return LogsErrorNullParameter("Client info", response);
                ValidationContext context = new ValidationContext(new ValidationClientInfo(clientInfo));
                if (!context.Execute())
                {
                    Logs(string.Format("Get config items validation failed : {0}", context.GetErrorMessage()), guidThreadId, string.Empty, string.Empty, Constant.LEVEL_ERROR);
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }
                guidThreadId = Utility.GetGuidThreadId(clientInfo.NewGuid);
                Logs("Starting get config items", guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ALL);

                response = new FingerLogic().GetConfigurationItems(guidThreadId, clientInfo);
                if (response.statusCode != Constant.NO_ERROR)
                {
                    Logs(response.statusMessage, guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ERROR);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, response);
                }
                Logs(response.statusMessage, guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ALL);
            }
            catch (System.Exception ex)
            {
                return CreateResponseException(string.Format("Get config items error exception : {0}", ex.Message), clientInfo, guidThreadId, response);
            }
            finally
            {
                if (clientInfo == null)
                {
                    Logs("End get config items", string.Empty, string.Empty, string.Empty, Constant.LEVEL_ALL);
                }
                else
                {
                    Logs("End Get config items", guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ALL);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        /// <summary>
        /// Validation User Login
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/validateuserlogin")] //route alias
        public HttpResponseMessage ValidateUserLogin([FromBody]ClientInfo clientInfo)
        {
            string guidThreadId = string.Empty;
            ServiceResponse response = new ServiceResponse();
            try
            {
                if (clientInfo == null) return LogsErrorNullParameter("Client info", response);
                ValidationContext context = new ValidationContext(new ValidationClientInfo(clientInfo));
                if (!context.Execute())
                {
                    Logs(string.Format("Validate user login validation failed : {0}", context.GetErrorMessage()), guidThreadId, string.Empty, string.Empty, Constant.LEVEL_ERROR);
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }
                guidThreadId = Utility.GetGuidThreadId(clientInfo.NewGuid);
                Logs("Starting validate user login", guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ALL);

                response = new FingerLogic().ValidateUserLogin(guidThreadId, clientInfo);
                if (response.statusCode != Constant.NO_ERROR)
                {
                    if (response.statusCode == 100) //Super Admin
                    {
                        Logs(response.statusMessage, guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_DEBUG);
                        return Request.CreateResponse(HttpStatusCode.OK, response);
                    }

                    Logs(response.statusMessage, guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ERROR);
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }
                Logs(response.statusMessage, guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ALL);
            }
            catch (System.Exception ex)
            {
                return CreateResponseException(string.Format("Validate user login error exception : {0}", ex.Message), clientInfo, guidThreadId, response);
            }
            finally
            {
                if (clientInfo == null)
                {
                    Logs("End validate user login", string.Empty, string.Empty, string.Empty, Constant.LEVEL_ALL);
                }
                else
                {
                    Logs("End validate user login", guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ALL);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, response);

        }

        /// <summary>
        /// Verification User Login
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/verifyuserlogin")] //route alias
        public HttpResponseMessage VerifyUserLogin([FromBody]VerifyLoginModel model)
        {
            string guidThreadId = string.Empty;
            ServiceResponse response = new ServiceResponse();
            try
            {
                if (model.ClientInfo == null) return LogsErrorNullParameter("Client info", response);
                ValidationContext clientContext = new ValidationContext(new ValidationVerifyLogin(model));
                if (!clientContext.Execute())
                {
                    Logs(string.Format("Verify user login validation failed : {0}", clientContext.GetErrorMessage()), guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ERROR);
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = clientContext.GetErrorMessage();
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }

                guidThreadId = Utility.GetGuidThreadId(model.ClientInfo.NewGuid);
                Logs("Starting verify user login", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ALL);

                response = new FingerLogic().VerifyLogin(model.ClientInfo, model.VerifyInfo, guidThreadId, model.UserLogTime);
                if (response.statusCode != Constant.NO_ERROR)
                {
                    Logs(response.statusMessage, guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ERROR);
                    if (response.statusCode != Constant.CANNOT_OPEN_DB)
                    {
                        new FingerLogic().SaveActivity("Login", response.statusMessage, guidThreadId, model.ClientInfo);
                    }
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }

                //modified by joe 8th April 2021 (Added check office code on FsBranchVersion table)
                var pErrorInfo = new FingerLogic().CheckExistBranchCode(model.ClientInfo.BranchCode, model.ClientInfo.OfficeCode, model.ClientInfo.LogonID);
                if (pErrorInfo.Code != Constant.NO_ERROR)
                {
                    Logs(string.Format("Login canceled, check brach code failed: {0}", response.statusMessage), guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ALL);
                    new FingerLogic().SaveActivity("Login", string.Format("Login (User ID : {0}) failed", model.ClientInfo.LogonID), guidThreadId, model.ClientInfo);
                    
                    response.statusCode = pErrorInfo.Code;
                    response.statusMessage = pErrorInfo.Message;
                }
                else
                {
                    Logs("Check existing branch code succeded", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ALL);
                    Logs(response.statusMessage, guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ALL);
                    new FingerLogic().SaveActivity("Login", string.Format("Login (User ID : {0})", model.ClientInfo.LogonID), guidThreadId, model.ClientInfo);
                }

                //original source code
                //Logs(response.statusMessage, guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ALL);
                //new FingerLogic().SaveActivity("Login", string.Format("Login (User ID : {0})", model.ClientInfo.LogonID), guidThreadId, model.ClientInfo);
            }
            catch (System.Exception ex)
            {
                return CreateResponseException(string.Format("Verify user login error exception : {0}", ex.Message), model.ClientInfo, guidThreadId, response);
            }
            finally
            {
                if (model.ClientInfo == null)
                {
                    Logs("End verify user login", string.Empty, string.Empty, string.Empty, Constant.LEVEL_ALL);
                }
                else
                {
                    Logs("End verify user login", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ALL);
                }

            }
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        /// <summary>
        /// Verification Finger
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/verify")] //route alias
        public HttpResponseMessage VerifyFinger([FromBody]JObject data)
        {
            string guidThreadId = string.Empty;
            ServiceResponse response = new ServiceResponse();
            ClientInfo clientInfo = new ClientInfo();
            try
            {
                clientInfo = data["ClientInfo"].ToObject<ClientInfo>();
                VerifyInfo verifyInfo = data["VerifyInfo"].ToObject<VerifyInfo>();

                if (clientInfo == null) return LogsErrorNullParameter("Client info", response);
                ValidationContext clientContext = new ValidationContext(new ValidationClientInfo(clientInfo));
                if (!clientContext.Execute())
                {
                    Logs(string.Format("Verify finger validation failed : {0}", clientContext.GetErrorMessage()), guidThreadId, string.Empty, string.Empty, Constant.LEVEL_ERROR);
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = clientContext.GetErrorMessage();
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }

                clientContext = new ValidationContext(new ValidationUserAccessMenu(clientInfo, Enumeration.EnumOfFSMGMTMenu.Verification));
                if (!clientContext.Execute())
                {
                    Logs(string.Format("Verify finger validation failed : {0}", clientContext.GetErrorMessage()), guidThreadId, string.Empty, string.Empty, Constant.LEVEL_ERROR);
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = clientContext.GetErrorMessage();
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }


                ValidationContext verifyContext = new ValidationContext(new ValidationVerifyInfo(verifyInfo));
                if (!verifyContext.Execute())
                {
                    Logs(string.Format("Verify finger validation failed : {0}", clientContext.GetErrorMessage()), guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ERROR);
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = verifyContext.GetErrorMessage();
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }

                guidThreadId = Utility.GetGuidThreadId(clientInfo.NewGuid);
                Logs("Starting verify finger", guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ALL);

                response = new FingerLogic().Verify(clientInfo, verifyInfo, guidThreadId);
                if (response.statusCode != Constant.NO_ERROR)
                {
                    Logs(response.statusMessage, guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ERROR);
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }
                Logs(response.statusMessage, guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ALL);
            }
            catch (System.Exception ex)
            {
                return CreateResponseException(string.Format("Verify finger error exception : {0}", ex.Message), clientInfo, guidThreadId, response);
            }
            finally
            {
                if (clientInfo == null)
                {
                    Logs("End verify finger", string.Empty, string.Empty, string.Empty, Constant.LEVEL_ALL);
                }
                else
                {
                    Logs("End verify finger", guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ALL);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        /// <summary>
        /// Logout FSMGMT
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/logout")] //route alias
        public HttpResponseMessage DeleteUserLog([FromBody]JObject data)
        {
            string guidThreadId = string.Empty;
            ServiceResponse response = new ServiceResponse();
            ClientInfo clientInfo = new ClientInfo();
            try
            {
                clientInfo = data["ClientInfo"].ToObject<ClientInfo>();
                UserLogInfo userLogInfo = data["UserLogInfo"].ToObject<UserLogInfo>();

                if (clientInfo == null) return LogsErrorNullParameter("Client info", response);
                ValidationContext clientContext = new ValidationContext(new ValidationClientInfo(clientInfo));
                if (!clientContext.Execute())
                {
                    Logs(string.Format("Delete user log validation failed : {0}", clientContext.GetErrorMessage()), guidThreadId, string.Empty, string.Empty, Constant.LEVEL_ERROR);
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = clientContext.GetErrorMessage();
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }

                ValidationContext userLogInfoContext = new ValidationContext(new ValidationUserLogInfo(userLogInfo));
                if (!userLogInfoContext.Execute())
                {
                    Logs(string.Format("Delete user log validation failed : {0}", clientContext.GetErrorMessage()), guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ERROR);
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = userLogInfoContext.GetErrorMessage();
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }

                guidThreadId = Utility.GetGuidThreadId(clientInfo.NewGuid);
                Logs("Starting delete user log", guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ALL);

                response = new FingerLogic().DeleteUserLogData(clientInfo, userLogInfo);
                if (response.statusCode != Constant.NO_ERROR)
                {
                    Logs(response.statusMessage, guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ERROR);
                    if (response.statusCode != Constant.CANNOT_OPEN_DB)
                    {
                        new FingerLogic().SaveActivity("Logout", response.statusMessage, guidThreadId, clientInfo);
                    }
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }
                Logs(response.statusMessage, guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ALL);
                new FingerLogic().SaveActivity("Logout", string.Format("Logout (User ID: {0})", clientInfo.LogonID), guidThreadId, clientInfo);
            }
            catch (System.Exception ex)
            {
                return CreateResponseException(string.Format("Delete user log error exception : {0}", ex.Message), clientInfo, guidThreadId, response);
            }
            finally
            {
                if (clientInfo == null)
                {
                    Logs("End delete user log", string.Empty, string.Empty, string.Empty, Constant.LEVEL_ALL);
                }
                else
                {
                    Logs("End delete user log", guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ALL);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        /// <summary>
        /// Gets Quality Special User
        /// </summary>
        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/getqualityspecialuser")] //route alias
        public HttpResponseMessage GetQualitySpecialUser([FromBody]JObject data)
        {
            string guidThreadId = string.Empty;
            ServiceResponse response = new ServiceResponse();
            ClientInfo clientInfo = null;
            try
            {
                clientInfo = data["ClientInfo"].ToObject<ClientInfo>();
                string userId = data["UserId"].ToString();

                if (clientInfo == null) return LogsErrorNullParameter("Client info", response);
                ValidationContext clientContext = new ValidationContext(new ValidationClientInfo(clientInfo));
                if (!clientContext.Execute())
                {
                    Logs(string.Format("Get quality special user validation failed : {0}", clientContext.GetErrorMessage()), guidThreadId, string.Empty, string.Empty, Constant.LEVEL_ERROR);
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = clientContext.GetErrorMessage();
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }

                if (string.IsNullOrEmpty(userId))
                {
                    Logs("Get quality special user validation failed : UserId is empty", guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ERROR);
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = clientContext.GetErrorMessage();
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }

                guidThreadId = Utility.GetGuidThreadId(clientInfo.NewGuid);
                Logs("Starting get quality special user", guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ALL);

                response = new FingerLogic().GetQualitySpecialUser(userId);
                if (response.statusCode != Constant.NO_ERROR)
                {
                    Logs(response.statusMessage, guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ERROR);
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }
                Logs(response.statusMessage, guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ALL);
            }
            catch (System.Exception ex)
            {
                return CreateResponseException(string.Format("Get quality special user error exception : {0}", ex.Message), clientInfo, guidThreadId, response);
            }
            finally
            {
                if (clientInfo == null)
                {
                    Logs("End get quality special user", string.Empty, string.Empty, string.Empty, Constant.LEVEL_ALL);
                }
                else
                {
                    Logs("End get quality special user", guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ALL);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        /// <summary>
        /// Validation Branch when user check branch in Register User
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/validatebranch")] //route alias
        public HttpResponseMessage ValidateBranchHierarchy([FromBody]BranchHierarchyModel model)
        {
            string guidThreadId = string.Empty;
            ServiceResponse response = new ServiceResponse();
            try
            {
                if (model.ClientInfo == null) return LogsErrorNullParameter("Client info", response);
                ValidationContext context = new ValidationContext(new ValidationBranchHierarchy(model));
                if (!context.Execute())
                {
                    Logs(string.Format("Validate Branch Hierarchy validation failed : {0}", context.GetErrorMessage()), guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ERROR);
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }

                guidThreadId = Utility.GetGuidThreadId(model.ClientInfo.NewGuid);
                Logs("Starting validate branch hierarchy", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ALL);

                response = new FingerLogic().CheckBranchHierarchy(model.ClientInfo.LogonID, model.DestinationBranchCode);
                if (response.statusCode != Constant.NO_ERROR)
                {
                    Logs(response.statusMessage, guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ERROR);
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }
                Logs(response.statusMessage, guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ALL);
            }
            catch (System.Exception ex)
            {
                return CreateResponseException(string.Format("Validate branch hierarchy error exception : {0}", ex.Message), model.ClientInfo, guidThreadId, response);
            }
            finally
            {
                if (model.ClientInfo == null)
                {
                    Logs("End validate branch hierarchy", string.Empty, string.Empty, string.Empty, Constant.LEVEL_ALL);
                }
                else
                {
                    Logs("End validate branch hierarchy", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ALL);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        /// <summary>
        /// Get Inquiry Activity Hub Office Report
        /// </summary>
        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/get_huboffice_activity")]
        public HttpResponseMessage GetInquiryHubOfficeActivity([FromBody]HubOfficeActivityInquiryModel model)
        {
            string guidThreadId = string.Empty;
            ServiceResponse response = new ServiceResponse();
            try
            {
                if (model.ClientInfo == null) return LogsErrorNullParameter("Client info", response);
                ValidationContext context = new ValidationContext(new ValidationHubOfficeActivityInquiry(model));
                if (!context.Execute())
                {
                    Logs(string.Format("Get inquiry hub office activity validation failed : {0}", context.GetErrorMessage()), guidThreadId, string.Empty, string.Empty, Constant.LEVEL_ERROR);
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }

                guidThreadId = Utility.GetGuidThreadId(model.ClientInfo.NewGuid);
                Logs("Starting get inquiry hub office activity", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ALL);

                response = new FingerLogic().GetInquiryHubOfficeActivityByCriteria(guidThreadId, model);
                if (response.statusCode != Constant.NO_ERROR)
                {
                    Logs(response.statusMessage, guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ERROR);
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }
                Logs(response.statusMessage, guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ALL);

            }
            catch (System.Exception ex)
            {
                return CreateResponseException(string.Format("Get inquiry hub office activity error exception : {0}", ex.Message), model.ClientInfo, guidThreadId, response);
            }
            finally
            {
                if (model.ClientInfo == null)
                {
                    Logs("End get inquiry hub office", string.Empty, string.Empty, string.Empty, Constant.LEVEL_ALL);
                }
                else
                {
                    Logs("End get inquiry hub office", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ALL);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        #region web admin
        /// <summary>
        /// Get Inquiry Finger API Configuration
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("admin/get_finger_api_config")]
        public HttpResponseMessage GetInquiryFingerAPIConfig([FromBody]FingerAPIInquiryModel model)
        {
            string guidThreadId = string.Empty;
            ServiceResponse response = new ServiceResponse();
            try
            {
                guidThreadId = Utility.GetGuidThreadId(Guid.NewGuid().ToString());
                ValidationContext context = new ValidationContext(new ValidationPagingInfo(model.PagingInfo));
                if (!context.Execute())
                {
                    WebAdminLog(guidThreadId, string.Format("Get inquiry finger api config validation failed : {0}", context.GetErrorMessage()), Constant.LEVEL_ERROR);
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }

                WebAdminLog(guidThreadId, "Starting get inquiry finger api config", Constant.LEVEL_ALL);
                response = new FingerLogic().GetInquiryFingerAPIConfig(guidThreadId, model);
                if (response.statusCode != Constant.NO_ERROR)
                {
                    WebAdminLog(guidThreadId, response.statusMessage, Constant.LEVEL_ERROR);
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }
                WebAdminLog(guidThreadId, response.statusMessage, Constant.LEVEL_ALL);
            }
            catch (System.Exception ex)
            {
                //return WebAdminCreateResponseException(string.Format("Get finger api config error exception : {0}", ex.Message), guidThreadId, response);
                return WebAdminCreateResponseException(ex.Message, guidThreadId, response);
            }
            finally
            {
                WebAdminLog(guidThreadId, "End get finger api config", Constant.LEVEL_ALL);
            }
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        /// <summary>
        /// Save Finger API Configuration
        /// </summary>
        //[EnableCors("*", "*", "*")]
        //[EnableCors(origins: "*", headers: "*", methods: "post")]
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("admin/save_finger_api_config")]
        public HttpResponseMessage SaveFingerAPIConfig([FromBody]FingerAPIConfigModel model)
        {
            string guidThreadId = string.Empty;
            ServiceResponse response = new ServiceResponse();
            Enumeration.EnumOfMode _mode = Enumeration.EnumOfMode.Add;
            try
            {
                if (model.FsAllowedAppsId > 0)
                {
                    _mode = Enumeration.EnumOfMode.Edit;
                }

                guidThreadId = Utility.GetGuidThreadId(Guid.NewGuid().ToString());
                ValidationContext context = new ValidationContext(new ValidationFingerAPIConfig(model, (short)_mode));
                if (!context.Execute())
                {
                    WebAdminLog(guidThreadId, string.Format("{0} finger api config validation failed : {1}", Enumeration.GetEnumDescription(_mode)
                        , context.GetErrorMessage()), Constant.LEVEL_ERROR);
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }

                WebAdminLog(guidThreadId, string.Format("Starting {0} finger api config", Enumeration.GetEnumDescription(_mode).ToLower()), Constant.LEVEL_ALL);
                response = new FingerLogic().SaveFingerAPIConfig(guidThreadId, model);
                if (response.statusCode != Constant.NO_ERROR)
                {
                    WebAdminLog(guidThreadId, response.statusMessage, Constant.LEVEL_ERROR);
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }
                WebAdminLog(guidThreadId, response.statusMessage, Constant.LEVEL_ALL);
            }
            catch (System.Exception ex)
            {
                //return WebAdminCreateResponseException(string.Format("{0} finger api config error exception : {1}", Enumeration.GetEnumDescription(_mode)
                //    , ex.Message), guidThreadId, response);
                return WebAdminCreateResponseException(ex.Message, guidThreadId, response);
            }
            finally
            {
                WebAdminLog(guidThreadId, string.Format("End {0} finger api config", Enumeration.GetEnumDescription(_mode).ToLower()), Constant.LEVEL_ALL);
            }
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        /// <summary>
        /// Delete Finger API Configuration
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("admin/del_finger_api_config")]
        public HttpResponseMessage DeleteFingerAPIConfig([FromBody]FingerAPIConfigModel model)
        {
            string guidThreadId = string.Empty;
            ServiceResponse response = new ServiceResponse();
            try
            {
                guidThreadId = Utility.GetGuidThreadId(Guid.NewGuid().ToString());
                ValidationContext context = new ValidationContext(new ValidationFingerAPIConfig(model, (short)Enumeration.EnumOfMode.Delete));
                if (!context.Execute())
                {
                    WebAdminLog(guidThreadId, string.Format("Delete finger api config validation failed : {0}", context.GetErrorMessage()), Constant.LEVEL_ERROR);
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }

                WebAdminLog(guidThreadId, "Starting delete finger api config", Constant.LEVEL_ALL);

                response = new FingerLogic().DeleteFingerAPIConfig(guidThreadId, model);
                if (response.statusCode != Constant.NO_ERROR)
                {
                    WebAdminLog(guidThreadId, response.statusMessage, Constant.LEVEL_ERROR);
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }
                WebAdminLog(guidThreadId, response.statusMessage, Constant.LEVEL_ALL);
            }
            catch (System.Exception ex)
            {
                //return WebAdminCreateResponseException(string.Format("Delete finger api config error exception : {0}", ex.Message), guidThreadId, response);
                return WebAdminCreateResponseException(ex.Message, guidThreadId, response);
            }
            finally
            {
                WebAdminLog(guidThreadId, "End delete finger api config", Constant.LEVEL_ALL);
            }
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
        #endregion

        #region For Bioagent
        /// <summary>
        /// Bioagent Verification Finger
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/bio_verify")] //route alias
        public HttpResponseMessage BioVerifyFinger([FromBody]JObject data)
        {
            string guidThreadId = string.Empty;
            ServiceResponse response = new ServiceResponse();
            ClientInfo clientInfo = new ClientInfo();
            try
            {
                clientInfo = data["ClientInfo"].ToObject<ClientInfo>();
                VerifyInfo verifyInfo = data["VerifyInfo"].ToObject<VerifyInfo>();

                if (clientInfo == null) return LogsErrorNullParameter("Client info", response);
                ValidationContext clientContext = new ValidationContext(new ValidationClientInfo(clientInfo));
                if (!clientContext.Execute())
                {
                    Logs(string.Format("Verify finger validation failed : {0}", clientContext.GetErrorMessage()), guidThreadId, string.Empty, string.Empty, Constant.LEVEL_ERROR);
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = clientContext.GetErrorMessage();
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }

                ValidationContext verifyContext = new ValidationContext(new ValidationVerifyInfo(verifyInfo));
                if (!verifyContext.Execute())
                {
                    Logs(string.Format("Verify finger validation failed : {0}", clientContext.GetErrorMessage()), guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ERROR);
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = verifyContext.GetErrorMessage();
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }

                guidThreadId = Utility.GetGuidThreadId(clientInfo.NewGuid);
                Logs("Starting verify finger", guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ALL);

                response = new FingerLogic().Verify(clientInfo, verifyInfo, guidThreadId);
                if (response.statusCode != Constant.NO_ERROR)
                {
                    Logs(response.statusMessage, guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ERROR);
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }
                Logs(response.statusMessage, guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ALL);
            }
            catch (System.Exception ex)
            {
                return CreateResponseException(string.Format("Verify finger error exception : {0}", ex.Message), clientInfo, guidThreadId, response);
            }
            finally
            {
                if (clientInfo == null)
                {
                    Logs("End verify finger", string.Empty, string.Empty, string.Empty, Constant.LEVEL_ALL);
                }
                else
                {
                    Logs("End verify finger", guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ALL);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        /// <summary>
        /// Bioagent Verification for BDS IBS 
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/bio_verify_bds_ibs")] //route alias
        public HttpResponseMessage BioVerifyBDSIBS([FromBody]BDSVerifyModel model)
        {
            string guidThreadId = string.Empty;
            ServiceResponse response = new ServiceResponse();
            try
            {
                if (model.ClientInfo == null) return LogsErrorNullParameter("Client info", response);
                ValidationContext context = new ValidationContext(new ValidationBDSVerify(model));
                if (!context.Execute())
                {
                    Logs(string.Format("BDS IBS verify validation failed : {0}", context.GetErrorMessage()), guidThreadId, string.Empty, string.Empty, Constant.LEVEL_ERROR);
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();
                    response.responseData = JsonConvert.SerializeObject(new { model.ClientInfo.NewGuid });
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }

                guidThreadId = Utility.GetGuidThreadId(model.ClientInfo.NewGuid);
                Logs("Starting BDS IBS verify", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ALL);

                response = new FingerLogic().BDSIBSVerify(model, guidThreadId);
                if (response.statusCode != Constant.NO_ERROR)
                {
                    Logs(response.statusMessage, guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ERROR);
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }
                Logs(response.statusMessage, guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ALL);
            }
            catch (System.Exception ex)
            {
                return CreateResponseException(string.Format("BDS IBS verify error exception : {0}", ex.Message), model.ClientInfo, guidThreadId, response);
            }
            finally
            {
                if (model.ClientInfo == null)
                {
                    Logs("End BDS IBS verify", string.Empty, string.Empty, string.Empty, Constant.LEVEL_ALL);
                }
                else
                {
                    Logs("End BDS IBS verify", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ALL);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        /// <summary>
        /// Bioagent Verification for BDS WEB 
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/bio_verify_bds_web")] //route alias
        public HttpResponseMessage BioVerifyBDSWEB([FromBody]BDSVerifyModel model)
        {
            string guidThreadId = string.Empty;
            ServiceResponse response = new ServiceResponse();
            try
            {
                if (model.ClientInfo == null) return LogsErrorNullParameter("Client info", response);
                ValidationContext context = new ValidationContext(new ValidationBDSVerify(model));
                if (!context.Execute())
                {
                    Logs(string.Format("BDS WEB verify validation failed : {0}", context.GetErrorMessage()), guidThreadId, string.Empty, string.Empty, Constant.LEVEL_ERROR);
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();
                    response.responseData = JsonConvert.SerializeObject(new { model.ClientInfo.NewGuid });
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }

                guidThreadId = Utility.GetGuidThreadId(model.ClientInfo.NewGuid);
                Logs("Starting BDS WEB verify", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ALL);

                response = new FingerLogic().BDSWEBVerify(model, guidThreadId);
                if (response.statusCode != Constant.NO_ERROR)
                {
                    Logs(response.statusMessage, guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ERROR);
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }
                Logs(response.statusMessage, guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ALL);
            }
            catch (System.Exception ex)
            {
                return CreateResponseException(string.Format("BDS WEB verify error exception : {0}", ex.Message), model.ClientInfo, guidThreadId, response);
            }
            finally
            {
                if (model.ClientInfo == null)
                {
                    Logs("End BDS WEB verify", string.Empty, string.Empty, string.Empty, Constant.LEVEL_ALL);
                }
                else
                {
                    Logs("End BDS WEB verify", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ALL);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        /// <summary>
        /// Bioagent Identification for ATTENDANCE 
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/bio_identify_attendance")] //route alias
        public HttpResponseMessage BioIdentifyAttendance([FromBody]IdentifyAttendanceModel model)
        {
            string guidThreadId = string.Empty;
            ServiceResponse response = new ServiceResponse();
            try
            {
                if (model.ClientInfo == null) return LogsErrorNullParameter("Client info", response);
                ValidationContext context = new ValidationContext(new ValidationAttendance(model));
                if (!context.Execute())
                {
                    Logs(string.Format("Identify attendance validation failed : {0}", context.GetErrorMessage()), guidThreadId, string.Empty, string.Empty, Constant.LEVEL_ERROR);
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();
                    response.responseData = JsonConvert.SerializeObject(new { model.ClientInfo.NewGuid });
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }

                guidThreadId = Utility.GetGuidThreadId(model.ClientInfo.NewGuid);
                Logs("Starting identify attendance verify", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ALL);

                response = new FingerLogic().IdentifyAttendance(model, guidThreadId);
                if (response.statusCode != Constant.NO_ERROR)
                {
                    Logs(response.statusMessage, guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ERROR);
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }
                Logs(response.statusMessage, guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ALL);
            }
            catch (System.Exception ex)
            {
                return BioCreateResponseException(ex.Message, model.ClientInfo, guidThreadId, response);
            }
            finally
            {
                if (model.ClientInfo == null)
                {
                    Logs("End identify attendance", string.Empty, string.Empty, string.Empty, Constant.LEVEL_ALL);
                }
                else
                {
                    Logs("End identify attendance", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ALL);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        /// <summary>
        /// Connection service for bioagent
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/bio_connect")] //route alias
        public HttpResponseMessage BioConnect([FromBody]ClientInfo clientInfo)
        {
            string guidThreadId = string.Empty;
            ServiceResponse response = new ServiceResponse();
            try
            {
                if (clientInfo == null) return LogsErrorNullParameter("Client info", response);
                ValidationContext context = new ValidationContext(new ValidationClientInfo(clientInfo));
                if (!context.Execute())
                {
                    Logs(string.Format("Connect validation failed : {0}", context.GetErrorMessage()), guidThreadId, string.Empty, string.Empty, Constant.LEVEL_ERROR);
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }
                guidThreadId = Utility.GetGuidThreadId(clientInfo.NewGuid);
                Logs("Starting connect to central server", guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ALL);

                response = new FingerLogic().BioCheckConnection();
                if (response.statusCode != Constant.NO_ERROR)
                {
                    Logs(response.statusMessage, guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ERROR);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, response);
                }
                Logs(response.statusMessage, guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ALL);
            }
            catch (System.Exception ex)
            {
                return CreateResponseException(string.Format("Connect error exception : {0}", ex.Message), clientInfo, guidThreadId, response);
            }
            finally
            {
                if (clientInfo == null)
                {
                    Logs("End connect to central server", string.Empty, string.Empty, string.Empty, Constant.LEVEL_ALL);
                }
                else
                {
                    Logs("End connect to central server", guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ALL);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
        #endregion

        #region Migration Local Activity
        /// <summary>
        /// Send Migration Activity Local
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/migrateactivitylocal")]
        public HttpResponseMessage MigrateActivityLocal([FromBody]MigrateActivityLocalModel model)
        {
            string guidThreadId = string.Empty;
            ServiceResponse response = new ServiceResponse();
            try
            {
                if (model.ClientInfo == null) return LogsErrorNullParameter("Client info", response);
                ValidationContext context = new ValidationContext(new ValidationMigrateActivityLocal(model));
                if (!context.Execute())
                {
                    Logs(string.Format("Migration Activity Local validation failed : {0}", context.GetErrorMessage()), guidThreadId, string.Empty, string.Empty, Constant.LEVEL_ERROR);
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }

                guidThreadId = Utility.GetGuidThreadId(model.ClientInfo.NewGuid);
                Logs("Starting migration activity local", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ALL);

                response = new FingerLogic().MigrationActivityLocalProcess(guidThreadId, model);
                if (response.statusCode == Constant.ERROR_CODE_VALIDATION || response.statusCode == Constant.ERROR_CODE_SYSTEM)
                {
                    Logs(response.statusMessage, guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ERROR);
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }
                if (response.statusMessage.ToUpper() == Constant.FILE_NOT_COMPLETED.ToUpper())
                {
                    return Request.CreateResponse(HttpStatusCode.OK, response);
                }
                Logs(response.statusMessage, guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ALL);
            }
            catch (System.Exception ex)
            {
                return CreateResponseException(string.Format("Migration activity local error exception : {0}", ex.Message), model.ClientInfo, guidThreadId, response);
            }
            finally
            {
                if (model.ClientInfo == null)
                {
                    Logs("End migration activity local", string.Empty, string.Empty, string.Empty, Constant.LEVEL_ALL);
                }
                else
                {
                    Logs("End migration activity local", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ALL);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        #endregion

        #endregion

        #endregion

        #region private method(s)
        private void Logs(string message, string transactionUniqueId, string appName, string appVersion, string level)
        {
            LogInfo logInfo = new LogInfo();
            logInfo.AppName = appName;
            logInfo.AppVersion = appVersion;
            logInfo.LogName = Constant.FINGER_SERVICE;
            logInfo.Level = level;
            logInfo.GuidThreadId = transactionUniqueId;
            logInfo.Message = message;

            Utility.WriteLog(logInfo);
        }

        private HttpResponseMessage CreateResponseException(string exMessage, ClientInfo clientInfo, string guidThreadId, ServiceResponse response)
        {
            Logs(exMessage, guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ERROR);
            response.statusCode = Constant.ERROR_CODE_SYSTEM;
            response.statusMessage = exMessage;
            return Request.CreateResponse(HttpStatusCode.InternalServerError, response);
        }

        private HttpResponseMessage CreateResponseException(string exMessage, string applicationName, string applicationVersion, string guidThreadId, ServiceResponse response)
        {
            Logs(exMessage, guidThreadId, applicationName, applicationVersion, Constant.LEVEL_ERROR);
            response.statusCode = Constant.ERROR_CODE_SYSTEM;
            response.statusMessage = exMessage;
            return Request.CreateResponse(HttpStatusCode.InternalServerError, response);
        }

        private HttpResponseMessage BioCreateResponseException(string exMessage, ClientInfo clientInfo, string guidThreadId, ServiceResponse response)
        {
            Logs(string.Format("Error exception : {0}", exMessage), guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ERROR);
            response.statusCode = Constant.ERROR_CODE_SYSTEM;
            response.statusMessage = exMessage;
            response.responseData = JsonConvert.SerializeObject(new { clientInfo.NewGuid });
            return Request.CreateResponse(HttpStatusCode.InternalServerError, response);
        }

        private HttpResponseMessage WebAdminCreateResponseException(string exMessage, string guidThreadId, ServiceResponse response)
        {
            WebAdminLog(guidThreadId, exMessage, Constant.LEVEL_ERROR);
            response.statusCode = Constant.ERROR_CODE_SYSTEM;
            response.statusMessage = exMessage;
            return Request.CreateResponse(HttpStatusCode.InternalServerError, response);
        }

        private HttpResponseMessage LogsErrorNullParameter(string parameterName, ServiceResponse response)
        {
            Logs(string.Format("{0} parameter cannot be null", parameterName), string.Empty, string.Empty, string.Empty, Constant.LEVEL_ERROR);
            response.statusCode = Constant.ERROR_CODE_VALIDATION;
            response.statusMessage = string.Format("{0} parameter cannot be null", parameterName);
            return Request.CreateResponse(HttpStatusCode.BadRequest, response);
        }
        #endregion
    }
}
