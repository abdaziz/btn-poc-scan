﻿using Finger.Core.Entity;
using Finger.Entity;
using Finger.Framework.Common;
using Finger.Framework.Data;
using Finger.Framework.Entity;
using Finger.Framework.Models;
using Finger.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Globalization;
using System.IO;

namespace Finger.Data
{
    public partial class FingerDataProvider : FingerBaseProvider
    {
        #region private members
        private int id = 0;
        #endregion

        #region public method(s)

        #region finger web service
        public bool CheckSQLConnection(int retryConnection, int delayMilliseconds)
        {
            bool result = false;

            using (SqlConnection oSqlConnection = new SqlConnection(this.GetConnectionString()))
            {
                result = SQLProvider.Instance.CheckSQLConnection(oSqlConnection, GetDatabaseName(), retryConnection, delayMilliseconds);
            }

            return result;
        }

        public bool CheckSQLConnection(ref string dbName)
        {
            bool connected = false;
            try
            {
                int retry = 1;
                while ((!connected) && retry <= this.GetRetryTimeout())
                {
                    retry++;
                    try
                    {
                        dbName = this.GetDatabaseName();
                        using (SqlConnection oSqlConnection = new SqlConnection(this.GetConnectionString()))
                        {
                            oSqlConnection.Open();
                            string statusDatabase = string.Empty;
                            string query = string.Format("use Master Select state_desc from Sys.Databases where name = '{0}'", this.GetDatabaseName());
                            using (SqlCommand oSqlCommand = new SqlCommand(query, oSqlConnection))
                            {
                                using (SqlDataReader oSqlDataReader = oSqlCommand.ExecuteReader())
                                {
                                    if (oSqlDataReader.Read())
                                    {
                                        statusDatabase = oSqlDataReader["state_desc"] != DBNull.Value ? oSqlDataReader["state_desc"].ToString().ToLower() : string.Empty;
                                        connected = statusDatabase.ToLower() == "online";
                                    }
                                }
                            }
                            if (!connected) System.Threading.Thread.Sleep(this.GetDelayMilisecond());
                        }
                    }
                    catch
                    {
                        System.Threading.Thread.Sleep(this.GetDelayMilisecond());
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return connected;
        }

        public bool BioCheckSQLConnection()
        {
            bool connected = false;
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(this.GetConnectionString()))
                {
                    oSqlConnection.Open();
                    string statusDatabase = string.Empty;
                    string query = string.Format("use Master Select state_desc from Sys.Databases where name = '{0}'", this.GetDatabaseName());
                    using (SqlCommand oSqlCommand = new SqlCommand(query, oSqlConnection))
                    {
                        using (SqlDataReader oSqlDataReader = oSqlCommand.ExecuteReader())
                        {
                            if (oSqlDataReader.Read())
                            {
                                statusDatabase = oSqlDataReader["state_desc"] != DBNull.Value ? oSqlDataReader["state_desc"].ToString().ToLower() : string.Empty;
                                connected = statusDatabase.ToLower() == "online";
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return connected;
        }

        public Framework.Entity.ErrorInfo SaveActivity(FSActivity activity)
        {
            ErrorInfo erroInfo = new ErrorInfo();
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(this.GetConnectionString()))
                {
                    oSqlConnection.Open();
                    erroInfo = SQLProvider.Instance.SaveActivity(activity, oSqlConnection);
                }
            }
            catch (System.Exception ex)
            {
                erroInfo.Code = Constant.ERROR_CODE_SYSTEM;
                erroInfo.Message = ex.Message;
            }
            return erroInfo;
        }

        public List<Framework.Entity.cfgitems> GetCfgitems(out ServiceResponse response)
        {
            response = new ServiceResponse();
            List<Framework.Entity.cfgitems> oCfgitems = null;
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(GetConnectionString()))
                {
                    oSqlConnection.Open();
                    oCfgitems = SQLProvider.Instance.GetCentralCfgitems(oSqlConnection, this.GetCommandTimeOut());
                }
            }
            catch (Framework.Exception.CKVException ex)
            {
                response.statusCode = Constant.ERR_CODE_5051;
                response.statusMessage = ex.Message;
            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;
            }
            return oCfgitems;
        }

        public List<Framework.Entity.cfgitems> GetCfgitems(string branchCode, string officeCode, out ServiceResponse response)
        {
            response = new ServiceResponse();
            List<Framework.Entity.cfgitems> oCfgitems = null;
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(GetConnectionString()))
                {
                    oSqlConnection.Open();
                    oCfgitems = SQLProvider.Instance.GetCentralCfgitems(branchCode, officeCode, oSqlConnection, this.GetCommandTimeOut());
                }
            }
            catch (Framework.Exception.CKVException ex)
            {
                response.statusCode = Constant.ERR_CODE_5051;
                response.statusMessage = ex.Message;
            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;
            }
            return oCfgitems;
        }

        public List<Framework.Entity.cfgitems> GetCfgitems(out ServiceResponse response, params string[] param)
        {
            response = new ServiceResponse();
            List<Framework.Entity.cfgitems> oCfgitems = null;
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(GetConnectionString()))
                {
                    oSqlConnection.Open();
                    oCfgitems = SQLProvider.Instance.GetCentralCfgitems(oSqlConnection, this.GetCommandTimeOut(), param);
                }
            }
            catch (Framework.Exception.CKVException ex)
            {
                response.statusCode = Constant.ERR_CODE_5051;
                response.statusMessage = ex.Message;
            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;
            }
            return oCfgitems;
        }

        public Framework.Entity.cfgitems GetConfigItemsByName(string keyName, out ServiceResponse response)
        {
            response = new ServiceResponse();
            Framework.Entity.cfgitems oCfgitems = null;
            using (SqlConnection oSqlConnection = new SqlConnection(this.GetConnectionString()))
            {
                oSqlConnection.Open();
                oCfgitems = SQLProvider.Instance.GetCentralCfgitem(keyName, oSqlConnection, this.GetCommandTimeOut());
            }
            return oCfgitems;
        }

        public List<Framework.Entity.cfgitems> GetConfigItemsByArgs(string[] param, out ServiceResponse response)
        {
            response = new ServiceResponse();
            List<Framework.Entity.cfgitems> listCfgitems = null;
            using (SqlConnection oSqlConnection = new SqlConnection(this.GetConnectionString()))
            {
                oSqlConnection.Open();
                listCfgitems = SQLProvider.Instance.GetCentralCfgitems(oSqlConnection, this.GetCommandTimeOut(), param);
            }
            return listCfgitems;
        }

        public ServiceResponse GetConfigItemsByArgs(string[] param, out List<Framework.Entity.cfgitems> listCfgitems)
        {
            ServiceResponse response = new ServiceResponse();
            listCfgitems = new List<cfgitems>();
            try
            {
                listCfgitems = new List<cfgitems>();
                using (SqlConnection oSqlConnection = new SqlConnection(this.GetConnectionString()))
                {
                    oSqlConnection.Open();
                    response = SQLProvider.Instance.GetCentralCfgitems(oSqlConnection, this.GetCommandTimeOut(), out listCfgitems, param);
                }
            }
            catch (SqlException sqlex)
            {
                if (sqlex.Number == Constant.ERROR_CODE_CONNECTION)
                {
                    response.statusCode = Constant.ERROR_CODE_CONNECTION;
                    response.statusMessage = string.Format("Central server problem: {0}", Utility.GetErrorMessage(Constant.ERR_CODE_5050));
                }
                else
                {
                    response.statusCode = Constant.ERROR_CODE_SYSTEM;
                    response.statusMessage = sqlex.Message;
                }
            }

            return response;
        }

        public ServiceResponse ValidateUserLogin(string userLoginId, string branchCode, string officeCode)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand("[dbo].[usp_ValidateUserLogin]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@UserLoginId", userLoginId);
                        command.Parameters.AddWithValue("@ClientBranchCode", branchCode);
                        command.CommandTimeout = this.GetCommandTimeOut();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            return GetResponseMapper(reader);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;
            }
            return response;
        }

        public int CheckIsKP(string userLogonId)
        {
            int result = -1;
            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "[dbo].[usp_CheckIsKp]";
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@UserLogonId", userLogonId);
                        command.CommandTimeout = this.GetCommandTimeOut();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                result = reader["IsBranchKp"] != DBNull.Value ? Convert.ToInt32(reader["IsBranchKp"]) : -1;
                            }
                        }

                    }
                }
                return result;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public ServiceResponse SaveUserLog(ClientInfo clientInfo, UserLogInfo userLogInfo)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand("[dbo].[usp_SaveUserLog]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@CentralId", userLogInfo.CentralId);
                        command.Parameters.AddWithValue("@UserId", userLogInfo.UserId);
                        command.Parameters.AddWithValue("@UserLogTime", Convert.ToDateTime(userLogInfo.UserLogTime));
                        command.Parameters.AddWithValue("@SourceIpType", clientInfo.IPAddressType);
                        command.Parameters.AddWithValue("@SourceIp", clientInfo.IPAddress);
                        command.Parameters.AddWithValue("@SourceName", clientInfo.HostName);
                        command.Parameters.AddWithValue("@BranchCode", clientInfo.BranchCode);
                        command.Parameters.AddWithValue("@OfficeCode", clientInfo.OfficeCode);
                        command.CommandTimeout = this.GetCommandTimeOut();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            return GetResponseMapper(reader);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;
            }
            return response;
        }

        public ServiceResponse DeleteUserLog(int centralId, string branchCode, string officeCode)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand("[dbo].[usp_DeleteUserLog]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@CentralId", centralId);
                        command.Parameters.AddWithValue("@BranchCode", branchCode);
                        command.Parameters.AddWithValue("@OfficeCode", officeCode);
                        command.CommandTimeout = this.GetCommandTimeOut();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            return GetResponseMapper(reader);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;
            }
            return response;
        }

        public ServiceResponse ValidateIsNotExistsUserLog(int centralId, string hostName, string ipaddress)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand("[dbo].[usp_ValidateIsNotExistsUserLog]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@CentralId", centralId);
                        command.Parameters.AddWithValue("@ClientHostName", hostName);
                        command.Parameters.AddWithValue("@ClientIpAdrress", ipaddress);
                        command.CommandTimeout = this.GetCommandTimeOut();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            return GetResponseMapper(reader);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;
            }
            return response;
        }

        public int GetQualitySpecialUserByUserId(string userId)
        {
            int result = 0;
            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "[dbo].[usp_GetQualitySpecialUserByUserId]";
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@UserId", userId);
                        command.CommandTimeout = this.GetCommandTimeOut();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                result = reader["Quality"] != DBNull.Value ? Convert.ToInt32(reader["Quality"]) : 0;
                            }
                        }

                    }
                }
                return result;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public int GetQualityUserLoginId(string loginId)
        {
            int result = 0;
            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "[dbo].[usp_GetQualityUserLoginId]";
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@LoginId", loginId);
                        command.CommandTimeout = this.GetCommandTimeOut();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                result = reader["Quality"] != DBNull.Value ? Convert.ToInt32(reader["Quality"]) : 0;
                            }
                        }

                    }
                }
                return result;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public int GetFsCentralIdByUserId(string userId)
        {
            int centralId = 0;
            using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand("[dbo].[usp_GetFsCentralIdByNip]", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@nip", userId);
                    command.CommandTimeout = this.GetCommandTimeOut();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            centralId = reader["FsCentralId"] != DBNull.Value ? Convert.ToInt32(reader["FsCentralId"]) : 0;
                        }
                    }
                }
            }

            return centralId;
        }

        public ErrorInfo GetInquiryHubOfficeActivityByCriteria(HubOfficeActivityInquiryModel model, ref List<HubOfficeActivity> hubOfficeActivityList, ref int rowCount)
        {
            ErrorInfo errInfo = new ErrorInfo();
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(GetConnectionString()))
                {
                    oSqlConnection.Open();
                    using (SqlCommand oSqlCommand = oSqlConnection.CreateCommand())
                    {
                        oSqlCommand.CommandText = "[dbo].[usp_GetPagedInquiryHubOfficeActivityByCriteria]";
                        oSqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        oSqlCommand.CommandTimeout = this.GetCommandTimeOut();
                        oSqlCommand.Parameters.AddWithValue("@BranchType", model.HubOfficeActivityCriteria.BranchType);
                        oSqlCommand.Parameters.AddWithValue("@BranchCode", model.HubOfficeActivityCriteria.BranchCode);
                        oSqlCommand.Parameters.AddWithValue("@OfficeCode", model.HubOfficeActivityCriteria.OfficeCode);
                        oSqlCommand.Parameters.AddWithValue("@Nip", model.HubOfficeActivityCriteria.NIP);
                        oSqlCommand.Parameters.AddWithValue("@StartPeriode", string.Format("{0} {1}", model.HubOfficeActivityCriteria.StartPeriode, "00:00:00.000"));
                        oSqlCommand.Parameters.AddWithValue("@EndPeriode", string.Format("{0} {1}", model.HubOfficeActivityCriteria.EndPeriode, "23:59:59.999"));
                        oSqlCommand.Parameters.AddWithValue("@PageNumber", model.PagingInfo.Index);
                        oSqlCommand.Parameters.AddWithValue("@PageSize", model.PagingInfo.PageSize);
                        oSqlCommand.Parameters.AddWithValue("@OrderBy", string.Format("{0} {1}", model.PagingInfo.OrderBy, model.PagingInfo.OrderType));
                        oSqlCommand.Parameters.AddWithValue("@UserLoginID", model.ClientInfo.LogonID);

                        using (var reader = oSqlCommand.ExecuteReader())
                        {
                           

                            if (!reader.HasRows)
                            {
                                errInfo.Code = Constant.ERR_CODE_1300;
                                errInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_1300);
                                rowCount = 0;
                                return errInfo;
                            }

                            bool isError = false;
                            while (reader.Read())
                            {
                                if (!isError)
                                {
                                    try
                                    {
                                        if (reader["ErrorCode"] != DBNull.Value)
                                        {
                                            errInfo.Code = Convert.ToInt32(reader["ErrorCode"]);
                                            errInfo.Message = string.Format(Utility.GetErrorMessage(errInfo.Code), reader["BranchCode"].ToString(), reader["OfficeCode"].ToString());
                                            rowCount = 0;
                                            return errInfo;
                                        }
                                    }
                                    catch
                                    {
                                        isError = true;

                                    }
                                }

                                if (isError)
                                {
                                    var hubOfficeActivity = new HubOfficeActivity
                                    {
                                        CurrentBranchCode = reader["CurrentBranchCode"] != DBNull.Value ? reader["CurrentBranchCode"].ToString() : string.Empty,
                                        CurrentOfficeCode = reader["CurrentOfficeCode"] != DBNull.Value ? reader["CurrentOfficeCode"].ToString() : string.Empty,
                                        DestinationBranchCode = reader["DestinationBranchCode"] != DBNull.Value ? reader["DestinationBranchCode"].ToString() : string.Empty,
                                        DestinationOfficeCode = reader["DestinationOfficeCode"] != DBNull.Value ? reader["DestinationOfficeCode"].ToString() : string.Empty,
                                        NIP = reader["NIP"] != DBNull.Value ? reader["NIP"].ToString() : string.Empty,
                                        RegisterDate = reader["RegisterDate"] != DBNull.Value ? Convert.ToDateTime(reader["RegisterDate"]).ToString(Constant.ACTIVITY_DATE_FORMAT) : string.Empty,
                                        Status = reader["Status"] != DBNull.Value ? reader["Status"].ToString() : string.Empty,
                                        Message = reader["Message"] != DBNull.Value ? reader["Message"].ToString() : string.Empty,
                                    };
                                    hubOfficeActivityList.Add(hubOfficeActivity);
                                }
                                
                            }

                            if (reader.NextResult())
                            {
                                if (reader.Read())
                                {
                                    rowCount = reader["RowCount"] != DBNull.Value ? Convert.ToInt32(reader["RowCount"]) : 0;

                                }
                            }




                        }
                    }
                }
               
                if (hubOfficeActivityList.Count > 0)
                {
                    errInfo.Code = Constant.NO_ERROR;
                    errInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_1303);
                }
            }
            catch (System.Exception ex)
            {
                errInfo.Code = Constant.ERROR_CODE_VALIDATION;
                errInfo.Message = ex.Message;
            }
            return errInfo;
        }

        public ErrorInfo GetInquiryFingerAPIConfig(FingerAPIInquiryModel model, ref List<FSAllowedApps> allowedAppsList, ref int rowCount)
        {
            ErrorInfo errInfo = new ErrorInfo();
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(GetConnectionString()))
                {
                    oSqlConnection.Open();
                    using (SqlCommand oSqlCommand = oSqlConnection.CreateCommand())
                    {
                        oSqlCommand.CommandText = "[dbo].[usp_GetPagedInquiryFingerAPIConfig]";
                        oSqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        oSqlCommand.CommandTimeout = this.GetCommandTimeOut();
                        oSqlCommand.Parameters.AddWithValue("@PageNumber", model.PagingInfo.Index);
                        oSqlCommand.Parameters.AddWithValue("@PageSize", model.PagingInfo.PageSize);

                        using (var reader = oSqlCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                FSAllowedApps allowedApps = new FSAllowedApps();
                                allowedApps.FsAllowedAppsId = reader["FsAllowedAppsId"] != DBNull.Value ? Convert.ToInt32(reader["FsAllowedAppsId"]) : 0;
                                allowedApps.AppId = reader["AppId"] != DBNull.Value ? reader["AppId"].ToString() : string.Empty;
                                allowedApps.AppName = reader["AppName"] != DBNull.Value ? reader["AppName"].ToString() : string.Empty;
                                allowedApps.AppVersion = reader["AppVersion"] != DBNull.Value ? reader["AppVersion"].ToString() : string.Empty;
                                allowedApps.CreatedBy = reader["CreatedBy"] != DBNull.Value ? reader["CreatedBy"].ToString() : string.Empty;
                                allowedApps.CreatedDate = reader["CreatedDate"] != DBNull.Value ? reader["CreatedDate"].ToString() : string.Empty;
                                allowedApps.UpdatedBy = reader["UpdatedBy"] != DBNull.Value ? reader["UpdatedBy"].ToString() : string.Empty;
                                allowedApps.UpdatedDate = reader["UpdatedDate"] != DBNull.Value ? reader["UpdatedDate"].ToString() : string.Empty;
                                allowedApps.IsDelete = reader["IsDelete"] != DBNull.Value ? Convert.ToInt32(reader["IsDelete"]) : 0;
                                allowedApps.CKV = reader["CKV"] != DBNull.Value ? Convert.ToInt32(reader["CKV"]) : 0;
                                allowedApps.CKVStatus = RecalculateCKVStatus(allowedApps);
                                allowedAppsList.Add(allowedApps);
                            }

                            if (reader.NextResult() && reader.Read())
                            {
                                rowCount = reader["RowCount"] != DBNull.Value ? Convert.ToInt32(reader["RowCount"]) : 0;
                            }
                        }
                    }
                }
                if (allowedAppsList.Count == 0)
                {
                    errInfo.Code = Constant.ERR_CODE_1100;
                    errInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_1100);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return errInfo;
        }

        public ServiceResponse SaveFingerApiConfig(string guidThreadId, FingerAPIConfigModel model, string userLoginName)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand("[dbo].[usp_SaveFingerApiConfig]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = this.GetCommandTimeOut();
                        command.Parameters.AddWithValue("@FsAllowedAppsId", model.FsAllowedAppsId);
                        command.Parameters.AddWithValue("@AppID", model.AppID);
                        command.Parameters.AddWithValue("@AppName", model.AppName);
                        command.Parameters.AddWithValue("@AppVersion", model.AppVersion);
                        command.Parameters.AddWithValue("@UserLoginName", userLoginName);
                        command.Parameters.AddWithValue("@SecretKey", Utility.GenerateIVTKeySalt());
                        command.Parameters.AddWithValue("@ApplicationRequestName", Constant.WEB_ADMIN);
                        command.Parameters.AddWithValue("@ApplicationRequestVersion", Constant.WEB_ADMIN_VERSION);
                        command.Parameters.AddWithValue("@ApplicationTransactionId", guidThreadId);
                        command.Parameters.AddWithValue("@ActivityActionName", Constant.ACTION_FINGER_API_CONFIG);
                        command.Parameters.AddWithValue("@ActivityUserAddress", Utility.GetLocalIPAddress());
                        command.Parameters.AddWithValue("@ActivityUserAddressType", Utility.GetLocalIPAddressType());
                        command.Parameters.AddWithValue("@ActivityHostName", Utility.GetLocalHostName());
                        command.Parameters.AddWithValue("@ActivityUserName", Utility.GetLocalUserLoginName());
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            return GetResponseMapper(reader);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;
            }
            return response;
        }

        public ServiceResponse DeleteFingerApiConfig(string guidThreadId, FingerAPIConfigModel model, string userLoginName)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand("[dbo].[usp_DeleteFingerApiConfig]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = this.GetCommandTimeOut();
                        command.Parameters.AddWithValue("@dtFsAllowedAppsId", model.FsAllowedAppsIdCollection.ToDataTable());
                        command.Parameters.AddWithValue("@UserLoginName", userLoginName);
                        command.Parameters.AddWithValue("@SecretKey", Utility.GenerateIVTKeySalt());
                        command.Parameters.AddWithValue("@ApplicationRequestName", Constant.WEB_ADMIN);
                        command.Parameters.AddWithValue("@ApplicationRequestVersion", Constant.WEB_ADMIN_VERSION);
                        command.Parameters.AddWithValue("@ApplicationTransactionId", guidThreadId);
                        command.Parameters.AddWithValue("@ActivityActionName", Constant.ACTION_FINGER_API_CONFIG);
                        command.Parameters.AddWithValue("@ActivityUserAddress", Utility.GetLocalIPAddress());
                        command.Parameters.AddWithValue("@ActivityUserAddressType", Utility.GetLocalIPAddressType());
                        command.Parameters.AddWithValue("@ActivityHostName", Utility.GetLocalHostName());
                        command.Parameters.AddWithValue("@ActivityUserName", Utility.GetLocalUserLoginName());
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            return GetResponseMapper(reader);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;
            }
            return response;
        }

        public bool CheckCKVByUserId(string userId)
        {
            bool result = false;
            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "[dbo].[usp_CheckCkvByUserId]";
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Key", Utility.GenerateIVTKeySalt());
                        command.Parameters.AddWithValue("@UserId", userId);
                        command.Parameters["@ErrorNumber"].Direction = ParameterDirection.Output;
                        command.Parameters["@ErrorMessage"].Direction = ParameterDirection.Output;

                        command.CommandTimeout = this.GetCommandTimeOut();
                        command.ExecuteNonQuery();
                        result = Convert.ToInt32(command.Parameters["@ErrorNumber"].Value) == 0 ? true : false;
                    }
                }
                return result;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region for migration activity local
        public ServiceResponse ExtractDataAndBulkCopyData(string dbFilePath, string _branchCode, string _officeCode)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                string constring = string.Format("Data Source={0}", dbFilePath);
                using (SQLiteConnection sqlitecon = new SQLiteConnection(constring))
                {
                    using (SQLiteCommand sqliteCmd = sqlitecon.CreateCommand())
                    {
                        sqlitecon.Open();
                        sqliteCmd.CommandType = CommandType.Text;
                        sqliteCmd.CommandText = string.Format("SELECT EventId,UserId,EventTime,FingerIndex,Score,SourceIpType,SourceIp,SourceName,ActivityMessage,{0} AS BranchCode,{1} AS OfficeCode FROM FsActivity", _branchCode, _officeCode);
                        sqliteCmd.CommandTimeout = this.GetCommandTimeOut();

                        using (SqlConnection sqlsvrcon = new SqlConnection(this.GetConnectionString()))
                        {
                            sqlsvrcon.Open();
                            using (var transaction = sqlsvrcon.BeginTransaction(IsolationLevel.ReadCommitted))
                            {
                                try
                                {
                                    using (SQLiteDataReader sqliteReader = sqliteCmd.ExecuteReader())
                                    {
                                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(sqlsvrcon, SqlBulkCopyOptions.Default, transaction))
                                        {
                                            bulkCopy.DestinationTableName = "[dbo].[FsTemporaryLocalActivity]";
                                            bulkCopy.BulkCopyTimeout = this.GetCommandTimeOut();
                                            // Set up the column mappings by name.

                                            SqlBulkCopyColumnMapping branchCode =
                                                new SqlBulkCopyColumnMapping("BranchCode", "BranchCode");
                                            bulkCopy.ColumnMappings.Add(branchCode);

                                            SqlBulkCopyColumnMapping officeCode =
                                                new SqlBulkCopyColumnMapping("OfficeCode", "OfficeCode");
                                            bulkCopy.ColumnMappings.Add(officeCode);

                                            SqlBulkCopyColumnMapping eventTime =
                                                new SqlBulkCopyColumnMapping("EventTime", "EventTime");
                                            bulkCopy.ColumnMappings.Add(eventTime);

                                            SqlBulkCopyColumnMapping eventId =
                                                new SqlBulkCopyColumnMapping("EventId", "EventId");
                                            bulkCopy.ColumnMappings.Add(eventId);

                                            SqlBulkCopyColumnMapping userId =
                                                new SqlBulkCopyColumnMapping("UserId", "UserId");
                                            bulkCopy.ColumnMappings.Add(userId);

                                            SqlBulkCopyColumnMapping fingerIndex =
                                                new SqlBulkCopyColumnMapping("FingerIndex", "FingerIndex");
                                            bulkCopy.ColumnMappings.Add(fingerIndex);

                                            SqlBulkCopyColumnMapping score =
                                                new SqlBulkCopyColumnMapping("Score", "Score");
                                            bulkCopy.ColumnMappings.Add(score);

                                            SqlBulkCopyColumnMapping sourceIpType =
                                                new SqlBulkCopyColumnMapping("SourceIpType", "SourceIpType");
                                            bulkCopy.ColumnMappings.Add(sourceIpType);

                                            SqlBulkCopyColumnMapping sourceIp =
                                                new SqlBulkCopyColumnMapping("SourceIp", "SourceIp");
                                            bulkCopy.ColumnMappings.Add(sourceIp);

                                            SqlBulkCopyColumnMapping sourceName =
                                                new SqlBulkCopyColumnMapping("SourceName", "SourceName");
                                            bulkCopy.ColumnMappings.Add(sourceName);

                                            SqlBulkCopyColumnMapping activityMessage = new SqlBulkCopyColumnMapping("ActivityMessage", "ActivityMessage");
                                            bulkCopy.ColumnMappings.Add(activityMessage);

                                            // Write from the source to the destination.
                                            try
                                            {
                                                bulkCopy.WriteToServer(sqliteReader);
                                            }
                                            catch (System.Exception ex)
                                            {
                                                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                                                response.statusMessage = ex.Message;
                                            }

                                        }
                                    }
                                    transaction.Commit();
                                }
                                catch (System.Exception)
                                {
                                    transaction.Rollback();
                                }

                            }
                        }

                    }
                }
            }
            catch (SqlException sqlex)
            {
                if (sqlex.ErrorCode == Constant.ERROR_CODE_CONNECTION)
                {
                    response.statusCode = Constant.ERR_CODE_5050;
                    response.statusMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5050);
                }
                else
                {
                    response.statusCode = Constant.ERROR_CODE_SYSTEM;
                    response.statusMessage = sqlex.Message;
                }
            }
            return response;
        }
        public ServiceResponse ProcessMigrationLocalActivityCompleting(ClientInfo clientInfo)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "[dbo].[usp_ProcessMigrationLocalActivityCompleting]";
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@BranchCode", clientInfo.BranchCode);
                        command.Parameters.AddWithValue("@OfficeCode", clientInfo.OfficeCode);
                        command.Parameters.AddWithValue("@SecretKey", Utility.GenerateIVTKeySalt());
                        command.CommandTimeout = this.GetCommandTimeOut();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            return GetResponseMapper(reader);
                        }
                    }
                }
            }
            catch (SqlException sqlex)
            {
                if (sqlex.ErrorCode == Constant.ERROR_CODE_CONNECTION)
                {
                    response.statusCode = Constant.ERR_CODE_5050;
                    response.statusMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5050);
                }
                else
                {
                    response.statusCode = Constant.ERROR_CODE_SYSTEM;
                    response.statusMessage = sqlex.Message;
                }
            }
            return response;
        }
        public ServiceResponse DeleteTemporaryActivity(ClientInfo clientInfo)
        {
            ServiceResponse response = new ServiceResponse();
            bool result = false;
            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "[dbo].[usp_DeleteTemporaryLocalActivity]";
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandTimeout = this.GetCommandTimeOut();
                        command.Parameters.AddWithValue("@BranchCode", clientInfo.BranchCode);
                        command.Parameters.AddWithValue("@OfficeCode", clientInfo.OfficeCode);
                        result = Convert.ToBoolean(command.ExecuteNonQuery());
                        if (!result)
                        {
                            response.statusCode = Constant.ERROR_CODE_SYSTEM;
                            response.statusMessage = "Execute query failed";
                        }
                    }
                }
            }
            catch (SqlException sqlex)
            {
                if (sqlex.Number == Constant.ERROR_CODE_CONNECTION)
                {
                    response.statusCode = Constant.ERR_CODE_5050;
                    response.statusMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5050);
                }
                else
                {
                    response.statusCode = Constant.ERROR_CODE_SYSTEM;
                    response.statusMessage = sqlex.Message;
                }
            }
            return response;
        }

        public ServiceResponse SaveMigrationActivityMonitoring(string branchCode, string officeCode, DataTable dtDbName)
        {
            ServiceResponse response = new ServiceResponse();
            bool result = false;
            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "[dbo].[usp_SaveMigrationActivityMonitoring]";
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandTimeout = this.GetCommandTimeOut();
                        command.Parameters.AddWithValue("@BranchCode", branchCode);
                        command.Parameters.AddWithValue("@OfficeCode", officeCode);
                        command.Parameters.AddWithValue("@DtDbName", dtDbName);

                        result = Convert.ToBoolean(command.ExecuteNonQuery());
                        if (!result)
                        {
                            response.statusCode = Constant.ERROR_CODE_SYSTEM;
                            response.statusMessage = "Execute query failed";
                        }
                    }
                }
            }
            catch (SqlException sqlex)
            {
                if (sqlex.Number == Constant.ERROR_CODE_CONNECTION)
                {
                    response.statusCode = Constant.ERR_CODE_5050;
                    response.statusMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5050);
                }
                else
                {
                    response.statusCode = Constant.ERROR_CODE_SYSTEM;
                    response.statusMessage = sqlex.Message;
                }
            }
            return response;
        }

        public ServiceResponse ValidateExtractFileIsCompleted(string branchCode, string officeCode, ref bool result)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "[dbo].[usp_ValidateExtractFileIsCompleted]";
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandTimeout = this.GetCommandTimeOut();
                        command.Parameters.AddWithValue("@BranchCode", branchCode);
                        command.Parameters.AddWithValue("@OfficeCode", officeCode);
                        result = Convert.ToBoolean(command.ExecuteScalar());
                    }
                }
            }
            catch (SqlException sqlex)
            {
                if (sqlex.Number == Constant.ERROR_CODE_CONNECTION)
                {
                    response.statusCode = Constant.ERR_CODE_5050;
                    response.statusMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5050);
                }
                else
                {
                    response.statusCode = Constant.ERROR_CODE_SYSTEM;
                    response.statusMessage = sqlex.Message;
                }
            }
            return response;
        }
        #endregion

        #region for bioagent
        public ErrorInfo GetUserIdByBdsId(string bdsId, ref string userIdOut)
        {
            ErrorInfo errInfo = new ErrorInfo();
            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "[dbo].[usp_GetUserIdbyBdsId]";
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandTimeout = this.GetCommandTimeOut();
                        command.Parameters.AddWithValue("@BdsId", bdsId);
                        command.Parameters.AddWithValue("@SecretKey", Utility.GenerateIVTKeySalt());
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                userIdOut = reader["UserId"] != DBNull.Value ? reader["UserId"].ToString() : string.Empty;
                            }
                        }
                    }
                }

            }
            catch (System.Exception ex)
            {
                errInfo.Code = Constant.ERROR_CODE_SYSTEM;
                errInfo.Message = ex.Message;
            }
            return errInfo;
        }

        public ErrorInfo SaveAttendance(FSAttendance attendance)
        {
            ErrorInfo errInfo = new ErrorInfo();
            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand("[dbo].[usp_SaveAttendance]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = this.GetCommandTimeOut();
                        command.Parameters.AddWithValue("@FsCentralId", attendance.FSCentralId);
                        command.Parameters.AddWithValue("@BranchCode", attendance.BranchCode);
                        command.Parameters.AddWithValue("@OfficeCode", attendance.OfficeCode);
                        command.Parameters.AddWithValue("@AttendanceStatus", attendance.AttendanceStatus);
                        command.Parameters.AddWithValue("@AttendanceDate", attendance.AttendanceDate);
                        command.Parameters.AddWithValue("@FingerIndex", attendance.FingerIndex);
                        command.Parameters.AddWithValue("@FingerName", attendance.FingerName);
                        command.Parameters.AddWithValue("@Score", attendance.Score);
                        command.Parameters.AddWithValue("@IpAddressType", attendance.SourceIpType);
                        command.Parameters.AddWithValue("@IpAddress", attendance.SourceIp);
                        command.Parameters.AddWithValue("@SourceName", attendance.SourceName);
                        command.Parameters.AddWithValue("@SecretKey", Utility.GenerateIVTKeySalt());
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            return GetErrorInfoMapper(reader);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                errInfo.Code = Constant.ERROR_CODE_SYSTEM;
                errInfo.Message = ex.Message;
            }
            return errInfo;
        }

        public bool ValidateAttendanceEffectivePeriod(int centralId, string attBranchCode, string attOfficeCode)
        {

            bool result = false;

            try
            {

                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {

                    connection.Open();
                    using (SqlCommand command = new SqlCommand())
                    {

                        command.Connection = connection;

                        command.CommandText = "[dbo].[usp_ValidateAttendanceEffectivePeriod]";

                        command.CommandType = System.Data.CommandType.StoredProcedure;

                        command.CommandTimeout = this.GetCommandTimeOut();

                        command.Parameters.AddWithValue("@CentralId", centralId);

                        command.Parameters.AddWithValue("@AttendanceBranchCode", attBranchCode);

                        command.Parameters.AddWithValue("@AttendanceOfficeCode", attOfficeCode);

                        result = Convert.ToBoolean(command.ExecuteScalar());
                    }
                }
                return result;

            }

            catch (System.Exception ex)
            {

                throw ex;

            }

        }
        #endregion

        #endregion

        #region private method(s)
        private int generateId()
        {
            return id += 1;
        }
        private Framework.Entity.ErrorInfo GetErrorInfoMapper(SqlDataReader reader)
        {
            ErrorInfo _erroInfo = new ErrorInfo();
            while (reader.Read())
            {
                _erroInfo.Code = reader["ErrorNumber"] != DBNull.Value ? Convert.ToInt32(reader["ErrorNumber"]) : 0;
                _erroInfo.Message = reader["ErrorMessage"] != DBNull.Value ? reader["ErrorMessage"].ToString() : string.Empty;
            }
            return _erroInfo;
        }
        private ServiceResponse GetResponseMapper(SqlDataReader reader)
        {
            ServiceResponse oResponse = new ServiceResponse();
            while (reader.Read())
            {
                oResponse.statusCode = reader["ErrorNumber"] != DBNull.Value ? Convert.ToInt32(reader["ErrorNumber"]) : 0;
                oResponse.statusMessage = reader["ErrorMessage"] != DBNull.Value ? reader["ErrorMessage"].ToString() : string.Empty;
            }
            return oResponse;
        }

        private bool RecalculateCKVStatus(FSAllowedApps allowedApps)
        {
            bool ckvStatus = false;
            string[] strCkv = { allowedApps.FsAllowedAppsId.ToString(), allowedApps.AppId, allowedApps.AppName, allowedApps.AppVersion, allowedApps.IsDelete.ToString() };
            int recalculateCkv = Utility.GenerateCKV(strCkv);
            if (allowedApps.CKV != recalculateCkv)
            {
                ckvStatus = false;
            }
            else
            {
                ckvStatus = true;
            }
            return ckvStatus;
        }

        #endregion

        #region backup code
        public ServiceResponse SaveActivityReceiveZip(ClientInfo clientInfo, List<ActivityLocalFileInfo> listFileInfo)
        {

            ServiceResponse response = new ServiceResponse();
            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand("[dbo].[usp_SaveActivityReceiveZipFile]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@dtFileActivity", listFileInfo.ToDataTable());
                        command.Parameters.AddWithValue("@BranchCode", clientInfo.BranchCode);
                        command.Parameters.AddWithValue("@OfficeCode", clientInfo.OfficeCode);
                        command.Parameters.AddWithValue("@SourceIpType", clientInfo.IPAddressType);
                        command.Parameters.AddWithValue("@SourceIp", clientInfo.IPAddress);
                        command.Parameters.AddWithValue("@SoureName", clientInfo.HostName);
                        command.CommandTimeout = this.GetCommandTimeOut();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            return GetResponseMapper(reader);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;
            }

            return response;
        }

        public DataTable ExtractDataActivityLocal(string dbFilePath)
        {
            try
            {
                string constring = string.Format("Data Source={0}", dbFilePath);
                DataTable dt = new DataTable();
                dt.Columns.Add("FsActivityId", typeof(int));
                dt.Columns.Add("EventTime", typeof(string));
                dt.Columns.Add("EventId", typeof(int));
                dt.Columns.Add("UserId", typeof(string));
                dt.Columns.Add("FingerIndex", typeof(int));
                dt.Columns.Add("Score", typeof(int));
                dt.Columns.Add("Status", typeof(int));
                dt.Columns.Add("SysErrorCode", typeof(int));
                dt.Columns.Add("Remark", typeof(string));
                dt.Columns.Add("SourceIpType", typeof(int));
                dt.Columns.Add("SourceIp", typeof(string));
                dt.Columns.Add("SourceName", typeof(string));
                dt.Columns.Add("ckv", typeof(int));

                using (SQLiteConnection sqlitecon = new SQLiteConnection(constring))
                {
                    sqlitecon.Open();
                    string query = "SELECT * FROM FsActivity";
                    using (var cmd = new SQLiteCommand(query, sqlitecon))
                    {
                        using (SQLiteDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                DataRow dr = dt.NewRow();
                                dr["FsActivityId"] = reader["FsActivityId"] != DBNull.Value ? Convert.ToInt32(reader["FsActivityId"]) : 0;
                                long evtTime = reader["EventTime"] != DBNull.Value ? Convert.ToInt64(reader["EventTime"]) : 0;
                                string strEvtTime = Utility.Epoch2String(evtTime);
                                dr["EventTime"] = strEvtTime;
                                dr["EventId"] = reader["EventId"] != DBNull.Value ? Convert.ToInt32(reader["EventId"]) : 0;
                                dr["UserId"] = reader["UserId"] != DBNull.Value ? reader["UserId"].ToString() : string.Empty;
                                dr["FingerIndex"] = reader["FingerIndex"] != DBNull.Value ? Convert.ToInt32(reader["FingerIndex"]) : 0;
                                dr["Score"] = reader["Score"] != DBNull.Value ? Convert.ToInt32(reader["Score"]) : 0;
                                dr["Status"] = reader["Status"] != DBNull.Value ? Convert.ToInt32(reader["Status"]) : 0;
                                dr["SysErrorCode"] = reader["SysErrorCode"] != DBNull.Value ? Convert.ToInt32(reader["SysErrorCode"]) : 0;
                                dr["Remark"] = reader["Remark"] != DBNull.Value ? reader["Remark"].ToString() : string.Empty;
                                dr["SourceIpType"] = reader["SourceIpType"] != DBNull.Value ? Convert.ToInt32(reader["SourceIpType"]) : 0;
                                dr["SourceIp"] = reader["SourceIp"] != DBNull.Value ? reader["SourceIp"].ToString() : string.Empty;
                                dr["SourceName"] = reader["SourceName"] != DBNull.Value ? reader["SourceName"].ToString() : string.Empty;
                                dr["ckv"] = reader["ckv"] != DBNull.Value ? Convert.ToInt32(reader["ckv"]) : 0;
                                dt.Rows.Add(dr);
                            }
                        }
                    }
                }
                return dt;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }

        public ServiceResponse SaveActivityLocalToActivityCentral(DataTable dbActivityLocal, ClientInfo clientInfo)
        {

            ServiceResponse response = new ServiceResponse();
            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand("[dbo].[usp_SaveMigrationActivityLocalToActivityCentral]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@dtActivityLocal", dbActivityLocal);
                        command.Parameters.AddWithValue("@BranchCode", clientInfo.BranchCode);
                        command.Parameters.AddWithValue("@OfficeCode", clientInfo.OfficeCode);
                        command.Parameters.AddWithValue("@SourceIp", clientInfo.IPAddress);
                        command.Parameters.AddWithValue("@SourceIpType", clientInfo.IPAddressType);
                        command.CommandTimeout = this.GetCommandTimeOut();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            return GetResponseMapper(reader);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;
            }
            return response;
        }

        public ServiceResponse ExtractDataAndSendingToCentralDB_FORBDS(string dbFilePath, ClientInfo clientInfo)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                using (SqlConnection sqlsvrcon = new SqlConnection(this.GetConnectionString()))
                {
                    sqlsvrcon.Open();

                    string query = "INSERT INTO dbo.FsActivity ";
                    query += "(ActivityActionName,ActivityName,ActivityDate,ActivityMessage,ActivityUserAddress,ActivityUserAddressType,ActivityHostName,ActivityUserName,ActivityBranchCode,ActivityOfficeCode,ApplicationRequestName,ApplicationRequestVersion,ApplicationLoginId) ";
                    query += "VALUES (@actionName,@activityName,@activityDater,@activityMessage,@ipAddress,@ipAddressType,@hostName,@userName,@branchCode,@officeCode,@appName,@appVersion,@logonId);";


                    using (var sqlsvrCommand = sqlsvrcon.CreateCommand())
                    {
                        sqlsvrCommand.CommandText = query;

                        var actionNameParam = sqlsvrCommand.CreateParameter();
                        actionNameParam.ParameterName = "@actionName";
                        sqlsvrCommand.Parameters.Add(actionNameParam);

                        var activityNameParam = sqlsvrCommand.CreateParameter();
                        activityNameParam.ParameterName = "@activityName";
                        sqlsvrCommand.Parameters.Add(activityNameParam);

                        var activityDateParam = sqlsvrCommand.CreateParameter();
                        activityDateParam.ParameterName = "@activityDater";
                        sqlsvrCommand.Parameters.Add(activityDateParam);

                        var messageParam = sqlsvrCommand.CreateParameter();
                        messageParam.ParameterName = "@activityMessage";
                        sqlsvrCommand.Parameters.Add(messageParam);

                        var ipAddressParam = sqlsvrCommand.CreateParameter();
                        ipAddressParam.ParameterName = "@ipAddress";
                        sqlsvrCommand.Parameters.Add(ipAddressParam);

                        var ipAddressTypeParam = sqlsvrCommand.CreateParameter();
                        ipAddressTypeParam.ParameterName = "@ipAddressType";
                        sqlsvrCommand.Parameters.Add(ipAddressTypeParam);

                        var hostNameParam = sqlsvrCommand.CreateParameter();
                        hostNameParam.ParameterName = "@hostName";
                        sqlsvrCommand.Parameters.Add(hostNameParam);

                        var userNameParam = sqlsvrCommand.CreateParameter();
                        userNameParam.ParameterName = "@userName";
                        sqlsvrCommand.Parameters.Add(userNameParam);

                        var branchCodeParam = sqlsvrCommand.CreateParameter();
                        branchCodeParam.ParameterName = "@branchCode";
                        sqlsvrCommand.Parameters.Add(branchCodeParam);

                        var officeCodeParam = sqlsvrCommand.CreateParameter();
                        officeCodeParam.ParameterName = "@officeCode";
                        sqlsvrCommand.Parameters.Add(officeCodeParam);

                        var appNameParam = sqlsvrCommand.CreateParameter();
                        appNameParam.ParameterName = "@appName";
                        sqlsvrCommand.Parameters.Add(appNameParam);

                        var appVersionParam = sqlsvrCommand.CreateParameter();
                        appVersionParam.ParameterName = "@appVersion";
                        sqlsvrCommand.Parameters.Add(appVersionParam);

                        var logonIdParam = sqlsvrCommand.CreateParameter();
                        logonIdParam.ParameterName = "@logonId";
                        sqlsvrCommand.Parameters.Add(logonIdParam);

                        using (var transaction = sqlsvrcon.BeginTransaction())
                        {
                            string constring = string.Format("Data Source={0}", dbFilePath);
                            using (SQLiteConnection sqlitecon = new SQLiteConnection(constring))
                            {
                                sqlitecon.Open();
                                using (var sqliteCmd = sqlitecon.CreateCommand())
                                {
                                    sqliteCmd.CommandType = CommandType.Text;
                                    sqliteCmd.CommandText = "SELECT EventId,UserId,EventTime,SourceIpType,SourceIp,SourceName,ActivityMessage FROM FsActivity WHERE EventId = 300";
                                    sqliteCmd.CommandTimeout = 500000;

                                    using (SQLiteDataReader sqliteReader = sqliteCmd.ExecuteReader())
                                    {
                                        while (sqliteReader.Read())
                                        {
                                            DateTime activityDate = sqliteReader["EventTime"] != DBNull.Value ? Convert.ToDateTime(sqliteReader["EventTime"]) : DateTime.MinValue;
                                            string message = sqliteReader["activitymessage"] != DBNull.Value ? sqliteReader["activitymessage"].ToString() : string.Empty;
                                            string ipAddress = sqliteReader["sourceip"] != DBNull.Value ? sqliteReader["sourceip"].ToString() : string.Empty;
                                            int ipAddressType = sqliteReader["sourceiptype"] != DBNull.Value ? Convert.ToInt16(sqliteReader["sourceiptype"]) : 0;
                                            string hostName = sqliteReader["sourcename"] != DBNull.Value ? sqliteReader["sourcename"].ToString() : string.Empty;
                                            string userName = sqliteReader["userid"] != DBNull.Value ? sqliteReader["userid"].ToString() : string.Empty;
                                            string logonId = sqliteReader["userid"] != DBNull.Value ? sqliteReader["userid"].ToString() : string.Empty;

                                            actionNameParam.Value = "FingerVerify";
                                            activityNameParam.Value = "FingerVerify";
                                            activityDateParam.Value = activityDate;
                                            messageParam.Value = message;
                                            ipAddressParam.Value = ipAddress;
                                            ipAddressTypeParam.Value = ipAddressType;
                                            hostNameParam.Value = hostName;
                                            userNameParam.Value = userName;
                                            logonIdParam.Value = logonId;
                                            branchCodeParam.Value = clientInfo.BranchCode;
                                            officeCodeParam.Value = clientInfo.OfficeCode;
                                            appNameParam.Value = "BDS-IBS";
                                            appVersionParam.Value = "2.1.0.249";

                                            sqlsvrCommand.CommandTimeout = 50000;
                                            sqlsvrCommand.Connection = sqlsvrcon;
                                            sqlsvrCommand.ExecuteNonQuery();

                                        }
                                    }
                                }

                            }
                            transaction.Commit();
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;
            }
            return response;
        }

        private string MapperQueryForTest(SQLiteDataReader sqliteReader, ClientInfo clientInfo)
        {
            int evtId = sqliteReader["EventId"] != DBNull.Value ? Convert.ToInt32(sqliteReader["EventId"]) : 0;
            string userId = sqliteReader["UserId"] != DBNull.Value ? sqliteReader["UserId"].ToString() : string.Empty;
            string strEvtTime = sqliteReader["EventTime"] != DBNull.Value ? sqliteReader["EventTime"].ToString() : string.Empty;
            int fingerIndex = sqliteReader["FingerIndex"] != DBNull.Value ? Convert.ToInt32(sqliteReader["FingerIndex"]) : 0;
            int score = sqliteReader["Score"] != DBNull.Value ? Convert.ToInt32(sqliteReader["Score"]) : 0;
            int sourceIpType = sqliteReader["SourceIpType"] != DBNull.Value ? Convert.ToInt32(sqliteReader["SourceIpType"]) : 0;
            string sourceIp = sqliteReader["SourceIp"] != DBNull.Value ? sqliteReader["SourceIp"].ToString() : string.Empty;
            string sourceName = sqliteReader["SourceName"] != DBNull.Value ? sqliteReader["SourceName"].ToString() : string.Empty;
            string message = sqliteReader["ActivityMessage"] != DBNull.Value ? sqliteReader["ActivityMessage"].ToString() : string.Empty;

            return string.Format("exec [dbo].[usp_SaveMigrationActivityLocalDataToCentralData] {0}, '{1}', '{2}', '{3}', '{4}', {5}, {6}, {7}, '{8}', '{9}', '{10}', {11} \n", evtId.ToString(), userId, clientInfo.BranchCode, clientInfo.OfficeCode, strEvtTime, fingerIndex.ToString(), score.ToString(), sourceIpType.ToString(), sourceIp, sourceName, message, Utility.GenerateIVTKeySalt().ToString());

        }

        public ServiceResponse ExtractDataAndSendingToCentralDB_Test(string dbFilePath, ClientInfo clientInfo)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                string constring = string.Format("Data Source={0}", dbFilePath);
                using (SQLiteConnection sqlitecon = new SQLiteConnection(constring))
                {
                    using (SQLiteCommand sqliteCmd = sqlitecon.CreateCommand())
                    {
                        sqlitecon.Open();
                        sqliteCmd.CommandType = CommandType.Text;
                        sqliteCmd.CommandText = "SELECT EventId,UserId,EventTime,FingerIndex,Score,SourceIpType,SourceIp,SourceName,ActivityMessage FROM FsActivity WHERE EventId IN (200,201,300)";
                        sqliteCmd.CommandTimeout = this.GetCommandTimeOut();

                        using (SqlConnection sqlsvrcon = new SqlConnection(this.GetConnectionString()))
                        {
                            sqlsvrcon.Open();
                            using (SQLiteDataReader sqliteReader = sqliteCmd.ExecuteReader())
                            {
                                string strProcedure = string.Empty;
                                while (sqliteReader.Read())
                                {
                                    strProcedure += MapperQueryForTest(sqliteReader, clientInfo);
                                }
                                if (!string.IsNullOrEmpty(strProcedure))
                                {
                                    //InsertDataLocalToDataCentral(sqlsvrcon, strProcedure, GetCommandTimeOut());
                                }

                            }
                        }

                    }
                }
            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;
            }
            return response;
        }

        public ServiceResponse ExtractDataAndSendingToCentralDB(string dbFilePath, ClientInfo clientInfo)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                string constring = string.Format("Data Source={0}", dbFilePath);
                using (SQLiteConnection sqlitecon = new SQLiteConnection(constring))
                {
                    using (SQLiteCommand sqliteCmd = sqlitecon.CreateCommand())
                    {
                        sqlitecon.Open();
                        sqliteCmd.CommandType = CommandType.Text;
                        sqliteCmd.CommandText = "SELECT EventId,UserId,EventTime,FingerIndex,Score,SourceIpType,SourceIp,SourceName,ActivityMessage FROM FsActivity WHERE EventId IN (200,201,300)";
                        sqliteCmd.CommandTimeout = this.GetCommandTimeOut();

                        using (SqlConnection sqlsvrcon = new SqlConnection(this.GetConnectionString()))
                        {
                            sqlsvrcon.Open();
                            using (var transaction = sqlsvrcon.BeginTransaction())
                            {
                                try
                                {
                                    using (SQLiteDataReader sqliteReader = sqliteCmd.ExecuteReader())
                                    {
                                        while (sqliteReader.Read())
                                        {
                                            InsertDataLocalToDataCentral(sqlsvrcon, transaction, MapperStoreProcedure(sqliteReader, clientInfo), GetCommandTimeOut());
                                        }
                                    }
                                    transaction.Commit();
                                }
                                catch (System.Exception)
                                {
                                    transaction.Rollback();
                                }

                            }
                        }

                    }
                }
            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;
            }
            return response;
        }
        public ServiceResponse ExtractDataToDataTable(string dbFilePath, ClientInfo clientInfo, ref DataTable dt)
        {
            dt = new DataTable();
            dt.Columns.Add("EventTime", typeof(string));
            dt.Columns.Add("EventId", typeof(int));
            dt.Columns.Add("UserId", typeof(string));
            dt.Columns.Add("FingerIndex", typeof(int));
            dt.Columns.Add("Score", typeof(int));
            dt.Columns.Add("SourceIpType", typeof(int));
            dt.Columns.Add("SourceIp", typeof(string));
            dt.Columns.Add("SourceName", typeof(string));
            dt.Columns.Add("ActivityMessage", typeof(string));

            ServiceResponse response = new ServiceResponse();
            try
            {
                string constring = string.Format("Data Source={0}", dbFilePath);
                using (SQLiteConnection sqlitecon = new SQLiteConnection(constring))
                {
                    using (SQLiteCommand sqliteCmd = sqlitecon.CreateCommand())
                    {
                        sqlitecon.Open();
                        sqliteCmd.CommandType = CommandType.Text;
                        sqliteCmd.CommandText = "SELECT EventId,UserId,EventTime,FingerIndex,Score,SourceIpType,SourceIp,SourceName,ActivityMessage FROM FsActivity WHERE EventId IN (200,201,300)";
                        sqliteCmd.CommandTimeout = this.GetCommandTimeOut();
                        using (SQLiteDataReader sqliteReader = sqliteCmd.ExecuteReader())
                        {
                            while (sqliteReader.Read())
                            {
                                DataRow dr = dt.NewRow();
                                dr["EventTime"] = sqliteReader["EventTime"] != DBNull.Value ? sqliteReader["EventTime"].ToString() : string.Empty;
                                dr["EventId"] = sqliteReader["EventId"] != DBNull.Value ? Convert.ToInt32(sqliteReader["EventId"]) : 0;
                                dr["UserId"] = sqliteReader["UserId"] != DBNull.Value ? sqliteReader["UserId"].ToString() : string.Empty;
                                dr["FingerIndex"] = sqliteReader["FingerIndex"] != DBNull.Value ? Convert.ToInt32(sqliteReader["FingerIndex"]) : 0;
                                dr["Score"] = sqliteReader["Score"] != DBNull.Value ? Convert.ToInt32(sqliteReader["Score"]) : 0;
                                dr["SourceIpType"] = sqliteReader["SourceIpType"] != DBNull.Value ? Convert.ToInt32(sqliteReader["SourceIpType"]) : 0;
                                dr["SourceIp"] = sqliteReader["SourceIp"] != DBNull.Value ? sqliteReader["SourceIp"].ToString() : string.Empty;
                                dr["SourceName"] = sqliteReader["SourceName"] != DBNull.Value ? sqliteReader["SourceName"].ToString() : string.Empty;
                                dr["ActivityMessage"] = sqliteReader["SourceName"] != DBNull.Value ? sqliteReader["SourceName"].ToString() : string.Empty;
                                dt.Rows.Add(dr);
                            }
                        }

                    }
                }
            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;
            }
            return response;
        }
        public ServiceResponse BulkCopyActivityLocalToTemporary(DataTable dt)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        try
                        {
                            //using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.KeepIdentity,
                            //   transaction))
                            //{
                            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(connection))
                            {
                                sqlBulkCopy.BulkCopyTimeout = this.GetCommandTimeOut();
                                sqlBulkCopy.DestinationTableName = "dbo.FsTemporaryActivity";

                                sqlBulkCopy.WriteToServer(dt);
                            }
                            transaction.Commit();
                        }
                        catch (System.Exception)
                        {
                            transaction.Rollback();
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;
            }
            return response;
        }
        private void InsertDataLocalToDataCentral(SqlConnection sqlsvrcon, SqlTransaction transaction, string storeProcedure, int commandTimeOut)
        {
            try
            {
                using (SqlCommand sqlsvrCmd = sqlsvrcon.CreateCommand())
                {
                    sqlsvrCmd.CommandType = CommandType.Text;
                    sqlsvrCmd.CommandTimeout = commandTimeOut;
                    sqlsvrCmd.Transaction = transaction;
                    sqlsvrCmd.CommandText = storeProcedure;
                    sqlsvrCmd.ExecuteNonQuery();
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
        public DataTable ExtractDataActivityLocal(string dbFilePath, ClientInfo clientInfo)
        {
            try
            {
                string constring = string.Format("Data Source={0}", dbFilePath);
                DataTable dt = new DataTable();
                dt.Columns.Add("FsAttendanceId", typeof(int));
                dt.Columns.Add("NIP", typeof(string));
                dt.Columns.Add("BranchCode", typeof(string));
                dt.Columns.Add("OfficeCode", typeof(string));
                dt.Columns.Add("AttendanceStatus", typeof(string));
                dt.Columns.Add("AttendanceDate", typeof(DateTime));
                dt.Columns.Add("FingerIndex", typeof(int));
                dt.Columns.Add("Score", typeof(int));
                dt.Columns.Add("SourceIpType", typeof(int));
                dt.Columns.Add("SourceIp", typeof(string));
                dt.Columns.Add("SourceName", typeof(string));
                dt.Columns.Add("CreatedDate", typeof(DateTime));
                dt.Columns.Add("Ckv", typeof(int));

                using (SQLiteConnection sqlitecon = new SQLiteConnection(constring))
                {
                    sqlitecon.Open();
                    string query = "SELECT * FROM FsActivity";
                    using (var cmd = new SQLiteCommand(query, sqlitecon))
                    {
                        cmd.CommandTimeout = this.GetCommandTimeOut();
                        using (SQLiteDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                DataRow dr = dt.NewRow();
                                dr["FsAttendanceId"] = generateId();
                                dr["NIP"] = reader["UserId"] != DBNull.Value ? reader["UserId"].ToString() : string.Empty;
                                dr["BranchCode"] = clientInfo.BranchCode;
                                dr["OfficeCode"] = clientInfo.OfficeCode;
                                int attdStatus = reader["EventId"] != DBNull.Value ? Convert.ToInt32(reader["EventId"]) : 0;
                                dr["AttendanceStatus"] = (attdStatus == 200 ? 1 : 2);
                                long evtTime = reader["EventTime"] != DBNull.Value ? Convert.ToInt64(reader["EventTime"]) : 0;
                                string strEvtTime = Utility.Epoch2String(evtTime);
                                dr["AttendanceDate"] = Convert.ToDateTime(strEvtTime);
                                dr["FingerIndex"] = reader["FingerIndex"] != DBNull.Value ? Convert.ToInt32(reader["FingerIndex"]) : 0;
                                dr["Score"] = reader["Score"] != DBNull.Value ? Convert.ToInt32(reader["Score"]) : 0;
                                dr["SourceIpType"] = reader["SourceIpType"] != DBNull.Value ? Convert.ToInt32(reader["SourceIpType"]) : 0;
                                dr["SourceIp"] = reader["SourceIp"] != DBNull.Value ? reader["SourceIp"].ToString() : string.Empty;
                                dr["SourceName"] = reader["SourceName"] != DBNull.Value ? reader["SourceName"].ToString() : string.Empty;
                                dr["CreatedDate"] = DateTime.Now;
                                dr["ckv"] = reader["ckv"] != DBNull.Value ? Convert.ToInt32(reader["ckv"]) : 0;
                                dt.Rows.Add(dr);
                            }
                        }
                    }
                }
                return dt;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }
        public ServiceResponse CopyBulkActivityLocalToAttendanceCentral(DataTable dbActivityLocal)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(connection))
                    {
                        sqlBulkCopy.BulkCopyTimeout = this.GetCommandTimeOut();
                        sqlBulkCopy.DestinationTableName = "FsAttendance";
                        connection.Open();

                        sqlBulkCopy.WriteToServer(dbActivityLocal);

                        response.statusCode = Constant.NO_ERROR;
                        response.statusMessage = "Migration activity local has been succeeded";
                    }
                }
            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;
            }
            return response;
        }
        private string MapperStoreProcedure(SQLiteDataReader sqliteReader, ClientInfo clientInfo)
        {
            int evtId = sqliteReader["EventId"] != DBNull.Value ? Convert.ToInt32(sqliteReader["EventId"]) : 0;
            string userId = sqliteReader["UserId"] != DBNull.Value ? sqliteReader["UserId"].ToString() : string.Empty;
            string strEvtTime = sqliteReader["EventTime"] != DBNull.Value ? sqliteReader["EventTime"].ToString() : string.Empty;
            int fingerIndex = sqliteReader["FingerIndex"] != DBNull.Value ? Convert.ToInt32(sqliteReader["FingerIndex"]) : 0;
            int score = sqliteReader["Score"] != DBNull.Value ? Convert.ToInt32(sqliteReader["Score"]) : 0;
            int sourceIpType = sqliteReader["SourceIpType"] != DBNull.Value ? Convert.ToInt32(sqliteReader["SourceIpType"]) : 0;
            string sourceIp = sqliteReader["SourceIp"] != DBNull.Value ? sqliteReader["SourceIp"].ToString() : string.Empty;
            string sourceName = sqliteReader["SourceName"] != DBNull.Value ? sqliteReader["SourceName"].ToString() : string.Empty;
            string message = sqliteReader["ActivityMessage"] != DBNull.Value ? sqliteReader["ActivityMessage"].ToString() : string.Empty;

            return string.Format("exec [dbo].[usp_SaveMigrationActivityLocalDataToCentralData] {0}, '{1}', '{2}', '{3}', '{4}', {5}, {6}, {7}, '{8}', '{9}', '{10}', {11}", evtId.ToString(), userId, clientInfo.BranchCode, clientInfo.OfficeCode, strEvtTime, fingerIndex.ToString(), score.ToString(), sourceIpType.ToString(), sourceIp, sourceName, message, Utility.GenerateIVTKeySalt().ToString());
        }
        #endregion








    }
}