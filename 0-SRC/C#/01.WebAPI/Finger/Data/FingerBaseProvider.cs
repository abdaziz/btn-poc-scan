﻿using Finger.Framework.Common;
using Finger.Framework.Security;
using System;
using System.Configuration;
using System.Data.SqlClient;

namespace Finger.Data
{
    public class FingerBaseProvider
    {
        public string GetConnectionString()
        {
            return Cryptography.Decode(Utility.GetConfigurationValue(Constant.KEY_SQL_CONNECTION_STRING));
        }

        public string GetDatabaseName()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(Cryptography.Decode(Utility.GetConfigurationValue(Constant.KEY_SQL_CONNECTION_STRING)));
            string database = builder.InitialCatalog;
            return database;
        }

        public int GetCommandTimeOut()
        {
            return Convert.ToInt32(Utility.GetConfigurationValue(Constant.KEY_COMMAND_TIMEOUT));
        }

        public int GetRetryTimeout()
        {
            return Convert.ToInt32(Utility.GetConfigurationValue(Constant.KEY_CONNECTION_RETRY_TIMEOUT));
        }

        public int GetDelayMilisecond()
        {
            return Convert.ToInt32(Utility.GetConfigurationValue(Constant.KEY_CONNECTION_DELAY_MILISECOND));
        }


    }
}