﻿using Finger.Core.Entity;
using Finger.Entity;
using Finger.Framework.Common;
using Finger.Framework.Data;
using Finger.Framework.Entity;
using Finger.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Finger.Core.Base;
using System.Threading.Tasks;

namespace Finger.Data
{
    public partial class FingerDataProvider : FingerBaseProvider
    {
        public ErrorInfo GetUserInquery(List<UserInquery> UserInqueryCollection, PagingInfo oPagingInfo, string branchCode, string officeCode, string userLogonID, int branchType, string filterCondition)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(this.GetConnectionString()))
                {
                    using (SqlCommand oSqlCommand = oSqlConnection.CreateCommand())
                    {
                        oSqlConnection.Open();
                        oSqlCommand.CommandText = "[dbo].[usp_GetUserInquery]";
                        oSqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        oSqlCommand.CommandTimeout = this.GetCommandTimeOut();
                        oSqlCommand.Parameters.AddWithValue("@BranchCode", branchCode);
                        oSqlCommand.Parameters.AddWithValue("@OfficeCode", officeCode);
                        oSqlCommand.Parameters.AddWithValue("@UserLogon", userLogonID);
                        oSqlCommand.Parameters.AddWithValue("@IsUsedBranchHierarchy", Convert.ToBoolean(branchType));
                        oSqlCommand.Parameters.AddWithValue("@FilterCondition", filterCondition);
                        oSqlCommand.Parameters.AddWithValue("@PageNumber", oPagingInfo.Index);
                        oSqlCommand.Parameters.AddWithValue("@PageSize", oPagingInfo.PageSize);
                        oSqlCommand.Parameters.AddWithValue("@OrderBy", oPagingInfo.OrderBy);

                        using (SqlDataReader oSqlDataReader = oSqlCommand.ExecuteReader())
                        {
                            if (oSqlDataReader.HasRows && oSqlDataReader.Read())
                            {
                                int ErrorCode = oSqlDataReader["ErrorCode"] == DBNull.Value ? -1 : Convert.ToInt32(oSqlDataReader["ErrorCode"]);
                                return MappingErrorInfo(oErrorInfo, ErrorCode);
                            }
                            if (oSqlDataReader.NextResult() && oSqlDataReader.HasRows)
                            {
                                while (oSqlDataReader.Read())
                                {
                                    UserInquery oUserInquery = new UserInquery();
                                    oUserInquery.FsCentralId = oSqlDataReader["FsCentralId"] == DBNull.Value ? -1 : Convert.ToInt32(oSqlDataReader["FsCentralId"]);
                                    oUserInquery.FsLocationId = oSqlDataReader["FsLocationId"] == DBNull.Value ? -1 : Convert.ToInt32(oSqlDataReader["FsLocationId"]);
                                    oUserInquery.BranchCode = oSqlDataReader["BranchCode"] == DBNull.Value ? "-" : oSqlDataReader["BranchCode"].ToString();
                                    oUserInquery.OfficeCode = oSqlDataReader["OfficeCode"] == DBNull.Value ? "-" : oSqlDataReader["OfficeCode"].ToString();
                                    oUserInquery.UserId = oSqlDataReader["UserId"] == DBNull.Value ? "-" : oSqlDataReader["UserId"].ToString();
                                    oUserInquery.Name = oSqlDataReader["Name"] == DBNull.Value ? "-" : oSqlDataReader["Name"].ToString();
                                    oUserInquery.Reason = oSqlDataReader["Reason"] == DBNull.Value ? "-" : oSqlDataReader["Reason"].ToString();
                                    oUserInquery.ApprovedBy = oSqlDataReader["ApprovedBy"] == DBNull.Value ? "-" : oSqlDataReader["ApprovedBy"].ToString();
                                    oUserInquery.StartPeriod = oSqlDataReader["StartPeriod"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(oSqlDataReader["StartPeriod"]);
                                    oUserInquery.EndPeriod = oSqlDataReader["EndPeriod"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(oSqlDataReader["EndPeriod"]);
                                    oUserInquery.UserCategory = oSqlDataReader["Category"] == DBNull.Value ? "-" : oSqlDataReader["Category"].ToString();
                                    oUserInquery.CategoryId = oSqlDataReader["CategoryId"] == DBNull.Value ? (short)-1 : Convert.ToInt16(oSqlDataReader["CategoryId"]);
                                    oUserInquery.UserGroupId = oSqlDataReader["UserGroupId"] == DBNull.Value ? (short)-1 : Convert.ToInt16(oSqlDataReader["UserGroupId"]);
                                    oUserInquery.UserGroup = oSqlDataReader["UserGroup"] == DBNull.Value ? "-" : oSqlDataReader["UserGroup"].ToString();
                                    oUserInquery.UserStatus = oSqlDataReader["UserStatus"] == DBNull.Value ? "-" : oSqlDataReader["UserStatus"].ToString();
                                    oUserInquery.Shift = oSqlDataReader["Shift"] == DBNull.Value ? "-" : oSqlDataReader["Shift"].ToString();
                                    oUserInquery.Quality = oSqlDataReader["Quality"] == DBNull.Value ? (short)0 : Convert.ToInt16(oSqlDataReader["Quality"]);
                                    oUserInquery.UserType = oSqlDataReader["UserType"] == DBNull.Value ? "-" : oSqlDataReader["UserType"].ToString();
                                    oUserInquery.AlihDayaCode = oSqlDataReader["AlihDayaCode"] == DBNull.Value ? "-" : oSqlDataReader["AlihDayaCode"].ToString();
                                    oUserInquery.BDS1 = oSqlDataReader["BDS1"] == DBNull.Value ? "-" : oSqlDataReader["BDS1"].ToString();
                                    oUserInquery.BDS2 = oSqlDataReader["BDS2"] == DBNull.Value ? "-" : oSqlDataReader["BDS2"].ToString();
                                    oUserInquery.BDS3 = oSqlDataReader["BDS3"] == DBNull.Value ? "-" : oSqlDataReader["BDS3"].ToString();
                                    oUserInquery.BDS4 = oSqlDataReader["BDS4"] == DBNull.Value ? "-" : oSqlDataReader["BDS4"].ToString();
                                    oUserInquery.BDS5 = oSqlDataReader["BDS5"] == DBNull.Value ? "-" : oSqlDataReader["BDS5"].ToString();
                                    oUserInquery.BDS6 = oSqlDataReader["BDS6"] == DBNull.Value ? "-" : oSqlDataReader["BDS6"].ToString();
                                    oUserInquery.BDS7 = oSqlDataReader["BDS7"] == DBNull.Value ? "-" : oSqlDataReader["BDS7"].ToString();
                                    oUserInquery.BDS8 = oSqlDataReader["BDS8"] == DBNull.Value ? "-" : oSqlDataReader["BDS8"].ToString();
                                    oUserInquery.BDS9 = oSqlDataReader["BDS9"] == DBNull.Value ? "-" : oSqlDataReader["BDS9"].ToString();
                                    oUserInquery.BDS10 = oSqlDataReader["BDS10"] == DBNull.Value ? "-" : oSqlDataReader["BDS10"].ToString();
                                    UserInqueryCollection.Add(oUserInquery);
                                }
                            }
                            if (oSqlDataReader.NextResult() && oSqlDataReader.Read())
                            {
                                oPagingInfo.TotalPage = oSqlDataReader["TotalPage"] == DBNull.Value ? 1 : Convert.ToInt32(oSqlDataReader["TotalPage"]);
                            }
                        }
                    }
                }
                if (UserInqueryCollection.Count == 0)
                {
                    oErrorInfo.Code = Constant.ERR_CODE_6000;
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_6000);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return oErrorInfo;
        }

        public ErrorInfo GetUserInquery(List<UserInquery> UserInqueryCollection, DataTable Users)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(this.GetConnectionString()))
                {
                    using (SqlCommand oSqlCommand = oSqlConnection.CreateCommand())
                    {
                        oSqlConnection.Open();
                        oSqlCommand.CommandText = "[dbo].[usp_GetUserInqueryByUserId]";
                        oSqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        oSqlCommand.CommandTimeout = this.GetCommandTimeOut();
                        oSqlCommand.Parameters.AddWithValue("@udtUsers", Users).SqlDbType = SqlDbType.Structured;

                        using (SqlDataReader oSqlDataReader = oSqlCommand.ExecuteReader())
                        {
                            if (oSqlDataReader.HasRows)
                            {
                                while (oSqlDataReader.Read())
                                {
                                    UserInquery oUserInquery = new UserInquery();
                                    oUserInquery.FsCentralId = oSqlDataReader["FsCentralId"] == DBNull.Value ? -1 : Convert.ToInt32(oSqlDataReader["FsCentralId"]);
                                    oUserInquery.FsLocationId = oSqlDataReader["FsLocationId"] == DBNull.Value ? -1 : Convert.ToInt32(oSqlDataReader["FsLocationId"]);
                                    oUserInquery.BranchCode = oSqlDataReader["BranchCode"] == DBNull.Value ? "-" : oSqlDataReader["BranchCode"].ToString();
                                    oUserInquery.OfficeCode = oSqlDataReader["OfficeCode"] == DBNull.Value ? "-" : oSqlDataReader["OfficeCode"].ToString();
                                    oUserInquery.UserId = oSqlDataReader["UserId"] == DBNull.Value ? "-" : oSqlDataReader["UserId"].ToString();
                                    oUserInquery.Name = oSqlDataReader["Name"] == DBNull.Value ? "-" : oSqlDataReader["Name"].ToString();
                                    oUserInquery.Reason = oSqlDataReader["Reason"] == DBNull.Value ? "-" : oSqlDataReader["Reason"].ToString();
                                    oUserInquery.ApprovedBy = oSqlDataReader["ApprovedBy"] == DBNull.Value ? "-" : oSqlDataReader["ApprovedBy"].ToString();
                                    oUserInquery.StartPeriod = oSqlDataReader["StartPeriod"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(oSqlDataReader["StartPeriod"]);
                                    oUserInquery.EndPeriod = oSqlDataReader["EndPeriod"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(oSqlDataReader["EndPeriod"]);
                                    oUserInquery.UserCategory = oSqlDataReader["Category"] == DBNull.Value ? "-" : oSqlDataReader["Category"].ToString();
                                    oUserInquery.CategoryId = oSqlDataReader["CategoryId"] == DBNull.Value ? (short)-1 : Convert.ToInt16(oSqlDataReader["CategoryId"]);
                                    oUserInquery.UserGroupId = oSqlDataReader["UserGroupId"] == DBNull.Value ? (short)-1 : Convert.ToInt16(oSqlDataReader["UserGroupId"]);
                                    oUserInquery.UserGroup = oSqlDataReader["UserGroup"] == DBNull.Value ? "-" : oSqlDataReader["UserGroup"].ToString();
                                    oUserInquery.UserStatus = oSqlDataReader["UserStatus"] == DBNull.Value ? "-" : oSqlDataReader["UserStatus"].ToString();
                                    oUserInquery.Shift = oSqlDataReader["Shift"] == DBNull.Value ? "-" : oSqlDataReader["Shift"].ToString();
                                    oUserInquery.Quality = oSqlDataReader["Quality"] == DBNull.Value ? (short)0 : Convert.ToInt16(oSqlDataReader["Quality"]);
                                    oUserInquery.UserType = oSqlDataReader["UserType"] == DBNull.Value ? "-" : oSqlDataReader["UserType"].ToString();
                                    oUserInquery.AlihDayaCode = oSqlDataReader["AlihDayaCode"] == DBNull.Value ? "-" : oSqlDataReader["AlihDayaCode"].ToString();
                                    oUserInquery.BDS1 = oSqlDataReader["BDS1"] == DBNull.Value ? "-" : oSqlDataReader["BDS1"].ToString();
                                    oUserInquery.BDS2 = oSqlDataReader["BDS2"] == DBNull.Value ? "-" : oSqlDataReader["BDS2"].ToString();
                                    oUserInquery.BDS3 = oSqlDataReader["BDS3"] == DBNull.Value ? "-" : oSqlDataReader["BDS3"].ToString();
                                    oUserInquery.BDS4 = oSqlDataReader["BDS4"] == DBNull.Value ? "-" : oSqlDataReader["BDS4"].ToString();
                                    oUserInquery.BDS5 = oSqlDataReader["BDS5"] == DBNull.Value ? "-" : oSqlDataReader["BDS5"].ToString();
                                    oUserInquery.BDS6 = oSqlDataReader["BDS6"] == DBNull.Value ? "-" : oSqlDataReader["BDS6"].ToString();
                                    oUserInquery.BDS7 = oSqlDataReader["BDS7"] == DBNull.Value ? "-" : oSqlDataReader["BDS7"].ToString();
                                    oUserInquery.BDS8 = oSqlDataReader["BDS8"] == DBNull.Value ? "-" : oSqlDataReader["BDS8"].ToString();
                                    oUserInquery.BDS9 = oSqlDataReader["BDS9"] == DBNull.Value ? "-" : oSqlDataReader["BDS9"].ToString();
                                    oUserInquery.BDS10 = oSqlDataReader["BDS10"] == DBNull.Value ? "-" : oSqlDataReader["BDS10"].ToString();
                                    UserInqueryCollection.Add(oUserInquery);
                                }
                            }
                            else
                            {
                                oErrorInfo.Code = Constant.ERR_CODE_6000;
                                oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_6000);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return oErrorInfo;
        }

        public ErrorInfo DeleteUser(DataTable dtUsers, List<UserInquery> lUsers, ClientInfo oClientInfo)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(this.GetConnectionString()))
                {
                    oSqlConnection.Open();

                    Utility.WriteLog(new LogInfo(string.Format("Start validate : {0}", DateTime.Now.ToString("HH:mm:ss")), oClientInfo.NewGuid, oClientInfo.AppName, oClientInfo.AppVersion, Constant.LEVEL_DEBUG, Constant.FINGER_SERVICE));
                    if (!ValidasiDeleteUser(oErrorInfo, dtUsers, oClientInfo.LogonID, oSqlConnection))
                        return oErrorInfo;

                    Utility.WriteLog(new LogInfo(string.Format("End validate : {0}", DateTime.Now.ToString("HH:mm:ss")), oClientInfo.NewGuid, oClientInfo.AppName, oClientInfo.AppVersion, Constant.LEVEL_DEBUG, Constant.FINGER_SERVICE));

                    Utility.WriteLog(new LogInfo(string.Format("Start delete user on server : {0}", DateTime.Now.ToString("HH:mm:ss")), oClientInfo.NewGuid, oClientInfo.AppName, oClientInfo.AppVersion, Constant.LEVEL_DEBUG, Constant.FINGER_SERVICE));
                    using (SqlCommand oSqlCommand = oSqlConnection.CreateCommand())
                    {
                        oSqlCommand.CommandText = "[dbo].[usp_DeleteUserV3]";
                        oSqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        oSqlCommand.CommandTimeout = this.GetCommandTimeOut();
                        oSqlCommand.Parameters.AddWithValue("@SourceIpType", oClientInfo.IPAddressType);
                        oSqlCommand.Parameters.AddWithValue("@SourceIp", oClientInfo.IPAddress);
                        oSqlCommand.Parameters.AddWithValue("@SourceName", oClientInfo.HostName);
                        oSqlCommand.Parameters.AddWithValue("@UserLogon", oClientInfo.LogonID);
                        oSqlCommand.Parameters.AddWithValue("@udtUsers", dtUsers).SqlDbType = SqlDbType.Structured;
                        oSqlCommand.ExecuteNonQuery();
                    }
                    Utility.WriteLog(new LogInfo(string.Format("End delete user on server : {0}", DateTime.Now.ToString("HH:mm:ss")), oClientInfo.NewGuid, oClientInfo.AppName, oClientInfo.AppVersion, Constant.LEVEL_DEBUG, Constant.FINGER_SERVICE));

                    if (!DeleteFingerTemplateOnServer(dtUsers, lUsers, oClientInfo, oErrorInfo))
                    {
                        using (SqlCommand oSqlCommand = oSqlConnection.CreateCommand())
                        {
                            oSqlCommand.CommandText = "[dbo].[usp_DeleteUserV3]";
                            oSqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                            oSqlCommand.CommandTimeout = this.GetCommandTimeOut();
                            oSqlCommand.Parameters.AddWithValue("@SourceIpType", oClientInfo.IPAddressType);
                            oSqlCommand.Parameters.AddWithValue("@SourceIp", oClientInfo.IPAddress);
                            oSqlCommand.Parameters.AddWithValue("@SourceName", oClientInfo.HostName);
                            oSqlCommand.Parameters.AddWithValue("@UserLogon", oClientInfo.LogonID);
                            oSqlCommand.Parameters.AddWithValue("@udtUsers", dtUsers).SqlDbType = SqlDbType.Structured;
                            oSqlCommand.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return oErrorInfo;
        }

        public ErrorInfo RegUserMutation(UserMutation oUserMutation, string userLogonId)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(this.GetConnectionString()))
                {
                    using (SqlCommand oSqlCommand = oSqlConnection.CreateCommand())
                    {
                        oSqlConnection.Open();
                        oSqlCommand.CommandText = "[dbo].[usp_RegUserMutation]";
                        oSqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        oSqlCommand.CommandTimeout = this.GetCommandTimeOut();
                        oSqlCommand.Parameters.AddWithValue("@UserId", oUserMutation.UserId);
                        oSqlCommand.Parameters.AddWithValue("@DestBranchCode", oUserMutation.DestBranchCode);
                        oSqlCommand.Parameters.AddWithValue("@DestOfficeCode", oUserMutation.DestOfficeCode);
                        oSqlCommand.Parameters.AddWithValue("@EffectiveDate", oUserMutation.EffectiveDate);
                        oSqlCommand.Parameters.AddWithValue("@UserLogonId", userLogonId);

                        using (SqlDataReader oSqlDataReader = oSqlCommand.ExecuteReader())
                        {
                            if (oSqlDataReader.HasRows && oSqlDataReader.Read())
                            {
                                int ErrorCode = oSqlDataReader["ErrorCode"] == DBNull.Value ? -1 : Convert.ToInt32(oSqlDataReader["ErrorCode"]);
                                oErrorInfo = MappingErrorInfo(oErrorInfo, ErrorCode);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return oErrorInfo;
        }

        public ErrorInfo ResetUserLogon(int fsCentralId, string branchCode, string officeCode)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(this.GetConnectionString()))
                {
                    using (SqlCommand oSqlCommand = oSqlConnection.CreateCommand())
                    {
                        oSqlConnection.Open();
                        oSqlCommand.CommandText = "[dbo].[usp_DeleteUserLog]";
                        oSqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        oSqlCommand.CommandTimeout = this.GetCommandTimeOut();
                        oSqlCommand.Parameters.AddWithValue("@CentralId", fsCentralId);
                        oSqlCommand.Parameters.AddWithValue("@BranchCode", branchCode);
                        oSqlCommand.Parameters.AddWithValue("@OfficeCode", officeCode);
                        using (SqlDataReader oSqlDataReader = oSqlCommand.ExecuteReader())
                        {
                            if (oSqlDataReader.Read())
                            {
                                //oErrorInfo.Code = oSqlDataReader["ErrorNumber"] == DBNull.Value || Convert.ToInt32(oSqlDataReader["ErrorNumber"]) == Constant.NO_ERROR ? Constant.NO_ERROR : Constant.ERR_CODE_4004;
                                //oErrorInfo.Message = oErrorInfo.Code == Constant.NO_ERROR ? string.Empty : string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_4004), fsCentralId);
                                oErrorInfo.Code = oSqlDataReader["ErrorNumber"] != DBNull.Value ? Convert.ToInt32(oSqlDataReader["ErrorNumber"]) : 0; //bug fix uat sprint 4 18112020
                                oErrorInfo.Message = oSqlDataReader["ErrorMessage"] != DBNull.Value ? oSqlDataReader["ErrorMessage"].ToString() : string.Empty; //bug fix uat sprint 4 18112020
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return oErrorInfo;
        }

        public ErrorInfo UpdateUserMutation(DataTable Users, ClientInfo oClientInfo, Int16 mutationStatus)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(this.GetConnectionString()))
                {
                    using (SqlCommand oSqlCommand = oSqlConnection.CreateCommand())
                    {
                        oSqlConnection.Open();
                        oSqlCommand.CommandText = "[dbo].[usp_UpdateUserMutationStatus]";
                        oSqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        oSqlCommand.CommandTimeout = this.GetCommandTimeOut();
                        oSqlCommand.Parameters.AddWithValue("@UserLogon", oClientInfo.LogonID);
                        oSqlCommand.Parameters.AddWithValue("@udtUsers", Users).SqlDbType = SqlDbType.Structured;

                        using (SqlDataReader oSqlDataReader = oSqlCommand.ExecuteReader())
                        {
                            if (oSqlDataReader.HasRows && oSqlDataReader.Read())
                            {
                                int ErrorCode = oSqlDataReader["ErrorCode"] == DBNull.Value ? -1 : Convert.ToInt16(oSqlDataReader["ErrorCode"]);

                                if (ErrorCode == Constant.ERR_CODE_8003)
                                {
                                    string msg = Utility.GetErrorMessage(Constant.ERR_CODE_8003);
                                    msg = mutationStatus == (Int16)Enumeration.EnumOfMutationUser.Cancelled ? msg.Replace("{action}", "Cancel") : msg.Replace("{action}", "Accept");
                                    oErrorInfo.Code = Constant.ERR_CODE_8003;
                                    oErrorInfo.Message = msg;
                                }
                                else if (ErrorCode == Constant.ERR_CODE_8004)
                                {
                                    oErrorInfo.Code = Constant.ERR_CODE_8004;
                                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_8004);
                                }
                                else if (ErrorCode == Constant.ERR_CODE_8009)
                                {
                                    string msg = Utility.GetErrorMessage(Constant.ERR_CODE_8009);
                                    msg = mutationStatus == (Int16)Enumeration.EnumOfMutationUser.Cancelled ? msg.Replace("{action}", "Cancel") : msg.Replace("{action}", "Accept");
                                    oErrorInfo.Code = Constant.ERR_CODE_8009;
                                    oErrorInfo.Message = msg;
                                }
                                else
                                {
                                    oErrorInfo.Code = ErrorCode;
                                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_5056);
                                }
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return oErrorInfo;
        }

        public ErrorInfo GetUserMutationInquery(List<UserMutation> Users, PagingInfo oPagingInfo, ClientInfo oClientInfo, string branchCode, string officeCode, string userLogonID, bool isUsedBranchHierarchy, string filterCondition)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(this.GetConnectionString()))
                {
                    using (SqlCommand oSqlCommand = oSqlConnection.CreateCommand())
                    {
                        oSqlConnection.Open();
                        oSqlCommand.CommandText = "[dbo].[usp_GetUserMutationInquery]";
                        oSqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        oSqlCommand.CommandTimeout = this.GetCommandTimeOut();
                        oSqlCommand.Parameters.AddWithValue("@BranchCode", branchCode);
                        oSqlCommand.Parameters.AddWithValue("@OfficeCode", officeCode);
                        oSqlCommand.Parameters.AddWithValue("@UserLogon", userLogonID);
                        oSqlCommand.Parameters.AddWithValue("@IsUsedBranchHierarchy", isUsedBranchHierarchy);
                        oSqlCommand.Parameters.AddWithValue("@FilterCondition", filterCondition);
                        oSqlCommand.Parameters.AddWithValue("@PageNumber", oPagingInfo.Index);
                        oSqlCommand.Parameters.AddWithValue("@PageSize", oPagingInfo.PageSize);
                        oSqlCommand.Parameters.AddWithValue("@OrderBy", oPagingInfo.OrderBy);
                        oSqlCommand.Parameters.AddWithValue("@OrderType", oPagingInfo.OrderType);

                        using (SqlDataReader oSqlDataReader = oSqlCommand.ExecuteReader())
                        {
                            if (oSqlDataReader.HasRows && oSqlDataReader.Read())
                            {
                                int ErrorCode = oSqlDataReader["ErrorCode"] == DBNull.Value ? -1 : Convert.ToInt16(oSqlDataReader["ErrorCode"]);

                                if (ErrorCode == Constant.ERR_CODE_8001)
                                {
                                    oErrorInfo.Code = Constant.ERR_CODE_8001;
                                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_8001);
                                }
                                else if (ErrorCode == Constant.ERR_CODE_8005)
                                {
                                    oErrorInfo.Code = Constant.ERR_CODE_8005;
                                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_8005);
                                }
                                else
                                {
                                    oErrorInfo.Code = ErrorCode;
                                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_5056);
                                }
                                return oErrorInfo;
                            }
                            if (oSqlDataReader.NextResult() && oSqlDataReader.HasRows)
                            {
                                while (oSqlDataReader.Read())
                                {
                                    UserMutation oUserMutation = new UserMutation();
                                    oUserMutation.FsMutationUserId = oSqlDataReader["FsMutationUserId"] == DBNull.Value ? -1 : Convert.ToInt32(oSqlDataReader["FsMutationUserId"]);
                                    oUserMutation.FsCentralId = oSqlDataReader["FsCentralId"] == DBNull.Value ? -1 : Convert.ToInt32(oSqlDataReader["FsCentralId"]);
                                    oUserMutation.UserId = oSqlDataReader["UserId"] == DBNull.Value ? string.Empty : oSqlDataReader["UserId"].ToString();
                                    oUserMutation.InOutId = oSqlDataReader["InOutId"] == DBNull.Value ? (short)-1 : Convert.ToInt16(oSqlDataReader["InOutId"]);
                                    oUserMutation.InOut = oSqlDataReader["InOut"] == DBNull.Value ? string.Empty : oSqlDataReader["InOut"].ToString();
                                    oUserMutation.UserName = oSqlDataReader["UserName"] == DBNull.Value ? string.Empty : oSqlDataReader["UserName"].ToString();
                                    oUserMutation.BranchSourceCode = oSqlDataReader["BranchSourceCode"] == DBNull.Value ? null : oSqlDataReader["BranchSourceCode"].ToString();
                                    oUserMutation.OfficeSourceCode = oSqlDataReader["OfficeSourceCode"] == DBNull.Value ? null : oSqlDataReader["OfficeSourceCode"].ToString();
                                    oUserMutation.DestBranchCode = oSqlDataReader["BranchDestinationCode"] == DBNull.Value ? null : oSqlDataReader["BranchDestinationCode"].ToString();
                                    oUserMutation.DestOfficeCode = oSqlDataReader["OfficeDestinationCode"] == DBNull.Value ? null : oSqlDataReader["OfficeDestinationCode"].ToString();
                                    oUserMutation.EffectiveDate = oSqlDataReader["EffectiveDate"] == DBNull.Value ? (DateTime?)null : DateTime.Parse(oSqlDataReader["EffectiveDate"].ToString());
                                    oUserMutation.MutationStatus = oSqlDataReader["MutationStatus"] == DBNull.Value ? (short)-1 : Convert.ToInt16(oSqlDataReader["MutationStatus"]);
                                    oUserMutation.dbckv = oSqlDataReader["ckv"] == DBNull.Value ? (int?)null : int.Parse(oSqlDataReader["ckv"].ToString());
                                    oUserMutation.GenerateCKV();
                                    oUserMutation.CheckCKV(oClientInfo.NewGuid, oClientInfo.AppName, oClientInfo.AppVersion);
                                    Users.Add(oUserMutation);
                                }
                            }
                            if (oSqlDataReader.NextResult() && oSqlDataReader.Read())
                            {
                                oPagingInfo.TotalPage = oSqlDataReader["TotalPage"] == DBNull.Value ? 1 : Convert.ToInt32(oSqlDataReader["TotalPage"]);
                            }
                        }
                    }
                }
                if (Users.Count == 0)
                {
                    oErrorInfo.Code = Constant.ERR_CODE_8000;
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_8000);
                }
            }
            catch (Framework.Exception.CKVException ex)
            {
                throw ex;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return oErrorInfo;
        }

        public ErrorInfo GetUserMutationInquery(List<UserMutation> Users, DataTable udt)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(this.GetConnectionString()))
                {
                    using (SqlCommand oSqlCommand = oSqlConnection.CreateCommand())
                    {
                        oSqlConnection.Open();
                        oSqlCommand.CommandText = "[dbo].[usp_GetUserMutationInqueryByUserId]";
                        oSqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        oSqlCommand.CommandTimeout = this.GetCommandTimeOut();
                        oSqlCommand.Parameters.AddWithValue("@udtUsers", udt).SqlDbType = SqlDbType.Structured;

                        using (SqlDataReader oSqlDataReader = oSqlCommand.ExecuteReader())
                        {
                            if (oSqlDataReader.HasRows)
                            {
                                while (oSqlDataReader.Read())
                                {
                                    UserMutation oUserMutation = new UserMutation();
                                    oUserMutation.UserId = oSqlDataReader["UserId"] == DBNull.Value ? string.Empty : oSqlDataReader["UserId"].ToString();
                                    oUserMutation.UserName = oSqlDataReader["UserName"] == DBNull.Value ? string.Empty : oSqlDataReader["UserName"].ToString();
                                    oUserMutation.UserStatus = oSqlDataReader["UserStatus"] == DBNull.Value ? "-" : oSqlDataReader["UserStatus"].ToString();
                                    oUserMutation.UserGroup = oSqlDataReader["UserGroup"] == DBNull.Value ? "-" : oSqlDataReader["UserGroup"].ToString();
                                    oUserMutation.BranchSourceCode = oSqlDataReader["BranchSourceCode"] == DBNull.Value ? null : oSqlDataReader["BranchSourceCode"].ToString();
                                    oUserMutation.OfficeSourceCode = oSqlDataReader["OfficeSourceCode"] == DBNull.Value ? null : oSqlDataReader["OfficeSourceCode"].ToString();
                                    oUserMutation.DestBranchCode = oSqlDataReader["BranchDestinationCode"] == DBNull.Value ? null : oSqlDataReader["BranchDestinationCode"].ToString();
                                    oUserMutation.DestOfficeCode = oSqlDataReader["OfficeDestinationCode"] == DBNull.Value ? null : oSqlDataReader["OfficeDestinationCode"].ToString();
                                    oUserMutation.EffectiveDate = oSqlDataReader["EffectiveDate"] == DBNull.Value ? (DateTime?)null : DateTime.Parse(oSqlDataReader["EffectiveDate"].ToString());
                                    Users.Add(oUserMutation);
                                }
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return oErrorInfo;
        }

        public ErrorInfo FindUser(UserMutation oUserMutation, string userLogonId)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(this.GetConnectionString()))
                {
                    using (SqlCommand oSqlCommand = oSqlConnection.CreateCommand())
                    {
                        oSqlConnection.Open();
                        oSqlCommand.CommandText = "[dbo].[usp_FindUserMutation]";
                        oSqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        oSqlCommand.CommandTimeout = this.GetCommandTimeOut();
                        oSqlCommand.Parameters.AddWithValue("@userLogonId", userLogonId);
                        oSqlCommand.Parameters.AddWithValue("@UserId", oUserMutation.UserId);

                        using (SqlDataReader oSqlDataReader = oSqlCommand.ExecuteReader())
                        {
                            if (oSqlDataReader.HasRows && oSqlDataReader.Read())
                            {
                                int ErrorCode = oSqlDataReader["ErrorCode"] == DBNull.Value ? -1 : Convert.ToInt16(oSqlDataReader["ErrorCode"]);
                                return MappingErrorInfo(oErrorInfo, ErrorCode);
                            }
                            if (oSqlDataReader.NextResult() && oSqlDataReader.HasRows && oSqlDataReader.Read())
                            {
                                oUserMutation.FsCentralId = oSqlDataReader["FsCentralId"] == DBNull.Value ? -1 : Convert.ToInt32(oSqlDataReader["FsCentralId"]);
                                oUserMutation.UserId = oSqlDataReader["UserId"] == DBNull.Value ? string.Empty : oSqlDataReader["UserId"].ToString();
                                oUserMutation.UserStatusId = oSqlDataReader["UserStatusId"] == DBNull.Value ? -1 : Convert.ToInt32(oSqlDataReader["UserStatusId"]);
                                oUserMutation.UserCategoryId = oSqlDataReader["UserCategoryId"] == DBNull.Value ? -1 : Convert.ToInt32(oSqlDataReader["UserCategoryId"]);
                                oUserMutation.UserName = oSqlDataReader["Name"] == DBNull.Value ? string.Empty : oSqlDataReader["Name"].ToString();
                                oUserMutation.BranchSourceCode = oSqlDataReader["MainBranchCode"] == DBNull.Value ? null : oSqlDataReader["MainBranchCode"].ToString();
                                oUserMutation.OfficeSourceCode = oSqlDataReader["MainOfficeCode"] == DBNull.Value ? null : oSqlDataReader["MainOfficeCode"].ToString();
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return oErrorInfo;
        }


        #region Web Admin
        public ErrorInfo WebAdminValidateAccessControl(string userName, Enumeration.EnumOfWebAdminMenu MenuName, out Int16 roleId)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            roleId = 0;
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(this.GetConnectionString()))
                {
                    using (SqlCommand oSqlCommand = oSqlConnection.CreateCommand())
                    {
                        oSqlConnection.Open();
                        oSqlCommand.CommandText = "[dbo].[usp_WebAdminValidateAccessControl]";
                        oSqlCommand.CommandType = CommandType.StoredProcedure;
                        oSqlCommand.CommandTimeout = this.GetCommandTimeOut();
                        oSqlCommand.Parameters.AddWithValue("@UserName", userName);
                        oSqlCommand.Parameters.AddWithValue("@MenuAccess", (Int16)MenuName);

                        using (SqlDataReader oSqlDataReader = oSqlCommand.ExecuteReader())
                        {
                            if (oSqlDataReader.HasRows && oSqlDataReader.Read())
                            {
                                int ErrorCode = oSqlDataReader["ErrorCode"] == DBNull.Value ? -1 : Convert.ToInt16(oSqlDataReader["ErrorCode"]);
                                roleId = oSqlDataReader["RoleId"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(oSqlDataReader["RoleId"]);
                                switch (ErrorCode)
                                {
                                    case Constant.ERR_CODE_9500:
                                        oErrorInfo.Code = Constant.ERR_CODE_9500;
                                        oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9500);
                                        break;
                                    case Constant.ERR_CODE_9559:
                                        oErrorInfo.Code = Constant.ERR_CODE_9559;
                                        oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9559);
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                oErrorInfo.Code = Constant.ERROR_CODE_SYSTEM;
                oErrorInfo.Message = ex.Message;
            }
            return oErrorInfo;
        }

        public ErrorInfo WebAdminAddUser(string userName, string FullName, Int16? roleId, int sourceIPType, string sourceIP, string sourceName, string userLoginId)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(this.GetConnectionString()))
                {
                    using (SqlCommand oSqlCommand = oSqlConnection.CreateCommand())
                    {
                        oSqlConnection.Open();
                        oSqlCommand.CommandText = "[dbo].[usp_WebAdminInsertUser]";
                        oSqlCommand.CommandType = CommandType.StoredProcedure;
                        oSqlCommand.CommandTimeout = this.GetCommandTimeOut();
                        oSqlCommand.Parameters.AddWithValue("@RoleId", roleId);
                        oSqlCommand.Parameters.AddWithValue("@UserName", userName);
                        oSqlCommand.Parameters.AddWithValue("@FullName", FullName);
                        oSqlCommand.Parameters.AddWithValue("@SourceIPType", sourceIPType);
                        oSqlCommand.Parameters.AddWithValue("@SourceIP", sourceIP);
                        oSqlCommand.Parameters.AddWithValue("@SourceName", sourceName);
                        oSqlCommand.Parameters.AddWithValue("@UserLoginId", userLoginId);

                        using (SqlDataReader oSqlDataReader = oSqlCommand.ExecuteReader())
                        {
                            if (oSqlDataReader.HasRows && oSqlDataReader.Read())
                            {
                                int ErrorCode = oSqlDataReader["ErrorCode"] == DBNull.Value ? -1 : Convert.ToInt16(oSqlDataReader["ErrorCode"]);
                                switch (ErrorCode)
                                {
                                    case Constant.ERR_CODE_9550:
                                        oErrorInfo.Code = Constant.ERR_CODE_9550;
                                        oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9550);
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return oErrorInfo;
        }

        public ErrorInfo WebAdminInqueryUser(List<WebAdminUsers> WebAdminUserCollection, string filterCondition, int pageIndex, int pageSize, ref int totalRows)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(this.GetConnectionString()))
                {
                    using (SqlCommand oSqlCommand = oSqlConnection.CreateCommand())
                    {
                        oSqlConnection.Open();
                        oSqlCommand.CommandText = "[dbo].[usp_WebAdminGetUserInquery]";
                        oSqlCommand.CommandType = CommandType.StoredProcedure;
                        oSqlCommand.CommandTimeout = this.GetCommandTimeOut();
                        oSqlCommand.Parameters.AddWithValue("@FilterCondition", filterCondition);
                        oSqlCommand.Parameters.AddWithValue("@PageIndex", pageIndex);
                        oSqlCommand.Parameters.AddWithValue("@PageSize", pageSize);

                        using (SqlDataReader oSqlDataReader = oSqlCommand.ExecuteReader())
                        {
                            if (oSqlDataReader.HasRows)
                            {
                                while (oSqlDataReader.Read())
                                {
                                    WebAdminUsers oWebAdminUsers = new WebAdminUsers();
                                    oWebAdminUsers.UserName = oSqlDataReader["UserName"] == DBNull.Value ? string.Empty : oSqlDataReader["UserName"].ToString();
                                    oWebAdminUsers.FullName = oSqlDataReader["FullName"] == DBNull.Value ? string.Empty : oSqlDataReader["FullName"].ToString();
                                    oWebAdminUsers.RoleName = oSqlDataReader["RoleName"] == DBNull.Value ? string.Empty : oSqlDataReader["RoleName"].ToString();
                                    oWebAdminUsers.CreatedBy = oSqlDataReader["CreatedBy"] == DBNull.Value ? string.Empty : oSqlDataReader["CreatedBy"].ToString();
                                    oWebAdminUsers.CreatedDate = oSqlDataReader["CreatedDate"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(oSqlDataReader["CreatedDate"]);
                                    WebAdminUserCollection.Add(oWebAdminUsers);
                                }
                            }
                            if(oSqlDataReader.NextResult() && oSqlDataReader.Read())
                            {
                                totalRows = oSqlDataReader["TotalRows"] == DBNull.Value ? 0 : Convert.ToInt32(oSqlDataReader["TotalRows"]);
                            }
                        }
                    }
                }
                if (WebAdminUserCollection.Count == 0)
                {
                    oErrorInfo.Code = Constant.ERR_CODE_9552;
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9552);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return oErrorInfo;
        }

        public ErrorInfo WebAdminDeleteUser(string userName, string userLoginName)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(this.GetConnectionString()))
                {
                    using (SqlCommand oSqlCommand = oSqlConnection.CreateCommand())
                    {
                        oSqlConnection.Open();
                        oSqlCommand.CommandText = "[dbo].[usp_WebAdminDeleteUser]";
                        oSqlCommand.CommandType = CommandType.StoredProcedure;
                        oSqlCommand.CommandTimeout = this.GetCommandTimeOut();
                        oSqlCommand.Parameters.AddWithValue("@UserName", userName);
                        oSqlCommand.Parameters.AddWithValue("@UserLoginName", userLoginName);

                        using (SqlDataReader oSqlDataReader = oSqlCommand.ExecuteReader())
                        {
                            if (oSqlDataReader.HasRows && oSqlDataReader.Read())
                            {
                                int ErrorCode = oSqlDataReader["ErrorCode"] == DBNull.Value ? -1 : Convert.ToInt16(oSqlDataReader["ErrorCode"]);
                                switch (ErrorCode)
                                {
                                    case Constant.ERR_CODE_9556:
                                        oErrorInfo.Code = Constant.ERR_CODE_9556;
                                        oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9556);
                                        break;
                                    case Constant.ERR_CODE_9558:
                                        oErrorInfo.Code = Constant.ERR_CODE_9558;
                                        oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9558);
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return oErrorInfo;
        }

        public ErrorInfo WebAdminGetRoles(List<FsRoles> Roles, string userLoginName)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(this.GetConnectionString()))
                {
                    using (SqlCommand oSqlCommand = oSqlConnection.CreateCommand())
                    {
                        oSqlConnection.Open();
                        oSqlCommand.CommandText = "[dbo].[usp_WebAdminGetRoles]";
                        oSqlCommand.CommandType = CommandType.StoredProcedure;
                        oSqlCommand.CommandTimeout = this.GetCommandTimeOut();
                        oSqlCommand.Parameters.AddWithValue("@UserLoginName", userLoginName);

                        using (SqlDataReader oSqlDataReader = oSqlCommand.ExecuteReader())
                        {
                            if (oSqlDataReader.HasRows)
                            {
                                while (oSqlDataReader.Read())
                                {
                                    FsRoles oFsRoles = new FsRoles();
                                    oFsRoles.RoleId = oSqlDataReader["RoleId"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(oSqlDataReader["RoleId"]);
                                    oFsRoles.Name = oSqlDataReader["Name"] == DBNull.Value ? string.Empty : oSqlDataReader["Name"].ToString();
                                    Roles.Add(oFsRoles);
                                }
                            }
                        }
                    }
                }
                if (Roles.Count == 0)
                {
                    oErrorInfo.Code = Constant.ERR_CODE_9552;
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9552);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return oErrorInfo;
        }

        #endregion

        #region Private Method(s)
        private bool DeleteFingerTemplateOnServer(DataTable dtUsers, List<UserInquery> lUsers, ClientInfo oClientInfo, ErrorInfo oErrorInfo)
        {
            lUsers = lUsers.Where(x => x.CategoryId == 0).ToList();
            if (lUsers.Count > 0)
            {
                Utility.WriteLog(new LogInfo(string.Format("Begin delete finger template: {0}", DateTime.Now.ToString("HH:mm:ss")), oClientInfo.NewGuid, oClientInfo.AppName, oClientInfo.AppVersion, Constant.LEVEL_ALL, Constant.FINGER_SERVICE));
                FingerPrintServer oFingerPrintServer = new FingerPrintServer(oClientInfo.NewGuid, oClientInfo.AppName, oClientInfo.AppVersion);

                Parallel.ForEach(lUsers, user =>
                {
                    int FsCentralId = user.FsCentralId ?? 0;
                    try
                    {
                        oFingerPrintServer.DeleteFingerOnServerAsync(FsCentralId);
                    }
                    catch
                    {
                        DataRow dr = dtUsers.AsEnumerable().Where(x => x.Field<string>("UserId") == user.UserId).FirstOrDefault();
                        if (dr != null) dr["IsDelete"] = false;
                    }
                });
                Utility.WriteLog(new LogInfo(string.Format("End delete finger template: {0}", DateTime.Now.ToString("HH:mm:ss")), oClientInfo.NewGuid, oClientInfo.AppName, oClientInfo.AppVersion, Constant.LEVEL_ALL, Constant.FINGER_SERVICE));
            }
            int countSuccess = dtUsers.AsEnumerable().Where(x => x.Field<bool>("IsDelete") == true).Count();
            int countFailed = dtUsers.AsEnumerable().Where(x => x.Field<bool>("IsDelete") == false).Count();
            if (countFailed > 0) dtUsers = dtUsers.AsEnumerable().Where(x => x.Field<bool>("IsDelete") == false).CopyToDataTable();

            if (countSuccess > 0 && countFailed == 0)
            {
                string userIdSuccessToDeleted = String.Join(",", dtUsers.AsEnumerable().Where(x => x.Field<bool>("IsDelete") == true).Select(x => x.Field<string>("UserId").ToString()).ToArray());
                Utility.WriteLog(new LogInfo(string.Format("Delete success (Userid : {0})", userIdSuccessToDeleted), oClientInfo.NewGuid, oClientInfo.AppName, oClientInfo.AppVersion, Constant.LEVEL_ALL, Constant.FINGER_SERVICE));
                oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_6014);
            }
            else if (countSuccess == 0 && countFailed > 0)
            {
                string userIdFailedToDeleted = String.Join(",", dtUsers.AsEnumerable().Where(x => x.Field<bool>("IsDelete") == false).Select(x => x.Field<string>("UserId").ToString()).ToArray());
                Utility.WriteLog(new LogInfo(string.Format("Delete failed (Userid : {0})", userIdFailedToDeleted), oClientInfo.NewGuid, oClientInfo.AppName, oClientInfo.AppVersion, Constant.LEVEL_ALL, Constant.FINGER_SERVICE));
                oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_6015);

            }
            else if (countSuccess > 0 && countFailed > 0)
            {
                string userIdSuccessToDeleted = String.Join(",", dtUsers.AsEnumerable().Where(x => x.Field<bool>("IsDelete") == true).Select(x => x.Field<string>("UserId").ToString()).ToArray());
                string userIdFailedToDeleted = String.Join(",", dtUsers.AsEnumerable().Where(x => x.Field<bool>("IsDelete") == false).Select(x => x.Field<string>("UserId").ToString()).ToArray());
                Utility.WriteLog(new LogInfo(string.Format("Delete partially success (Success Userid : {0} | failed Userid : {1})", userIdSuccessToDeleted, userIdFailedToDeleted), oClientInfo.NewGuid, oClientInfo.AppName, oClientInfo.AppVersion, Constant.LEVEL_ALL, Constant.FINGER_SERVICE));
                oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_6007).Replace("[success]", countSuccess.ToString()).Replace("[failed]", countFailed.ToString());
            }
            return countFailed == 0;
        }
        private ErrorInfo MappingErrorInfo(ErrorInfo oErrorInfo, int errorCode)
        {
            switch (errorCode)
            {
                case Constant.ERR_CODE_6001:
                    oErrorInfo.Code = Constant.ERR_CODE_6001;
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_6001);
                    break;
                case Constant.ERR_CODE_6005:
                    oErrorInfo.Code = Constant.ERR_CODE_6005;
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_6005);
                    break;
                case Constant.ERR_CODE_8005:
                    oErrorInfo.Code = Constant.ERR_CODE_8005;
                    oErrorInfo.Message = string.Format("Destination {0}", Utility.GetErrorMessage(Constant.ERR_CODE_8005).ToLower());
                    break;
                case Constant.ERR_CODE_9000:
                    oErrorInfo.Code = Constant.ERR_CODE_9000;
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9000);
                    break;
                case Constant.ERR_CODE_9001:
                    oErrorInfo.Code = Constant.ERR_CODE_9001;
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9001);
                    break;
                case Constant.ERR_CODE_9002:
                    oErrorInfo.Code = Constant.ERR_CODE_9002;
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9002);
                    break;
                case Constant.ERR_CODE_9003:
                    oErrorInfo.Code = Constant.ERR_CODE_9003;
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9003);
                    break;
                case Constant.ERR_CODE_9004:
                    oErrorInfo.Code = Constant.ERR_CODE_9004;
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9004);
                    break;
                case Constant.ERR_CODE_9005:
                    oErrorInfo.Code = Constant.ERR_CODE_9005;
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9005);
                    break;
                case Constant.ERR_CODE_9006:
                    oErrorInfo.Code = Constant.ERR_CODE_9006;
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9006);
                    break;
                case Constant.ERR_CODE_9007:
                    oErrorInfo.Code = Constant.ERR_CODE_9007;
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9007);
                    break;
                case Constant.ERR_CODE_9008:
                    oErrorInfo.Code = Constant.ERR_CODE_9008;
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9008);
                    break;
                case Constant.ERR_CODE_5051:
                    oErrorInfo.Code = Constant.ERR_CODE_5051;
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_5051);
                    break;
                default:
                    oErrorInfo.Code = errorCode;
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_5056);
                    break;
            }
            return oErrorInfo;
        }
        private bool ValidasiDeleteUser(ErrorInfo oErrorInfo, DataTable Users, string LogonID, SqlConnection oSqlConnection)
        {
            List<string> ExceptUsers = new List<string>();
            int ErrorCode = Constant.NO_ERROR;
            using (SqlCommand oSqlCommand = oSqlConnection.CreateCommand())
            {
                oSqlCommand.CommandText = "[dbo].[usp_ValidateDeleteUserV3]";
                oSqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                oSqlCommand.CommandTimeout = this.GetCommandTimeOut();
                oSqlCommand.Parameters.AddWithValue("@UserLogon", LogonID);
                oSqlCommand.Parameters.AddWithValue("@udtUsers", Users).SqlDbType = SqlDbType.Structured;

                using (SqlDataReader oSqlDataReader = oSqlCommand.ExecuteReader())
                {
                    if (oSqlDataReader.HasRows)
                    {
                        while (oSqlDataReader.Read())
                        {
                            string userid = oSqlDataReader["UserId"] == DBNull.Value ? string.Empty : oSqlDataReader["UserId"].ToString();
                            if (!string.IsNullOrEmpty(userid)) ExceptUsers.Add(userid);
                            ErrorCode = oSqlDataReader["ErrorCode"] == DBNull.Value ? -1 : Convert.ToInt16(oSqlDataReader["ErrorCode"]);
                        }

                        if (ErrorCode == Constant.ERR_CODE_6002)
                        {
                            oErrorInfo.Code = Constant.ERR_CODE_6002;
                            oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_6002);
                        }
                        else if (ErrorCode == Constant.ERR_CODE_6012)
                        {
                            oErrorInfo.Code = Constant.ERR_CODE_6012;
                            oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_6012).Replace("NIP/NIK/Vendor NIP", string.Join(";", ExceptUsers.Distinct()));
                        }
                        else if (ErrorCode == Constant.ERR_CODE_6013)
                        {
                            oErrorInfo.Code = Constant.ERR_CODE_6013;
                            oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_6013).Replace("NIP/NIK/Vendor NIP", string.Join(";", ExceptUsers.Distinct()));
                        }
                        else if (ErrorCode == Constant.ERR_CODE_6004)
                        {
                            oErrorInfo.Code = Constant.ERR_CODE_6004;
                            oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_6004);
                        }
                        else if (ErrorCode == Constant.ERR_CODE_6005)
                        {
                            oErrorInfo.Code = Constant.ERR_CODE_6005;
                            oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_6005);
                        }
                        else if (ErrorCode == Constant.ERR_CODE_6016)
                        {
                            oErrorInfo.Code = Constant.ERR_CODE_6016;
                            oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_6016);
                        }
                        else if (ErrorCode == Constant.ERR_CODE_6017)
                        {
                            oErrorInfo.Code = Constant.ERR_CODE_6017;
                            oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_6017);
                        }
                        else
                        {
                            oErrorInfo.Code = ErrorCode;
                            oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_5056);
                        }
                    }
                }
            }
            return ErrorCode == Constant.NO_ERROR;
        }
        #endregion
    }
}