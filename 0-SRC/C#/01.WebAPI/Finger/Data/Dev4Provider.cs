﻿using Finger.Core.Entity;
using Finger.Entity;
using Finger.Framework.Common;
using Finger.Framework.Entity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Finger.Data
{
    public partial class FingerDataProvider : FingerBaseProvider
    {

        #region Properties

        public int CKVAsciiKeyId
        {
            get
            {
                return Utility.Generate2Ascii(Constant.KEY_FINGER_PRINT);

            }

        }

        #endregion

        #region Public Method(s)


        public bool ValidateUserIdAndCategoryIsAlreadyRegistered(string userId, Int16 categoryId, string branchCode, string officeCode)
        {
            bool result = false;
            try
            {

                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "[dbo].[usp_ValidateUserIdAndCategoryIsAlreadyRegistered]";
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandTimeout = this.GetCommandTimeOut();
                        command.Parameters.AddWithValue("@UserId", userId);
                        command.Parameters.AddWithValue("@CategoryId", categoryId);
                        command.Parameters.AddWithValue("@BranchCode", branchCode);
                        command.Parameters.AddWithValue("@OfficeCode", officeCode);
                        command.Parameters.AddWithValue("@StartDate", DBNull.Value);
                        command.Parameters.AddWithValue("@EndDate", DBNull.Value);
                        command.Parameters.AddWithValue("@ValidationType", 1);
                        result = Convert.ToBoolean(command.ExecuteScalar());
                    }
                }
            }
            catch (System.Exception ex)
            {

                throw ex;
            }

            return result;
        }

        public bool ValidateUserIdAndCategoryIsAlreadyRegistered(string userId, Int16 categoryId, string branchCode, string officeCode, DateTime startDate, DateTime endDate)
        {
            bool result = false;
            try
            {

                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "[dbo].[usp_ValidateUserIdAndCategoryIsAlreadyRegistered]";
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandTimeout = this.GetCommandTimeOut();
                        command.Parameters.AddWithValue("@UserId", userId);
                        command.Parameters.AddWithValue("@CategoryId", categoryId);
                        command.Parameters.AddWithValue("@BranchCode", branchCode);
                        command.Parameters.AddWithValue("@OfficeCode", officeCode);
                        command.Parameters.AddWithValue("@StartDate", startDate);
                        command.Parameters.AddWithValue("@EndDate", endDate);
                        command.Parameters.AddWithValue("@ValidationType", 1);
                        result = Convert.ToBoolean(command.ExecuteScalar());
                    }
                }
            }
            catch (System.Exception ex)
            {

                throw ex;
            }

            return result;
        }

        public bool ValidateUserIdNormalIsAlreadyRegisteredInAnotherBranch(string userId, string branchCode, string officeCode)
        {
            bool result = false;
            try
            {

                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "[dbo].[usp_ValidateUserIdAndCategoryIsAlreadyRegistered]";
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandTimeout = this.GetCommandTimeOut();
                        command.Parameters.AddWithValue("@UserId", userId);
                        command.Parameters.AddWithValue("@CategoryId", (short)Enumeration.EnumOfCategory.CATEGORY_NORMAL);
                        command.Parameters.AddWithValue("@BranchCode", branchCode);
                        command.Parameters.AddWithValue("@OfficeCode", officeCode);
                        command.Parameters.AddWithValue("@StartDate", DBNull.Value);
                        command.Parameters.AddWithValue("@EndDate", DBNull.Value);
                        command.Parameters.AddWithValue("@ValidationType", 2);
                        result = Convert.ToBoolean(command.ExecuteScalar());
                    }
                }
            }
            catch (System.Exception ex)
            {

                throw ex;
            }

            return result;
        }

        public bool ValidateUserIdIsAlreadyRegisteredWithinActivePeriodInAnotherBranch(string userId, Int16 categoryId, string branchCode, string officeCode, DateTime startDate, DateTime endDate)
        {
            bool result = false;
            try
            {

                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "[dbo].[usp_ValidateUserIdAndCategoryIsAlreadyRegistered]";
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandTimeout = this.GetCommandTimeOut();
                        command.Parameters.AddWithValue("@UserId", userId);
                        command.Parameters.AddWithValue("@CategoryId", categoryId);
                        command.Parameters.AddWithValue("@BranchCode", branchCode);
                        command.Parameters.AddWithValue("@OfficeCode", officeCode);
                        command.Parameters.AddWithValue("@StartDate", startDate);
                        command.Parameters.AddWithValue("@EndDate", endDate);
                        command.Parameters.AddWithValue("@ValidationType", 3);
                        result = Convert.ToBoolean(command.ExecuteScalar());
                    }
                }
            }
            catch (System.Exception ex)
            {

                throw ex;
            }

            return result;
        }

        public bool ValidateUserIdIsAlreadyRegisteredWithinActivePeriodInSameBranch(string userId, Int16 categoryId, string branchCode, string officeCode, DateTime startDate, DateTime endDate)
        {
            bool result = false;
            try
            {

                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "[dbo].[usp_ValidateUserIdAndCategoryIsAlreadyRegistered]";
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandTimeout = this.GetCommandTimeOut();
                        command.Parameters.AddWithValue("@UserId", userId);
                        command.Parameters.AddWithValue("@CategoryId", categoryId);
                        command.Parameters.AddWithValue("@BranchCode", branchCode);
                        command.Parameters.AddWithValue("@OfficeCode", officeCode);
                        command.Parameters.AddWithValue("@StartDate", startDate);
                        command.Parameters.AddWithValue("@EndDate", endDate);
                        command.Parameters.AddWithValue("@ValidationType", 4);
                        result = Convert.ToBoolean(command.ExecuteScalar());
                    }
                }
            }
            catch (System.Exception ex)
            {

                throw ex;
            }

            return result;
        }


        public bool ValidateDuplicateUserInAnotherBranchForThisPeriod(string userId, Int16 categoryId, string branchCode, string officeCode, DateTime startDate, DateTime endDate, int fsLocationId)
        {
            bool result = false;
            try
            {

                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "[dbo].[usp_ValidateDuplicateUserInAnotherBranchForThisPeriod]";
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandTimeout = this.GetCommandTimeOut();
                        command.Parameters.AddWithValue("@UserId", userId);
                        command.Parameters.AddWithValue("@CategoryId", categoryId);
                        command.Parameters.AddWithValue("@BranchCode", branchCode);
                        command.Parameters.AddWithValue("@OfficeCode", officeCode);
                        command.Parameters.AddWithValue("@StartDate", startDate);
                        command.Parameters.AddWithValue("@EndDate", endDate);
                        command.Parameters.AddWithValue("@FsLocationId", fsLocationId);
                        result = Convert.ToBoolean(command.ExecuteScalar());
                    }
                }
            }
            catch (System.Exception ex)
            {

                throw ex;
            }

            return result;
        }

        public bool ValidateDuplicateUserInSameBranchForThisPeriod(string userId, Int16 categoryId, string branchCode, string officeCode, DateTime startDate, DateTime endDate, int fsLocationId)
        {
            bool result = false;
            try
            {

                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "[dbo].[usp_ValidateDuplicateUserInSameBranchForThisPeriod]";
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandTimeout = this.GetCommandTimeOut();
                        command.Parameters.AddWithValue("@UserId", userId);
                        command.Parameters.AddWithValue("@CategoryId", categoryId);
                        command.Parameters.AddWithValue("@BranchCode", branchCode);
                        command.Parameters.AddWithValue("@OfficeCode", officeCode);
                        command.Parameters.AddWithValue("@StartDate", startDate);
                        command.Parameters.AddWithValue("@EndDate", endDate);
                        command.Parameters.AddWithValue("@FsLocationId", fsLocationId);
                        result = Convert.ToBoolean(command.ExecuteScalar());
                    }
                }
            }
            catch (System.Exception ex)
            {

                throw ex;
            }

            return result;
        }


        public bool ValidateUserExistForUpdate(string userId, Int16 categoryId, string branchCode, string officeCode, int fsLocationId)
        {
            bool result = false;
            try
            {

                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "[dbo].[usp_ValidateDataNotExistForUpdate]";
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandTimeout = this.GetCommandTimeOut();
                        command.Parameters.AddWithValue("@UserId", userId);
                        command.Parameters.AddWithValue("@CategoryId", categoryId);
                        command.Parameters.AddWithValue("@BranchCode", branchCode);
                        command.Parameters.AddWithValue("@OfficeCode", officeCode);
                        command.Parameters.AddWithValue("@FsLocationId", fsLocationId);
                        command.Parameters.AddWithValue("@ValidationType", 1);
                        result = Convert.ToBoolean(command.ExecuteScalar());
                    }
                }
            }
            catch (System.Exception ex)
            {

                throw ex;
            }

            return result;

        }

        public bool ValidateFsLocationIDExistForUpdate(string userId, Int16 categoryId, string branchCode, string officeCode, int fsLocationId)
        {
            bool result = false;
            try
            {

                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "[dbo].[usp_ValidateDataNotExistForUpdate]";
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandTimeout = this.GetCommandTimeOut();
                        command.Parameters.AddWithValue("@UserId", userId);
                        command.Parameters.AddWithValue("@CategoryId", categoryId);
                        command.Parameters.AddWithValue("@BranchCode", branchCode);
                        command.Parameters.AddWithValue("@OfficeCode", officeCode);
                        command.Parameters.AddWithValue("@FsLocationId", fsLocationId);
                        command.Parameters.AddWithValue("@ValidationType", 2);
                        result = Convert.ToBoolean(command.ExecuteScalar());
                    }
                }
            }
            catch (System.Exception ex)
            {

                throw ex;
            }

            return result;

        }


        public bool ValidateBDSIDAlreadyExist(string userId, string destinationBranchCode, string destinationOfficeCode, Int16 categoryId, DateTime? categoryStartPeriode, DateTime? categoryEndPeriode, List<FSMapping> FSMapping, out string BDSIDs)
        {
            bool isAlreadyExist = false;
            BDSIDs = string.Empty;
            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();
                    DataTable dtIdMapping = CreateIdMappingTable(FSMapping);

                    using (SqlCommand command = new SqlCommand())
                    {

                        command.Connection = connection;
                        command.CommandText = "[dbo].[usp_ValidateIsAlreadyExistBDSID]";
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandTimeout = this.GetCommandTimeOut();
                        command.Parameters.AddWithValue("@userid", userId);
                        command.Parameters.AddWithValue("@categoryid", categoryId);
                        if (categoryStartPeriode.HasValue)
                        {
                            command.Parameters.AddWithValue("@categoryStartPeriod", categoryStartPeriode.Value);
                        }

                        if (categoryEndPeriode.HasValue)
                        {
                            command.Parameters.AddWithValue("@categoryEndPeriod", categoryEndPeriode.Value);

                        }
                        command.Parameters.AddWithValue("@branchcode", destinationBranchCode);
                        command.Parameters.AddWithValue("@officecode", destinationOfficeCode);
                        SqlParameter dtParameter;
                        dtParameter = command.Parameters.AddWithValue("@dtFsMapping", dtIdMapping);
                        dtParameter.SqlDbType = SqlDbType.Structured;
                        dtParameter.TypeName = "dbo.dtFsMapping";

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                isAlreadyExist = reader["IsExist"] != DBNull.Value ? Convert.ToBoolean(reader["IsExist"]) : false;
                                BDSIDs = reader["BDSIDFound"] != DBNull.Value ? Convert.ToString(reader["BDSIDFound"]) : string.Empty;
                            }

                        }

                    }

                }

            }
            catch (System.Exception ex)
            {

                throw ex;
            }

            return isAlreadyExist;

        }

        public bool ValidateBDSIDBranchCodeNotRegisteredInOrganizationMatrix(List<FSMapping> FSMapping, out string BDSIDs)
        {

            bool isAlreadyExist = false;
            BDSIDs = string.Empty;
            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();
                    DataTable dtIdMapping = CreateIdMappingTable(FSMapping);

                    using (SqlCommand command = new SqlCommand())
                    {

                        command.Connection = connection;
                        command.CommandText = "[dbo].[usp_ValidateBDSIDBranchCodeNotRegisteredInOrganizationMatrix]";
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandTimeout = this.GetCommandTimeOut();
                        SqlParameter dtParameter;
                        dtParameter = command.Parameters.AddWithValue("@dtFsMapping", dtIdMapping);
                        dtParameter.SqlDbType = SqlDbType.Structured;
                        dtParameter.TypeName = "dbo.dtFsMapping";

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                isAlreadyExist = reader["IsExist"] != DBNull.Value ? Convert.ToBoolean(reader["IsExist"]) : false;
                                BDSIDs = reader["BDSIDFound"] != DBNull.Value ? Convert.ToString(reader["BDSIDFound"]) : string.Empty;
                            }

                        }

                    }

                }

            }
            catch (System.Exception ex)
            {

                throw ex;
            }

            return isAlreadyExist;
        }

        public ServiceResponse ValidateBranchHierarchy(string userLoginId, string destinationBranchCode)
        {
            ServiceResponse response = null;
            try
            {

                response = new ServiceResponse();

                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand("[dbo].[usp_ValidateBranchHierarchy]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = this.GetCommandTimeOut();
                        command.Parameters.AddWithValue("@UserLoginId", userLoginId);
                        command.Parameters.AddWithValue("@ClientBranchCode", destinationBranchCode);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            response = GetResponseMapper(reader);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return response;
        }

        public void RegisterUserInfo(UserInfo userInfo)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();
                    DataTable dtIdMapping = CreateIdMappingTable(userInfo.FSMapping);
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "[dbo].[usp_SaveRegisterUserInfo]";
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandTimeout = this.GetCommandTimeOut();
                        SqlParameter InOutParam = new SqlParameter("@FsCentralId", userInfo.FSCentral.FsCentralId);
                        InOutParam.SqlDbType = SqlDbType.Int;
                        InOutParam.Direction = ParameterDirection.InputOutput;
                        command.Parameters.Add(InOutParam);


                        SqlParameter UserIdInOutParam = new SqlParameter("@userid", userInfo.FSCentral.UserId);
                        UserIdInOutParam.SqlDbType = SqlDbType.VarChar;
                        UserIdInOutParam.Size = 16;
                        UserIdInOutParam.Direction = ParameterDirection.InputOutput;
                        command.Parameters.Add(UserIdInOutParam);

                        //command.Parameters.AddWithValue("@userid", userInfo.FSCentral.UserId);
                        command.Parameters.AddWithValue("@mainbranchcode", userInfo.FSCentral.MainBranchCode);
                        command.Parameters.AddWithValue("@mainofficecode", userInfo.FSCentral.MainOfficeCode);
                        command.Parameters.AddWithValue("@sourceiptype", userInfo.FSCentral.SourceIPType);
                        command.Parameters.AddWithValue("@sourceip", userInfo.FSCentral.SourceIP);
                        command.Parameters.AddWithValue("@sourcename", userInfo.FSCentral.SourceName);
                        command.Parameters.AddWithValue("@categoryid", userInfo.FSCategory.CategoryId);
                        command.Parameters.AddWithValue("@startperiod", userInfo.FSCategory.StartPeriod);
                        command.Parameters.AddWithValue("@endperiod", userInfo.FSCategory.EndPeriod);
                        command.Parameters.AddWithValue("@name", userInfo.FSMain.Name);
                        command.Parameters.AddWithValue("@groupid", userInfo.FSMain.GroupId);
                        command.Parameters.AddWithValue("@templatelayout", userInfo.FSMain.TemplateLayout);
                        command.Parameters.AddWithValue("@imagemap", userInfo.FSMain.ImageMap);
                        command.Parameters.AddWithValue("@quality", userInfo.FSMain.Quality);
                        command.Parameters.AddWithValue("@userFAR", userInfo.FSMain.UserFAR);
                        command.Parameters.AddWithValue("@ApprovedBy", userInfo.FSMain.ApprovedBy);
                        command.Parameters.AddWithValue("@Reason", userInfo.FSMain.Reason);
                        SqlParameter dtParameter;
                        dtParameter = command.Parameters.AddWithValue("@dtFsMapping", dtIdMapping);
                        dtParameter.SqlDbType = SqlDbType.Structured;
                        dtParameter.TypeName = "dbo.dtFsMapping";
                        command.Parameters.AddWithValue("@FsImageIndex", userInfo.FSImages.FSImageIndex);
                        command.Parameters.Add("@FsImage0", SqlDbType.VarBinary, 8000).Value = userInfo.FSImages.FSImage0 != null ? (object)userInfo.FSImages.FSImage0 : DBNull.Value;
                        command.Parameters.Add("@FsImage1", SqlDbType.VarBinary, 8000).Value = userInfo.FSImages.FSImage1 != null ? (object)userInfo.FSImages.FSImage1 : DBNull.Value;
                        command.Parameters.Add("@FsImage2", SqlDbType.VarBinary, 8000).Value = userInfo.FSImages.FSImage2 != null ? (object)userInfo.FSImages.FSImage2 : DBNull.Value;
                        command.Parameters.Add("@FsImage3", SqlDbType.VarBinary, 8000).Value = userInfo.FSImages.FSImage3 != null ? (object)userInfo.FSImages.FSImage3 : DBNull.Value;
                        command.Parameters.Add("@FsImage4", SqlDbType.VarBinary, 8000).Value = userInfo.FSImages.FSImage4 != null ? (object)userInfo.FSImages.FSImage4 : DBNull.Value;
                        command.Parameters.Add("@FsImage5", SqlDbType.VarBinary, 8000).Value = userInfo.FSImages.FSImage5 != null ? (object)userInfo.FSImages.FSImage5 : DBNull.Value;
                        command.Parameters.Add("@FsImage6", SqlDbType.VarBinary, 8000).Value = userInfo.FSImages.FSImage6 != null ? (object)userInfo.FSImages.FSImage6 : DBNull.Value;
                        command.Parameters.Add("@FsImage7", SqlDbType.VarBinary, 8000).Value = userInfo.FSImages.FSImage7 != null ? (object)userInfo.FSImages.FSImage7 : DBNull.Value;
                        command.Parameters.Add("@FsImage8", SqlDbType.VarBinary, 8000).Value = userInfo.FSImages.FSImage8 != null ? (object)userInfo.FSImages.FSImage8 : DBNull.Value;
                        command.Parameters.Add("@FsImage9", SqlDbType.VarBinary, 8000).Value = userInfo.FSImages.FSImage9 != null ? (object)userInfo.FSImages.FSImage9 : DBNull.Value;
                        command.Parameters.AddWithValue("@createdBy", userInfo.CreatedBy);
                        command.ExecuteNonQuery();
                        userInfo.FSCentral.FsCentralId = Convert.ToInt32(command.Parameters["@FsCentralId"].Value);
                        if (userInfo.FSMapping.Count > 0)
                        {
                            if (userInfo.FSMapping[0].UserType == (Int16)Enumeration.EnumOfUserStatus.USER_STATUS_ALIH_DAYA)
                            {
                                userInfo.FSCentral.UserId = command.Parameters["@userid"].Value.ToString();
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {

                throw ex;
            }
        }

        public void UpdateUserInfo(UserInfo userInfo)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {

                    connection.Open();
                    DataTable dtIdMapping = CreateIdMappingTable(userInfo.FSMapping);
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "[dbo].[usp_UpdateRegistrationUserInfo]";
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandTimeout = this.GetCommandTimeOut();
                        command.Parameters.AddWithValue("@FsCentralId", userInfo.FSCentral.FsCentralId);
                        command.Parameters.AddWithValue("@userid", userInfo.FSCentral.UserId);
                        command.Parameters.AddWithValue("@mainbranchcode", userInfo.FSCentral.MainBranchCode);
                        command.Parameters.AddWithValue("@mainofficecode", userInfo.FSCentral.MainOfficeCode);
                        command.Parameters.AddWithValue("@locationId", userInfo.FsLocationId);
                        command.Parameters.AddWithValue("@sourceiptype", userInfo.FSCentral.SourceIPType);
                        command.Parameters.AddWithValue("@sourceip", userInfo.FSCentral.SourceIP);
                        command.Parameters.AddWithValue("@sourcename", userInfo.FSCentral.SourceName);
                        command.Parameters.AddWithValue("@startperiod", userInfo.FSCategory.StartPeriod);
                        command.Parameters.AddWithValue("@endperiod", userInfo.FSCategory.EndPeriod);
                        command.Parameters.AddWithValue("@categoryid", userInfo.FSCategory.CategoryId);
                        command.Parameters.AddWithValue("@groupid", userInfo.FSMain.GroupId);
                        command.Parameters.AddWithValue("@createdBy", userInfo.CreatedBy);

                        SqlParameter dtParameter;
                        dtParameter = command.Parameters.AddWithValue("@dtFsMapping", dtIdMapping);
                        dtParameter.SqlDbType = SqlDbType.Structured;
                        dtParameter.TypeName = "dbo.dtFsMapping";
                        command.ExecuteNonQuery();

                    }

                }


            }
            catch (System.Exception ex)
            {

                throw ex;
            }

        }

        public UserInfo GetUserInfoByUserId(string userId)
        {

            UserInfo userInfo = null;
            List<FSMapping> listOfMapping = null;

            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "[dbo].[usp_GetUserInfoByUserId]";
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandTimeout = this.GetCommandTimeOut();
                        command.Parameters.AddWithValue("@userid", userId);

                        userInfo = new UserInfo();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {

                            Int16 userType = -1;
                            Int16 subType = -1;

                            if (reader.Read())
                            {
                                userInfo.FSCentral = new FSCentral();
                                userInfo.FSCentral.FsCentralId = reader["FsCentralId"] != DBNull.Value ? Convert.ToInt32(reader["FsCentralId"]) : -1;
                                userInfo.FSCentral.UserId = reader["UserId"] != DBNull.Value ? Convert.ToString(reader["UserId"]) : string.Empty;
                                userInfo.FSCentral.MainBranchCode = reader["MainBranchCode"] != DBNull.Value ? Convert.ToString(reader["MainBranchCode"]) : string.Empty;
                                userInfo.FSCentral.MainOfficeCode = reader["MainOfficeCode"] != DBNull.Value ? Convert.ToString(reader["MainOfficeCode"]) : string.Empty;
                                userType = reader["UserType"] != DBNull.Value ? Convert.ToInt16(reader["UserType"]) : (short)-1;
                                subType = reader["SubType"] != DBNull.Value ? Convert.ToInt16(reader["SubType"]) : (short)-1;
                            }

                            if (reader.NextResult())
                            {
                                if (reader.Read())
                                {
                                    userInfo.FSMain = new FSMain();
                                    userInfo.FSMain.Name = reader["Name"] != DBNull.Value ? Convert.ToString(reader["Name"]) : string.Empty;
                                    userInfo.FSMain.GroupId = reader["GroupId"] != DBNull.Value ? Convert.ToInt32(reader["GroupId"]) : -1;
                                    userInfo.FSMain.TemplateLayout = reader["TemplateLayout"] != DBNull.Value ? Convert.ToString(reader["TemplateLayout"]) : string.Empty;
                                    userInfo.FSMain.ImageMap = reader["ImageMap"] != DBNull.Value ? Convert.ToInt16(reader["ImageMap"]) : (short)-1;
                                    userInfo.FSMain.Quality = reader["Quality"] != DBNull.Value ? Convert.ToInt16(reader["Quality"]) : (short)-1;
                                    userInfo.FSMain.UserFAR = reader["UserFAR"] != DBNull.Value ? Convert.ToDouble(reader["UserFAR"]) : 0;
                                    userInfo.FSMain.ApprovedBy = reader["ApprovedBy"] != DBNull.Value ? Convert.ToString(reader["ApprovedBy"]) : string.Empty;
                                    userInfo.FSMain.Reason = reader["Reason"] != DBNull.Value ? Convert.ToString(reader["Reason"]) : string.Empty;
                                }
                            }

                            if (reader.NextResult())
                            {
                                if (reader.Read())
                                {
                                    userInfo.FSCategory = new FSCategory();
                                    userInfo.FSCategory.CategoryId = reader["CategoryId"] != DBNull.Value ? Convert.ToInt16(reader["CategoryId"]) : (short)-1;
                                    userInfo.FSCategory.StartPeriod = reader["StartPeriod"] != DBNull.Value ? (DateTime?)reader["StartPeriod"] : null;
                                    userInfo.FSCategory.EndPeriod = reader["EndPeriod"] != DBNull.Value ? (DateTime?)reader["EndPeriod"] : null;
                                }
                            }

                            if (reader.NextResult())
                            {
                                listOfMapping = new List<FSMapping>();
                                while (reader.Read())
                                {
                                    FSMapping fsMapping = new FSMapping();
                                    fsMapping.FsCentralId = userInfo.FSCentral.FsCentralId;
                                    fsMapping.CategoryId = userInfo.FSCategory.CategoryId;
                                    fsMapping.BranchCode = userInfo.FSCentral.MainBranchCode;
                                    fsMapping.OfficeCode = userInfo.FSCentral.MainOfficeCode;
                                    fsMapping.UserName = reader["BDSID"] != DBNull.Value ? Convert.ToString(reader["BDSID"]) : string.Empty;
                                    fsMapping.UserCategory = (Int16)Finger.Framework.Common.Enumeration.EnumOfUserCategory.USER_CATEGORY_BDS;
                                    fsMapping.UserType = userType;
                                    fsMapping.SubType = subType;
                                    listOfMapping.Add(fsMapping);
                                }
                                userInfo.FSMapping = listOfMapping;
                            }






                        }
                    }



                }


            }
            catch (System.Exception ex)
            {

                throw ex;
            }

            return userInfo;
        }

        public UserInfoDetail GetUserInfoDetail(int fsCentralId, Int16 categoryId, string branchCode, string officeCode, int locationId)
        {
            UserInfoDetail userInfoDetail = null;
            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "[dbo].[usp_GetUserInfoDetails]";
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandTimeout = this.GetCommandTimeOut();
                        command.Parameters.AddWithValue("@FsCentralID", fsCentralId);
                        command.Parameters.AddWithValue("@CategoryID", categoryId);
                        command.Parameters.AddWithValue("@BranchCode", branchCode);
                        command.Parameters.AddWithValue("@OfficeCode", officeCode);
                        command.Parameters.AddWithValue("@LocationId", locationId);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                userInfoDetail = new UserInfoDetail();
                                userInfoDetail.FsCentralId = reader["FsCentralId"] != DBNull.Value ? Convert.ToInt32(reader["FsCentralId"]) : -1;
                                userInfoDetail.BranchCode = reader["BranchCode"] != DBNull.Value ? Convert.ToString(reader["BranchCode"]) : string.Empty;
                                userInfoDetail.OfficeCode = reader["OfficeCode"] != DBNull.Value ? Convert.ToString(reader["OfficeCode"]) : string.Empty;
                                userInfoDetail.UserId = reader["UserId"] != DBNull.Value ? Convert.ToString(reader["UserId"]) : string.Empty;
                                userInfoDetail.Name = reader["Name"] != DBNull.Value ? Convert.ToString(reader["Name"]) : string.Empty;
                                userInfoDetail.Quality = reader["Quality"] != DBNull.Value ? Convert.ToInt16(reader["Quality"]) : (short)0;
                                userInfoDetail.ApprovedBy = reader["ApprovedBy"] != DBNull.Value ? Convert.ToString(reader["ApprovedBy"]) : string.Empty;
                                userInfoDetail.Reason = reader["Reason"] != DBNull.Value ? Convert.ToString(reader["Reason"]) : string.Empty;
                                userInfoDetail.UserStatusId = reader["UserStatusId"] != DBNull.Value ? Convert.ToInt32(reader["UserStatusId"]) : -1;
                                userInfoDetail.UserStatus = reader["UserStatus"] != DBNull.Value ? Convert.ToString(reader["UserStatus"]) : string.Empty;
                                userInfoDetail.UserSubType = reader["UserSubType"] != DBNull.Value ? Convert.ToString(reader["UserSubType"]) : string.Empty;
                                userInfoDetail.UserGroupId = reader["UserGroupId"] != DBNull.Value ? Convert.ToInt32(reader["UserGroupId"]) : -1;
                                userInfoDetail.UserGroup = reader["UserGroup"] != DBNull.Value ? Convert.ToString(reader["UserGroup"]) : string.Empty;
                                userInfoDetail.UserCategoryId = reader["UserCategoryId"] != DBNull.Value ? Convert.ToInt32(reader["UserCategoryId"]) : -1;
                                userInfoDetail.UserCategory = reader["UserCategory"] != DBNull.Value ? Convert.ToString(reader["UserCategory"]) : string.Empty;
                                userInfoDetail.StartPeriod = reader["StartPeriod"] != DBNull.Value ? Convert.ToString(reader["StartPeriod"]) : string.Empty;
                                userInfoDetail.EndPeriod = reader["EndPeriod"] != DBNull.Value ? Convert.ToString(reader["EndPeriod"]) : string.Empty;
                                userInfoDetail.BDSID1 = reader["BDS1"] != DBNull.Value ? Convert.ToString(reader["BDS1"]) : string.Empty;
                                userInfoDetail.BDSID2 = reader["BDS2"] != DBNull.Value ? Convert.ToString(reader["BDS2"]) : string.Empty;
                                userInfoDetail.BDSID3 = reader["BDS3"] != DBNull.Value ? Convert.ToString(reader["BDS3"]) : string.Empty;
                                userInfoDetail.BDSID4 = reader["BDS4"] != DBNull.Value ? Convert.ToString(reader["BDS4"]) : string.Empty;
                                userInfoDetail.BDSID5 = reader["BDS5"] != DBNull.Value ? Convert.ToString(reader["BDS5"]) : string.Empty;
                                userInfoDetail.BDSID6 = reader["BDS6"] != DBNull.Value ? Convert.ToString(reader["BDS6"]) : string.Empty;
                                userInfoDetail.BDSID7 = reader["BDS7"] != DBNull.Value ? Convert.ToString(reader["BDS7"]) : string.Empty;
                                userInfoDetail.BDSID8 = reader["BDS8"] != DBNull.Value ? Convert.ToString(reader["BDS8"]) : string.Empty;
                                userInfoDetail.BDSID9 = reader["BDS9"] != DBNull.Value ? Convert.ToString(reader["BDS9"]) : string.Empty;
                                userInfoDetail.BDSID10 = reader["BDS10"] != DBNull.Value ? Convert.ToString(reader["BDS10"]) : string.Empty;
                                userInfoDetail.TemplateLayout = reader["TemplateLayout"] != DBNull.Value ? Convert.ToInt16(reader["TemplateLayout"]) : (short)-1;

                            }
                        }
                    }
                }

            }
            catch (System.Exception ex)
            {

                throw ex;
            }
            return userInfoDetail;
        }

        public bool DeleteUserInfo(int fsCentralId, string createdBy)
        {
            bool result = false;
            try
            {

                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "[dbo].[usp_DeleteUserInfo]";
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandTimeout = this.GetCommandTimeOut();
                        command.Parameters.AddWithValue("@FsCentralId", fsCentralId);
                        command.Parameters.AddWithValue("@createdBy", createdBy);
                        result = Convert.ToBoolean(command.ExecuteNonQuery());

                    }
                }
            }
            catch (System.Exception ex)
            {

                throw ex;
            }

            return result;

        }

        public List<RecapAttendanceInquiryResult> GetRecapAttendanceInquiry(RecapAttendanceCriteria criteria, bool isExport)
        {
            List<RecapAttendanceInquiryResult> listOfResult = null;

            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "[dbo].[usp_GetRecapAttendanceInquiry]";
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandTimeout = this.GetCommandTimeOut();
                        command.Parameters.AddWithValue("@BranchCode", criteria.BranchCode);
                        command.Parameters.AddWithValue("@OfficeCode", criteria.OfficeCode);
                        command.Parameters.AddWithValue("@FilterKey", criteria.FilterKey);
                        command.Parameters.AddWithValue("@FilterValue", criteria.FilterValue);
                        command.Parameters.AddWithValue("@ShiftingStatus", criteria.ShiftingStatus);
                        command.Parameters.AddWithValue("@StartPeriode", criteria.StartPeriode);
                        command.Parameters.AddWithValue("@EndPeriode", criteria.EndPeriode);
                        command.Parameters.AddWithValue("@UserLogon", criteria.UserLogonId);


                        if (!isExport)
                        {
                            command.Parameters.AddWithValue("@PageNumber", criteria.PagingInfo.Index);
                            command.Parameters.AddWithValue("@PageSize", criteria.PagingInfo.PageSize);
                            command.Parameters.AddWithValue("@OrderBy", BuildOrderBy(criteria.PagingInfo.OrderBy, criteria.PagingInfo.OrderType));
                        }

                        using (SqlDataReader reader = command.ExecuteReader())
                        {

                            listOfResult = new List<RecapAttendanceInquiryResult>();
                            while (reader.Read())
                            {
                                RecapAttendanceInquiryResult result = new RecapAttendanceInquiryResult();
                                result.BranchCode = reader["BranchCode"] != DBNull.Value ? Convert.ToString(reader["BranchCode"]) : string.Empty;
                                result.OfficeCode = reader["OfficeCode"] != DBNull.Value ? Convert.ToString(reader["OfficeCode"]) : string.Empty;
                                result.CreatedDate = reader["AttendanceDate"] != DBNull.Value ? Convert.ToDateTime(reader["AttendanceDate"]).ToString(Constant.IND_DATE_FORMAT) : string.Empty;
                                result.CreatedBy = reader["CreatedBy"] != DBNull.Value ? Convert.ToString(reader["CreatedBy"]) : string.Empty;
                                result.Activity = reader["Activity"] != DBNull.Value ? Convert.ToString(reader["Activity"]) : string.Empty;
                                result.UserID = reader["UserID"] != DBNull.Value ? Convert.ToString(reader["UserID"]) : string.Empty;
                                result.Name = reader["Name"] != DBNull.Value ? Convert.ToString(reader["Name"]) : string.Empty;
                                result.UserStatus = reader["UserStatus"] != DBNull.Value ? Convert.ToString(reader["UserStatus"]) : string.Empty;
                                result.Shift = reader["Shift"] != DBNull.Value ? Convert.ToString(reader["Shift"]) : string.Empty;
                                result.FingerName = reader["FingerName"] != DBNull.Value ? Convert.ToString(reader["FingerName"]) : string.Empty;
                                result.Score = reader["Score"] != DBNull.Value ? Convert.ToInt32(reader["Score"]) : 0;
                                result.AttendanceDate = reader["AttendanceDate"] != DBNull.Value ? Convert.ToDateTime(reader["AttendanceDate"]) : new DateTime(1900, 01, 01);
                                result.AttendanceTime = reader["AttendanceTime"] != DBNull.Value ? Convert.ToString(reader["AttendanceTime"]) : string.Empty;
                                result.TemplateLayout = reader["TemplateLayout"] != DBNull.Value ? Convert.ToString(reader["TemplateLayout"]) : string.Empty;
                                result.LastIP = reader["LastIP"] != DBNull.Value ? Convert.ToString(reader["LastIP"]) : string.Empty;
                                listOfResult.Add(result);
                            }

                            if (reader.NextResult() && reader.Read())
                            {

                                criteria.PagingInfo.TotalPage = reader["TotalPage"] == DBNull.Value ? 1 : Convert.ToInt32(reader["TotalPage"]);

                            }

                        }

                    }
                }
            }
            catch (System.Exception ex)
            {

                throw ex;
            }


            return listOfResult;

        }

        public Int16? GetBranchVersion(string branchCode, string officeCode)
        {
            Int16? branchVersion = null;
            try
            {
                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand("[dbo].[usp_GetBranchVersion]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = this.GetCommandTimeOut();
                        command.Parameters.AddWithValue("@BranchCode", branchCode);
                        command.Parameters.AddWithValue("@OfficeCode", officeCode);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                if (reader["Version"] != DBNull.Value)
                                {
                                    branchVersion = Convert.ToInt16(reader["Version"]);
                                }
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return branchVersion;

        }

        public ServiceResponse CheckUserLogExists(string userId)
        {
            ServiceResponse response = null;
            try
            {
                response = new ServiceResponse();

                using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand("[dbo].[usp_CheckUserLogExists]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = this.GetCommandTimeOut();
                        command.Parameters.AddWithValue("@UserID", userId);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                response.statusCode = reader["ErrorNumber"] != DBNull.Value ? Convert.ToInt32(reader["ErrorNumber"]) : 0;
                                response.statusMessage = reader["ErrorMessage"] != DBNull.Value ? reader["ErrorMessage"].ToString() : string.Empty;
                                bool isUserLogExist = reader["IsUserLogExist"] != DBNull.Value ? Convert.ToBoolean( reader["IsUserLogExist"]) : false;
                                response.responseData = JsonConvert.SerializeObject(new { IsUserLogExists = isUserLogExist });
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return response;
        }
        #endregion

        #region Private Method(s)
        private DataTable CreateIdMappingTable(List<FSMapping> idMapping)
        {

            DataTable table = new DataTable();
            table.Columns.Add("CategoryId", typeof(Int16));
            table.Columns.Add("UserName", typeof(String));
            table.Columns.Add("UserCategory", typeof(Int16));
            table.Columns.Add("UserType", typeof(Int16));
            table.Columns.Add("SubType", typeof(Int16));
            table.Columns.Add("BranchCode", typeof(String));
            table.Columns.Add("OfficeCode", typeof(String));

            if (idMapping == null)
                return table;

            if (idMapping.Count == 0)
                return table;

            foreach (var item in idMapping)
            {
                DataRow dr = table.NewRow();
                dr["CategoryId"] = item.CategoryId;
                dr["UserName"] = item.UserName;
                dr["UserCategory"] = item.UserCategory;
                dr["UserType"] = item.UserType;
                dr["SubType"] = item.SubType;
                dr["BranchCode"] = item.BranchCode;
                dr["OfficeCode"] = item.OfficeCode;
                table.Rows.Add(dr);

            }

            return table;

        }

        private string BuildOrderBy(string orderBy, string orderType)
        {

            return string.Format("{0} {1}", string.IsNullOrEmpty(orderBy) ? "BranchCode" : orderBy, string.IsNullOrEmpty(orderType) ? "ASC" : orderType);
        }
        #endregion
    }
}