﻿using Finger.Entity;
using Finger.Framework.Common;
using Finger.Framework.Entity;
using Finger.Framework.Security;
using Finger.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Finger.Logic
{
    public partial class FingerLogic
    {
        private const string saltKey = "2954F8A2-561B-49DE-8D4A-B0A3B22AFBE8";

        #region Public Method(s)
        public ErrorInfo getUserInquery(UserInqueryModel oUserInqueryModel, out DataTable exportUserData)
        {
            ErrorInfo oErrorInfo;
            exportUserData = new DataTable();
            try
            {
                oErrorInfo = ValidateIsExistLogonID(oUserInqueryModel.ClientInfo.LogonID);

                if (oErrorInfo.Code != Constant.NO_ERROR)
                    return oErrorInfo;

                oUserInqueryModel.Users = new List<UserInquery>();
                oUserInqueryModel.PagingInfo.OrderBy = "[" + oUserInqueryModel.PagingInfo.OrderBy + "] " + oUserInqueryModel.PagingInfo.OrderType;
                oErrorInfo = fingerDataProvider.GetUserInquery(oUserInqueryModel.Users, oUserInqueryModel.PagingInfo, oUserInqueryModel.BranchCode
                    , oUserInqueryModel.OfficeCode, oUserInqueryModel.ClientInfo.LogonID, oUserInqueryModel.BranchType, FilterCondition(oUserInqueryModel.PagingInfo));

                if (oErrorInfo.Code == Constant.NO_ERROR)
                {
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_6008);
                }
                else if (oErrorInfo.Code == Constant.ERR_CODE_6000)
                {
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_6000);
                }
                else if (oErrorInfo.Code == Constant.ERR_CODE_6001)
                {
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_6001);
                }
                else if (oErrorInfo.Code == Constant.ERR_CODE_6005)
                {
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_6005);
                }

                if (oErrorInfo.Code == Constant.NO_ERROR)
                {
                    if (oUserInqueryModel.PagingInfo.Index == null)
                    {
                        exportUserData = ConvertToUserTable(oUserInqueryModel.Users, oUserInqueryModel.PagingInfo.ExportFileType);
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return oErrorInfo;
        }

        public ErrorInfo deleteUser(DeleteUserModel oDeleteUserModel)
        {
            ErrorInfo oErrorInfo;
            try
            {
                oErrorInfo = ValidateIsExistLogonID(oDeleteUserModel.ClientInfo.LogonID);
                if (oErrorInfo.Code != Constant.NO_ERROR)
                    return oErrorInfo;

                oErrorInfo = ValidateMandatoryFieldTobeDelete(oDeleteUserModel.Users);
                if (oErrorInfo.Code != Constant.NO_ERROR)
                    return oErrorInfo;

                if (ValidUserIsUserLogon(oDeleteUserModel.Users.Select(x => x.UserId).ToArray(), oDeleteUserModel.ClientInfo.LogonID))
                {
                    oErrorInfo.Code = Constant.ERR_CODE_6011;
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_6011);
                    return oErrorInfo;
                }

                DataTable dt = GetUserToDataTable(oDeleteUserModel.Users);
                oErrorInfo = fingerDataProvider.DeleteUser(dt, oDeleteUserModel.Users, oDeleteUserModel.ClientInfo);

                if (oErrorInfo.Code == Constant.NO_ERROR)
                {
                    oDeleteUserModel.Users = new List<UserInquery>();
                    fingerDataProvider.GetUserInquery(oDeleteUserModel.Users, dt);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return oErrorInfo;
        }

        public ErrorInfo regUserMutation(UserMutationModel oUserMutationModel)
        {
            ErrorInfo oErrorInfo;
            try
            {
                oErrorInfo = ValidateIsExistLogonID(oUserMutationModel.ClientInfo.LogonID);

                if (oErrorInfo.Code != Constant.NO_ERROR)
                    return oErrorInfo;

                if (ValidUserIsUserLogon(new string[] { oUserMutationModel.UserMutation.UserId }, oUserMutationModel.ClientInfo.LogonID))
                {
                    oErrorInfo.Code = Constant.ERR_CODE_9000;
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9000);
                    return oErrorInfo;
                }

                oErrorInfo = fingerDataProvider.RegUserMutation(oUserMutationModel.UserMutation, oUserMutationModel.ClientInfo.LogonID);

                if (oErrorInfo.Code == Constant.NO_ERROR)
                {
                    oUserMutationModel.Users = new List<UserMutation>();
                    oUserMutationModel.Users.Add(new UserMutation()
                    {
                        FsMutationUserId = 0,
                        FsCentralId = 0,
                        UserId = oUserMutationModel.UserMutation.UserId,
                        DestBranchCode = oUserMutationModel.UserMutation.DestBranchCode,
                        DestOfficeCode = oUserMutationModel.UserMutation.DestOfficeCode
                    });
                    DataTable dtUser = GetUserToDataTable(oUserMutationModel.Users, (Int16)Enumeration.EnumOfMutationUser.WaitingApproval);
                    oUserMutationModel.Users = new List<UserMutation>();
                    oErrorInfo = fingerDataProvider.GetUserMutationInquery(oUserMutationModel.Users, dtUser);
                    if (oErrorInfo.Code == Constant.NO_ERROR)
                        oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9006);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return oErrorInfo;
        }

        public ErrorInfo resetUserLogon(ResetUserLogonModel oResetUserLogonModel)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                if (ValidUserIsUserLogon(new string[] { oResetUserLogonModel.ResetUserInfo.UserId }, oResetUserLogonModel.ClientInfo.LogonID))
                {
                    oErrorInfo.Code = Constant.ERR_CODE_4001;
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_4001);
                    return oErrorInfo;
                }

                oErrorInfo = fingerDataProvider.ResetUserLogon(oResetUserLogonModel.ResetUserInfo.FsCentralId, oResetUserLogonModel.ResetUserInfo.BranchCode, oResetUserLogonModel.ResetUserInfo.OfficeCode);

                if (oErrorInfo.Code == Constant.NO_ERROR)
                {
                    oErrorInfo.Message = "Reset user logon success";
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return oErrorInfo;
        }

        public ErrorInfo getUserMutation(UserMutationModel oUserMutationModel, out System.Data.DataTable exportMutationData, PagingInfo pagingInfo)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            exportMutationData = new DataTable();
            try
            {
                oErrorInfo = ValidateIsExistLogonID(oUserMutationModel.ClientInfo.LogonID);

                if (oErrorInfo.Code != Constant.NO_ERROR)
                    return oErrorInfo;

                oUserMutationModel.Users = new List<UserMutation>();
                MappingOrderBy(oUserMutationModel.PagingInfo);
                oErrorInfo = fingerDataProvider.GetUserMutationInquery(oUserMutationModel.Users, oUserMutationModel.PagingInfo, oUserMutationModel.ClientInfo
                    , oUserMutationModel.BranchCode, oUserMutationModel.OfficeCode, oUserMutationModel.ClientInfo.LogonID, oUserMutationModel.IsUsedBranchHierarchy
                    , FilterCondition(oUserMutationModel.PagingInfo));

                if (oErrorInfo.Code == Constant.NO_ERROR)
                {
                    if (pagingInfo.Index == null)
                    {
                        exportMutationData = ConvertToMutationTable(oUserMutationModel.Users, pagingInfo.ExportFileType);
                    }
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_6008);
                }
            }
            catch (Framework.Exception.CKVException ex)
            {
                throw ex;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return oErrorInfo;
        }

        public ErrorInfo CancelUserMutation(UserMutationModel oUserMutationModel)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                oErrorInfo = ValidateIsExistLogonID(oUserMutationModel.ClientInfo.LogonID);

                if (oErrorInfo.Code != Constant.NO_ERROR)
                    return oErrorInfo;

                oErrorInfo = ValidateMandatoryFieldTobeCancelorApprove(oUserMutationModel.Users, (Int16)Enumeration.EnumOfMutationUser.Cancelled);
                if (oErrorInfo.Code != Constant.NO_ERROR)
                    return oErrorInfo;

                if (ValidUserIsUserLogon(oUserMutationModel.Users.Select(x => x.UserId).ToArray(), oUserMutationModel.ClientInfo.LogonID))
                {
                    oErrorInfo.Code = Constant.ERR_CODE_8003;
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_8003).Replace("{action}", "Cancel");
                    return oErrorInfo;
                }

                DataTable dtUser = GetUserToDataTable(oUserMutationModel.Users, (Int16)Enumeration.EnumOfMutationUser.Cancelled);
                oErrorInfo = fingerDataProvider.UpdateUserMutation(dtUser, oUserMutationModel.ClientInfo, (Int16)Enumeration.EnumOfMutationUser.Cancelled);

                if (oErrorInfo.Code == Constant.NO_ERROR)
                {
                    oUserMutationModel.Users = new List<UserMutation>();
                    oErrorInfo = fingerDataProvider.GetUserMutationInquery(oUserMutationModel.Users, dtUser);
                    if (oErrorInfo.Code == Constant.NO_ERROR) oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_8008);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return oErrorInfo;
        }

        public ErrorInfo ApproveUserMutation(UserMutationModel oUserMutationModel)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                oErrorInfo = ValidateIsExistLogonID(oUserMutationModel.ClientInfo.LogonID);

                if (oErrorInfo.Code != Constant.NO_ERROR)
                    return oErrorInfo;

                oErrorInfo = ValidateMandatoryFieldTobeCancelorApprove(oUserMutationModel.Users, (Int16)Enumeration.EnumOfMutationUser.OnProgress);
                if (oErrorInfo.Code != Constant.NO_ERROR)
                    return oErrorInfo;

                if (ValidUserIsUserLogon(oUserMutationModel.Users.Select(x => x.UserId).ToArray(), oUserMutationModel.ClientInfo.LogonID))
                {
                    oErrorInfo.Code = Constant.ERR_CODE_8003;
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_8003).Replace("{action}", "Accept");
                    return oErrorInfo;
                }

                oErrorInfo = fingerDataProvider.UpdateUserMutation(GetUserToDataTable(oUserMutationModel.Users
                    , (Int16)Enumeration.EnumOfMutationUser.OnProgress), oUserMutationModel.ClientInfo, (Int16)Enumeration.EnumOfMutationUser.OnProgress);

                if (oErrorInfo.Code == Constant.NO_ERROR)
                {
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_8006);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return oErrorInfo;
        }

        public ErrorInfo FindUserMutation(UserMutationModel oUserMutationModel)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                oErrorInfo = ValidateIsExistLogonID(oUserMutationModel.ClientInfo.LogonID);

                if (oErrorInfo.Code != Constant.NO_ERROR)
                    return oErrorInfo;

                if (ValidUserIsUserLogon(new string[] { oUserMutationModel.UserMutation.UserId }, oUserMutationModel.ClientInfo.LogonID))
                {
                    oErrorInfo.Code = Constant.ERR_CODE_9000;
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9000);
                    return oErrorInfo;
                }

                oErrorInfo = fingerDataProvider.FindUser(oUserMutationModel.UserMutation, oUserMutationModel.ClientInfo.LogonID);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return oErrorInfo;
        }

        #region Web Admin
        public ErrorInfo WebAdminLogin(BCAUserModel oBCAUserModel, Enumeration.EnumOfWebAdminMenu MenuName, params Enumeration.EnumOfWebAdminRole[] UserRole)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                if (string.IsNullOrEmpty(oBCAUserModel.UserName) && string.IsNullOrEmpty(oBCAUserModel.Password))
                {
                    oErrorInfo.Code = Constant.ERR_CODE_9501;
                    oErrorInfo.Message = string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_9501), "Username and Password");
                    return oErrorInfo;
                }
                else if (string.IsNullOrEmpty(oBCAUserModel.UserName))
                {
                    oErrorInfo.Code = Constant.ERR_CODE_9501;
                    oErrorInfo.Message = string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_9501), "Username");
                    return oErrorInfo;
                }
                else if (string.IsNullOrEmpty(oBCAUserModel.Password) && oBCAUserModel.UserName.ToLower() != Enumeration.GetEnumDescription(Enumeration.EnumOfWebAdminRole.SuperAdmin).ToLower())
                {
                    oErrorInfo.Code = Constant.ERR_CODE_9501;
                    oErrorInfo.Message = string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_9501), "Password");
                    return oErrorInfo;
                }

                BCAUserModel CurrentUserLoginInfo = new BCAUserModel();
                CurrentUserLoginInfo.UserName = oBCAUserModel.UserName;
                CurrentUserLoginInfo.Password = oBCAUserModel.Password;
                oBCAUserModel.Password = string.Empty;

                oErrorInfo = WebAdminValidateMenuAccessControl(CurrentUserLoginInfo, MenuName, UserRole);
                if (oErrorInfo.Code != Constant.NO_ERROR)
                    return oErrorInfo;

                oBCAUserModel.UserData = Cryptography.Encode(JsonConvert.SerializeObject(CurrentUserLoginInfo), saltKey);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return oErrorInfo;
        }

        public ErrorInfo WebAdminAddUser(BCAUserModel oBCAUserModel, Enumeration.EnumOfWebAdminMenu MenuName, params Enumeration.EnumOfWebAdminRole[] UserRole)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                if (string.IsNullOrEmpty(oBCAUserModel.UserData))
                {
                    oErrorInfo.Code = Constant.ERR_CODE_9501;
                    oErrorInfo.Message = string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_9501), "UserData");
                    return oErrorInfo;
                }
                else if (string.IsNullOrWhiteSpace(oBCAUserModel.UserRequest))
                {
                    oErrorInfo.Code = Constant.ERR_CODE_9501;
                    oErrorInfo.Message = string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_9501), "User Request");
                    return oErrorInfo;
                }
                else if (oBCAUserModel.UserRequestRoleId == null)
                {
                    oErrorInfo.Code = Constant.ERR_CODE_9501;
                    oErrorInfo.Message = string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_9501), "Role");
                    return oErrorInfo;
                }

                BCAUserModel CurrentUserLoginInfo = Deserialize(oBCAUserModel.UserData);

                if (CurrentUserLoginInfo.UserName.ToLower() == Enumeration.GetEnumDescription(Enumeration.EnumOfWebAdminRole.SuperAdmin).ToLower())
                {
                    if (oBCAUserModel.UserRequestRoleId != (Int16)Enumeration.EnumOfWebAdminRole.Admin)
                    {
                        oErrorInfo.Code = Constant.ERR_CODE_9557;
                        oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9557);
                        return oErrorInfo;
                    }
                    else if (string.IsNullOrEmpty(oBCAUserModel.UserName) && string.IsNullOrEmpty(oBCAUserModel.Password))
                    {
                        oErrorInfo.Code = Constant.ERR_CODE_9501;
                        oErrorInfo.Message = string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_9501), "Username and Password");
                        return oErrorInfo;
                    }
                    else if (string.IsNullOrEmpty(oBCAUserModel.UserName))
                    {
                        oErrorInfo.Code = Constant.ERR_CODE_9501;
                        oErrorInfo.Message = string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_9501), "Username");
                        return oErrorInfo;
                    }
                    else if (string.IsNullOrEmpty(oBCAUserModel.Password) && oBCAUserModel.UserName.ToLower() != Enumeration.GetEnumDescription(Enumeration.EnumOfWebAdminRole.SuperAdmin).ToLower())
                    {
                        oErrorInfo.Code = Constant.ERR_CODE_9501;
                        oErrorInfo.Message = string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_9501), "Password");
                        return oErrorInfo;
                    }
                }
                else if (CurrentUserLoginInfo.UserName.ToLower() != Enumeration.GetEnumDescription(Enumeration.EnumOfWebAdminRole.SuperAdmin).ToLower())
                {
                    if (oBCAUserModel.UserRequestRoleId != (Int16)Enumeration.EnumOfWebAdminRole.Admin && oBCAUserModel.UserRequestRoleId != (Int16)Enumeration.EnumOfWebAdminRole.PICApps)
                    {
                        oErrorInfo.Code = Constant.ERR_CODE_9557;
                        oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9557);
                        return oErrorInfo;
                    }
                    oBCAUserModel.UserName = CurrentUserLoginInfo.UserName;
                    oBCAUserModel.Password = CurrentUserLoginInfo.Password;
                }

                oErrorInfo = WebAdminValidateMenuAccessControl(CurrentUserLoginInfo, MenuName, UserRole);

                if (oErrorInfo.Code != Constant.NO_ERROR)
                {
                    if (oErrorInfo.Code == Constant.ERR_CODE_9500)
                    {
                        oErrorInfo.Code = Constant.ERR_CODE_9559;
                        oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9559);
                    }

                    return oErrorInfo;
                }

                ADUserInfo oADUserInfo = new LdapAuthentication().GetUserInActiveDirectory(oBCAUserModel.UserName
                    , oBCAUserModel.Password, oBCAUserModel.UserRequest, ref oErrorInfo);

                if (oADUserInfo == null)
                    return oErrorInfo;

                oErrorInfo = fingerDataProvider.WebAdminAddUser(oADUserInfo.LoginName, oADUserInfo.FullName, oBCAUserModel.UserRequestRoleId
                    , Utility.GetLocalIPAddressType(), Utility.GetLocalIPAddress(), Utility.GetLocalHostName(), CurrentUserLoginInfo.UserName);

                if (oErrorInfo.Code == Constant.NO_ERROR)
                {
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9551);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return oErrorInfo;
        }

        public ErrorInfo WebAdminInqueryUser(UsersManagementModel oUsersManagementModel, List<WebAdminUsers> Users, ref int totalRows, Enumeration.EnumOfWebAdminMenu MenuName, params Enumeration.EnumOfWebAdminRole[] UserRole)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                if (string.IsNullOrEmpty(oUsersManagementModel.BCAUserModel.UserData))
                {
                    oErrorInfo.Code = Constant.ERR_CODE_9501;
                    oErrorInfo.Message = string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_9501), "UserData");
                    return oErrorInfo;
                }

                oErrorInfo = WebAdminValidateMenuAccessControl(Deserialize(oUsersManagementModel.BCAUserModel.UserData), MenuName, UserRole);

                if (oErrorInfo.Code != Constant.NO_ERROR)
                {
                    if (oErrorInfo.Code == Constant.ERR_CODE_9500)
                    {
                        oErrorInfo.Code = Constant.ERR_CODE_9559;
                        oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9559);
                    }

                    return oErrorInfo;
                }

                oErrorInfo = fingerDataProvider.WebAdminInqueryUser(Users, FilterCondition(oUsersManagementModel.BCAUserModel), oUsersManagementModel.PageIndex
                    , oUsersManagementModel.PageSize, ref totalRows);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return oErrorInfo;
        }

        public ErrorInfo WebAdminDeleteUser(BCAUserModel oBCAUserModel, Enumeration.EnumOfWebAdminMenu MenuName, params Enumeration.EnumOfWebAdminRole[] UserRole)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                if (string.IsNullOrEmpty(oBCAUserModel.UserData))
                {
                    oErrorInfo.Code = Constant.ERR_CODE_9501;
                    oErrorInfo.Message = string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_9501), "UserData");
                    return oErrorInfo;
                }
                else if (string.IsNullOrWhiteSpace(oBCAUserModel.UserRequest))
                {
                    oErrorInfo.Code = Constant.ERR_CODE_9501;
                    oErrorInfo.Message = string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_9501), "User Request");
                    return oErrorInfo;
                }

                BCAUserModel CurrentUserLoginInfo = Deserialize(oBCAUserModel.UserData);

                if (oBCAUserModel.UserRequest.ToLower() == CurrentUserLoginInfo.UserName.ToLower())
                {
                    oErrorInfo.Code = Constant.ERR_CODE_9555;
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9555);
                    return oErrorInfo;
                }

                oErrorInfo = WebAdminValidateMenuAccessControl(CurrentUserLoginInfo, MenuName, UserRole);

                if (oErrorInfo.Code != Constant.NO_ERROR)
                {
                    if (oErrorInfo.Code == Constant.ERR_CODE_9500)
                    {
                        oErrorInfo.Code = Constant.ERR_CODE_9559;
                        oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9559);
                    }

                    return oErrorInfo;
                }

                oErrorInfo = fingerDataProvider.WebAdminDeleteUser(oBCAUserModel.UserRequest, CurrentUserLoginInfo.UserName);

                if (oErrorInfo.Code == Constant.NO_ERROR)
                {
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9554);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return oErrorInfo;
        }

        public ErrorInfo WebAdminGetRoles(BCAUserModel oBCAUserModel, List<FsRoles> Roles, Enumeration.EnumOfWebAdminMenu MenuName, params Enumeration.EnumOfWebAdminRole[] UserRole)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                if (string.IsNullOrEmpty(oBCAUserModel.UserData))
                {
                    oErrorInfo.Code = Constant.ERR_CODE_9501;
                    oErrorInfo.Message = string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_9501), "UserData");
                    return oErrorInfo;
                }

                BCAUserModel CurrentUserLoginInfo = Deserialize(oBCAUserModel.UserData);
                oErrorInfo = WebAdminValidateMenuAccessControl(CurrentUserLoginInfo, MenuName, UserRole);

                if (oErrorInfo.Code != Constant.NO_ERROR)
                {
                    if (oErrorInfo.Code == Constant.ERR_CODE_9500)
                    {
                        oErrorInfo.Code = Constant.ERR_CODE_9559;
                        oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9559);
                    }

                    return oErrorInfo;
                }

                oErrorInfo = fingerDataProvider.WebAdminGetRoles(Roles, CurrentUserLoginInfo.UserName);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return oErrorInfo;
        }
        #endregion
        #endregion

        #region Private Method(s)
        private bool ValidUserIsUserLogon(string[] obj, string userLogon)
        {
            bool flag = false;
            for (int i = 0; i < obj.Length; i++)
            {
                if (obj[i] == userLogon)
                {
                    flag = true;
                    break;
                }
            }
            return flag;
        }
        private DataTable GetUserToDataTable(List<UserInquery> Users)
        {
            DataTable oDataTable = new DataTable();
            oDataTable.Columns.Add("Id", typeof(int));
            oDataTable.Columns.Add("FsCentralId", typeof(int));
            oDataTable.Columns.Add("FsLocationId", typeof(int));
            oDataTable.Columns.Add("UserId", typeof(string));
            oDataTable.Columns.Add("CategoryId", typeof(int));
            oDataTable.Columns.Add("BranchCode", typeof(string));
            oDataTable.Columns.Add("OfficeCode", typeof(string));
            oDataTable.Columns.Add("IsDelete", typeof(bool));

            for (int i = 0; i < Users.Count; i++)
            {
                DataRow row = oDataTable.NewRow();
                row["Id"] = i + 1;
                row["FsCentralId"] = Users[i].FsCentralId;
                row["FsLocationId"] = Users[i].FsLocationId;
                row["UserId"] = Users[i].UserId;
                row["CategoryId"] = Users[i].CategoryId;
                row["BranchCode"] = Users[i].BranchCode;
                row["OfficeCode"] = Users[i].OfficeCode;
                row["IsDelete"] = true;
                oDataTable.Rows.Add(row);
            }

            return oDataTable;
        }
        private DataTable GetUserToDataTable(List<UserMutation> Users, Int16 mutationStatus)
        {
            DataTable oDataTable = new DataTable();
            oDataTable.Columns.Add("Id", typeof(int));
            oDataTable.Columns.Add("FsUserMutationId", typeof(int));
            oDataTable.Columns.Add("FsCentralId", typeof(int));
            oDataTable.Columns.Add("UserId", typeof(string));
            oDataTable.Columns.Add("BranchCode", typeof(string));
            oDataTable.Columns.Add("OfficeCode", typeof(string));
            oDataTable.Columns.Add("MutationStatus", typeof(Int16));

            for (int i = 0; i < Users.Count; i++)
            {
                DataRow row = oDataTable.NewRow();
                row["Id"] = i + 1;
                row["FsUserMutationId"] = Users[i].FsMutationUserId;
                row["FsCentralId"] = Users[i].FsCentralId;
                row["UserId"] = Users[i].UserId;
                row["BranchCode"] = mutationStatus == (Int16)Enumeration.EnumOfMutationUser.Cancelled ? Users[i].BranchSourceCode : Users[i].DestBranchCode;
                row["OfficeCode"] = mutationStatus == (Int16)Enumeration.EnumOfMutationUser.Cancelled ? Users[i].OfficeSourceCode : Users[i].DestOfficeCode;
                row["MutationStatus"] = mutationStatus;
                oDataTable.Rows.Add(row);
            }

            return oDataTable;
        }
        private string FilterCondition(PagingInfo oPagingInfo)
        {
            List<string> result = new List<string>();
            if (oPagingInfo.FilterKey != null)
            {
                for (int i = 0; i < oPagingInfo.FilterKey.Length; i++)
                {
                    if (oPagingInfo.FilterKey[i] == "Name")
                        result.Add("{" + oPagingInfo.FilterKey[i] + "} LIKE '%" + oPagingInfo.FilterValue[i] + "%'");
                    else if (oPagingInfo.FilterKey[i] == "BDS_ID")
                    {
                        List<string> bds = new List<string>();
                        for (int j = 1; j <= 10; j++)
                        {
                            bds.Add("{" + oPagingInfo.FilterKey[i] + j + "} = '" + oPagingInfo.FilterValue[i] + "'");
                        }
                        result.Add("(" + string.Join(" OR ", bds) + ")");
                    }
                    else if (oPagingInfo.FilterKey[i] == "UserType")
                    {
                        if (oPagingInfo.FilterValue[i] == "0")
                            result.Add("{" + oPagingInfo.FilterKey[i] + "} = '0'");
                        else if (oPagingInfo.FilterValue[i] == "1")
                            result.Add("{" + oPagingInfo.FilterKey[i] + "} <> '0'");
                    }
                    else
                        result.Add("{" + oPagingInfo.FilterKey[i] + "} = '" + oPagingInfo.FilterValue[i] + "'");
                }
            }
            return result.Count > 0 ? string.Join(" AND ", result) : null;
        }
        private void MappingOrderBy(PagingInfo oPagingInfo)
        {
            oPagingInfo.OrderBy = "{" + oPagingInfo.OrderBy + "}";
        }
        private ErrorInfo ValidateIsExistLogonID(string LogonID)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            if (LogonID.ToLower().Equals("admin")) return oErrorInfo;

            UserInfoModel userLoginInfoModel = GetUserInfoByUserId(LogonID);
            if (userLoginInfoModel == null)
            {
                oErrorInfo.Code = Constant.ERR_CODE_7003;
                oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_7003).Replace("{NIP/NIK/Vendor NIP}", LogonID);
            }
            return oErrorInfo;
        }
        private ErrorInfo ValidateMandatoryFieldTobeDelete(List<UserInquery> Users)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            List<string> user = new List<string>();
            for (int i = 0; i < Users.Count; i++)
            {
                int lengthUserId = Users[i].UserId.Length;
                if (lengthUserId < 6 || lengthUserId > 6 && lengthUserId < 10 || lengthUserId > 10 && lengthUserId < 16)
                {
                    oErrorInfo.Code = Constant.ERR_CODE_6006;
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_6006);
                    break;
                }

                bool flag = false;
                if (Users[i].FsCentralId == null)
                {
                    user.Add(string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_6003), "FsCentralId"));
                    flag = true;
                }
                if (Users[i].FsLocationId == null)
                {
                    user.Add(string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_6003), "FsLocationId"));
                    flag = true;
                }
                if (string.IsNullOrWhiteSpace(Users[i].UserId))
                {
                    user.Add(string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_6003), "UserId"));
                    flag = true;
                }
                if (string.IsNullOrWhiteSpace(Users[i].BranchCode))
                {
                    user.Add(string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_6003), "BranchCode"));
                    flag = true;
                }
                if (string.IsNullOrWhiteSpace(Users[i].OfficeCode))
                {
                    user.Add(string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_6003), "OfficeCode"));
                    flag = true;
                }
                if (Users[i].CategoryId == null)
                {
                    user.Add(string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_6003), "CategoryId"));
                    flag = true;
                }

                if (flag)
                {
                    oErrorInfo.Code = Constant.ERR_CODE_6003;
                    oErrorInfo.Message = string.Join(";", user);
                    break;
                }
            }
            return oErrorInfo;
        }
        private ErrorInfo ValidateMandatoryFieldTobeCancelorApprove(List<UserMutation> Users, Int16 mutationStatus)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            List<string> user = new List<string>();
            for (int i = 0; i < Users.Count; i++)
            {
                int lengthUserId = Users[i].UserId.Length;
                if (lengthUserId < 6 || lengthUserId > 6 && lengthUserId < 10 || lengthUserId > 10 && lengthUserId < 16)
                {
                    oErrorInfo.Code = Constant.ERR_CODE_6006;
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_6006);
                    break;
                }

                bool flag = false;
                if (Users[i].FsMutationUserId == null)
                {
                    user.Add(string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_8007), "FsMutationUserId"));
                    flag = true;
                }
                if (Users[i].FsCentralId == null)
                {
                    user.Add(string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_8007), "FsCentralId"));
                    flag = true;
                }
                if (string.IsNullOrWhiteSpace(Users[i].UserId))
                {
                    user.Add(string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_8007), "UserId"));
                    flag = true;
                }
                if (mutationStatus == (Int16)Enumeration.EnumOfMutationUser.Cancelled && string.IsNullOrWhiteSpace(Users[i].BranchSourceCode))
                {
                    user.Add(string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_8007), "BranchSourceCode"));
                    flag = true;
                }
                else if (mutationStatus == (Int16)Enumeration.EnumOfMutationUser.OnProgress && string.IsNullOrWhiteSpace(Users[i].DestBranchCode))
                {
                    user.Add(string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_8007), "DestinationBranchCode"));
                    flag = true;
                }
                if (mutationStatus == (Int16)Enumeration.EnumOfMutationUser.Cancelled && string.IsNullOrWhiteSpace(Users[i].OfficeSourceCode))
                {
                    user.Add(string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_8007), "OfficeSourceCode"));
                    flag = true;
                }
                else if (mutationStatus == (Int16)Enumeration.EnumOfMutationUser.OnProgress && string.IsNullOrWhiteSpace(Users[i].DestOfficeCode))
                {
                    user.Add(string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_8007), "DestinationOfficeCode"));
                    flag = true;
                }

                if (flag)
                {
                    oErrorInfo.Code = Constant.ERR_CODE_8007;
                    oErrorInfo.Message = string.Join(";", user);
                    break;
                }
            }
            return oErrorInfo;
        }
        private BCAUserModel Deserialize(string userData)
        {
            return JsonConvert.DeserializeObject<BCAUserModel>(Cryptography.Decode(userData, saltKey));
        }
        private ErrorInfo WebAdminValidateMenuAccessControl(BCAUserModel oBCAUserModel, Enumeration.EnumOfWebAdminMenu MenuName, params Enumeration.EnumOfWebAdminRole[] UserRole)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();

            Int16 roleId = 0;
            oErrorInfo = fingerDataProvider.WebAdminValidateAccessControl(oBCAUserModel.UserName, MenuName, out roleId);

            if (oErrorInfo.Code == Constant.NO_ERROR)
            {
                bool isHaveAccess = false;
                for (int i = 0; i < UserRole.Length; i++)
                {
                    if ((Int16)UserRole[i] == roleId)
                    {
                        isHaveAccess = true;
                        break;
                    }
                }

                if (!isHaveAccess)
                {
                    oErrorInfo.Code = Constant.ERR_CODE_5057;
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_5057).Replace("{menuname}", Enumeration.GetEnumDescription(MenuName));
                    return oErrorInfo;
                }

                if (oBCAUserModel.UserName.ToLower() != Enumeration.GetEnumDescription(Enumeration.EnumOfWebAdminRole.SuperAdmin).ToLower())
                {
                    oErrorInfo = new LdapAuthentication().Authenticated(oBCAUserModel.UserName, oBCAUserModel.Password);
                }
            }

            return oErrorInfo;
        }
        private string FilterCondition(BCAUserModel oBCAUserModel)
        {
            List<string> result = new List<string>();

            if (!string.IsNullOrWhiteSpace(oBCAUserModel.UserRequest))
            {
                result.Add("{UserName} = '" + oBCAUserModel.UserRequest + "'");
            }
            if (oBCAUserModel.UserRequestRoleId != null &&
                (oBCAUserModel.UserRequestRoleId == (Int16?)Framework.Common.Enumeration.EnumOfWebAdminRole.Admin ||
                oBCAUserModel.UserRequestRoleId == (Int16?)Framework.Common.Enumeration.EnumOfWebAdminRole.PICApps))
            {
                result.Add("{RoleId} = " + oBCAUserModel.UserRequestRoleId);
            }

            return result.Count > 0 ? string.Join(" AND ", result) : null;
        }

        private DataTable ConvertToUserTable(List<UserInquery> listOfUser, string exportType)
        {
            DataTable oDataTable = new DataTable();
            oDataTable.Columns.Add("BranchCode", typeof(string));
            oDataTable.Columns.Add("OfficeCode", typeof(string));
            oDataTable.Columns.Add("UserStatus", typeof(string));
            oDataTable.Columns.Add("Shift", typeof(string));
            oDataTable.Columns.Add("UserId", typeof(string));
            oDataTable.Columns.Add("Name", typeof(string));
            oDataTable.Columns.Add("UserGroup", typeof(string));
            if (exportType.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfExportType.CSV_File)))
            {
                oDataTable.Columns.Add("Category", typeof(string));
                oDataTable.Columns.Add("StartPeriod", typeof(string));
                oDataTable.Columns.Add("EndPeriod", typeof(string));
                oDataTable.Columns.Add("UserType", typeof(string));
                oDataTable.Columns.Add("Quality", typeof(string));
                oDataTable.Columns.Add("ApprovedBy", typeof(string));
                oDataTable.Columns.Add("Reason", typeof(string));
            }
            oDataTable.Columns.Add("BDS1", typeof(string));
            oDataTable.Columns.Add("BDS2", typeof(string));
            oDataTable.Columns.Add("BDS3", typeof(string));
            oDataTable.Columns.Add("BDS4", typeof(string));
            oDataTable.Columns.Add("BDS5", typeof(string));
            oDataTable.Columns.Add("BDS6", typeof(string));
            oDataTable.Columns.Add("BDS7", typeof(string));
            oDataTable.Columns.Add("BDS8", typeof(string));
            oDataTable.Columns.Add("BDS9", typeof(string));
            oDataTable.Columns.Add("BDS10", typeof(string));

            foreach (var item in listOfUser)
            {
                DataRow dataRow = oDataTable.NewRow();
                dataRow[oDataTable.Columns[0].ColumnName] = item.BranchCode;
                dataRow[oDataTable.Columns[1].ColumnName] = item.OfficeCode;
                dataRow[oDataTable.Columns[2].ColumnName] = item.UserStatus;
                dataRow[oDataTable.Columns[3].ColumnName] = item.Shift;
                dataRow[oDataTable.Columns[4].ColumnName] = item.UserId;
                dataRow[oDataTable.Columns[5].ColumnName] = item.Name;
                dataRow[oDataTable.Columns[6].ColumnName] = item.UserGroup;
                if (exportType.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfExportType.CSV_File)))
                {
                    dataRow[oDataTable.Columns[7].ColumnName] = item.UserCategory;
                    dataRow[oDataTable.Columns[8].ColumnName] = item.StartPeriodString;
                    dataRow[oDataTable.Columns[9].ColumnName] = item.EndPeriodString;
                    dataRow[oDataTable.Columns[10].ColumnName] = item.UserType;
                    dataRow[oDataTable.Columns[11].ColumnName] = item.Quality;
                    dataRow[oDataTable.Columns[12].ColumnName] = item.ApprovedBy;
                    dataRow[oDataTable.Columns[13].ColumnName] = item.Reason;
                    dataRow[oDataTable.Columns[14].ColumnName] = item.BDS1;
                    dataRow[oDataTable.Columns[15].ColumnName] = item.BDS2;
                    dataRow[oDataTable.Columns[16].ColumnName] = item.BDS3;
                    dataRow[oDataTable.Columns[17].ColumnName] = item.BDS4;
                    dataRow[oDataTable.Columns[18].ColumnName] = item.BDS5;
                    dataRow[oDataTable.Columns[19].ColumnName] = item.BDS6;
                    dataRow[oDataTable.Columns[20].ColumnName] = item.BDS7;
                    dataRow[oDataTable.Columns[21].ColumnName] = item.BDS8;
                    dataRow[oDataTable.Columns[22].ColumnName] = item.BDS9;
                    dataRow[oDataTable.Columns[23].ColumnName] = item.BDS10;


                }
                else if (exportType.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfExportType.PDF_File)))
                {
                    dataRow[oDataTable.Columns[7].ColumnName] = item.BDS1;
                    dataRow[oDataTable.Columns[8].ColumnName] = item.BDS2;
                    dataRow[oDataTable.Columns[9].ColumnName] = item.BDS3;
                    dataRow[oDataTable.Columns[10].ColumnName] = item.BDS4;
                    dataRow[oDataTable.Columns[11].ColumnName] = item.BDS5;
                    dataRow[oDataTable.Columns[12].ColumnName] = item.BDS6;
                    dataRow[oDataTable.Columns[13].ColumnName] = item.BDS7;
                    dataRow[oDataTable.Columns[14].ColumnName] = item.BDS8;
                    dataRow[oDataTable.Columns[15].ColumnName] = item.BDS9;
                    dataRow[oDataTable.Columns[16].ColumnName] = item.BDS10;

                }
                oDataTable.Rows.Add(dataRow);

            }
            return oDataTable;
        }
        private DataTable ConvertToMutationTable(List<UserMutation> listOfMutation, string exportType)
        {
            DataTable oDataTable = new DataTable();
            oDataTable.Columns.Add("InOut", typeof(string));
            oDataTable.Columns.Add("UserId", typeof(string));
            oDataTable.Columns.Add("UserName", typeof(string));
            oDataTable.Columns.Add("BranchSourceCode", typeof(string));
            oDataTable.Columns.Add("OfficeSourceCode", typeof(string));
            oDataTable.Columns.Add("DestBranchCode", typeof(string));
            oDataTable.Columns.Add("DestOfficeCode", typeof(string));
            oDataTable.Columns.Add("EffectiveDateString", typeof(string));
            oDataTable.Columns.Add("MutationStatusString", typeof(string));

            foreach (var item in listOfMutation)
            {
                DataRow dataRow = oDataTable.NewRow();
                dataRow[oDataTable.Columns[0].ColumnName] = item.InOut;
                dataRow[oDataTable.Columns[1].ColumnName] = item.UserId;
                dataRow[oDataTable.Columns[2].ColumnName] = item.UserName;
                dataRow[oDataTable.Columns[3].ColumnName] = item.BranchSourceCode;
                dataRow[oDataTable.Columns[4].ColumnName] = item.OfficeSourceCode;
                dataRow[oDataTable.Columns[5].ColumnName] = item.DestBranchCode;
                dataRow[oDataTable.Columns[6].ColumnName] = item.DestOfficeCode;
                dataRow[oDataTable.Columns[7].ColumnName] = item.EffectiveDateString;
                dataRow[oDataTable.Columns[8].ColumnName] = item.MutationStatusString;
                oDataTable.Rows.Add(dataRow);

            }
            return oDataTable;
        }
        #endregion
    }
}