﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Entity
{
    public class FSAttendance
    {
        public int FSCentralId { get; set; }
        public string BranchCode { get; set; }
        public string OfficeCode { get; set; }
        public int AttendanceStatus { get; set; }
        public DateTime AttendanceDate { get; set; }
        public int FingerIndex { get; set; }
        public string FingerName { get; set; }
        public int Score { get; set; }
        public int SourceIpType { get; set; }
        public string SourceIp { get; set; }
        public string SourceName { get; set; }
    }
}