﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Entity
{
    public class HubOfficeActivity
    {
        public string CurrentBranchCode { get; set; }
        public string CurrentOfficeCode { get; set; }
        public string DestinationBranchCode { get; set; }
        public string DestinationOfficeCode { get; set; }
        public string NIP { get; set; }
        public string RegisterDate { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
    }


}