﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Entity
{
    public class UserLogInfo
    {
        public int CentralId { get; set; }
        public string UserId { get; set; }
        public string UserLogTime { get; set; }
        public int Mode { get; set; }
        public string MainBranchCode { get; set; }
        public string MainOfficeCode { get; set; }
    }
}