﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Entity
{
    public class UserInfo
    {

        public FSCentral FSCentral { get; set; }

        public FSMain FSMain { get; set; }

        public FSCategory FSCategory { get; set; }

        public List<FSMapping> FSMapping { get; set; }

        public FSImages FSImages { get; set; }

        public int FsLocationId { get; set; }

        public string CreatedBy { get; set; }

    }
}