﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Entity
{
    public class PagingInfo
    {
        public PagingInfo()
        {
            this.OrderType = "ASC";
        }
        public int? Index { get; set; }
        public int? PageSize { get; set; }
        public int TotalPage { get; set; }
        public string[] FilterKey { get; set; }
        public string[] FilterValue { get; set; }
        public string OrderBy { get; set; }
        public string OrderType { get; set; }
        public string ExportFileType { get; set; }

    }
}