﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Entity
{
    public class ZipFileInfo
    {
        public string FileName { get; set; }
        public string FileData { get; set; }
    }
}