﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Entity
{
    public class FSAllowedApps
    {
        public int FsAllowedAppsId { get; set; }
        public string AppId { get; set; }
        public string AppName { get; set; }
        public string AppVersion { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public int IsDelete { get; set; }
        public int CKV { get; set; }
        public bool CKVStatus { get; set; }
    }
}