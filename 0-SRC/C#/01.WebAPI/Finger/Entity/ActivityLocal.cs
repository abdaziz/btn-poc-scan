﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Entity
{
    public class ActivityLocal
    {
        public DateTime EventTime { get; set; }
        public int EventId { get; set; }
        public string UserId { get; set; }
        public int FingerIndex { get; set; }
        public int Score { get; set; }
        public int Status { get; set; }
        public int SysErrorCode { get; set; }
        public string Remark { get; set; }
        public int SourceIpType { get; set; }
        public string SourceIp { get; set; }
        public string SourceName { get; set; }
        public int CKV { get; set; }
    }
}