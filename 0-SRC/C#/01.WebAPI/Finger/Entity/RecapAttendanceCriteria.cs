﻿using System;


namespace Finger.Entity
{
    public class RecapAttendanceCriteria
    {
        public string BranchCode { get; set; }
        public string OfficeCode { get; set; }
        public DateTime StartPeriode { get; set; }
        public DateTime EndPeriode { get; set; }
        public string FilterKey { get; set; }
        public string FilterValue { get; set; }
        public short ShiftingStatus { get; set; }
        public PagingInfo PagingInfo { get; set; }
        public string UserLogonId { get; set; }
    }
}