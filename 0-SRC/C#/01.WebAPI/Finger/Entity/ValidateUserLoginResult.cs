﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Entity
{
    public class ValidateUserLoginResult
    {
        public int IsBranchKP { get; set; }
        public int MinQuality { get; set; }
    }

}