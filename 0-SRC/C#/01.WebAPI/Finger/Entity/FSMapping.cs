﻿using Finger.Framework.Common;
using Finger.Framework.Entity;
using Finger.Framework.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Entity
{
    public class FSMapping : CalculateCKV
    {
        public FSMapping()
        {
            this.CategoryId = -1;
        }

        public int FsCentralId { get; set; }
        public Int16 CategoryId { get; set; }
        public string UserName { get; set; }
        public short UserCategory { get; set; }
        public short UserType { get; set; }
        public short SubType { get; set; }
        public string BranchCode { get; set; }
        public string OfficeCode { get; set; }
        public string CreatedBy { get; set; }
        public int? ckv { get; set; }
        public int? dbckv { get; set; }

        public override void GenerateCKV()
        {
            string[] objArray = new string[7];
            objArray[0] = this.CategoryId.ToString();
            objArray[1] = this.UserName;
            objArray[2] = this.UserCategory.ToString();
            objArray[3] = this.UserType.ToString();
            objArray[4] = this.SubType.ToString();
            objArray[5] = this.BranchCode;
            objArray[6] = this.OfficeCode;

            this.ckv = Utility.GenerateCKV(objArray);
        }

        public override string CheckCKV(string fieldName)
        {
            if (this.CategoryId == -1 && this.dbckv == null) return string.Empty;

            string msg = string.Empty;
            if (this.dbckv != this.ckv)
            {
                msg = string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_5051), fieldName);
                throw new CKVException(msg);
            }
            return msg;
        }

       

    }
}