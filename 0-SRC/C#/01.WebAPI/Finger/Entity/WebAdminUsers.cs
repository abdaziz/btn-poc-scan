﻿using Finger.Framework.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Entity
{
    public class WebAdminUsers
    {
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string RoleName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedDateString { get { return this.CreatedDate.HasValue ? this.CreatedDate.Value.ToString(Constant.ACTIVITY_DATE_FORMAT) : string.Empty; } }
    }
}