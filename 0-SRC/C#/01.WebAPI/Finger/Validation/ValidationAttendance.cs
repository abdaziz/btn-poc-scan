﻿using Finger.Framework.Common;
using Finger.Helper;
using Finger.Models;
using System;


namespace Finger.Validation
{
    public class ValidationAttendance : ValidationBase
    {
        #region Properties
        public IdentifyAttendanceModel IdentifyAttendance { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string FieldNameCollection { get; set; }
        #endregion

        #region Constructor
        public ValidationAttendance(IdentifyAttendanceModel identifyAttendance)
        {
            this.IdentifyAttendance = identifyAttendance;
            this.ErrorCode = 0;
            this.ErrorMessage = string.Empty;
        }
        #endregion

        #region Public Method(s)
        public override bool Validate()
        {
            try
            {
                if (ValidateAllObjectPropertiesObjectNull())
                {
                    if (ValidateClientInfo())
                    {
                        ValidateAttendance();
                    }
                }

                return string.IsNullOrEmpty(this.ErrorMessage);
            }
            catch (System.Exception ex)
            {
                this.ErrorCode = Constant.ERROR_CODE_SYSTEM;
                throw ex;
            }
        }

        public override string GetErrorMessage()
        {
            return this.ErrorMessage;
        }

        public override int GetErrorCode()
        {
            return this.ErrorCode;
        }
        #endregion

        #region Private Method(s)

        private bool ValidateAllObjectPropertiesObjectNull()
        {
            if (this.IdentifyAttendance.ClientInfo == null)
            {
                MappingFieldName("Client Info");
            }

            if (this.IdentifyAttendance.Attendance == null)
            {
                MappingFieldName("Attendance");
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateClientInfo()
        {
            if (string.IsNullOrEmpty(this.IdentifyAttendance.ClientInfo.NewGuid))
            {
                MappingFieldName("Client Info-New Guid");

            }

            if (string.IsNullOrEmpty(this.IdentifyAttendance.ClientInfo.AppName))
            {
                MappingFieldName("Client Info-Application Name");

            }

            if (string.IsNullOrEmpty(this.IdentifyAttendance.ClientInfo.AppVersion))
            {
                MappingFieldName("Client Info-Application Version");

            }

            if (string.IsNullOrEmpty(this.IdentifyAttendance.ClientInfo.ActionName))
            {
                MappingFieldName("Client Info-Action Name");

            }

            if (string.IsNullOrEmpty(this.IdentifyAttendance.ClientInfo.IPAddress))
            {
                MappingFieldName("Client Info-IP Address");

            }

            if (this.IdentifyAttendance.ClientInfo.IPAddressType == 0)
            {
                MappingFieldName("Client Info-IP Address Type");

            }

            if (string.IsNullOrEmpty(this.IdentifyAttendance.ClientInfo.HostName))
            {
                MappingFieldName("Client Info-Host Name");

            }

            if (string.IsNullOrEmpty(this.IdentifyAttendance.ClientInfo.UserName))
            {
                MappingFieldName("Client Info-User Name");

            }

            if (string.IsNullOrEmpty(this.IdentifyAttendance.ClientInfo.BranchCode))
            {
                MappingFieldName("Client Info-Branch Code");

            }

            if (string.IsNullOrEmpty(this.IdentifyAttendance.ClientInfo.OfficeCode))
            {
                MappingFieldName("Client Info-Office Code");

            }

            if (string.IsNullOrEmpty(this.IdentifyAttendance.ClientInfo.LogonID))
            {
                MappingFieldName("Client Info-Logon ID");

            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);

        }

        private void ValidateAttendance()
        {
            if (!ValidateMandatoryField()) return;
            if (!ValidateLengthFormat()) return;
            if (!ValidateTemplate()) return;
        }

        private bool ValidateMandatoryField()
        {
            if (string.IsNullOrEmpty(this.IdentifyAttendance.Attendance.AttendanceDate))
            {
                MappingFieldName("Attendance-Attendance Date");

            }

            if (this.IdentifyAttendance.Attendance.AttendanceStatus == 0)
            {
                MappingFieldName("Attendance-Attendance Status");

            }

            if (this.IdentifyAttendance.Attendance.AttendanceUserType == 0)
            {
                MappingFieldName("Attendance-Attendance Type");

            }

            if (string.IsNullOrEmpty(this.IdentifyAttendance.Attendance.Template))
            {
                MappingFieldName("Attendance-Template");

            }

            if (this.IdentifyAttendance.Attendance.AttendanceUserType == (int)Enumeration.EnumOfAttendanceUserType.SPECIAL)
            {
                if (string.IsNullOrEmpty(this.IdentifyAttendance.Attendance.UserId))
                {
                    MappingFieldName("Attendance-User Id");
                }
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateLengthFormat()
        {
            if (this.IdentifyAttendance.Attendance.AttendanceUserType == (int)Enumeration.EnumOfAttendanceUserType.SPECIAL)
            {
                if (this.IdentifyAttendance.Attendance.UserId.Length < 6)
                {
                    string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5009).Replace("{NIP/NIK/Vendor NIP}", "NIP");
                    errorMessage = errorMessage.Replace("{Length Character}", "6");
                    ErrorMapping(Constant.ERR_CODE_5009, errorMessage);
                }
                else if (this.IdentifyAttendance.Attendance.UserId.Length > 6 && this.IdentifyAttendance.Attendance.UserId.Length < 10)
                {
                    string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5009).Replace("{NIP/NIK/Vendor NIP}", "Vendor NIP");
                    errorMessage = errorMessage.Replace("{Length Character}", "10");
                    ErrorMapping(Constant.ERR_CODE_5009, errorMessage);
                }
                else if (this.IdentifyAttendance.Attendance.UserId.Length > 10 && this.IdentifyAttendance.Attendance.UserId.Length < 16)
                {
                    string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5009).Replace("{NIP/NIK/Vendor NIP}", "NIK");
                    errorMessage = errorMessage.Replace("{Length Character}", "16");
                    ErrorMapping(Constant.ERR_CODE_5009, errorMessage);
                }
            }

            return string.IsNullOrEmpty(this.ErrorMessage);

        }

        private bool ValidateTemplate()
        {
            if (this.IdentifyAttendance.Attendance.Template.Length > 0)
            {
                if (!IsValidTemplate(this.IdentifyAttendance.Attendance.Template))
                {
                    string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5021);
                    ErrorMapping(Constant.ERR_CODE_5021, errorMessage);
                }
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private void SetErrorMandatoryField(string fieldName)
        {
            string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5008).Replace("{Mandatory Field}", fieldName);
            ErrorMapping(Constant.ERR_CODE_5008, errorMessage);
        }

        private void ErrorMapping(int errorCode, string errorMessage)
        {
            this.ErrorCode = errorCode;
            this.ErrorMessage = errorMessage;
        }

        private void MappingFieldName(string fieldName)
        {
            if (string.IsNullOrEmpty(this.FieldNameCollection))
            {
                this.FieldNameCollection = fieldName;
            }
            else
            {
                this.FieldNameCollection += "," + fieldName;
            }
        }

        private bool IsValidTemplate(string template)
        {
            bool isValid = false;
            try
            {
                byte[] NTemplate = ServiceMapper.Instance.ConvertToSingleTemplateBuffer(template);
                if (NTemplate.Length != 0)
                {
                    isValid = true;
                }
            }
            catch
            {


            }

            return isValid;
        }
        #endregion
    }
}