﻿using Finger.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Finger.Framework.Common;
using System.Text.RegularExpressions;
using Finger.Framework.Entity;
using Finger.Logic;

namespace Finger.Validation
{
    public class ValidationResetUserLogInquiry : ValidationBase
    {
        #region Private Field(s)
        private FingerLogic _fingerLogic;
        #endregion

        #region Properties
        public PagingInfo PagingInfo { get; set; }
        public string ErrorMessage { get; set; }
        public int ErrorCode { get; set; }



        public string UserLoginID { get; set; }

        public FingerLogic FingerLogic
        {
            get
            {
                if (_fingerLogic == null)
                {
                    _fingerLogic = new FingerLogic();
                }
                return _fingerLogic;
            }
        }
        #endregion

        #region constructor
        public ValidationResetUserLogInquiry(string userLoginID, PagingInfo oPagingInfo)
        {
            this.ErrorCode = 0;
            this.ErrorMessage = string.Empty;
            this.PagingInfo = oPagingInfo;

            this.UserLoginID = userLoginID;
        }
        #endregion

        #region Public Method(s)
        public override bool Validate()
        {
            if (this.PagingInfo.FilterKey != null && this.PagingInfo.FilterValue.Length != 0)
            {
                string FilterKey = this.PagingInfo.FilterKey[0];
                string FilterValue = this.PagingInfo.FilterValue[0];
                if (ValidateLength(FilterKey, FilterValue))
                {
                    ValidateContent(FilterKey, FilterValue);

                }
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }
        public override int GetErrorCode()
        {
            return this.ErrorCode;
        }
        public override string GetErrorMessage()
        {
            return this.ErrorMessage;
        }
        #endregion

        #region private
        private void ErrorMapping(int errorCode, string errorMessage)
        {
            this.ErrorCode = errorCode;
            this.ErrorMessage = errorMessage;
        }

        private bool ValidateLength(string FilterKey, string FilterValue)
        {
            switch (FilterKey)
            {
                case "Name":
                    if (FilterValue.Length == 0)
                    {
                        string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_4006).Replace("{0}", "Name");
                        ErrorMapping(Constant.ERR_CODE_4006, errorMessage);
                    }
                    //else if (!Regex.IsMatch(FilterKey, @"^[a-zA-Z]{1,64}$"))
                    //{
                    //    string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_4005).Replace("{0}", "Name");
                    //    ErrorMapping(Constant.ERR_CODE_4005, errorMessage.Replace("{1}", "4"));
                    //}
                    break;
                case "MainBranchCode":
                    if (FilterValue.Length == 0)
                    {
                        string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_4006).Replace("{0}", "Branch Code");
                        ErrorMapping(Constant.ERR_CODE_4006, errorMessage);
                    }
                    else if (!Regex.IsMatch(FilterValue, @"^[0-9]{4}$"))
                    {
                        string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_4005).Replace("{0}", "Branch Code");
                        errorMessage = errorMessage.Replace("{1}", "4");
                        ErrorMapping(Constant.ERR_CODE_4005, errorMessage);
                    }
                    break;
                case "MainOfficeCode":
                    if (FilterValue.Length == 0)
                    {
                        string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_4006).Replace("{0}", "Office Code");
                        ErrorMapping(Constant.ERR_CODE_4006, errorMessage);
                    }
                    else if (!Regex.IsMatch(FilterValue, @"^[0-9]{3}$"))
                    {
                        string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_4005).Replace("{0}", "Office Code");
                        errorMessage = errorMessage.Replace("{1}", "3");
                        ErrorMapping(Constant.ERR_CODE_4005, errorMessage);
                    }
                    break;
                default:
                    if (FilterValue.Length == 0)
                    {
                        string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_4006).Replace("{0}", "NIP");
                        ErrorMapping(Constant.ERR_CODE_4006, errorMessage);
                    }
                    else if (FilterValue.Length < 6)
                    {
                        string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_4005).Replace("{0}", "NIP");
                        errorMessage = errorMessage.Replace("{1}", "6");
                        ErrorMapping(Constant.ERR_CODE_4005, errorMessage.Replace("{1}", "6"));
                    }
                    else if (FilterValue.Length > 6 && FilterValue.Length < 10)
                    {
                        string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_4005).Replace("{0}", "Vendor NIP");
                        errorMessage = errorMessage.Replace("{1}", "10");
                        ErrorMapping(Constant.ERR_CODE_4005, errorMessage.Replace("{1}", "10"));
                    }
                    else if (FilterValue.Length > 10 && FilterValue.Length < 16)
                    {
                        string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_4005).Replace("{0}", "NIK");
                        errorMessage = errorMessage.Replace("{1}", "16");
                        ErrorMapping(Constant.ERR_CODE_4005, errorMessage.Replace("{1}", "16"));
                    }
                    else if (FilterValue.Length > 16)
                    {
                        string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_4005).Replace("{0}", "NIK");
                        errorMessage = errorMessage.Replace("{1}", "16");
                        ErrorMapping(Constant.ERR_CODE_4005, errorMessage);
                    }
                    break;
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateContent(string FilterKey, string FilterValue)
        {
            if (FilterKey == "UserId")
            {
                switch (FilterValue.Length)
                {
                    case 16:
                        if (!Regex.IsMatch(FilterValue, @"^[0-9]{16}$"))
                        {
                            string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_4005).Replace("{0}", "NIP");
                            errorMessage = errorMessage.Replace("{1}", "6");
                            ErrorMapping(Constant.ERR_CODE_4005, errorMessage);
                        }

                        break;
                    case 10:
                        if (!Regex.IsMatch(FilterValue, @"^[0-9]{10}$"))
                        {
                            string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_4005).Replace("{0}", "NIP");
                            errorMessage = errorMessage.Replace("{1}", "6");
                            ErrorMapping(Constant.ERR_CODE_4005, errorMessage);
                        }

                        break;
                    default:
                        if (!Regex.IsMatch(FilterValue, @"^[0-9]{6}$"))
                        {
                            string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_4005).Replace("{0}", "NIP");
                            errorMessage = errorMessage.Replace("{1}", "6");
                            ErrorMapping(Constant.ERR_CODE_4005, errorMessage.Replace("{1}", "6"));
                        }
                        break;
                }
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateBranchHierarchy(string branchCode)
        {

            ServiceResponse response = FingerLogic.ValidateBranchHierarchy(this.UserLoginID, branchCode);
            if (response.statusCode != Constant.NO_ERROR)
            {
                ErrorMapping(response.statusCode, response.statusMessage);

            }

            return string.IsNullOrEmpty(this.ErrorMessage);

        }
        #endregion
    }
}