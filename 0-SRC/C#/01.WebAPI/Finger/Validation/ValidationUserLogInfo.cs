﻿using Finger.Entity;
using System;

namespace Finger.Validation
{
    public class ValidationUserLogInfo : ValidationBase
    {
        public int CentralId { get; set; }
        public string UserId { get; set; }
        public string UserLogTime { get; set; }
        public int mode { get; set; }


        public string ErrorMessage { get; set; }

        #region constructor
        public ValidationUserLogInfo(UserLogInfo userLogInfo)
        {
            this.CentralId = userLogInfo.CentralId;
            this.UserId = userLogInfo.UserId;
            this.UserLogTime = userLogInfo.UserLogTime;
            this.mode = userLogInfo.Mode;
        }
        #endregion

        #region Public Method(s)
        public override bool Validate()
        {
            string message = string.Empty;
            if (CentralId == 0)
            {
                message = "CentralId is mandatory";
                ErrorMapping(message);
            }
            if (string.IsNullOrEmpty(UserId))
            {
                message = "UseId is mandatory";
                ErrorMapping(message);
            }
            if (string.IsNullOrEmpty(UserLogTime))
            {
                message = "UserLogInfo is mandatory";
                ErrorMapping(message);
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        public override string GetErrorMessage()
        {
            return this.ErrorMessage;
        }
        #endregion

        #region Private Method(s)
        private void ErrorMapping(string message)
        {
            if (string.IsNullOrEmpty(this.ErrorMessage))
            {
                this.ErrorMessage = message;
            }
            else
            {
                this.ErrorMessage += "," + message;
            }
        }
        #endregion

        public override int GetErrorCode()
        {
            throw new NotImplementedException();
        }
    }
}