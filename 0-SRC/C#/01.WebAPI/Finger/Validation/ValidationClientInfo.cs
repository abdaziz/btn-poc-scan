﻿using Finger.Core.Entity;
using Finger.Entity;
using Finger.Framework.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Validation
{
    public class ValidationClientInfo : ValidationBase
    {
   
        #region Properties

        public ClientInfo ClientInfo { get; set; }
        public string FieldNameCollection { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }

        #endregion

        #region constructor
        public ValidationClientInfo(ClientInfo clientInfo)
        {
            this.ClientInfo = clientInfo;
            this.ErrorCode = 0;
            this.ErrorMessage = string.Empty;
        }
        #endregion

        #region Public Method(s)
        public override bool Validate()
        {
            ValidateClientInfo();
            return string.IsNullOrEmpty(this.ErrorMessage);
        }


        public override int GetErrorCode()
        {
            return this.ErrorCode;
        }
        public override string GetErrorMessage()
        {
            return this.ErrorMessage;
        }
        #endregion

        #region Private Method(s)

        private bool ValidateClientInfo()
        {
            if (string.IsNullOrEmpty(this.ClientInfo.NewGuid))
            {
                MappingFieldName("Client Info-New Guid");

            }

            if (string.IsNullOrEmpty(this.ClientInfo.AppName))
            {
                MappingFieldName("Client Info-Application Name");

            }

            if (string.IsNullOrEmpty(this.ClientInfo.AppVersion))
            {
                MappingFieldName("Client Info-Application Version");

            }

            if (string.IsNullOrEmpty(this.ClientInfo.ActionName))
            {
                MappingFieldName("Client Info-Action Name");

            }

            if (string.IsNullOrEmpty(this.ClientInfo.IPAddress))
            {
                MappingFieldName("Client Info-IP Address");

            }

            if (this.ClientInfo.IPAddressType == 0)
            {
                MappingFieldName("Client Info-IP Address Type");

            }

            if (string.IsNullOrEmpty(this.ClientInfo.HostName))
            {
                MappingFieldName("Client Info-Host Name");

            }

            if (string.IsNullOrEmpty(this.ClientInfo.UserName))
            {
                MappingFieldName("Client Info-User Name");

            }

            if (string.IsNullOrEmpty(this.ClientInfo.BranchCode))
            {
                MappingFieldName("Client Info-Branch Code");

            }

            if (string.IsNullOrEmpty(this.ClientInfo.OfficeCode))
            {
                MappingFieldName("Client Info-Office Code");

            }

            if (string.IsNullOrEmpty(this.ClientInfo.LogonID))
            {
                MappingFieldName("Client Info-Logon ID");

            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);

        }

        private void MappingFieldName(string fieldName)
        {
            if (string.IsNullOrEmpty(this.FieldNameCollection))
            {
                this.FieldNameCollection = fieldName;
            }
            else
            {
                this.FieldNameCollection += "," + fieldName;
            }
        }

        private void ErrorMapping(int errorCode, string errorMessage)
        {
            this.ErrorCode = errorCode;
            this.ErrorMessage = errorMessage;
        }

        private void SetErrorMandatoryField(string fieldName)
        {
            string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5008).Replace("{Mandatory Field}", fieldName);
            ErrorMapping(Constant.ERR_CODE_5008, errorMessage);
        }

        #endregion

       
    }

}