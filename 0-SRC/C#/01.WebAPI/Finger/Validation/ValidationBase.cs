﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Validation
{
    public abstract class ValidationBase
    {
        public abstract bool Validate();
        public abstract string GetErrorMessage();
        public abstract int GetErrorCode();
    }
}