﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Validation
{
    public class ValidationIdentify : ValidationBase
    {
        #region Properties
        public string ErrorMessage { get; set; }
        public string Template { get; set; }
        #endregion

        #region Constructor
        public ValidationIdentify(string template)
        {
            this.Template = template;

        }
        #endregion

        #region Public Method(s)
        public override bool Validate()
        {
            string message = string.Empty;
            if (string.IsNullOrEmpty(Template))
            {
                message = "Template cannot be null";
                ErrorMapping(message);

            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        public override string GetErrorMessage()
        {
            return this.ErrorMessage;
        }
        #endregion

        #region Private Method(s)
        private void ErrorMapping(string message)
        {
            if (string.IsNullOrEmpty(this.ErrorMessage))
            {
                this.ErrorMessage = message;
            }
            else
            {
                this.ErrorMessage += "," + message;
            }
        }
        #endregion

        public override int GetErrorCode()
        {
            throw new NotImplementedException();
        }
    }
}