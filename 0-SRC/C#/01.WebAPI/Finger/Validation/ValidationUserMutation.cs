﻿using Finger.Core.Entity;
using Finger.Entity;
using Finger.Framework.Common;
using Finger.Logic;
using Finger.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Validation
{
    public class ValidationUserMutation : ValidationBase
    {
        #region Private Field(s)
        private FingerLogic _fingerLogic;
        #endregion

        #region Properties
        public UserMutation UserMutation { get; set; }

        public ClientInfo ClientInfo { get; set; }

        public Int16 MutationAction { get; set; }

        public string FieldNameCollection { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }

        public FingerLogic FingerLogic
        {
            get
            {
                if (_fingerLogic == null)
                {
                    _fingerLogic = new FingerLogic();
                }
                return _fingerLogic;
            }
        }
        #endregion

        #region constructor
        public ValidationUserMutation(UserMutation oUserMutation, ClientInfo clientInfo, Int16 mutationAction)
        {
            this.UserMutation = oUserMutation;
            this.ClientInfo = clientInfo;
            this.ErrorCode = 0;
            this.ErrorMessage = string.Empty;
            this.MutationAction = mutationAction;
        }

        #endregion

        #region Public Method(s)
        public override bool Validate()
        {
            if (MutationAction == (Int16)Enumeration.EnumOfMutationAction.REGISTER_MUTATION)
            {
                ValidateUserMutation();
                ValidateAuthorizationRegisterMutation();
            }
            else if (MutationAction == (Int16)Enumeration.EnumOfMutationAction.CANCEL_MUTATION)
            {
                ValidateCancelMutation();
            
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }
        public override int GetErrorCode()
        {
            return this.ErrorCode;
        }
        public override string GetErrorMessage()
        {
            return this.ErrorMessage;
        }
        #endregion

        #region Private Method(s)
        private void ValidateUserMutation()
        {
            if (string.IsNullOrEmpty(this.UserMutation.UserId))
            {
                MappingFieldName("NIP/NIK/Vendor NIP");
            }

            if (string.IsNullOrEmpty(this.UserMutation.DestBranchCode))
            {
                MappingFieldName("Destination Branch Code");
            }

            if (string.IsNullOrEmpty(this.UserMutation.DestOfficeCode))
            {
                MappingFieldName("Destination Office Code");
            }

            if (this.UserMutation.EffectiveDate == null)
            {
                MappingFieldName("Effective Date");
            }
            else if (this.UserMutation.EffectiveDate <= DateTime.Now)
            {
                MappingFieldName("Effective date could not less than or equal today.");
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

           
        }

        private void ValidateAuthorizationRegisterMutation()
        {
            UserInfoModel model = FingerLogic.GetUserInfoByUserId(this.ClientInfo.LogonID);
            if (model != null)
            {
                UserInfoModel mutationUser = FingerLogic.GetUserInfoByUserId(this.UserMutation.UserId);
                if (mutationUser != null)
                {
                    if (model.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_ENROLLER && mutationUser.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_MANAGER)
                    {
                        ErrorMapping(Constant.ERR_CODE_9009, Utility.GetErrorMessage(Constant.ERR_CODE_9009));
                    }
                    else if (model.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_ENROLLER && mutationUser.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_ENROLLER)
                    {
                        ErrorMapping(Constant.ERR_CODE_9009, Utility.GetErrorMessage(Constant.ERR_CODE_9009));
                    }
                    else if (model.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_ENROLLER && mutationUser.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_REPORTER)
                    {
                        ErrorMapping(Constant.ERR_CODE_9009, Utility.GetErrorMessage(Constant.ERR_CODE_9009));
                    }
                    else
                    {
                        if (model.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_REPORTER || model.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_USER)
                        {
                            ErrorMapping(Constant.ERR_CODE_9009, Utility.GetErrorMessage(Constant.ERR_CODE_9009));
                        }

                    }
                }
                else
                {

                    ErrorMapping(Constant.ERR_CODE_9010, Utility.GetErrorMessage(Constant.ERR_CODE_9010).Replace("{NIP/NIK/Vendor NIP}", this.UserMutation.UserId));
                }

            }
            else
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7003).Replace("{NIP/NIK/Vendor NIP}", this.ClientInfo.LogonID);
                ErrorMapping(Constant.ERR_CODE_7003, errorMessage);

            }
        }

        private void ValidateCancelMutation()
        {
            UserInfoModel model = FingerLogic.GetUserInfoByUserId(this.ClientInfo.LogonID);
            if (model != null)
            {

                if (model.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_REPORTER || model.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_USER)
                {
                    ErrorMapping(Constant.ERR_CODE_9011, Utility.GetErrorMessage(Constant.ERR_CODE_9011));
                }

            }
            else
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7003).Replace("{NIP/NIK/Vendor NIP}", this.ClientInfo.LogonID);
                ErrorMapping(Constant.ERR_CODE_7003, errorMessage);

            }
        }


        private void MappingFieldName(string fieldName)
        {
            if (string.IsNullOrEmpty(this.FieldNameCollection))
            {
                this.FieldNameCollection = fieldName;
            }
            else
            {
                this.FieldNameCollection += "," + fieldName;
            }
        }
        private void SetErrorMandatoryField(string fieldName)
        {
            string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5008).Replace("{Mandatory Field}", fieldName);
            ErrorMapping(Constant.ERR_CODE_5008, errorMessage);
        }
        private void ErrorMapping(int errorCode, string errorMessage)
        {
            this.ErrorCode = errorCode;
            this.ErrorMessage = errorMessage;
        }
        #endregion
    }
}