﻿using Finger.Core;
using Finger.Core.Base;
using Finger.Core.Common;
using Finger.Core.Entity;
using Finger.Entity;
using Finger.Framework.Common;
using Finger.Framework.Entity;
using Finger.Helper;
using Finger.Logic;
using Finger.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Validation
{
    public class ValidationRegisterUserInfo : ValidationBase
    {

        #region Private Field(s)
        private FingerLogic _fingerLogic;
        #endregion

        #region Properties
        public FingerLogic FingerLogic
        {
            get
            {
                if (_fingerLogic == null)
                {
                    _fingerLogic = new FingerLogic();
                }
                return _fingerLogic;
            }
        }
        public UserInfoModel UserInfo { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }

        public string FieldNameCollection { get; set; }

        public string GuidThreadId { get; set; }

        #endregion

        #region Constant
        private const string USER_DESTINATION_BRANCH_CODE_FIELD_NAME = "Destination Branch Code";
        private const string USER_DESTINATION_OFFICE_CODE_FIELD_NAME = "Destination Office Code";
        private const string USER_ID_FIELD_NAME = "User Id";
        private const string USER_NAME_FIELD_NAME = "Name";
        private const string USER_TYPE_FIELD_NAME = "User Status";
        private const string USER_TEMPLATE_FIELD_NAME = "Template";
        private const string USER_GROUP_FIELD_NAME = "User Group";
        private const string USER_QUALITY_FIELD_NAME = "Finger Quality";
        private const string USER_IMAGE_MAP_FIELD_NAME = "Image Map";
        private const string USER_TEMPLATE_LAYOUT_FIELD_NAME = "Template Layout";
        private const string USER_CATEGORY_ID = "Category";
        private const string USER_CATEGORY_START_PERIODE = "Category Start Periode";
        private const string USER_CATEGORY_END_PERIODE = "Category End Periode";
        private const int MAXIMUM_BDSID_LENGTH_CHARACTER = 8;
        private const int MAXIMUM_BRANCH_CODE_LENGTH_CHARACTER = 4;
        private const int MAXIMUM_OFFICE_CODE_LENGTH_CHARACTER = 3;
        #endregion

        #region Constructor
        public ValidationRegisterUserInfo(string guidThreadId, UserInfoModel userInfo)
        {
            this.UserInfo = userInfo;
            this.ErrorCode = 0;
            this.ErrorMessage = string.Empty;
            this.GuidThreadId = guidThreadId;

        }
        #endregion

        #region Public Method(s)
        public override bool Validate()
        {
            try
            {
                //if (ValidateAllObjectPropertiesUserInfoObjectNull())
                //{
                //    if (ValidateClientInfo())
                //    {
                        ValidateRegisterInfo();
                //    }
                //}

                return string.IsNullOrEmpty(this.ErrorMessage);

            }
            catch (System.Exception ex)
            {
                this.ErrorCode = Constant.ERROR_CODE_SYSTEM;
                throw ex;
            }
        }

        public override string GetErrorMessage()
        {
            return this.ErrorMessage;
        }

        public override int GetErrorCode()
        {
            return this.ErrorCode;
        }
        #endregion

        #region Private Method(s)

        private bool ValidateClientInfo()
        {
            if (string.IsNullOrEmpty(this.UserInfo.ClientInfo.NewGuid))
            {
                MappingFieldName("Client Info-New Guid");

            }

            if (string.IsNullOrEmpty(this.UserInfo.ClientInfo.AppName))
            {
                MappingFieldName("Client Info-Application Name");

            }

            if (string.IsNullOrEmpty(this.UserInfo.ClientInfo.AppVersion))
            {
                MappingFieldName("Client Info-Application Version");

            }

            if (string.IsNullOrEmpty(this.UserInfo.ClientInfo.ActionName))
            {
                MappingFieldName("Client Info-Action Name");

            }

            if (string.IsNullOrEmpty(this.UserInfo.ClientInfo.IPAddress))
            {
                MappingFieldName("Client Info-IP Address");

            }

            if (this.UserInfo.ClientInfo.IPAddressType == 0)
            {
                MappingFieldName("Client Info-IP Address Type");

            }

            if (string.IsNullOrEmpty(this.UserInfo.ClientInfo.HostName))
            {
                MappingFieldName("Client Info-Host Name");

            }

            if (string.IsNullOrEmpty(this.UserInfo.ClientInfo.UserName))
            {
                MappingFieldName("Client Info-User Name");

            }

            if (string.IsNullOrEmpty(this.UserInfo.ClientInfo.BranchCode))
            {
                MappingFieldName("Client Info-Branch Code");

            }

            if (string.IsNullOrEmpty(this.UserInfo.ClientInfo.OfficeCode))
            {
                MappingFieldName("Client Info-Office Code");

            }

            if (string.IsNullOrEmpty(this.UserInfo.ClientInfo.LogonID))
            {
                MappingFieldName("Client Info-Logon ID");

            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);

        }

        private void ValidateRegisterInfo()
        {
            //if (!ValidateMandatoryField()) return;

            //if (!ValidateIsExistLogonID()) return;

            //if (!ValidateAuthorizedMenuByUserId()) return;

            //if (!ValidateOnlyNormalAndPenempatanSementaraCanbeRegister()) return;

            //if (!ValidateAllowNumberOnly()) return;

            //if (!ValidateBranchCodeLength()) return;

            //if (!ValidateOfficeCodeLength()) return;

            //if (!ValidateUserIDLength()) return;

            //if (!ValidateBDSIDLength()) return;

            //if (!ValidateDuplicateInputBDSID()) return;

            //if (!ValidateFourDigitBranchCodeBDSIDNotRegisteredInOrganizationMatrix()) return;

            //if (!ValidateDestinationBranchCodeAndOfficeCodeVersionIsAlreadyRegistered()) return;

            //if (!ValidateDestinationBranchCodeAndOfficeCodeIsVersion3()) return;

            if (!ValidateBDSIDAlreadyRegistered()) return;

            //if (!ValidateBDSFormat()) return;

            //if (!ValidateBranchHierarchy()) return;

            //if (!ValidateUserStatusVsUserGroup()) return;

            //if (!ValidateUserStatusVsUserCategory()) return;

            //if (!ValidateUserGroupWithLogonUserGroup()) return;

            if (!ValidateTemplate()) return;

            //if (!ValidateUserNotRegisteredAsNormalForSpecialCategory()) return;

            //if (!ValidateUserIdNotMatchedWithCentralId()) return;

            //if (!ValidateStartPeriod()) return;

            //if (!ValidateEndPeriod()) return;

            if (!ValidateFingerIsAlreadyRegisteredAsNormalCategoryInCentralServer()) return;

            //if (!ValidateUserIdAndCategoryIsAlreadyRegisteredInTheSameBranch()) return;

            //if (!ValidateUserIdNormalIsAlreadyRegisteredInAnotherBranch()) return;

            //if (!ValidateUserIdIsAlreadyRegisteredWithinActivePeriodInSameBranch()) return;

            //if (!ValidateUserIdIsAlreadyRegisteredWithinActivePeriodInAnotherBranch()) return;

            //if (!ValidateVendorNipNotMatchedWithDestinationBranchInNormalCategory()) return;

            //if (!ValidateVendorCodeFormatForAlihdaya()) return;
        }

        private bool ValidateAllObjectPropertiesUserInfoObjectNull()
        {
            if (this.UserInfo.FingerCentral == null)
            {
                MappingFieldName("User Info-FingerCentral");
            }

            if (this.UserInfo.FingerMain == null)
            {
                MappingFieldName("User Info-FingerMain");
            }
            else
            {

                if (this.UserInfo.FingerMain.GroupId <= 0)
                {
                    MappingFieldName("User Info-GroupId");
                }

            }

            if (this.UserInfo.FingerCategory == null)
            {
                MappingFieldName("User Info-FingerCategory");
            }
            else
            {

                if (this.UserInfo.FingerCategory.CategoryId < 0)
                {
                    MappingFieldName("User Info-CategoryId");
                }

            }

            if (this.UserInfo.FingerBDSID == null)
            {
                MappingFieldName("User Info-FingerBDSID");
            }
            else
            {

                if (this.UserInfo.FingerBDSID.UserType <= 0)
                {
                    MappingFieldName("User Info-UserType");
                }
                else
                {
                    if (this.UserInfo.FingerBDSID.UserType == (Int16)Enumeration.EnumOfUserStatus.USER_STATUS_ALIH_DAYA)
                    {
                        if (this.UserInfo.FingerBDSID.SubType <= 0)
                        {
                            MappingFieldName("User Info-SubType");

                        }
                    }
                }
            }


            if (this.UserInfo.FingerCategory != null)
            {

                if (this.UserInfo.FingerCategory.CategoryId == (Int16)Enumeration.EnumOfCategory.CATEGORY_NORMAL)
                {
                    if (this.UserInfo.FingerImage == null)
                    {
                        MappingFieldName("User Info-FingerImage");
                    }
                    else
                    {
                        if (
                            string.IsNullOrEmpty(this.UserInfo.FingerImage.FSImages0) &&
                            string.IsNullOrEmpty(this.UserInfo.FingerImage.FSImages1) &&
                            string.IsNullOrEmpty(this.UserInfo.FingerImage.FSImages2) &&
                            string.IsNullOrEmpty(this.UserInfo.FingerImage.FSImages3) &&
                            string.IsNullOrEmpty(this.UserInfo.FingerImage.FSImages4) &&
                            string.IsNullOrEmpty(this.UserInfo.FingerImage.FSImages5) &&
                            string.IsNullOrEmpty(this.UserInfo.FingerImage.FSImages6) &&
                            string.IsNullOrEmpty(this.UserInfo.FingerImage.FSImages7) &&
                            string.IsNullOrEmpty(this.UserInfo.FingerImage.FSImages8) &&
                            string.IsNullOrEmpty(this.UserInfo.FingerImage.FSImages9)
                            )
                        {
                            MappingFieldName("User Info-FingerImage");
                        }
                    }
                }
            }


            if (this.UserInfo.FingerCategory != null)
            {

                if (this.UserInfo.FingerCategory.CategoryId == (Int16)Enumeration.EnumOfCategory.CATEGORY_NORMAL)
                {
                    if (this.UserInfo.Template == null)
                    {
                        MappingFieldName("User Info-Template");
                    }
                }
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateMandatoryField()
        {

            if (string.IsNullOrEmpty(this.UserInfo.FingerCentral.DestinationBranchCode))
            {
                MappingFieldName(USER_DESTINATION_BRANCH_CODE_FIELD_NAME);
            }

            if (string.IsNullOrEmpty(this.UserInfo.FingerCentral.DestinationOfficeCode))
            {
                MappingFieldName(USER_DESTINATION_OFFICE_CODE_FIELD_NAME);
            }


            if (this.UserInfo.FingerBDSID.UserType != (Int16)Enumeration.EnumOfUserStatus.USER_STATUS_ALIH_DAYA)
            {
                if (string.IsNullOrEmpty(this.UserInfo.FingerCentral.UserId))
                {
                    MappingFieldName(USER_ID_FIELD_NAME);
                }
            }


            if (string.IsNullOrEmpty(this.UserInfo.FingerMain.Name))
            {
                MappingFieldName(USER_NAME_FIELD_NAME);
            }

            if (this.UserInfo.FingerMain.GroupId < 1)
            {
                MappingFieldName(USER_GROUP_FIELD_NAME);
            }


            if (!string.IsNullOrEmpty(this.UserInfo.FingerMain.Reason) && !string.IsNullOrEmpty(this.UserInfo.FingerMain.ApprovedBy))
            {
                if (this.UserInfo.FingerMain.Quality < 1)
                {
                    MappingFieldName(USER_QUALITY_FIELD_NAME);
                }
            }


            if (this.UserInfo.FingerBDSID.UserType < 1)
            {
                MappingFieldName(USER_TYPE_FIELD_NAME);
            }

            if (this.UserInfo.FingerMain.ImageMap < 1)
            {
                MappingFieldName(USER_IMAGE_MAP_FIELD_NAME);
            }

            if (string.IsNullOrEmpty(this.UserInfo.FingerMain.TemplateLayout))
            {
                MappingFieldName(USER_TEMPLATE_LAYOUT_FIELD_NAME);
            }

            if (this.UserInfo.FingerCategory.CategoryId < 0)
            {
                MappingFieldName(USER_CATEGORY_ID);
            }

            if (this.UserInfo.FingerCategory.CategoryId == (Int16)Enumeration.EnumOfCategory.CATEGORY_NORMAL)
            {
                if (this.UserInfo.Template.Length == 0)
                {
                    MappingFieldName(USER_TEMPLATE_FIELD_NAME);
                }
            }

            if (this.UserInfo.FingerCategory.CategoryId == (Int16)Enumeration.EnumOfCategory.CATEGORY_PENEMPATAN_SEMENTARA
                )
            {
                DateTime defaultDate = new DateTime(1900, 01, 01);
                if (this.UserInfo.FingerCategory.CategoryStartPeriod == defaultDate)
                {
                    MappingFieldName(USER_CATEGORY_START_PERIODE);
                }

                if (this.UserInfo.FingerCategory.CategoryEndPeriod == defaultDate)
                {
                    MappingFieldName(USER_CATEGORY_END_PERIODE);
                }
            }


            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateOnlyNormalAndPenempatanSementaraCanbeRegister()
        {
            if (this.UserInfo.FingerCategory.CategoryId == (short)Enumeration.EnumOfCategory.CATEGORY_NORMAL ||
                this.UserInfo.FingerCategory.CategoryId == (short)Enumeration.EnumOfCategory.CATEGORY_PENEMPATAN_SEMENTARA
                )
            {
                return true;
            }

            string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5031);
            ErrorMapping(Constant.ERR_CODE_5031, errorMessage);
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateIsExistLogonID()
        {
            if (this.UserInfo.ClientInfo.LogonID.ToLower().Equals(Constant.ADMIN)) return true;

            UserInfoModel userLoginInfoModel = FingerLogic.GetUserInfoByUserId(this.UserInfo.ClientInfo.LogonID);
            if (userLoginInfoModel == null)
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7003).Replace("{NIP/NIK/Vendor NIP}", this.UserInfo.ClientInfo.LogonID);
                ErrorMapping(Constant.ERR_CODE_7003, errorMessage);
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateAuthorizedMenuByUserId()
        {
            bool isValidAccessMenu = FingerLogic.IsValidAccessMenu(this.UserInfo.ClientInfo, Enumeration.EnumOfFSMGMTMenu.Registration);
            if (isValidAccessMenu) return true;

            if (!isValidAccessMenu)
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5057).Replace("{menuname}", Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.Registration));
                ErrorMapping(Constant.ERR_CODE_5057, errorMessage);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateUserGroupWithLogonUserGroup()
        {
            if (this.UserInfo.ClientInfo.LogonID.ToLower().Equals(Constant.ADMIN)) return true;

            UserInfoModel userLoginInfoModel = FingerLogic.GetUserInfoByUserId(this.UserInfo.ClientInfo.LogonID);
            if (userLoginInfoModel.FingerCentral != null && userLoginInfoModel.FingerMain != null)
            {
                bool isNotValid = false;
                if (userLoginInfoModel.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_ENROLLER &&
                    this.UserInfo.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_MANAGER)
                {
                    isNotValid = true;
                }
                else if (userLoginInfoModel.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_ENROLLER &&
                   this.UserInfo.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_REPORTER)
                {
                    isNotValid = true;
                }
                else if (userLoginInfoModel.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_ENROLLER &&
              this.UserInfo.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_ENROLLER)
                {
                    isNotValid = true;
                }
                else if (userLoginInfoModel.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_REPORTER &&
             this.UserInfo.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_MANAGER)
                {
                    isNotValid = true;
                }
                else if (userLoginInfoModel.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_REPORTER &&
         this.UserInfo.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_ENROLLER)
                {
                    isNotValid = true;
                }
                else if (userLoginInfoModel.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_REPORTER &&
                  this.UserInfo.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_REPORTER)
                {
                    isNotValid = true;
                }
                else if (userLoginInfoModel.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_REPORTER &&
                 this.UserInfo.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_USER)
                {
                    isNotValid = true;
                }
                else if (userLoginInfoModel.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_USER &&
                this.UserInfo.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_MANAGER)
                {
                    isNotValid = true;
                }
                else if (userLoginInfoModel.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_USER &&
               this.UserInfo.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_ENROLLER)
                {
                    isNotValid = true;
                }
                else if (userLoginInfoModel.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_USER &&
            this.UserInfo.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_REPORTER)
                {
                    isNotValid = true;
                }
                else if (userLoginInfoModel.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_USER &&
          this.UserInfo.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_USER)
                {
                    isNotValid = true;
                }


                if (isNotValid)
                {
                    string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7004);
                    ErrorMapping(Constant.ERR_CODE_7004, errorMessage);
                }
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateUserIDLength()
        {
            if (this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_TETAP || this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_BAKTI)
            {
                if (this.UserInfo.FingerCentral.UserId.Length != 6)
                {
                    string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5009).Replace("{NIP/NIK/Vendor NIP}", USER_ID_FIELD_NAME);
                    errorMessage = errorMessage.Replace("{Length Character}", "6");
                    ErrorMapping(Constant.ERR_CODE_5009, errorMessage);
                }
            }
            else if (this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_ALIH_DAYA)
            {
                if (this.UserInfo.FingerCentral.UserId.Length != 10)
                {
                    string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5009).Replace("{NIP/NIK/Vendor NIP}", USER_ID_FIELD_NAME);
                    errorMessage = errorMessage.Replace("{Length Character}", "10");
                    ErrorMapping(Constant.ERR_CODE_5009, errorMessage);

                }
            }
            else if (this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_TANPA_NIP)
            {
                if (this.UserInfo.FingerCentral.UserId.Length != 16)
                {
                    string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5009).Replace("{NIP/NIK/Vendor NIP}", USER_ID_FIELD_NAME);
                    errorMessage = errorMessage.Replace("{Length Character}", "16");
                    ErrorMapping(Constant.ERR_CODE_5009, errorMessage);

                }
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateAllowNumberOnly()
        {
            if (!Utility.IsNumericOnly(this.UserInfo.ClientInfo.BranchCode))
            {
                MappingFieldName("Client Info-Branch Code");
            }

            if (!Utility.IsNumericOnly(this.UserInfo.FingerCentral.DestinationBranchCode))
            {
                MappingFieldName(USER_DESTINATION_BRANCH_CODE_FIELD_NAME);
            }


            if (!Utility.IsNumericOnly(this.UserInfo.ClientInfo.OfficeCode))
            {
                MappingFieldName("Client Info-Office Code");
            }

            if (!this.UserInfo.ClientInfo.LogonID.ToLower().Equals(Constant.ADMIN))
            {
                if (!Utility.IsNumericOnly(this.UserInfo.ClientInfo.LogonID))
                {
                    MappingFieldName("Client Info-LogonID");
                }
            }

            if (!Utility.IsNumericOnly(this.UserInfo.FingerCentral.DestinationOfficeCode))
            {
                MappingFieldName(USER_DESTINATION_OFFICE_CODE_FIELD_NAME);
            }


            if (!Utility.IsNumericOnly(this.UserInfo.FingerCentral.UserId))
            {
                MappingFieldName("NIP/NIK/Vendor NIP");
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorAllowNumberOnly(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateBDSIDLength()
        {
            if (this.UserInfo.FingerBDSID.BDSIDNotInput()) return true;

            if (!string.IsNullOrEmpty(this.UserInfo.FingerBDSID.BDSID1))
            {
                if (this.UserInfo.FingerBDSID.BDSID1.Length != MAXIMUM_BDSID_LENGTH_CHARACTER)
                {
                    MappingFieldName("BDSID1");
                }
            }

            if (!string.IsNullOrEmpty(this.UserInfo.FingerBDSID.BDSID2))
            {
                if (this.UserInfo.FingerBDSID.BDSID2.Length != MAXIMUM_BDSID_LENGTH_CHARACTER)
                {
                    MappingFieldName("BDSID2");
                }
            }

            if (!string.IsNullOrEmpty(this.UserInfo.FingerBDSID.BDSID3))
            {
                if (this.UserInfo.FingerBDSID.BDSID3.Length != MAXIMUM_BDSID_LENGTH_CHARACTER)
                {
                    MappingFieldName("BDSID3");
                }
            }

            if (!string.IsNullOrEmpty(this.UserInfo.FingerBDSID.BDSID4))
            {
                if (this.UserInfo.FingerBDSID.BDSID4.Length != MAXIMUM_BDSID_LENGTH_CHARACTER)
                {
                    MappingFieldName("BDSID4");
                }
            }

            if (!string.IsNullOrEmpty(this.UserInfo.FingerBDSID.BDSID5))
            {
                if (this.UserInfo.FingerBDSID.BDSID5.Length != MAXIMUM_BDSID_LENGTH_CHARACTER)
                {
                    MappingFieldName("BDSID5");
                }
            }

            if (!string.IsNullOrEmpty(this.UserInfo.FingerBDSID.BDSID6))
            {
                if (this.UserInfo.FingerBDSID.BDSID6.Length != MAXIMUM_BDSID_LENGTH_CHARACTER)
                {
                    MappingFieldName("BDSID6");
                }
            }

            if (!string.IsNullOrEmpty(this.UserInfo.FingerBDSID.BDSID7))
            {
                if (this.UserInfo.FingerBDSID.BDSID7.Length != MAXIMUM_BDSID_LENGTH_CHARACTER)
                {
                    MappingFieldName("BDSID7");
                }
            }

            if (!string.IsNullOrEmpty(this.UserInfo.FingerBDSID.BDSID8))
            {
                if (this.UserInfo.FingerBDSID.BDSID8.Length != MAXIMUM_BDSID_LENGTH_CHARACTER)
                {
                    MappingFieldName("BDSID8");
                }
            }

            if (!string.IsNullOrEmpty(this.UserInfo.FingerBDSID.BDSID9))
            {
                if (this.UserInfo.FingerBDSID.BDSID9.Length != MAXIMUM_BDSID_LENGTH_CHARACTER)
                {
                    MappingFieldName("BDSID9");
                }
            }

            if (!string.IsNullOrEmpty(this.UserInfo.FingerBDSID.BDSID10))
            {
                if (this.UserInfo.FingerBDSID.BDSID10.Length != MAXIMUM_BDSID_LENGTH_CHARACTER)
                {
                    MappingFieldName("BDSID10");
                }
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorLength(this.FieldNameCollection, MAXIMUM_BDSID_LENGTH_CHARACTER);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);

        }


        private bool ValidateDuplicateInputBDSID()
        {
            if (this.UserInfo.FingerBDSID.BDSIDNotInput()) return true;

            string BDSIDDuplicate = string.Empty;
            if (this.UserInfo.FingerBDSID.BDSIDDuplicate(out BDSIDDuplicate))
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5028).Replace("{BDSIDs}", BDSIDDuplicate);
                ErrorMapping(Constant.ERR_CODE_5028, errorMessage);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);

        }


        private bool ValidateFourDigitBranchCodeBDSIDNotRegisteredInOrganizationMatrix()
        {
            if (this.UserInfo.FingerBDSID.BDSIDNotInput()) return true;
            List<FSMapping> fsMapping = ServiceMapper.Instance.MappingBDSID(this.UserInfo);
            string BDSIDDuplicate = string.Empty;

            if (FingerLogic.ValidateBDSIDBranchCodeNotRegisteredInOrganizationMatrix(fsMapping, out BDSIDDuplicate))
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5055).Replace("{BDSIDs}", BDSIDDuplicate);
                ErrorMapping(Constant.ERR_CODE_5055, errorMessage);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);

        }


        private bool ValidateDestinationBranchCodeAndOfficeCodeVersionIsAlreadyRegistered()
        {
            ErrorInfo errorInfo = FingerLogic.ValidateCurrentBranchIsV2(this.UserInfo.FingerCentral.DestinationBranchCode, this.UserInfo.FingerCentral.DestinationOfficeCode);

            if (errorInfo.Code == 9710)
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5034);
                ErrorMapping(Constant.ERR_CODE_5034, errorMessage);
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateDestinationBranchCodeAndOfficeCodeIsVersion3()
        {
            ErrorInfo errorInfo = FingerLogic.ValidateCurrentBranchIsV2(this.UserInfo.FingerCentral.DestinationBranchCode, this.UserInfo.FingerCentral.DestinationOfficeCode);

            if (errorInfo.Code == 9709)
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5033);
                ErrorMapping(Constant.ERR_CODE_5033, errorMessage);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateBDSIDAlreadyRegistered()
        {
            if (this.UserInfo.FingerBDSID.BDSIDNotInput()) return true;

            List<FSMapping> fsMapping = ServiceMapper.Instance.MappingBDSID(this.UserInfo);
            string BDSIDDuplicate = string.Empty;
            if (FingerLogic.ValidateIsAlreadyExistBDSIDInDatabase(this.UserInfo.FingerCentral.UserId, this.UserInfo.FingerCentral.DestinationBranchCode, this.UserInfo.FingerCentral.DestinationOfficeCode, this.UserInfo.FingerCategory.CategoryId, this.UserInfo.FingerCategory.CategoryStartPeriod, this.UserInfo.FingerCategory.CategoryEndPeriod, fsMapping, out BDSIDDuplicate))
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5029).Replace("{BDSIDs}", BDSIDDuplicate);
                ErrorMapping(Constant.ERR_CODE_5029, errorMessage);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);

        }

        private bool ValidateBDSFormat()
        {
            if (this.UserInfo.FingerBDSID.BDSIDNotInput()) return true;

            string BDSIDInValidFormat = string.Empty;
            if (!this.UserInfo.FingerBDSID.BDSIDIsValidFormat(out BDSIDInValidFormat))
            {

                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5030).Replace("{BDSIDs}", BDSIDInValidFormat);
                ErrorMapping(Constant.ERR_CODE_5030, errorMessage);
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }


        private bool ValidateBranchCodeLength()
        {

            if (!string.IsNullOrEmpty(this.UserInfo.ClientInfo.BranchCode))
            {
                if (this.UserInfo.ClientInfo.BranchCode.Length != MAXIMUM_BRANCH_CODE_LENGTH_CHARACTER)
                {
                    MappingFieldName("Client Info Branch Code");
                }
            }

            if (!string.IsNullOrEmpty(this.UserInfo.FingerCentral.DestinationBranchCode))
            {
                if (this.UserInfo.FingerCentral.DestinationBranchCode.Length != MAXIMUM_BRANCH_CODE_LENGTH_CHARACTER)
                {
                    MappingFieldName("Destination Branch Code");
                }
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorLength(this.FieldNameCollection, MAXIMUM_BRANCH_CODE_LENGTH_CHARACTER);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateOfficeCodeLength()
        {

            if (!string.IsNullOrEmpty(this.UserInfo.ClientInfo.OfficeCode))
            {
                if (this.UserInfo.ClientInfo.OfficeCode.Length != MAXIMUM_OFFICE_CODE_LENGTH_CHARACTER)
                {
                    MappingFieldName("Client Info Office Code");
                }
            }

            if (!string.IsNullOrEmpty(this.UserInfo.FingerCentral.DestinationOfficeCode))
            {
                if (this.UserInfo.FingerCentral.DestinationOfficeCode.Length != MAXIMUM_OFFICE_CODE_LENGTH_CHARACTER)
                {
                    MappingFieldName("Destination Office Code");
                }
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorLength(this.FieldNameCollection, MAXIMUM_OFFICE_CODE_LENGTH_CHARACTER);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateBranchHierarchy()
        {
            if (this.UserInfo.ClientInfo.LogonID.ToLower().Equals(Constant.ADMIN)) return true;

            ServiceResponse response = FingerLogic.ValidateBranchHierarchy(this.UserInfo.ClientInfo.LogonID, this.UserInfo.FingerCentral.DestinationBranchCode);
            if (response.statusCode != Constant.NO_ERROR)
            {
                ErrorMapping(response.statusCode, response.statusMessage);

            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateUserStatusVsUserGroup()
        {

            if ((this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_ALIH_DAYA ||
                  this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_BAKTI ||
                  this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_TANPA_NIP
                  )
                && this.UserInfo.FingerMain.GroupId != (short)Enumeration.EnumOfUserGroup.GROUP_USER)
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5010).Replace("{User Group}", GetUserGroup());
                errorMessage = errorMessage.Replace("{User Status}", GetUserStatus());
                ErrorMapping(Constant.ERR_CODE_5010, errorMessage);
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateUserStatusVsUserCategory()
        {
            //untuk alih daya & tanpa nip tidak boleh mendaftarkan huboffice
            if ((this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_TANPA_NIP ||
                    this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_ALIH_DAYA
                )

                &&
                 this.UserInfo.FingerCategory.CategoryId == (short)Enumeration.EnumOfCategory.CATEGORY_HUB_OFFICE
                 )
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5011).Replace("{User Category}", GetCategory());
                errorMessage = errorMessage.Replace("{User Status}", GetUserStatus());
                ErrorMapping(Constant.ERR_CODE_5011, errorMessage);
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateVendorNipNotMatchedWithDestinationBranchInNormalCategory()
        {

            if (this.UserInfo.FingerCategory == null) return true;

            if (this.UserInfo.FingerCategory.CategoryId == (short)Enumeration.EnumOfCategory.CATEGORY_PENEMPATAN_SEMENTARA) return true;

            if (this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_ALIH_DAYA)
            {
                string vendorBranchCode = this.UserInfo.FingerCentral.UserId.Left(4);

                if (!vendorBranchCode.Equals(this.UserInfo.FingerCentral.DestinationBranchCode))
                {
                    ErrorMapping(Constant.ERR_CODE_5026, Utility.GetErrorMessage(Constant.ERR_CODE_5026));
                }

            }

            return string.IsNullOrEmpty(this.ErrorMessage);

        }

        private bool ValidateVendorCodeFormatForAlihdaya()
        {
            if (this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_ALIH_DAYA)
            {
                string vendorCode = this.UserInfo.FingerCentral.UserId.Mid(4, 2);
                if (!IsValidVendorCode(vendorCode))
                {
                    ErrorMapping(Constant.ERR_CODE_5027, Utility.GetErrorMessage(Constant.ERR_CODE_5027));
                }

            }
            return string.IsNullOrEmpty(this.ErrorMessage);

        }

        private bool ValidateStartPeriod()
        {
            if ((this.UserInfo.FingerCategory.CategoryId == (short)Enumeration.EnumOfCategory.CATEGORY_PENEMPATAN_SEMENTARA
                )
                && (this.UserInfo.FingerCategory.CategoryStartPeriod == null ||
                    this.UserInfo.FingerCategory.CategoryStartPeriod < DateTime.Now.Date)
                )
            {
                ErrorMapping(Constant.ERR_CODE_5013, Utility.GetErrorMessage(Constant.ERR_CODE_5013));
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateEndPeriod()
        {
            if ((this.UserInfo.FingerCategory.CategoryId == (short)Enumeration.EnumOfCategory.CATEGORY_PENEMPATAN_SEMENTARA

                )
                && (this.UserInfo.FingerCategory.CategoryEndPeriod == null ||
                    this.UserInfo.FingerCategory.CategoryEndPeriod < this.UserInfo.FingerCategory.CategoryStartPeriod)
                )
            {
                ErrorMapping(Constant.ERR_CODE_5014, Utility.GetErrorMessage(Constant.ERR_CODE_5014));
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateUserIdNotMatchedWithCentralId()
        {
            UserInfoModel userLoginInfoModel = FingerLogic.GetUserInfoByUserId(this.UserInfo.FingerCentral.UserId);
            if (userLoginInfoModel != null)
            {
                if (userLoginInfoModel.FingerCentral != null)
                {
                    if (userLoginInfoModel.FingerCentral.FsCentralId != this.UserInfo.FingerCentral.FsCentralId)
                    {
                        string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5025).Replace("{NIP/NIK/Vendor NIP}", this.UserInfo.FingerCentral.UserId);
                        ErrorMapping(Constant.ERR_CODE_5025, errorMessage);
                    }
                }
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateFingerIsAlreadyRegisteredAsNormalCategoryInCentralServer()
        {
            if (this.UserInfo.FingerCategory.CategoryId != (Int16)Enumeration.EnumOfCategory.CATEGORY_NORMAL)
                return true;

            byte[] buffTemplate = ServiceMapper.Instance.ConvertToSingleTemplateBuffer(this.UserInfo.Template[0]);
            CoreServer server = new CoreServer(new FingerPrintServer(this.GuidThreadId, this.UserInfo.ClientInfo.AppName, this.UserInfo.ClientInfo.AppVersion));
            FingerPrintResult result = server.IdentifyFingerOnServer(buffTemplate);
            if (result.FingerPrintStatus)
            {
                ErrorMapping(Constant.ERR_CODE_5019, Utility.GetErrorMessage(Constant.ERR_CODE_5019));
            }
            return string.IsNullOrEmpty(this.ErrorMessage);

        }

        private bool ValidateUserIdAndCategoryIsAlreadyRegisteredInTheSameBranch()
        {

            if (this.UserInfo.FingerCategory != null)
            {
                if (this.UserInfo.FingerCategory.CategoryId == (short)Enumeration.EnumOfCategory.CATEGORY_NORMAL)
                {
                    if (FingerLogic.CheckUserIdAndCategoryIsAlreadyRegistered(this.UserInfo.FingerCentral.UserId, this.UserInfo.FingerCategory.CategoryId, this.UserInfo.FingerCentral.DestinationBranchCode, this.UserInfo.FingerCentral.DestinationOfficeCode))
                    {

                        string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5016).Replace("{User Category}", GetCategory());
                        errorMessage = errorMessage.Replace("{NIP/NIK/Vendor NIP}", this.UserInfo.FingerCentral.UserId);
                        ErrorMapping(Constant.ERR_CODE_5016, errorMessage);
                    }
                }
                //else if (this.UserInfo.FingerCategory.CategoryId == (short)Enumeration.EnumOfCategory.CATEGORY_PENEMPATAN_SEMENTARA)
                //{
                //    if (FingerLogic.CheckUserIdAndCategoryIsAlreadyRegistered(this.UserInfo.FingerCentral.UserId, this.UserInfo.FingerCategory.CategoryId, this.UserInfo.FingerCentral.DestinationBranchCode, this.UserInfo.FingerCentral.DestinationOfficeCode,
                //        Convert.ToDateTime(this.UserInfo.FingerCategory.CategoryStartPeriod), Convert.ToDateTime(this.UserInfo.FingerCategory.CategoryEndPeriod)))
                //    {

                //        string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5016).Replace("{User Category}", GetCategory());
                //        errorMessage = errorMessage.Replace("{NIP/NIK/Vendor NIP}", this.UserInfo.FingerCentral.UserId);
                //        ErrorMapping(Constant.ERR_CODE_5016, errorMessage);
                //    }
                //}
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateUserIdNormalIsAlreadyRegisteredInAnotherBranch()
        {
            if (this.UserInfo.FingerCategory.CategoryId == (short)Enumeration.EnumOfCategory.CATEGORY_NORMAL)
            {
                if (FingerLogic.CheckUserIdNormalIsAlreadyRegisteredInAnotherBranch(this.UserInfo.FingerCentral.UserId, this.UserInfo.FingerCentral.DestinationBranchCode, this.UserInfo.FingerCentral.DestinationOfficeCode))
                {
                    string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5017).Replace("{NIP/NIK/Vendor NIP}", this.UserInfo.FingerCentral.UserId);
                    ErrorMapping(Constant.ERR_CODE_5017, errorMessage);
                }
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateUserIdIsAlreadyRegisteredWithinActivePeriodInAnotherBranch()
        {
            if (this.UserInfo.FingerCategory.CategoryId == (short)Enumeration.EnumOfCategory.CATEGORY_PENEMPATAN_SEMENTARA
                )
            {
                if (FingerLogic.CheckUserIdIsAlreadyRegisteredWithinActivePeriodInAnotherBranch(this.UserInfo.FingerCentral.UserId, this.UserInfo.FingerCategory.CategoryId, this.UserInfo.FingerCentral.DestinationBranchCode, this.UserInfo.FingerCentral.DestinationOfficeCode, Convert.ToDateTime(this.UserInfo.FingerCategory.CategoryStartPeriod), Convert.ToDateTime(this.UserInfo.FingerCategory.CategoryEndPeriod)))
                {

                    string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5018).Replace("{User Category}", GetCategory());
                    errorMessage = errorMessage.Replace("{NIP/NIK/Vendor NIP}", this.UserInfo.FingerCentral.UserId);
                    ErrorMapping(Constant.ERR_CODE_5018, errorMessage);
                }
            }
            return string.IsNullOrEmpty(this.ErrorMessage);


        }

        private bool ValidateUserIdIsAlreadyRegisteredWithinActivePeriodInSameBranch()
        {
            if (this.UserInfo.FingerCategory.CategoryId == (short)Enumeration.EnumOfCategory.CATEGORY_PENEMPATAN_SEMENTARA
                )
            {
                if (FingerLogic.CheckUserIdIsAlreadyRegisteredWithinActivePeriodInSameBranch(this.UserInfo.FingerCentral.UserId, this.UserInfo.FingerCategory.CategoryId, this.UserInfo.FingerCentral.DestinationBranchCode, this.UserInfo.FingerCentral.DestinationOfficeCode, Convert.ToDateTime(this.UserInfo.FingerCategory.CategoryStartPeriod), Convert.ToDateTime(this.UserInfo.FingerCategory.CategoryEndPeriod)))
                {

                    string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5032).Replace("{User Category}", GetCategory());
                    errorMessage = errorMessage.Replace("{NIP/NIK/Vendor NIP}", this.UserInfo.FingerCentral.UserId);
                    ErrorMapping(Constant.ERR_CODE_5032, errorMessage);
                }
            }
            return string.IsNullOrEmpty(this.ErrorMessage);


        }

        private bool ValidateUserNotRegisteredAsNormalForSpecialCategory()
        {

            if (this.UserInfo.FingerCentral.FsCentralId <= 0 && this.UserInfo.FingerCategory.CategoryId != (int)Enumeration.EnumOfCategory.CATEGORY_NORMAL)
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5024).Replace("{NIP/NIK/Vendor NIP}", this.UserInfo.FingerCentral.UserId);
                ErrorMapping(Constant.ERR_CODE_5024, errorMessage);
            }
            else if (this.UserInfo.FingerCategory.CategoryId != (int)Enumeration.EnumOfCategory.CATEGORY_NORMAL)
            {
                UserInfoModel userLoginInfoModel = FingerLogic.GetUserInfoByUserId(this.UserInfo.FingerCentral.UserId);
                if (userLoginInfoModel == null)
                {
                    string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5024).Replace("{NIP/NIK/Vendor NIP}", this.UserInfo.FingerCentral.UserId);
                    ErrorMapping(Constant.ERR_CODE_5024, errorMessage);
                }

            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateTemplate()
        {
            if (this.UserInfo.FingerCategory.CategoryId != (Int16)Enumeration.EnumOfCategory.CATEGORY_NORMAL)
                return true;

            if (this.UserInfo.Template.Length > 0)
            {
                if (!IsValidTemplate(this.UserInfo.Template))
                {
                    string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5021);
                    ErrorMapping(Constant.ERR_CODE_5021, errorMessage);
                }
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private string GetUserStatus()
        {
            string userStatus = string.Empty;
            userStatus = FingerLogic.GetUserStatus(this.UserInfo.FingerBDSID.UserType);
            return userStatus;
        }

        private string GetUserGroup()
        {
            string userGroup = string.Empty;
            userGroup = FingerLogic.GetUserGroup(this.UserInfo.FingerMain.GroupId);
            return userGroup;
        }

        private string GetCategory()
        {
            string userCategory = string.Empty;
            userCategory = FingerLogic.GetUserCategory(this.UserInfo.FingerCategory.CategoryId);
            return userCategory;

        }

        private void SetErrorMandatoryField(string fieldName)
        {
            string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5008).Replace("{Mandatory Field}", fieldName);
            ErrorMapping(Constant.ERR_CODE_5008, errorMessage);
        }

        private void SetErrorLength(string fieldName, int length)
        {
            string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5009).Replace("{NIP/NIK/Vendor NIP}", fieldName);
            errorMessage = errorMessage.Replace("{Length Character}", length.ToString());
            ErrorMapping(Constant.ERR_CODE_5009, errorMessage);
        }

        private void SetErrorAllowNumberOnly(string fieldName)
        {
            string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5022).Replace("{NIP}", fieldName);
            ErrorMapping(Constant.ERR_CODE_5022, errorMessage);
        }

        private void ErrorMapping(int errorCode, string errorMessage)
        {
            this.ErrorCode = errorCode;
            this.ErrorMessage = errorMessage;
        }

        private void MappingFieldName(string fieldName)
        {
            if (string.IsNullOrEmpty(this.FieldNameCollection))
            {
                this.FieldNameCollection = fieldName;
            }
            else
            {
                this.FieldNameCollection += "," + fieldName;
            }
        }

        private bool IsValidTemplate(string[] template)
        {
            bool isValid = false;
            try
            {
                List<byte[]> NTemplate = ServiceMapper.Instance.ConvertToTemplateBuffer(template);
                if (NTemplate.Count > 0)
                {

                    isValid = FingerUtility.IsValidTemplate(NTemplate);
                }
            }
            catch
            {


            }

            return isValid;
        }

        private bool IsValidVendorCode(string vendorCode)
        {
            bool isValid = true;
            if (vendorCode.Equals("00"))
            {
                isValid = false;
            }
            else
            {
                string firstNumber = vendorCode.Left(1);
                if (firstNumber != "0")
                {
                    int number = Convert.ToInt32(vendorCode);
                    if (number > 20)
                    {
                        isValid = false;
                    }
                }
            }

            return isValid;

        }
        #endregion



    }
}