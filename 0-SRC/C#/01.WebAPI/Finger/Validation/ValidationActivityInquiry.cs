﻿using Finger.Framework.Common;
using Finger.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Finger.Entity;
using System.Text.RegularExpressions;
using Finger.Framework.Entity;
using Finger.Logic;
using Finger.Core.Models;
using Finger.Data;


namespace Finger.Validation
{
    public class ValidationActivityInquiry : ValidationBase
    {
        #region Constant
        private const string ACTIVITY_CURRENT_BRANCH_CODE_FIELD_NAME = "Branch Code";
        private const string ACTIVITY_CURRENT_OFFICE_CODE_FIELD_NAME = "Office Code";
        private const string ACTIVITY_FILTER_BY_APPLICATION_VALUE_FIELD_NAME = "Application";
        private const string ACTIVITY_FILTER_BY_ACTIVITY_VALUE_FIELD_NAME = "Activity";
        private const string ACTIVITY_FILTER_BY_PERIOD_START_FIELD_NAME = "Period Start";
        private const string ACTIVITY_FILTER_BY_PERIOD_END_FIELD_NAME = "Period End";
        private const string ACTIVITY_FILTER_BY_PERIOD = "Activity filter period";
        private const string AVTIVITY_FILTER_RETENTION_PERIOD = "1";
        #endregion

        #region Private Field(s)
        private FingerLogic _fingerLogic;
        #endregion

        #region Properties

        public string UserLoginID { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public ActivityCriteriaModel ActivityCriteria { get; set; }
        public string FieldNameCollection { get; set; }

        public string ClientBranchCode { get; set; }

        public string ClientOfficeCode { get; set; }

        public FingerLogic FingerLogic
        {
            get
            {
                if (_fingerLogic == null)
                {
                    _fingerLogic = new FingerLogic();
                }
                return _fingerLogic;
            }
        }
        #endregion

        #region Constructor
        public ValidationActivityInquiry(ActivityCriteriaModel activityCriteria, string userLoginID, string clientBranchCode, string clientOfficeCode)
        {
            this.ActivityCriteria = activityCriteria;
            this.UserLoginID = userLoginID;
            this.ClientBranchCode = clientBranchCode;
            this.ClientOfficeCode = clientOfficeCode;
        }
        #endregion

        #region Private Method(s)
        private void ErrorMapping(int errorCode, string errorMessage)
        {
            this.ErrorCode = errorCode;
            this.ErrorMessage = errorMessage;
        }
        private void MappingFieldName(string fieldName)
        {
            if (string.IsNullOrEmpty(this.FieldNameCollection))
            {
                this.FieldNameCollection = fieldName;
            }
            else
            {
                this.FieldNameCollection += "," + fieldName;
            }
        }
        private bool ValidateMandatoryField()
        {
            if (string.IsNullOrEmpty(this.ActivityCriteria.BranchCode))
            {
                MappingFieldName(ACTIVITY_CURRENT_BRANCH_CODE_FIELD_NAME);
            }

            if (string.IsNullOrEmpty(this.ActivityCriteria.OfficeCode))
            {
                MappingFieldName(ACTIVITY_CURRENT_OFFICE_CODE_FIELD_NAME);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }
        private bool ValidateFieldLength()
        {
            string errorMessage = string.Empty;

            if (this.ActivityCriteria.BranchCode.Length == 0)
            {
                errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_1112);
                ErrorMapping(Constant.ERR_CODE_1112, errorMessage);
            }
            else if (this.ActivityCriteria.BranchCode.Length != 4)
            {
                errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_1105).Replace("{field}", ACTIVITY_CURRENT_BRANCH_CODE_FIELD_NAME);
                errorMessage = errorMessage.Replace("{Length Character}", "4");
                ErrorMapping(Constant.ERR_CODE_1105, errorMessage);
            }


            if (this.ActivityCriteria.OfficeCode.Length == 0)
            {
                errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_1113);
                ErrorMapping(Constant.ERR_CODE_1113, errorMessage);
            }
            else if (this.ActivityCriteria.OfficeCode.Length != 3)
            {
                errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_1105).Replace("{field}", ACTIVITY_CURRENT_OFFICE_CODE_FIELD_NAME);
                errorMessage = errorMessage.Replace("{Length Character}", "3");
                ErrorMapping(Constant.ERR_CODE_1105, errorMessage);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }
        private bool ValidateFieldType()
        {
            if (!Regex.IsMatch(this.ActivityCriteria.BranchCode.ToString(), @"^[0-9]*$"))
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_1103).Replace("{NIP}", ACTIVITY_CURRENT_BRANCH_CODE_FIELD_NAME);
                ErrorMapping(Constant.ERR_CODE_1103, errorMessage);
            }

            if (!Regex.IsMatch(this.ActivityCriteria.OfficeCode.ToString(), @"^[0-9]*$"))
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_1103).Replace("{NIP}", ACTIVITY_CURRENT_OFFICE_CODE_FIELD_NAME);
                ErrorMapping(Constant.ERR_CODE_1103, errorMessage);
            }

            if (!Regex.IsMatch(this.ActivityCriteria.FilterByApplicationValue, @"^[A-Za-z0-9- ]*$"))
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_1106).Replace("{field}", ACTIVITY_FILTER_BY_APPLICATION_VALUE_FIELD_NAME);
                errorMessage = errorMessage.Replace("{word}", "letter, number and dash");
                ErrorMapping(Constant.ERR_CODE_1106, errorMessage);
            }

            if (!Regex.IsMatch(this.ActivityCriteria.FilterByActivityValue, @"^[A-Za-z0-9- ]*$"))
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_1106).Replace("{field}", ACTIVITY_FILTER_BY_ACTIVITY_VALUE_FIELD_NAME);
                errorMessage = errorMessage.Replace("{word}", "letter, number, space and dash");
                ErrorMapping(Constant.ERR_CODE_1106, errorMessage);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }
        private bool ValidateFilterRetentionPeriod()
        {
            string errorMessage = string.Empty;
            var date1 = Convert.ToDateTime(string.Format("{0} {1}", this.ActivityCriteria.FilterByPeriodStart, "00:00:00.000"));
            var date2 = Convert.ToDateTime(string.Format("{0} {1}", this.ActivityCriteria.FilterByPeriodEnd, "23:59:59.999"));
            int result = Convert.ToInt32(Math.Ceiling((date2 - date1).TotalDays));
            int endYear = date2.Year;
            if (date1 > date2)
            {
                errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_1101);
                ErrorMapping(Constant.ERR_CODE_1101, errorMessage);
            }
            else
            {
                ServiceResponse response = new ServiceResponse();
                FingerConfigItems configItems = FingerLogic.GetConfigurationItems(this.ClientBranchCode, this.ClientOfficeCode, out response);
                if (configItems != null)
                {
                    if (result > configItems.retentionFingerActivityPeriod)
                    {
                        errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_1110).Replace("{0}", configItems.retentionFingerActivityPeriod.ToString());
                        ErrorMapping(Constant.ERR_CODE_1110, errorMessage);
                    }
                }
                else {

                    ErrorMapping(response.statusCode, response.statusMessage);
                }
            }
            return string.IsNullOrEmpty(this.ErrorMessage);


            //else if (endYear % 400 == 0 || endYear % 4 == 0)
            //{
            //    if (result > Constant.DAYS_IN_ONE_YEAR_KABISAT)
            //    {
            //        //Can only select 1 year of data
            //        errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_1110);
            //        ErrorMapping(Constant.ERR_CODE_1110, errorMessage);
            //    }
            //}

            //else
            //{
            //    if (result > Constant.DAYS_IN_ONE_YEAR)
            //    {
            //        errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_1110);
            //        ErrorMapping(Constant.ERR_CODE_1110, errorMessage);
            //    }
            //}

            //return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateBranchHierarchy()
        {
            if (this.ActivityCriteria.BranchType == (int)Enumeration.EnumOfBranchHierarchyType.BRANCH_HIERARCHY)
            {
                ServiceResponse response = FingerLogic.ValidateBranchHierarchy(this.UserLoginID, this.ActivityCriteria.BranchCode);
                if (response.statusCode != Constant.NO_ERROR)
                {
                    ErrorMapping(response.statusCode, response.statusMessage);

                }
            }
            return string.IsNullOrEmpty(this.ErrorMessage);

        }

        

        #endregion

        #region Public Method(s)
        public override bool Validate()
        {
            try
            {
                if (ValidateMandatoryField())
                    if (ValidateFieldLength())
                        if (ValidateFieldType())
                            if (ValidateFilterRetentionPeriod())
                                ValidateBranchHierarchy();

                return string.IsNullOrEmpty(this.ErrorMessage);
            }
            catch (System.Exception ex)
            {
                this.ErrorCode = Constant.ERROR_CODE_SYSTEM;
                throw ex;
            }
        }

        public override string GetErrorMessage()
        {
            return this.ErrorMessage;
        }

        public override int GetErrorCode()
        {
            return this.ErrorCode;
        }
        #endregion
    }
}