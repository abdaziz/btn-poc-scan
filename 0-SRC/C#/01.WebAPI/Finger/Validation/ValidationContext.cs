﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Validation
{
    public class ValidationContext
    {
        #region Private Member(s)
        private ValidationBase _validationBase;
        #endregion

        #region Constructor
        public ValidationContext(ValidationBase validationBase)
        {
            this._validationBase = validationBase;

        }
        #endregion

        #region Public Method(s)
        public bool Execute()
        {
            return _validationBase.Validate();

        }

        public int GetErrorCode()
        {
            return _validationBase.GetErrorCode();

        }

        public string GetErrorMessage()
        {
            return _validationBase.GetErrorMessage();

        }
        #endregion
    }
}