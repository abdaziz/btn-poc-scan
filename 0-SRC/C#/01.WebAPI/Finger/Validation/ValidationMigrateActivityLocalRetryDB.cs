﻿using Finger.Framework.Common;
using Finger.Models;
using System.Linq;
using System;
using System.Collections.Generic;
using Finger.Framework.Entity;
using Finger.Data;
using System.IO;


namespace Finger.Validation
{
    public class ValidationMigrateActivityLocalRetryDB : ValidationBase
    {
        #region Properties

        public MigrationActivityLocalRetryDBModel MigrateActivityLocalRetryDB { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string FieldNameCollection { get; set; }

        public string DBNameProblem { get; set; }

        #endregion

        public ValidationMigrateActivityLocalRetryDB(MigrationActivityLocalRetryDBModel model, out string dbProblem)
        {

            this.MigrateActivityLocalRetryDB = model;
            dbProblem = this.DBNameProblem;

        }

        public override bool Validate()
        {
            try
            {

                if (ValidateAllObjectPropertiesObjectNull())
                {
                    if (ValidateClientInfo())
                    {

                        ValidateMigrateActivityLocalRetryDB();
                    }
                }

                return string.IsNullOrEmpty(this.ErrorMessage);
            }
            catch (System.Exception ex)
            {
                this.ErrorCode = Constant.ERROR_CODE_SYSTEM;
                throw ex;
            }
        }

        private bool ValidateClientInfo()
        {
            if (string.IsNullOrEmpty(this.MigrateActivityLocalRetryDB.ClientInfo.NewGuid))
            {
                MappingFieldName("Client Info-New Guid");

            }

            if (string.IsNullOrEmpty(this.MigrateActivityLocalRetryDB.ClientInfo.AppName))
            {
                MappingFieldName("Client Info-Application Name");

            }

            if (string.IsNullOrEmpty(this.MigrateActivityLocalRetryDB.ClientInfo.AppVersion))
            {
                MappingFieldName("Client Info-Application Version");

            }

            if (string.IsNullOrEmpty(this.MigrateActivityLocalRetryDB.ClientInfo.ActionName))
            {
                MappingFieldName("Client Info-Action Name");

            }

            if (string.IsNullOrEmpty(this.MigrateActivityLocalRetryDB.ClientInfo.IPAddress))
            {
                MappingFieldName("Client Info-IP Address");

            }

            if (this.MigrateActivityLocalRetryDB.ClientInfo.IPAddressType == 0)
            {
                MappingFieldName("Client Info-IP Address Type");

            }

            if (string.IsNullOrEmpty(this.MigrateActivityLocalRetryDB.ClientInfo.HostName))
            {
                MappingFieldName("Client Info-Host Name");

            }

            if (string.IsNullOrEmpty(this.MigrateActivityLocalRetryDB.ClientInfo.UserName))
            {
                MappingFieldName("Client Info-User Name");

            }

            if (string.IsNullOrEmpty(this.MigrateActivityLocalRetryDB.ClientInfo.BranchCode))
            {
                MappingFieldName("Client Info-Branch Code");

            }

            if (string.IsNullOrEmpty(this.MigrateActivityLocalRetryDB.ClientInfo.OfficeCode))
            {
                MappingFieldName("Client Info-Office Code");

            }

            if (string.IsNullOrEmpty(this.MigrateActivityLocalRetryDB.ClientInfo.LogonID))
            {
                MappingFieldName("Client Info-Logon ID");

            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);

        }

        private bool ValidateAllObjectPropertiesObjectNull()
        {
            if (this.MigrateActivityLocalRetryDB.ClientInfo == null)
            {
                MappingFieldName("Migrate Activity Local Retry DB-Client Info");
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private void ValidateMigrateActivityLocalRetryDB()
        {
            if (this.MigrateActivityLocalRetryDB.DBNameCollection == null)
            {
                MappingFieldName("DBNameCollection");
            }
            else
            {

                if (this.MigrateActivityLocalRetryDB.DBNameCollection.Count == 0)
                {
                    MappingFieldName("DBNameCollection");
                }
                else
                {

                    int count = this.MigrateActivityLocalRetryDB.DBNameCollection.Where(a => string.IsNullOrEmpty(a)).Count();
                    if (count > 0)
                    {
                        MappingFieldName("DBNameCollection");
                    }
                    else
                    {

                        ValidateMigrateActivityLocalDBFileExists();
                    }
                }
            }
        }

        private void ValidateMigrateActivityLocalDBFileExists()
        {
            List<cfgitems> configItems = new List<cfgitems>();
            string[] strParam = { Constant.KEY_MIGRATION_ACTIVITY_DB_STORAGE_PATH };
            ServiceResponse response = new FingerDataProvider().GetConfigItemsByArgs(strParam, out configItems);
            if (response.statusCode != Constant.NO_ERROR)
            {
                this.ErrorCode = response.statusCode;
                this.ErrorMessage = response.statusMessage;
            }
            else
            {
                string desDBStorage = configItems.Where(x => x.name == Constant.KEY_MIGRATION_ACTIVITY_DB_STORAGE_PATH).First().value.ToString();
                foreach (var dbName in this.MigrateActivityLocalRetryDB.DBNameCollection)
                {
                    string dbFullPath = Path.Combine(desDBStorage, string.Concat(this.MigrateActivityLocalRetryDB.ClientInfo.BranchCode, this.MigrateActivityLocalRetryDB.ClientInfo.OfficeCode), dbName);

                    if (!File.Exists(dbFullPath))
                    {
                        this.DBNameProblem = dbName;
                        string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_9805).Replace("{DBName}", dbName);
                        ErrorMapping(Constant.ERR_CODE_9805, errorMessage);
                        break;
                    }
                }
            }
        }

        private void MappingFieldName(string fieldName)
        {
            if (string.IsNullOrEmpty(this.FieldNameCollection))
            {
                this.FieldNameCollection = fieldName;
            }
            else
            {
                this.FieldNameCollection += "," + fieldName;
            }
        }

        private void SetErrorMandatoryField(string fieldName)
        {
            string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5008).Replace("{Mandatory Field}", fieldName);
            ErrorMapping(Constant.ERR_CODE_5008, errorMessage);
        }

        private void ErrorMapping(int errorCode, string errorMessage)
        {
            this.ErrorCode = errorCode;
            this.ErrorMessage = errorMessage;
        }

        public override string GetErrorMessage()
        {
            return this.ErrorMessage;
        }

        public override int GetErrorCode()
        {
            return this.ErrorCode;
        }


    }
}