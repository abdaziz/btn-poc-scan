﻿using Finger.Framework.Common;
using Finger.Logic;
using Finger.Models;
using System;

namespace Finger.Validation
{
    public class ValidationGetUserInfoDetails : ValidationBase
    {

        #region Private Field(s)
        private FingerLogic _fingerLogic;
        #endregion

        #region Properties

        public FingerLogic FingerLogic
        {
            get
            {
                if (_fingerLogic == null)
                {
                    _fingerLogic = new FingerLogic();
                }
                return _fingerLogic;
            }
        }
        public UserInfoDetailCriteriaModel Criteria { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string FieldNameCollection { get; set; }
        public string GuidThreadId { get; set; }

        #endregion

        #region Constant
        private const string FS_CENTRAL_ID_FIELD_NAME = "FsCentralId";
        private const string CATEGORY_ID_FIELD_NAME = "CategoryId";
        private const string BRANCHCODE_FIELD_NAME = "BranchCode";
        private const string OFFICECODE_FIELD_NAME = "OfficeCode";
        private const string LOCATION_ID_FIELD_NAME = "LocationId";
        private const string ADMIN = "admin";
        #endregion

        #region Constructor
        public ValidationGetUserInfoDetails(string guidThreadId, UserInfoDetailCriteriaModel criteria)
        {
            this.Criteria = criteria;
            this.ErrorCode = 0;
            this.ErrorMessage = string.Empty;
            this.GuidThreadId = guidThreadId;

        }
        #endregion

        #region Public Method(s)
        public override bool Validate()
        {
            try
            {
                if (ValidateClientInfo())
                {
                    ValidateGetUserInfoDetail();
                }


                return string.IsNullOrEmpty(this.ErrorMessage);

            }
            catch (System.Exception ex)
            {
                this.ErrorCode = Constant.ERROR_CODE_SYSTEM;
                throw ex;
            }
        }

        public override string GetErrorMessage()
        {
            return this.ErrorMessage;
        }

        public override int GetErrorCode()
        {
            return this.ErrorCode;
        }
        #endregion

        #region Private Method(s)

        private bool ValidateClientInfo()
        {
            if (string.IsNullOrEmpty(this.Criteria.ClientInfo.NewGuid))
            {
                MappingFieldName("Client Info-New Guid");

            }

            if (string.IsNullOrEmpty(this.Criteria.ClientInfo.AppName))
            {
                MappingFieldName("Client Info-Application Name");

            }

            if (string.IsNullOrEmpty(this.Criteria.ClientInfo.AppVersion))
            {
                MappingFieldName("Client Info-Application Version");

            }

            if (string.IsNullOrEmpty(this.Criteria.ClientInfo.ActionName))
            {
                MappingFieldName("Client Info-Action Name");

            }

            if (string.IsNullOrEmpty(this.Criteria.ClientInfo.IPAddress))
            {
                MappingFieldName("Client Info-IP Address");

            }

            if (this.Criteria.ClientInfo.IPAddressType == 0)
            {
                MappingFieldName("Client Info-IP Address Type");

            }

            if (string.IsNullOrEmpty(this.Criteria.ClientInfo.HostName))
            {
                MappingFieldName("Client Info-Host Name");

            }

            if (string.IsNullOrEmpty(this.Criteria.ClientInfo.UserName))
            {
                MappingFieldName("Client Info-User Name");

            }

            if (string.IsNullOrEmpty(this.Criteria.ClientInfo.BranchCode))
            {
                MappingFieldName("Client Info-Branch Code");

            }

            if (string.IsNullOrEmpty(this.Criteria.ClientInfo.OfficeCode))
            {
                MappingFieldName("Client Info-Office Code");

            }

            if (string.IsNullOrEmpty(this.Criteria.ClientInfo.LogonID))
            {
                MappingFieldName("Client Info-Logon ID");

            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);

        }

        private void ValidateGetUserInfoDetail()
        {
            if (!ValidateMandatoryField()) return;

            if (!ValidateBranchCodeLength()) return;

            if (!ValidateOfficeCodeLength()) return;

            if (!ValidateIsExistLogonID()) return;

        }


        private bool ValidateMandatoryField()
        {

            if (this.Criteria.FsCentralId < 0)
            {
                MappingFieldName(FS_CENTRAL_ID_FIELD_NAME);
            }

            if (this.Criteria.CategoryId < 0)
            {
                MappingFieldName(CATEGORY_ID_FIELD_NAME);
            }

            if (string.IsNullOrEmpty(this.Criteria.BranchCode))
            {
                MappingFieldName(BRANCHCODE_FIELD_NAME);
            }

            if (string.IsNullOrEmpty(this.Criteria.OfficeCode))
            {
                MappingFieldName(OFFICECODE_FIELD_NAME);
            }

            if (this.Criteria.LocationId < 0)
            {
                MappingFieldName(LOCATION_ID_FIELD_NAME);
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateBranchCodeLength()
        {
            if (this.Criteria.BranchCode.Length != 4)
            {

                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5009).Replace("{NIP/NIK/Vendor NIP}", BRANCHCODE_FIELD_NAME);
                errorMessage = errorMessage.Replace("{Length Character}", "4");
                ErrorMapping(Constant.ERR_CODE_5009, errorMessage);
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateOfficeCodeLength()
        {
            if (this.Criteria.OfficeCode.Length != 3)
            {

                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5009).Replace("{NIP/NIK/Vendor NIP}", OFFICECODE_FIELD_NAME);
                errorMessage = errorMessage.Replace("{Length Character}", "3");
                ErrorMapping(Constant.ERR_CODE_5009, errorMessage);
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }


        private bool ValidateIsExistLogonID()
        {
            if (this.Criteria.ClientInfo.LogonID.ToLower().Equals(ADMIN)) return true;

            UserInfoModel userLoginInfoModel = FingerLogic.GetUserInfoByUserId(this.Criteria.ClientInfo.LogonID);
            if (userLoginInfoModel == null)
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7003).Replace("{NIP/NIK/Vendor NIP}", this.Criteria.ClientInfo.LogonID);
                ErrorMapping(Constant.ERR_CODE_7003, errorMessage);
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private void ErrorMapping(int errorCode, string errorMessage)
        {
            this.ErrorCode = errorCode;
            this.ErrorMessage = errorMessage;
        }

        private void SetErrorMandatoryField(string fieldName)
        {
            string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5008).Replace("{Mandatory Field}", fieldName);
            ErrorMapping(Constant.ERR_CODE_5008, errorMessage);
        }

        private void MappingFieldName(string fieldName)
        {
            if (string.IsNullOrEmpty(this.FieldNameCollection))
            {
                this.FieldNameCollection = fieldName;
            }
            else
            {
                this.FieldNameCollection += "," + fieldName;
            }
        }



        #endregion

    }
}