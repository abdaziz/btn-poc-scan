﻿using Finger.Core.Models;
using Finger.Data;
using Finger.Framework.Common;
using Finger.Framework.Entity;
using Finger.Logic;
using Finger.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Finger.Validation
{
    public class ValidationHubOfficeActivityInquiry : ValidationBase
    {

        #region Private Field(s)
        private FingerLogic _fingerLogic;
        #endregion

        #region Properties
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public HubOfficeActivityInquiryModel HubOfficeActivity { get; set; }

        public FingerLogic FingerLogic
        {
            get
            {
                if (_fingerLogic == null)
                {
                    _fingerLogic = new FingerLogic();
                }
                return _fingerLogic;
            }
        }
        public string FieldNameCollection { get; set; }
        #endregion

        #region Constructor
        public ValidationHubOfficeActivityInquiry(HubOfficeActivityInquiryModel hubOfficeActivity)
        {
            this.HubOfficeActivity = hubOfficeActivity;
        }
        #endregion

        #region Public Method(s)
        public override bool Validate()
        {
            try
            {
                if (ValidateAllObjectPropertiesObjectNull())
                {
                    if (ValidateClientInfo())
                    {
                        if (ValidatePagingInfo())
                        {
                            ValidateHubOfficeCriteria();
                        }
                    }
                }

                return string.IsNullOrEmpty(this.ErrorMessage);
            }
            catch (System.Exception ex)
            {
                this.ErrorCode = Constant.ERROR_CODE_SYSTEM;
                throw ex;
            }
        }

        public override string GetErrorMessage()
        {
            return this.ErrorMessage;
        }

        public override int GetErrorCode()
        {
            return this.ErrorCode;
        }
        #endregion

        #region Private Method(s)

        private void ValidateHubOfficeCriteria()
        {
            if (!ValidateMandatoryField()) return;
            if (!ValidateFieldLength()) return;
            if (!ValidateFilterRetentionPeriod()) return;
            if (!ValidateIsExistLogonID()) return;
            if (!ValidateAuthorizedMenuByUserId()) return;
            if (!ValidateBranchHierarchy()) return;
        }

        private bool ValidateMandatoryField()
        {
            if (HubOfficeActivity.HubOfficeActivityCriteria.BranchType < 0)
            {
                MappingFieldName("Hub office Criteria-Branch Type");
            }

            if (string.IsNullOrEmpty(HubOfficeActivity.HubOfficeActivityCriteria.BranchCode))
            {
                MappingFieldName("Hub office Criteria-Branch Code");
            }

            if (string.IsNullOrEmpty(HubOfficeActivity.HubOfficeActivityCriteria.OfficeCode))
            {
                MappingFieldName("Hub office Criteria-Office Code");
            }

            //if (string.IsNullOrEmpty(HubOfficeActivity.HubOfficeActivityCriteria.NIP))
            //{
            //    MappingFieldName("Hub office Criteria-NIP");
            //}

            if (string.IsNullOrEmpty(HubOfficeActivity.HubOfficeActivityCriteria.StartPeriode))
            {
                MappingFieldName("Hub office Criteria-Start Periode");
            }

            if (string.IsNullOrEmpty(HubOfficeActivity.HubOfficeActivityCriteria.EndPeriode))
            {
                MappingFieldName("Hub office Criteria-End Periode");
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateAllObjectPropertiesObjectNull()
        {
            if (this.HubOfficeActivity.ClientInfo == null)
            {
                MappingFieldName("HubOffice Activity-Client Info");
            }

            if (this.HubOfficeActivity.HubOfficeActivityCriteria == null)
            {
                MappingFieldName("HubOffice Activity-Hub Office Activity Criteria");
            }

            if (this.HubOfficeActivity.PagingInfo == null)
            {
                MappingFieldName("HubOffice Activity-Paging Info");
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateFilterRetentionPeriod()
        {
            string errorMessage = string.Empty;
            var date1 = Convert.ToDateTime(string.Format("{0} {1}", this.HubOfficeActivity.HubOfficeActivityCriteria.StartPeriode, "00:00:00.000"));
            var date2 = Convert.ToDateTime(string.Format("{0} {1}", this.HubOfficeActivity.HubOfficeActivityCriteria.EndPeriode, "23:59:59.999"));
            int result = Convert.ToInt32(Math.Ceiling((date2 - date1).TotalDays));
            int endYear = date2.Year;
            if (date1 > date2)
            {
                errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_1101);
                ErrorMapping(Constant.ERR_CODE_1101, errorMessage);
            }
            else
            {
                ServiceResponse response = new ServiceResponse();
                FingerConfigItems configItems = FingerLogic.GetConfigurationItems(this.HubOfficeActivity.ClientInfo.BranchCode, this.HubOfficeActivity.ClientInfo.OfficeCode, out response);
                if (configItems != null)
                {
                    if (result > configItems.retentionHubOfficeActivityPeriod)
                    {
                        errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_1110).Replace("{0}", configItems.retentionHubOfficeActivityPeriod.ToString());
                        ErrorMapping(Constant.ERR_CODE_1110, errorMessage);
                    }
                }
                else
                {

                    ErrorMapping(response.statusCode, response.statusMessage);
                }
            }
            return string.IsNullOrEmpty(this.ErrorMessage);

            //else if (endYear % 400 == 0 || endYear % 4 == 0)
            //{
            //    if (result > Constant.DAYS_IN_ONE_YEAR_KABISAT)
            //    {
            //        errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_1110);
            //        ErrorMapping(Constant.ERR_CODE_1110, errorMessage);
            //    }
            //}

            //else
            //{
            //    if (result > Constant.DAYS_IN_ONE_YEAR)
            //    {
            //        errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_1110);
            //        ErrorMapping(Constant.ERR_CODE_1110, errorMessage);
            //    }
            //}

            //return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateClientInfo()
        {
            if (string.IsNullOrEmpty(this.HubOfficeActivity.ClientInfo.NewGuid))
            {
                MappingFieldName("Client Info-New Guid");

            }

            if (string.IsNullOrEmpty(this.HubOfficeActivity.ClientInfo.AppName))
            {
                MappingFieldName("Client Info-Application Name");

            }

            if (string.IsNullOrEmpty(this.HubOfficeActivity.ClientInfo.AppVersion))
            {
                MappingFieldName("Client Info-Application Version");

            }

            if (string.IsNullOrEmpty(this.HubOfficeActivity.ClientInfo.ActionName))
            {
                MappingFieldName("Client Info-Action Name");

            }

            if (string.IsNullOrEmpty(this.HubOfficeActivity.ClientInfo.IPAddress))
            {
                MappingFieldName("Client Info-IP Address");

            }

            if (this.HubOfficeActivity.ClientInfo.IPAddressType == 0)
            {
                MappingFieldName("Client Info-IP Address Type");

            }

            if (string.IsNullOrEmpty(this.HubOfficeActivity.ClientInfo.HostName))
            {
                MappingFieldName("Client Info-Host Name");

            }

            if (string.IsNullOrEmpty(this.HubOfficeActivity.ClientInfo.UserName))
            {
                MappingFieldName("Client Info-User Name");

            }

            if (string.IsNullOrEmpty(this.HubOfficeActivity.ClientInfo.BranchCode))
            {
                MappingFieldName("Client Info-Branch Code");

            }

            if (string.IsNullOrEmpty(this.HubOfficeActivity.ClientInfo.OfficeCode))
            {
                MappingFieldName("Client Info-Office Code");

            }

            if (string.IsNullOrEmpty(this.HubOfficeActivity.ClientInfo.LogonID))
            {
                MappingFieldName("Client Info-Logon ID");

            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);

        }

        private bool ValidatePagingInfo()
        {
            if (this.HubOfficeActivity.PagingInfo.Index == 0)
            {
                MappingFieldName("Paging Info-Index Page");
            }

            if (this.HubOfficeActivity.PagingInfo.PageSize == 0)
            {
                MappingFieldName("Paging Info-Page Size");
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateFieldLength()
        {
            if (this.HubOfficeActivity.HubOfficeActivityCriteria.NIP.Length > 0)
            {
                if (this.HubOfficeActivity.HubOfficeActivityCriteria.NIP.Length != 6)
                {
                    string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_1105).Replace("{field}", "NIP");
                    errorMessage = errorMessage.Replace("{Length Character}", "6");
                    ErrorMapping(Constant.ERR_CODE_1105, errorMessage);
                }
            }

            if (this.HubOfficeActivity.HubOfficeActivityCriteria.BranchCode.Length != 4)
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_1105).Replace("{field}", "Branch Code");
                errorMessage = errorMessage.Replace("{Length Character}", "4");
                ErrorMapping(Constant.ERR_CODE_1105, errorMessage);
            }

            if (this.HubOfficeActivity.HubOfficeActivityCriteria.OfficeCode.Length != 3)
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_1105).Replace("{field}", "Office Code");
                errorMessage = errorMessage.Replace("{Length Character}", "3");
                ErrorMapping(Constant.ERR_CODE_1105, errorMessage);
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateIsExistLogonID()
        {
            if (this.HubOfficeActivity.ClientInfo.LogonID.ToLower().Equals(Constant.ADMIN)) return true;

            UserInfoModel userLoginInfoModel = FingerLogic.GetUserInfoByUserId(this.HubOfficeActivity.ClientInfo.LogonID);
            if (userLoginInfoModel == null)
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7003).Replace("{NIP/NIK/Vendor NIP}", this.HubOfficeActivity.ClientInfo.LogonID);
                ErrorMapping(Constant.ERR_CODE_7003, errorMessage);
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateAuthorizedMenuByUserId()
        {
            bool isValidAccessMenu = FingerLogic.IsValidAccessMenu(this.HubOfficeActivity.ClientInfo, Enumeration.EnumOfFSMGMTMenu.HubOfficeActivity);
            if (isValidAccessMenu) return true;

            if (!isValidAccessMenu)
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5057).Replace("{menuname}", Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.HubOfficeActivity));
                ErrorMapping(Constant.ERR_CODE_5057, errorMessage);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateBranchHierarchy()
        {
            if (this.HubOfficeActivity.HubOfficeActivityCriteria.BranchType == (int)Enumeration.EnumOfBranchHierarchyType.BRANCH_HIERARCHY)
            {
                ServiceResponse response = FingerLogic.ValidateBranchHierarchy(this.HubOfficeActivity.ClientInfo.LogonID, this.HubOfficeActivity.HubOfficeActivityCriteria.BranchCode);
                if (response.statusCode != Constant.NO_ERROR)
                {
                    ErrorMapping(response.statusCode, response.statusMessage);

                }
            }
            return string.IsNullOrEmpty(this.ErrorMessage);

        }

        

        private void ErrorMapping(int errorCode, string errorMessage)
        {
            this.ErrorCode = errorCode;
            this.ErrorMessage = errorMessage;
        }

        private void MappingFieldName(string fieldName)
        {
            if (string.IsNullOrEmpty(this.FieldNameCollection))
            {
                this.FieldNameCollection = fieldName;
            }
            else
            {
                this.FieldNameCollection += "," + fieldName;
            }
        }
        private void SetErrorMandatoryField(string fieldName)
        {
            string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5008).Replace("{Mandatory Field}", fieldName);
            ErrorMapping(Constant.ERR_CODE_5008, errorMessage);
        }


        #endregion
    }
}