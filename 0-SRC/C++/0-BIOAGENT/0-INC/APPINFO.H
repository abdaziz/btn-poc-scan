#ifndef __APPINFO_H
#define __APPINFO_H


#ifdef __cplusplus
extern "C"
{
#endif


extern char appStartPath [];
extern char appConfigFile [];


int getAppStartUpInfo (void);

#ifdef __cplusplus
}
#endif


#endif // __APPINFO_H

