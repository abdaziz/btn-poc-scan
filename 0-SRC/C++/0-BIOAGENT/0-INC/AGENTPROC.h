#ifndef __AGENTPROC_H
#define __AGENTPROC_H


#ifdef __cplusplus
extern "C"
{
#endif


	int processBioRequest(void);
	int processBioCancelScan(void);
	int processBioResult(int err);


#ifdef __cplusplus
}
#endif


#endif // __AGENTPROC_H
