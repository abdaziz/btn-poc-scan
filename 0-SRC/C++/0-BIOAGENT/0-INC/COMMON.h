
#ifndef __COMMON_H
#define __COMMON_H


#ifdef __cplusplus
extern "C"
{
#endif

	const char* GetUrlRoute(char* endpoint, char* route);
	int RestAPIRequest(char* jsonData, char* sUrl, char* sAcceptHeader, char* sContentType, char* sCharset, char* sCustomHeader, char* sMethod, int sTimeOut, char** response);

#ifdef __cplusplus
}
#endif

#endif 