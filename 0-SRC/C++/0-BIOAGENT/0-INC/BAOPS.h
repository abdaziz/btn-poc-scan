#ifndef __BAOPS_H
#define __BAOPS_H


#ifdef __cplusplus
extern "C"
{
#endif

	int getResponseData(BA_REQ_HDL *reqHdl, BA_STATUS *status, BA_RESULT *result);
	int baSendRequest(int timeOut, BA_REQ_HDL *reqHdl, int op, char* reqUniqueId, char* reqId);
	int baWaitResponse(int timeOut, BA_REQ_HDL *reqHdl, BA_STATUS *status, BA_RESULT *result);
	void baCloseHandle(BA_REQ_HDL *reqHdl);


#ifdef __cplusplus
}
#endif


#endif // __BAOPS_H

