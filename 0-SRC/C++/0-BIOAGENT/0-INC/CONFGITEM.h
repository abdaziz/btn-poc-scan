#ifndef __CONFGITEM_H
#define __CONFGITEM_H


typedef struct CFGITEMS_STC
{
	char    ServiceURL[MAX_PATH];
	
} CFGITEMS;


#ifdef __cplusplus
extern "C"
{
#endif

	int readConfigFile(char *fn, CFGITEMS *configItem);

#ifdef __cplusplus
}
#endif


#endif //__CONFGITEM_H
