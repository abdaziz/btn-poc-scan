#ifndef __FINGERBTN_STC_H
#define __FINGERBTN_STC_H

#define RC_GENERAL_FAILURE          0
#define RC_SUCCESS								0
#define RC_VERIFY_FAIL							1
#define RC_LICENSING_ERROR						2
#define RC_VERIFY_REQID_CANNOT_EMPTY			3
#define RC_VERIFY_REQID_NOT_FOUND				4
#define RC_TIME_OUT								5
#define RC_CANNOT_OPEN_SCANNER_DEVICE			6
#define RC_UNSPECIFIED_ERROR					7
#define RC_CONNECTION_DATABASE_CENTRAL_ERROR	8
#define RC_DEVICE_BUSY							9
#define RC_CONFIG_FILE_NOT_FOUND				10
#define RC_CENTRAL_SERVICE_PROBLEM				11

typedef struct FINGER_REQ_STC
{
	char    reqId[9];
} FINGER_REQ;


typedef struct FINGER_RESP_STC
{
	char    reqId[32];
	int    respCode;

} FINGER_RESP;


int mapBioAgentError(int err, int *respCode);
const char* NewGuid();


#endif // __BDSFS_H
