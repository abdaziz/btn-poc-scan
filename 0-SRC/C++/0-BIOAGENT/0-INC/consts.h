#ifndef __CONSTS_H	
#define __CONSTS_H	


/*----------------------------------	Defaults -----------------------*/

#define CONTROLLER_PORT	24931
#define LICSERVER_PORT	24933

#define MATCHERSERVER_PORT	25452
#define MATCHERSERVER_ADMINPORT	24932

#define ATT_TYPE_FTP	1
#define ATT_TYPE_SFTP	2
#define ATT_TYPE_FTPES	3
#define ATT_PORT	22

#define MAX_FINGER_DATA	10
#define MIN_GENERALIZE	3
#define MAX_GENERALIZE	10
#define MAX_ID_MAPPING	11
#define MAX_ADDITIONAL_MAPPING	10
#define MAX_NIP_JOB_DEL_WEEKEND_BANKING_DATA	500
#define MAX_NIP_MIGRATION_DATA	500


#define DEF_CAPTURE_TIMEOUT	(30 * 1000)
#define DEF_MAX_ROTATE	180

#define DEF_FINGERIMAGE_WIDTH	208
#define DEF_FINGERIMAGE_HEIGHT	288

#define DEF_TEMPLATE_SIZE	ntsLarge

#define ATTD_EVT_FADE_SEC	7

#define MAX_BACKUP_CNT	128

#define SQL_BACKUP_RESTORE_TIMEOUT	(10 * 60)

#define DEF_DBCFG_FILENAME	"""BIODB.CFG"""
#define DEF_ATT_SVR_CFG_FN	"""ATTSVRCF.INI"""
#define DEF_MIGRATIONDBCFG_FILENAME	"""MIGRATIONDB.CFG"""
#define DEF_BDSCLIENTCFG_FILENAME	"""BDSCLIENT.CFG"""
#define DEF_IMPERSONATECFG_FILENAME	"""IMPERSONATE.CFG"""
#define DEF_ADDITIONAL_FILENAME	"""ADDITIONAL.CFG"""


/*----------------------------------	Aliases ------------------------*/

#define LICTYPES_LOCAL	0
#define LICTYPES_SERVER	1
#define LICTYPES_SIGNING	2

#define FINGER_LEFT_LITTLE	0
#define FINGER_LEFT_RING	1
#define FINGER_LEFT_MIDDLE	2
#define FINGER_LEFT_POINTER	3
#define FINGER_LEFT_THUMB	4
#define FINGER_RIGHT_THUMB	5
#define FINGER_RIGHT_POINTER	6
#define FINGER_RIGHT_MIDDLE	7
#define FINGER_RIGHT_RING	8
#define FINGER_RIGHT_LITTLE	9

#define FINGER_LEFT_THUMB_BIT	(1 << FINGER_LEFT_THUMB   )
#define FINGER_LEFT_POINTER_BIT	(1 << FINGER_LEFT_POINTER )
#define FINGER_LEFT_MIDDLE_BIT	(1 << FINGER_LEFT_MIDDLE  )
#define FINGER_LEFT_RING_BIT	(1 << FINGER_LEFT_RING    )
#define FINGER_LEFT_LITTLE_BIT	(1 << FINGER_LEFT_LITTLE  )
#define FINGER_RIGHT_THUMB_BIT	(1 << FINGER_RIGHT_THUMB  )
#define FINGER_RIGHT_POINTER_BIT	(1 << FINGER_RIGHT_POINTER)
#define FINGER_RIGHT_MIDDLE_BIT	(1 << FINGER_RIGHT_MIDDLE )
#define FINGER_RIGHT_RING_BIT	(1 << FINGER_RIGHT_RING   )
#define FINGER_RIGHT_LITTLE_BIT	(1 << FINGER_RIGHT_LITTLE )


#define EVTID_MGMT_START	100
#define EVTID_MGMT_LOGIN	(EVTID_MGMT_START + 0)
#define EVTID_MGMT_LOGOUT	(EVTID_MGMT_START + 1)
#define EVTID_MGMT_CONFIGURE	(EVTID_MGMT_START + 2)
#define EVTID_MGMT_IDENTIFY	(EVTID_MGMT_START + 3)
#define EVTID_MGMT_VERIFY	(EVTID_MGMT_START + 4)
#define EVTID_MGMT_ADD_USER	(EVTID_MGMT_START + 5)
#define EVTID_MGMT_CHG_USER	(EVTID_MGMT_START + 6)
#define EVTID_MGMT_DEL_USER	(EVTID_MGMT_START + 7)
//#define EVTID_MGMT_ADD_ID	(EVTID_MGMT_START + 8)
//#define EVTID_MGMT_CHG_ID	(EVTID_MGMT_START + 9)
//#define EVTID_MGMT_DEL_ID	(EVTID_MGMT_START + 10)
#define EVTID_MGMT_BACKUP	(EVTID_MGMT_START + 11)
#define EVTID_MGMT_RESTORE	(EVTID_MGMT_START + 12)
#define EVTID_MGMT_FORCE_LOGOUT	(EVTID_MGMT_START + 13)
#define EVTID_MGMT_CHG_THRESHOLD	(EVTID_MGMT_START + 14)
#define EVTID_MGMT_ADD_USER_SPECIAL	(EVTID_MGMT_START + 15)
#define EVTID_MGMT_END	(EVTID_MGMT_START + 99)
#define EVTID_MGMT_WEEKEND_BANKING	(EVTID_MGMT_START + 88)
#define EVTID_MGMT_RESIGN_USER	(EVTID_MGMT_START + 108)


#define EVTID_USE_ATT_START	200
#define EVTID_USE_ATT_IN			(EVTID_USE_ATT_START + 0)
#define EVTID_USE_ATT_OUT			(EVTID_USE_ATT_START + 1)
#define EVTID_USE_ATT_IN_VERIFY		(EVTID_USE_ATT_START + 2)
#define EVTID_USE_ATT_OUT_VERIFY	(EVTID_USE_ATT_START + 3)
#define EVTID_USE_ATT_IN_OFFLINE	(EVTID_USE_ATT_START + 4)
#define EVTID_USE_ATT_OUT_OFFLINE	(EVTID_USE_ATT_START + 5)
#define EVTID_USE_ATT_END			(EVTID_USE_ATT_START + 99)

#define EVTID_USE_BDS_START	300
#define EVTID_USE_BDS_VERIFY	(EVTID_USE_BDS_START + 0)
#define EVTID_USE_BDS_END	(EVTID_USE_BDS_START + 99)


#define USER_CATEGORY_ANY	UINT8_MAX
#define USER_CATEGORY_NONE	0
#define USER_CATEGORY_NIP	1
#define USER_CATEGORY_BDS	2

#define USER_TYPE_ANY	UINT8_MAX
#define USER_TYPE_NONE	0

#define USER_TYPE_BDS	1

#define USER_TYPE_EMPLOYEE	2
#define USER_TYPE_BAKTI	3
#define USER_TYPE_OUTSOURCE	4
#define USER_TYPE_TANPA_NIP	5

#define USER_STANDARD_THRESHOLD	0
#define USER_SPECIAL_THRESHOLD	1


#define WEEKEND_BANKING_CATEGORY 2

/*----------------------FOR FIN BAKTI----------------------------------*/
#define USER_TYPE_FIN				23
/*----------------------------------	-----------------------------------*/

#define USER_SUBTYPE_ANY	UINT8_MAX
#define USER_SUBTYPE_NONE	0

#define USER_SUBTYPE_EMPL_NONSHIFT	1
#define USER_SUBTYPE_EMPL_SHIFT	2

#define USER_SUBTYPE_OUTSOURCE_1	1
#define USER_SUBTYPE_OUTSOURCE_2	2
#define USER_SUBTYPE_OUTSOURCE_3	3
#define USER_SUBTYPE_OUTSOURCE_4	4
#define USER_SUBTYPE_OUTSOURCE_5	5
#define USER_SUBTYPE_OUTSOURCE_6	6
#define USER_SUBTYPE_OUTSOURCE_7	7
#define USER_SUBTYPE_OUTSOURCE_8	8
#define USER_SUBTYPE_OUTSOURCE_9	9
#define USER_SUBTYPE_OUTSOURCE_10	10
#define USER_SUBTYPE_OUTSOURCE_11	11
#define USER_SUBTYPE_OUTSOURCE_12	12
#define USER_SUBTYPE_OUTSOURCE_13	13
#define USER_SUBTYPE_OUTSOURCE_14	14
#define USER_SUBTYPE_OUTSOURCE_15	15
#define USER_SUBTYPE_OUTSOURCE_16	16
#define USER_SUBTYPE_OUTSOURCE_17	17
#define USER_SUBTYPE_OUTSOURCE_18	18
#define USER_SUBTYPE_OUTSOURCE_19	19
#define USER_SUBTYPE_OUTSOURCE_20	20


#define GRP_EVERYONE	0
#define GRP_USER	1
#define GRP_ENROLLER	2
#define GRP_REPORTER	3
#define GRP_MANAGER	4
#define GRP_SUPER_USER	5
#define GRP_BIOCONTROLLER	6
#define GRP_SYSTEM_ADMIN	7
#define GRP_MANAGER_NON_KP	8
#define GRP_ENROLLER_NON_KP	9
#define GRP_REPORTER_NON_KP	10


/*----------------------------------	----------------------------------------*/

#define SECS_IN_DAYS	(24 * 60 * 60)

#define PERIOD_DAYS	0
#define PERIOD_MONTHS	1
#define PERIOD_YEARS	2

#define TIME_MGMT_LOCAL	0
#define TIME_MGMT_SERVER_PERIODIC	1
#define TIME_MGMT_SERVER_START	2

/*----------------------------------	Length -------------------------*/

/* Max query string length is 1024,"	"command+filter+order approx 40, that leave us 459 */
#define MAX_SQL_QUERY_SZ	1024 // My assumption about maximum sql query we need to build
#define BIG_SQL_QUERY_SZ	2048 // My assumption about maximum sql query we need to build

#define BRANCHID_WIDTH	4
#define OFFICEID_WIDTH	3

#define USERNIP_LEN	6
#define USERBDS_LEN   8
#define USER_TANPA_NIP_LEN 16
#define USER_VENDOR_NIP_LEN 10

#define COMPIP_LEN	16
#define COMPNAME_LEN	64
#define SVR_PATH_LEN	260 // Make it equal to _MAX_PATH
#define SVR_ENCR_IV_LEN	64
#define SVR_ENCR_KEY_LEN	64

#define FLD_CFGNAME_LEN	64
#define FLD_CFGVALUE_LEN	1024

#define FLD_REMARK_LEN	1024

#define FLD_NAME_LEN	64
#define FLD_NAME2_LEN	150

#define FLD_TEMPLATELAYOUT_LEN	MAX_FINGER_DATA

#define FLD_USERID_LEN	64
#define FLD_GROUPID_LEN	16
#define FLD_USER_LEN	16
#define FLD_PWDTXT_LEN	64
#define FLD_PWDBIN_LEN	64
#define FLD_GUID_LEN	36	

#define FLD_TEMPORARY_LEN	255

#define FLD_CATEGORYID_LEN 64
#define FLD_CATEGORYI_NAME_LEN 150
#define FLD_GROUP_NAME_LEN 64
#define FLD_USER_STATUS_NAME_LEN 64
#define FLD_SHIFT_LEN 32
#define FLD_BRANCH_CODE_LEN 7
#define FLD_OFFICE_CODE_LEN 5
#define FLD_BDS_ID_LEN 10
#define FLD_KODE_ALIH_DAYA_LEN 5
#define FLD_PERIOD_LEN 12
#define FLD_TEMPLATE_LAYOUT_LEN 5

#define CATEGORY_NORMAL 0
#define CATEGORY_MOBIL_KAS 1
#define CATEGORY_PENEMPATAN_SEMETARA 2
#define CATEGORY_SECONDARY_WORKPLACE 3
#define CATEGORY_MULTIPLE_LOGIN 4
#define CATEGORY_TEMPORARY_WORKPLACE 5
#define CATEGORY_DUAL_OFFICE	6
#define CATEGORY_HUB_OFFICE		7

#define USER_STATUS_TETAP	2
#define USER_STATUS_BAKTI	3
#define USER_STATUS_ALIH_DAYA	4
#define USER_STATUS_TANPA_NIP	5

#define MAX_ADD_MAPPING 11
#define MAX_BDS_ID 10

#define DEF_FILECFG_FILENAME        "BDSCLIENT.CFG"
#define DEF_FILECFG_DIR				"CONFIG\\"
#define ABSOLUTE_FILECFG_DIR_64				"C:\\Program Files (x86)\\Ivatama Teknologi\\Finger Print\\CONFIG\\"
#define ABSOLUTE_FILECFG_DIR_32				"C:\\Program Files\\Ivatama Teknologi\\Finger Print\\CONFIG\\"



//=========================================Finger V3======================================================================

#define WS_MAX_MESSAGE_SIZE			2147483647
#define WS_INFO_BUFFER_SIZE			32767
#define FLD_FINGER_NAME_LEN			50
#define FLD_REQ_ID_LEN				6


#define WBCTRL_APP						"Job Weekend Banking"
#define RUCTRL_APP						"Job Resign User"
#define FORCE_DELETE_APP				"Force Delete"
#define BIOAGENT_APP					"Bioagent"
#define VERIFY_FINGER					"Verify"
#define DELETE_RESIGN_USER				"Delete Resign User"
#define DELETE_USER_WB					"Delete Weekend Banking User"
#define FORCE_DELETE					"Delete User From Force Delete App"
#define INIT_FSMGMT						"Initialization"
#define GET_LIST_USER_LOG				"Get List User Log"
#define ENROLLMENT						"Enrollment"
#define GET_LIST_USER_INQUIRY			"Get List User Inquiry"
#define GET_LIST_FINGER_ACTIVITY		"Get List Finger Activity"
#define EXPORT_FINGER_ACTIVITY		    "Export Finger Activity"
#define EXPORT_HUB_OFFICE_ACTIVITY		"Export Hub Office Activity"
#define EXPORT_USER						"Export User"
#define STRING_EMPTY					"EMPTY"
#define FIND_USER_MUTATION				"Find User Mutation"
#define GET_USER_INFO_DETAILS			"Get User Info Details"
#define VALIDATE_BRANCH_HIERARCHY		"Validate Branch Hierarchy"
#define INIT_BIOAGENT					"Init BioAgent"
#define GET_LIST_HUB_OFFICE_ACTIVITY	"Get List Hub Office Activity"
#define GET_LIST_MUTATION				"Get List Mutation"
#define EXPORT_MUTATION					"Export Mutation"
#define SEARCH_MUTATION					"Search Mutation"
#define SEARCH_BERTA_ACTIVITY			"Search Berta Activity"
#define EXPORT_BERTA_ACTIVITY			"Export Berta Activity"
#define VERIFY_BERTA_ACTIVITY			"Verify Berta Activity"
#define BDS_IBS_APP						"BDS IBS"
#define BDS_WEB_APP						"BDS WEB"
#define EXPORT_RECAP_ATTENDANCE_REPORT	"Export Recap Attendance Report"
#define SEARCH_RECAP_ATTENDANCE_REPORT	"Search Recap Attendance Report"
#define GET_RECAP_ATTENDANCE_RETENTION	"Get Recap Attendance Retention"

//========================================== ACTION NAME V3 FOR REPORT ACTIVITY ================================================
#define CONNECT						"Connect"
#define LOGIN_FSMGMT				"Login"
#define LOGOUT_FSMGMT				"Logout"
#define IDENTIFY_FINGER				"Identification"
#define VERIFICATION				"Verification"
#define RESET_USER_LOG				"Reset Logon"
#define REGISTRATION				"Registration"
#define UPDATE_USER_INFO			"Edit"
#define DELETE_USER					"Delete"
#define REGISTER_MUTATION			"Register Mutation"
#define CANCEL_MUTATION				"Cancel Mutation"
#define APPROVE_MUTATION			"Approved Mutation"
#define FINGER_API_CONFIG			"Finger API Configuration"
#define ATTENDANCE_IN_OUT			"IN/OUT"
#define ABSENSI						"Attendance"
#define DISPLAY_EDIT_USER_INFO		"Display Edit User Info"

//========================================== APPLICATION NAME V3 FOR REPORT ACTIVITY ================================================
#define FSMGMT_APP					"Management"
#define ATTENDANCE_APP				"Attendance"
#define BDS_IBS_APP					"BDS IBS"
#define BDS_WEB_APP					"BDS WEB"



#define USER_LOG_SAVE			1
#define USER_LOG_DELETE			3
#define USER_LOG_VALIDATE		4
#define INFO_BUFFER_SIZE		32767

#define HTTPS  1
#define HTTP   0


#define NORMAL							"Normal"
#define PENEMPATAN_SEMENTARA			"Penempatan Sementara"
#define JARI_TIDAK_DAPAT_DIDAFTARKAN	"Jari tidak dapat didaftarkan"
#define KESULITAN_IDENTIFIKASI			"Kesulitan identifikasi"
//#define SECONDARY_WORKPLACE			"Secondary Workplace"
//#define DUAL_OFFICE					"Dual Office"

#define USER		"User"
#define ENROLLER	"Enroller"
#define REPORTER	"Reporter" 
#define MANAGER		"Manager"

#define _NIP		"NIP"
#define NIK			"NIK"
#define VENDOR_NIP	"Vendor NIP"
#define NIP_NIK_VENDORNIP "NIP/NIK/VENDOR NIP"

#define TETAP		"Tetap"
#define BAKTI		"Bakti"
#define ALIH_DAYA	"Alih Daya"
#define TANPA_NIP	"Tanpa NIP"

#define STANDARD_USER	"Standard User"
#define SPECIAL_USER	"Special User"

#define CKV_BROKEN	5051
#define ERROR_VALIDATION 5008
#define SECRET_KEY  "1V@T4MA T3KN0L0G!"
#define ADMIN	"Admin"

#define ONLINE	1
#define OFFLINE	0

#define IS_STANDARD_USER	1
#define IS_SPECIAL_USER		2

#define ATTENDANCE_STATUS_IN	1
#define ATTENDANCE_STATUS_OUT	2

#define IVT_KEY_SECRET		"1V@T4MA T3KN0L0G!"



//===================================SQLite=============================================

#define	FLD_BRANCH_CODE						"branchCode"
#define	FLD_OFFICE_CODE						"officeCode"
#define	FLD_MAX_COUNTER						"maxCounter"
#define FLD_SVR_HOST_NAME					"serverHostName"
#define FLD_FINGER_SVC_URL					"fingerServiceURL"
#define FLD_CLIENT_TIMEOUT					"clientTimeout"
#define FLD_PAGE_SIZE						"pageSize"
#define FLD_BERTA_SVC_URL					"bertaServiceURL"
#define FLD_BERTA_FILTER_ACTIVITY			"bertaFilterActivityInHours"
#define FLD_DOC_KEY							"docKey"
#define FLD_IS_ALLREADY_MIGRATE				"isAllReadyMigrate"
#define FLD_ATTD_STORAGE_PATH				"attendanceDbStoragePath"
#define FLD_MIGRATION_ACT_STORAGE_PATH		"migrationActivityDbStoragePath"


//=======================================MESSAGE=========================================
#define MSG_CENTRAL_SVC_CON_ERROR						"Central service connection error"
#define MSG_EMPTY_APP_ID								"Please input App ID"
#define MSG_EMPTY_APP_NAME								"Please input App Name"
#define MSG_EMPTY_APP_VERSION							"Please input App Version"
#define MSG_LOWER_APP_VERSION							"App version can't below the previous version"

//=======================================LENGHT=========================================
#define USERID_LEN	16

//===================================== REST API HEADER ====================================
#define APPLICATION_JSON			"application/json"
#define UTF_8						"utf-8"
#define HTTP_POST					"POST"
#define HTTP_GET					"GET"
#define HTTP_PUT					"PUT"
#define HTTP_DELETE					"DELETE"


#endif // __CONSTS_H

//================================CALENDAR FORMAT CONSTANTS===============================
#define CALENDAR_FORMAT	"dd-MM-yyyy"
#define DATE_TIME_FORMAT	"dd-MM-yyyy HH:mm:ss"



//================================STYLESHEET QT CONSTANTS===============================
#define BUTTON_GREEN_ARMY		"background-color: rgb(76, 175, 80);color: rgb(255, 255, 255);font-weight: bold;font-size: 13pt;font-family: Calibri;"
#define	BUTTON_BLUE				"background-color: rgb(0, 85, 255);color: rgb(255, 255, 255);font-weight: bold;font-size: 13pt;font-family: Calibri;"
#define BUTTON_RED				"background-color: rgb(255, 0, 0);color: rgb(255, 255, 255);font-weight: bold;font-size: 13pt;font-family: Calibri;"
#define BUTTON_ORANGE			"background-color: rgb(249, 162, 45);color: rgb(255, 255, 255);font-weight: bold;font-size: 13pt;font-family: Calibri;"
#define LABEL_HEADER			"color: rgb(255, 255, 255);font-weight: bold;font-size: 16pt;font-family: MS Shell Dlg 2;"
#define	LABEL_FIELD				"color: rgb(255, 255, 255);font-weight: bold;font-size: 11pt;font-family: Arial Unicode MS;"
#define	LABEL_MESSAGE			"color: rgb(255, 255, 255);font-weight: bold;font-size: 13pt;font-family: Arial Unicode MS;"
#define	LABEL_INFO				"color: rgb(255, 255, 0);font-weight: bold;font-size: 11pt;font-family: Arial Unicode MS;"
#define	EDIT_FIELD				"background-color: rgb(255, 255, 255);color: rgb(0, 0, 0);font-size: 11pt;font-family: Arial Unicode MS;"
#define	CALENDAR_FIELD			"font-size: 11pt;font-family: Arial Unicode MS;"
#define	COMBOBOX_FIELD			"color: rgb(0, 0, 0);font-size: 10pt;font-family: Arial Unicode MS;"
#define	GRID_ITEM				"color: rgb(0, 0, 0);font-size: 10pt;font-family: Arial Unicode MS;"
#define	CHECKBOX_FIELD			"color: rgb(255, 255, 255);font-weight: bold;font-size: 10pt;font-family: Arial Unicode MS;"
#define GREY_FIELD				"color : rgb(192, 192, 192);color: rgb(0, 0, 0);font-size: 11pt;font-family: Arial Unicode MS;"
#define DISABLED_FIELD			"background-color: rgb(255, 255, 255); color: rgb(192,192,192);"
#define	CHECKBOX_DISABLED_FIELD	"background-color: rgb(192,192,192);"
#define	GRID_HEADER_COLOR		"QHeaderView::section {background-color:lightGray; border-right:0px solid gray; selection-color: lightGray; selection-background-color: lightGray;color:black;}"



#define ICO						":/ico/Finger.ico"
#define INVALID_DATA			"Invalid data"

#define CHECKBOX_WIDTH 120
//================================Sorting  List===============================
#define SORT_BY_ASCENDING "ASC";
#define SORT_BY_DESCENDING "DESC";
