#ifndef __APPGUID_H
#define __APPGUID_H


// BioAgent
#define BA_GUID                 "16E43D3E-4568-45D1-BBEB-63DD01C2C5FE"
#define BA_LGUID                "Local\\" BA_GUID
#define BA_GGUID                "Global\\" BA_GUID

#define BA_MTX_APP              BA_LGUID "MtxApp"


// Attendance
#define FA_GUID                 "CC142E0A-C42F-4202-89E3-C2505071083E"
#define FA_LGUID                "Local\\" FA_GUID
#define FA_GGUID                "Global\\" FA_GUID

#define FA_MTX_APP              FA_LGUID "MtxApp"
#define FA_EVT_INVOKEPREVIOUS   FA_LGUID "RiseAndShine"


// Management
#define FM_GUID                 "64DCEDAC-E11A-45C1-9713-7C69C0BF9B92"
#define FM_LGUID                "Local\\" FM_GUID
#define FM_GGUID                "Global\\" FM_GUID

#define FM_MTX_APP              FM_LGUID "MtxApp"
#define FM_EVT_INVOKEPREVIOUS   FM_LGUID "RiseAndShine"


// Configuration
#define CF_GUID                 "2B023B40-9D08-46D5-8B22-56F6C114FFC0"
#define CF_LGUID                "Local\\" CF_GUID
#define CF_GGUID                "Global\\" CF_GUID

#define CF_MTX_APP              CF_LGUID "MtxApp"
#define CF_EVT_INVOKEPREVIOUS   CF_LGUID "RiseAndShine"


// Server Configuration
#define SC_GUID                 "26799E12-58F3-48E9-88B9-D7BA6EAE2E81"
#define SC_LGUID                "Local\\" SC_GUID
#define SC_GGUID                "Global\\" SC_GUID

#define SC_MTX_APP              SC_LGUID "MtxApp"
#define SC_EVT_INVOKEPREVIOUS   SC_LGUID "RiseAndShine"


#endif // __APPGUID_H

