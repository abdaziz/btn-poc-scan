#ifndef __INITAGENT_H
#define __INITAGENT_H


#ifdef __cplusplus
extern "C"
{
#endif

	int closeAgent(void);
	int initAgent();

#ifdef __cplusplus
}
#endif


#endif // __INITAGENT_H

