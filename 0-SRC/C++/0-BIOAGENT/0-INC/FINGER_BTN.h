#ifndef __FINGER_BTN_H
#define __FINGER_BTN_H

#ifdef __cplusplus
extern "C"
{
#endif

	int __stdcall fs_send(char *message);
	int __stdcall fs_receive(int *result);

#ifdef __cplusplus
}
#endif


#endif // __FINGER_BTN_H
