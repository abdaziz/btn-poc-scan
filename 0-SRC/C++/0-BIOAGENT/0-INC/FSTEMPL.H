#ifndef __FSTEMPL_H
#define __FSTEMPL_H



#ifdef __cplusplus
extern "C"
{
#endif


#pragma warning (disable: 4996)

int fsGeneralizeTemplate (
    HNBuffer *hGenTempl,
    HNBuffer *hTempls,
    int numTempl,
    NfeExtractionStatus *extrStatus
);

#pragma warning (default: 4996)


int fsCreateTemplate (
    HNBuffer *hTemplates,
    HNBuffer *hTempls,
    int numTempl
);

int fsSplitTemplate (
    HNBuffer *hTempls,
    HNBuffer hTemplates,
    int numTempl
);






#ifdef __cplusplus
}
#endif


#endif // __FSTEMPL_H

