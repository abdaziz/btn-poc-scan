#ifndef __SVCCOMMAND_H
#define __SVCCOMMAND_H

#ifdef __cplusplus
extern "C"
{
#endif

	int VerifyFingerSystem(char *fingerServicURL, int timeOut, const char* reqUniqueId, const char* reqId, HNBuffer hTemplate, FSMATCHINGINFO* matchingInfo, int* matchCount, time_t* matchingTime, char* verifyDate);

#ifdef __cplusplus
}
#endif


#endif //__SVCCOMMAND_H
