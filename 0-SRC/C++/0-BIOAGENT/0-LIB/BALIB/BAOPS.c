#include <windows.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#include "glb_defs.h"
#include "appguid.h"
#include "fscommon.h"
#include "baglb.h"
#include "bacglb.h"
#include "fserrba.h"
#include "fserr.h"
#include "fsierr.h"
#include "pgrep.h"
#include "baops.h"

/*********************************************************************/
//Constant
/*********************************************************************/

#define EVT_RESPONSE_IDX    0
#define EVT_PGSTATUS_IDX    1
#define EVT_PGIMAGE_IDX     2
#define NUM_WAIT_EVT        3


/*********************************************************************/
//Method
/*********************************************************************/

int getResponseData(BA_REQ_HDL *reqHdl, BA_STATUS *status, BA_RESULT *result)
{
	int numRes;
	int loop;


	status->errCode = reqHdl->fmMemData->status.errCode;
	status->fsiLastErr = reqHdl->fmMemData->status.fsiLastErr;
	strncpy_s(status->fsiMsg, sizeof (status->fsiMsg), reqHdl->fmMemData->status.fsiMsg, sizeof (status->fsiMsg) - 1);
	*(status->fsiMsg + sizeof (status->fsiMsg) - 1) = 0;

	if (status->errCode == 0) {
		result->matchTime = reqHdl->fmMemData->result.matchTime;

		numRes = reqHdl->fmMemData->result.matchInfoCnt;
		result->matchInfoCnt = numRes;

		for (loop = 0; loop < numRes; loop++) {
			strcpy_s(result->matchInfo[loop].reqId, sizeof(result->matchInfo[loop].reqId), reqHdl->fmMemData->result.matchInfo[loop].reqId);
			result->matchInfo[loop].score = reqHdl->fmMemData->result.matchInfo[loop].score;
			result->matchInfo[loop].index = reqHdl->fmMemData->result.matchInfo[loop].index;
		}
	}

	return 0;
}

int baSendRequest(int timeOut, BA_REQ_HDL *reqHdl, int op, char* reqUniqueId, char* reqId)
{
	progressReport(PGD_LOG, PG_PRINTF, "Starting request event");

	int waitRes;
	int err;


	err = 0;

	reqHdl->fileShareMap = NULL;
	reqHdl->fmMemData = NULL;

	reqHdl->mtxBioAgent = NULL;
	reqHdl->mtxPgStatus = NULL;
	reqHdl->mtxPgImage = NULL;

	reqHdl->evtCancelScan = NULL;

	reqHdl->evtRequest = NULL;
	reqHdl->evtResponse = NULL;
	reqHdl->evtPgStatus = NULL;
	reqHdl->evtPgImage = NULL;

	err = openMutex(&reqHdl->mtxBioAgent, BA_MTX_BIOAGENT);

	if (err == 0) {
		if ((waitRes = WaitForSingleObject(reqHdl->mtxBioAgent, DEF_WAIT_FOR_OBJECT)) != WAIT_OBJECT_0) {
			if (waitRes == WAIT_FAILED) {
				err = GetLastError();
				progressReport(PGD_ALL, PG_BAC_AGENT_FAILED, err);
			}
			else {
				err = BAE_C_APP_BUSY;
				progressReport(PGD_ALL, PG_BAC_AGENT_BUSY, err);
			}
		}
	}

	if (err == 0) err = openFileShareMap(&reqHdl->fileShareMap, &reqHdl->fmMemData, BA_SHARE_MEM);

	if (err == 0) err = openMutex(&reqHdl->mtxPgStatus, BA_MTX_PG_STATUS);
	if (err == 0) err = openMutex(&reqHdl->mtxPgImage, BA_MTX_PG_IMAGE);
	if (err == 0) err = openEvent(&reqHdl->evtRequest, EVENT_MODIFY_STATE, BA_EVT_REQUEST);
	if (err == 0) err = openEvent(&reqHdl->evtCancelScan, EVENT_MODIFY_STATE, BA_EVT_CANCEL_SCAN);
	if (err == 0) err = openEvent(&reqHdl->evtResponse, EVENT_MODIFY_STATE | SYNCHRONIZE, BA_EVT_RESPONSE);
	if (err == 0) err = openEvent(&reqHdl->evtPgStatus, EVENT_MODIFY_STATE | SYNCHRONIZE, BA_EVT_PG_STATUS);
	if (err == 0) err = openEvent(&reqHdl->evtPgImage, EVENT_MODIFY_STATE | SYNCHRONIZE, BA_EVT_PG_IMAGE);

	if (err == 0) {
		reqHdl->fmMemData->request.reqCode = op;
		strcpy(reqHdl->fmMemData->request.reqUniqueId, reqUniqueId);
		strcpy(reqHdl->fmMemData->request.reqId, reqId);
		reqHdl->fmMemData->request.timeOut = timeOut * 1000;

		progressReport(PGD_LOG, PG_PRINTF, "Reset event");
		ResetEvent(reqHdl->evtCancelScan);
		ResetEvent(reqHdl->evtResponse);
		ResetEvent(reqHdl->evtPgStatus);
		ResetEvent(reqHdl->evtPgImage);

		if (SetEvent(reqHdl->evtRequest) == 0) {
			err = GetLastError();
			progressReport(PGD_ALL, PG_BAC_REQUEST, err);
		}
	}

	progressReport(PGD_LOG, PG_PRINTF, "Request event finished");

	return err;
}

int baWaitResponse(int timeOut, BA_REQ_HDL *reqHdl, BA_STATUS *status, BA_RESULT *result)
{
	HANDLE waitThArr[NUM_WAIT_EVT];

	time_t startWaitResp;
	int endWait;
	int waitRes;
	int err;


	err = 0;

	timeOut += 2;               // Add 2 secs for bioAgent clean up in case of time-out
	time(&startWaitResp);

	waitThArr[EVT_RESPONSE_IDX] = reqHdl->evtResponse;
	waitThArr[EVT_PGSTATUS_IDX] = reqHdl->evtPgStatus;
	waitThArr[EVT_PGIMAGE_IDX] = reqHdl->evtPgImage;

	endWait = 0;

	while (endWait == 0) {
		waitRes = WaitForMultipleObjects(NUM_WAIT_EVT, waitThArr, FALSE, 1000);

		switch (waitRes) {
		case WAIT_OBJECT_0 + EVT_RESPONSE_IDX:
			getResponseData(reqHdl, status, result);
			endWait = 1;
			break;

		case WAIT_OBJECT_0 + EVT_PGSTATUS_IDX:
		case WAIT_OBJECT_0 + EVT_PGIMAGE_IDX:
			break;

		case WAIT_ABANDONED_0 + EVT_RESPONSE_IDX:
			progressReport(PGD_ALL, PG_BA_WAIT_FAILED, "response abandoned", GetLastError());
			endWait = 1;
			break;

		case WAIT_ABANDONED_0 + EVT_PGSTATUS_IDX:
		case WAIT_ABANDONED_0 + EVT_PGIMAGE_IDX:
			break;

		case WAIT_TIMEOUT:
			break;

		default:
			progressReport(PGD_ALL, PG_BA_WAIT_FAILED, "unknown response", GetLastError());
			endWait = 1;
			break;
		}

		if ((endWait == 0) && (timeOut != 0)) {
			if (time(NULL) - startWaitResp >= timeOut) {
				status->errCode = FSE_TIME_OUT;
				status->fsiLastErr = FSE_I_NASYNCOPERATIONWAITTIMED;
				strcpy_s(status->fsiMsg, sizeof (status->fsiMsg), "Time out");

				SetEvent(reqHdl->evtCancelScan);
				endWait = 1;
			}
		}
	}

	return err;
}

void baCloseHandle(BA_REQ_HDL *reqHdl)
{
	if (reqHdl->mtxBioAgent != NULL) {
		ReleaseMutex(reqHdl->mtxBioAgent);
		CloseHandle(reqHdl->mtxBioAgent);
	}
	if (reqHdl->mtxPgStatus != NULL) CloseHandle(reqHdl->mtxPgStatus);
	if (reqHdl->mtxPgImage != NULL) CloseHandle(reqHdl->mtxPgImage);

	if (reqHdl->evtCancelScan != NULL) CloseHandle(reqHdl->evtCancelScan);

	if (reqHdl->evtRequest != NULL) CloseHandle(reqHdl->evtRequest);
	if (reqHdl->evtResponse != NULL) CloseHandle(reqHdl->evtResponse);
	if (reqHdl->evtPgStatus != NULL) CloseHandle(reqHdl->evtPgStatus);
	if (reqHdl->evtPgImage != NULL) CloseHandle(reqHdl->evtPgImage);

	closeFileShareMap(reqHdl->fileShareMap, reqHdl->fmMemData);

	reqHdl->fileShareMap = NULL;
	reqHdl->fmMemData = NULL;

	reqHdl->mtxBioAgent = NULL;
	reqHdl->mtxPgStatus = NULL;
	reqHdl->mtxPgImage = NULL;

	reqHdl->evtCancelScan = NULL;

	reqHdl->evtRequest = NULL;
	reqHdl->evtResponse = NULL;
	reqHdl->evtPgStatus = NULL;
	reqHdl->evtPgImage = NULL;
}






