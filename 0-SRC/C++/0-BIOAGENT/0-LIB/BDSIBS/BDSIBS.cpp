
#include <windows.h>
#include <stdio.h>
#include <direct.h>
#include <iostream>
#include "BDSIBS.h"
using namespace std;

typedef int(__stdcall *fs_send_c)(char *);
typedef int(__stdcall *fs_receive_c)(int *);
typedef int(__stdcall *fs_coba_c)(int *);


int main()
{
	printf("============================================================================== \n");
	printf("************************* BTN SIMULATION ************************************* \n");
	printf("============================================================================== \n");

	char currdir[_MAX_PATH];

	_getcwd(currdir, sizeof(currdir));
	cout << "Working directory location: " << currdir << std::endl;

#ifdef DEBUG
	cout << "Working directory location: " << currdir << std::endl;
#endif

	strcat_s(currdir, sizeof(currdir), "\\FINGER_BTN.dll");


#ifdef DEBUG
	cout << "FingerKBD2.dll location: " << currdir << std::endl;


	cout << "-> Load FINGER_KBD2.dll process" << std::endl;
#endif

	int err = 0;
	HINSTANCE hGetProcIDDLL = LoadLibraryA(currdir);
	if (!hGetProcIDDLL) {
		cout << "Error : could not load FINGER_BTN.dll" << std::endl;
		system("pause");
		return EXIT_FAILURE;
	}

#ifdef DEBUG
	cout << "Info : Load FINGER_KBD2.dll success" << std::endl;
	cout << "-> Load fs_init function process" << std::endl;
#endif

	char BDSID[9];
	cout << "Please enter BDSID: ";
	cin >> BDSID;

	cout << "Your BDSID is " << BDSID << std::endl;

#ifdef DEBUG
	cout << "-> Load fs_send function process" << std::endl;
#endif

	fs_send_c fs_sendFunc = (fs_send_c)GetProcAddress(hGetProcIDDLL, "fs_send");
	if (!fs_sendFunc) {
		cout << "Error : could not locate the fs_send function" << std::endl;
		system("pause");
		return EXIT_FAILURE;
	}

#ifdef DEBUG
	cout << "Info : Load fs_send function success" << std::endl;

	cout << "-> call fs_send function" << std::endl;

#endif


	unsigned int bytesent;
	bytesent = 0;

	err = fs_sendFunc(BDSID);
	if (err != NO_ERROR)
	{
		cout << "Error : could not call fs_send" << std::endl;
		system("pause");
		return EXIT_FAILURE;
	}

#ifdef DEBUG
	cout << "Info : call fs_send function success" << std::endl;
	cout << "-> Load fs_receive function process" << std::endl;
#endif

	fs_receive_c fs_receiveFunc = (fs_receive_c)GetProcAddress(hGetProcIDDLL, "fs_receive");
	if (!fs_receiveFunc) {
		cout << "Error : could not locate the fs_receive function" << std::endl;
		system("pause");
		return EXIT_FAILURE;
	}

#ifdef DEBUG
	cout << "Info : Load fs_receive function success" << std::endl;
#endif

	//FINGER_RESP* fsResp = (FINGER_RESP*)malloc(sizeof(FINGER_RESP));
	//strcpy_s(fsResp->reqId, "");
	//fsResp->respCode = RC_SUCCESS;

	int respCode;
	//responseResult = (char*)fsResp;
	//int respCode = 0;

#ifdef DEBUG
	cout << "-> call fs_receive function" << std::endl;
#endif

	cout << "-> Please place your finger..." << std::endl;
	err = fs_receiveFunc(&respCode);
	if (err != NO_ERROR)
	{
		//delete fsResp;
		cout << "Error : could not call fs_receive" << std::endl;
		system("pause");
		return EXIT_FAILURE;
	}

#ifdef DEBUG
	cout << "Info : call fs_receive function success" << std::endl;
#endif


	//FINGER_RESP *response = (FINGER_RESP*)responseResult;

	if (respCode == RC_SUCCESS)
	{
		cout << "Verification success" << std::endl;
	}
	else if (respCode == RC_VERIFY_FAIL){

		cout << "Verification failed(finger not match)" << std::endl;
	}
	else if (respCode == RC_TIME_OUT){

		cout << "Verification failed (Bioagent Timeout)" << std::endl;
	}
	else if (respCode == RC_LICENSING_ERROR){

		cout << "Verification failed (License not verified)" << std::endl;
	}
	else if (respCode == RC_VERIFY_REQID_NOT_FOUND){

		cout << "Verification failed (BDSID not found)" << std::endl;
	}
	else if (respCode == RC_CONNECTION_DATABASE_CENTRAL_ERROR){

		cout << "Verification failed (Central Server Problem : Can't connect to central database)" << std::endl;
	}
	else if (respCode == RC_CANNOT_OPEN_SCANNER_DEVICE){

		cout << "Verification failed (Scanner error)" << std::endl;
	}
	else if (respCode == RC_DEVICE_BUSY){

		cout << "Verification failed (Device busy)" << std::endl;
	}
	else if (respCode == RC_CENTRAL_SERVICE_PROBLEM){

		cout << "Verification failed (Central Service Problem)" << std::endl;
	}
	else if (respCode == RC_UNSPECIFIED_ERROR){

		cout << "Verification failed (system error)" << std::endl;
	}

	cout << "Verification finished (Please see log file for more detail)" << std::endl;



	if (hGetProcIDDLL != NULL) FreeLibrary(hGetProcIDDLL);

	/*free(fsResp);
	free(response);*/
	system("pause");



}