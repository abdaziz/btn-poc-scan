/*
  GLBTRACE.C
** Revision 05.00.00.071814
*/


#include <windows.h>
#include <lmcons.h>
#include <lmuse.h>
#include <lmapibuf.h>
#include <lmerr.h>

#include <shlobj.h>

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <malloc.h>
#include <io.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <share.h>
#include <direct.h>
#include <conio.h>
#include <time.h>
#include <errno.h>

#include "glb_defs.h"
#include "glbtrace.h"

static int getCreateDir (char *baseDir, int baseDirSz, char *subDir);
static char *dumpData (char *strbuff, int strbuffsz, void *buff, int sz);


/***********************************************************************/
/*                                                                     */
/***********************************************************************/

#if !defined(NDEBUG) && (GLB_DEBUG_LVL != 0)

//#define DUMPTOFILE      // Comment this line to dump using OutputDebugString
//#define DUMPTOLOCALFILE // Comment this to dump to server


static int  lastTraceLevel;
static char lastFilename [MAX_PATH];
static int  lastLineNo;
static time_t lastTime;

#ifdef DUMPTOFILE

char *getDbgOutFilename (void)
{
static char dbgOutFilename [_MAX_PATH] = { 0 };
static time_t logStartTime = 0;
static char *dbgOutFNBasePtr = NULL;
struct tm tm;
time_t timet;

    if (*dbgOutFilename == 0) {
#ifdef DUMPTOLOCALFILE
        if (getCreateDir (dbgOutFilename, sizeof (dbgOutFilename), "IvatamaTeknologi\\LOGS") != 0)
            *dbgOutFilename = 0;
#else
        if (_getcwd (dbgOutFilename, sizeof (dbgOutFilename) - 34) == NULL)
            *dbgOutFilename = 0;
#endif
        sprintf_s (dbgOutFilename + strlen (dbgOutFilename), sizeof (dbgOutFilename) - strlen (dbgOutFilename), "\\00-");
        dbgOutFNBasePtr = dbgOutFilename + strlen (dbgOutFilename);
    }
    timet = time (NULL);
    if (timet - logStartTime >= 3600) {
        localtime_s (&tm, &timet);
        sprintf_s (dbgOutFNBasePtr, sizeof (dbgOutFilename) - strlen (dbgOutFilename), "%02d%02d%02d%02d%02d%02d-DBGOUT.TRC", tm.tm_year % 100, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
        logStartTime = timet;
    }

    return dbgOutFilename;
}

#endif


int getFilename (char *path, char *fn, int fnSz)
{
char fname [_MAX_FNAME];
char ext [_MAX_EXT];
int err;


    if ((err = _splitpath_s (path, NULL, 0, NULL, 0, fname, sizeof (fname), ext, sizeof (ext))) == 0)
        err = _makepath_s (fn, fnSz, NULL, NULL, fname, ext);

    if (err != 0)
        strcpy_s (fn, fnSz, path);

    return 0;
}


void _GLB_DISPLAY_TRC (int level, char *filename, int lineNo, char *fmt, ...)
{
#ifdef DUMPTOFILE
int  f;
char *outfilename;

SYSTEMTIME systime;
#endif

va_list valist;
char outstring [2048 + MAX_PATH];
size_t  outstringLen;
char *printbuff;
size_t pbsz;
void *ddbuff;
int  ddsz;


    lastTraceLevel = level;
    getFilename (filename, lastFilename, sizeof (lastFilename));
    lastLineNo = lineNo;
    lastTime = time (NULL);

    if (!(lastTraceLevel & _GLB_TRACE_CURR_LEVEL))
        return;

    printbuff = outstring;

    va_start (valist, fmt);

#ifndef DUMPTOFILE
    sprintf_s (outstring, sizeof (outstring) - 2, "%s:%d (%ld) ", lastFilename, lastLineNo, GetCurrentThreadId ());
#else
    GetLocalTime (&systime);
    sprintf_s (
        outstring, sizeof (outstring) - 2,
        "%02d%02d%02d%03d %s:%d (%ld) ",
        systime.wHour, systime.wMinute, systime.wSecond, systime.wMilliseconds,
        lastFilename, lastLineNo,
        GetCurrentThreadId ()
    );
#endif
    *(outstring + sizeof (outstring) - 2) = 0;

    if (strcmp (fmt, DUMPDATAFMT) == 0) {
        ddbuff = va_arg (valist, void *);
        ddsz = va_arg (valist, int);
        fmt = va_arg (valist, char *);
    }
    else {
        ddbuff = NULL;
        ddsz = 0;
    }

    outstringLen = strlen (outstring);
    if (fmt) {
        if (outstringLen < sizeof (outstring) - 2)
            _vsnprintf_s (outstring + outstringLen, (sizeof (outstring) - 2) - outstringLen, _TRUNCATE, fmt, valist);
        *(outstring + sizeof (outstring) - 2) = 0;
        if ((ddbuff == 0) || (ddsz > 16))
            *(unsigned short *)(outstring + strlen (outstring)) = '\n';
        else
            *(unsigned short *)(outstring + strlen (outstring)) = ':';
    }
    else {
        if (outstringLen > sizeof (outstring) - 2)
            outstringLen = sizeof (outstring) - 2;
        *(unsigned short *)(outstring + outstringLen) = '\n';
    }

    if (ddbuff) {
        pbsz = (strlen (outstring) + 16) + (((ddsz + 15) / 16) * 100);
        printbuff = malloc (pbsz);
        if (printbuff == NULL) {
            printbuff = outstring;
        }
        else {
            strcpy_s (printbuff, pbsz - 2, outstring);
            pbsz -= strlen (outstring);
            dumpData (printbuff + strlen (printbuff), pbsz, ddbuff, ddsz);
            *(unsigned short *)(printbuff + strlen (printbuff)) = '\n';
        }
    }

#ifndef DUMPTOFILE
    OutputDebugString (printbuff);
#else
//    printf (printbuff);
    outfilename = getDbgOutFilename ();
    _sopen_s (&f, outfilename, O_CREAT | O_BINARY | O_RDWR | O_APPEND, SH_DENYNO, S_IREAD | S_IWRITE);
    if (f >= 0) {
        outstringLen = strlen (printbuff);
        *(unsigned short *)(printbuff + outstringLen - 1) = '\r\n';
        _write (f, printbuff, outstringLen + 1);
        _close (f);
    }
#endif

    if (printbuff != outstring)
        free (printbuff);

    va_end (valist);

    return;
}

char *dumpData (char *strbuff, int strbuffsz, void *buff, int sz)
{
unsigned char *tmpBuff;
int loop, loop2, loop3;
unsigned char ch;
int lsz;

    tmpBuff = buff;
    *strbuff = 0;
    for (loop = 0;loop < (sz + 0x0f) / 0x10;loop++) {
        lsz = min (0x10, (sz - (loop * 0x10)));
        if (sz > 0x10) {
            sprintf_s (strbuff, strbuffsz, "%07x0: ", loop);
            strbuff += strlen (strbuff);
            strbuffsz -= strlen (strbuff);
        }
        for (loop2 = 0;loop2 < lsz;loop2++) {
            sprintf_s (strbuff, sizeof (strbuff), "%02x", *(tmpBuff++));
            strbuff += strlen (strbuff);
            strbuffsz -= strlen (strbuff);
            if (loop2 == lsz - 1) {
                for (;loop2 < 0x0f;loop2++) {
                    sprintf_s (strbuff, sizeof (strbuff), "   ");
                    strbuff += strlen (strbuff);
                    strbuffsz -= strlen (strbuff);
                }
                tmpBuff -= lsz;
                sprintf_s (strbuff, sizeof (strbuff), "  ");
                strbuff += strlen (strbuff);
                strbuffsz -= strlen (strbuff);
                for (loop3 = 0;loop3 < lsz;loop3++) {
                    ch = *(tmpBuff++);
                    if (!isprint (ch))
                        ch = '.';
                    sprintf_s (strbuff, sizeof (strbuff), "%c", ch);
                    strbuff += strlen (strbuff);
                    strbuffsz -= strlen (strbuff);
                }
                sprintf_s (strbuff, sizeof (strbuff), "\n");
                strbuff += strlen (strbuff);
                strbuffsz -= strlen (strbuff);
            }
            else {
                if ((loop2 & 0x07) == 0x07)
                    sprintf_s (strbuff, sizeof (strbuff), "-");
                else
                    sprintf_s (strbuff, sizeof (strbuff), " ");
                strbuff += strlen (strbuff);
                strbuffsz -= strlen (strbuff);
            }
        }
    }

    return strbuff;
}


#ifdef DUMPTOLOCALFILE
int getCreateDir (char *baseDir, int baseDirSz, char *subDir)
{
int err;


    err = SHGetFolderPath (NULL, CSIDL_COMMON_APPDATA, NULL, SHGFP_TYPE_CURRENT, baseDir);

    if (SUCCEEDED (err)) {
        if (*(baseDir + strlen (baseDir) - 1) != '\\')
            *((short*)(baseDir + strlen (baseDir))) = '\\';
        strcat_s (baseDir, baseDirSz, subDir);
        if (_access (baseDir, 2) != 0) {
            if (errno == ENOENT) {
                if (SHCreateDirectoryEx (NULL, baseDir, NULL) != ERROR_SUCCESS)
                    err = GetLastError ();
            }
            else
                err = 1;
        }
    }
    else {
        switch (err) {
            case E_INVALIDARG :
                err = ERROR_INVALID_PARAMETER;
                break;
            case S_FALSE :
                err = ERROR_FILE_NOT_FOUND;
                break;
            default :
                err = ERROR_INVALID_FUNCTION;
                break;
        }
    }

    return err;
}
#endif // DUMPTOLOCALFILE


#endif // !NDEBUG && GLB_DEBUG_LVL != 0


