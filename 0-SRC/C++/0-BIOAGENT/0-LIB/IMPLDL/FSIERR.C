#include <stdint.h>
#include <stdio.h>

#include <NCore.h>

#include "glb_defs.h"
#include "glbtrace.h"

#include "fsierr.h"


/************************************************************************/

static const char *fsiErrStr0 [] =
{
    "No error",
    "Undefined error %d",
    "NObjectSet failed!",
    "NStringSet failed!",
    "NObjectSet %s failed!",
    "Delete hImage #%d failed!",
    "Delete hBuffer #%d failed!",
    "NObjectSetProperty() failed!",
    "NStringGetBuffer() failed!",
    "NSubjectCreate() failed!",
    "NStringCreate failed",
    "NEnumToStringP failed",
    "NObjectSaveToMemory failed",
    "NObjectUnrefArray failed!",
};

#define NUM_FSIERRSTR0 (sizeof (fsiErrStr0) / sizeof (fsiErrStr0 [0]))


static const char *fsiErrStr100 [] =
{
    "NBiometricClientCreate() failed!",                     /* 100 */
    "NBiometricClientSetBiometricTypes() failed!",          /* 101 */
    "NBiometricClientSetUseDeviceManager() failed!",        /* 102 */
    "NBiometricEngineInitialize() failed!",                 /* 103 */
    "NBiometricClientGetDeviceManager() failed!",           /* 104 */
    "NDeviceManagerGetDeviceCount() failed!",               /* 105 */
    "NDeviceManagerGetDevice() failed!",                    /* 106 */
    "NBiometricClientSetFingerScanner() failed!",           /* 107 */
    "NDeviceGetDisplayNameN() failed!",                     /* 108 */
    "NFingerCreate() failed!",                              /* 109 */
    "NSubjectAddFinger() failed!",                          /* 110 */
    "NBiometricClientCapture() failed!",                    /* 111 */
    "NFrictionRidgeGetImage() failed!",                     /* 112 */
    "NBiometricEngineCreateTemplate() failed!",             /* 113 */
    "NSubjectGetTemplateBuffer() failed!",                  /* 114 */
    "NBiometricClientAddRemoteConnection () failed",        /* 115 */
    "NBiometricEngineCreateTask () failed",                 /* 116 */
    "NBiometricEnginePerformTask () failed",                /* 117 */
    "NBiometricTaskGetError () failed",                     /* 118 */
    "NBiometricTaskGetStatus () failed",                    /* 119 */
    "NClusterAddressCreateN () failed",                     /* 120 */
    "NClusterBiometricConnectionAddAddress () failed",      /* 121 */
    "NClusterBiometricConnectionCreate () failed",          /* 122 */
    "NFTemplateAddRecordEx () failed",                      /* 123 */
    "NFTemplateCreateEx () failed",                         /* 124 */
    "NFTemplateGetRecordCount () failed",                   /* 125 */
    "NFTemplateGetRecordEx () failed",                      /* 126 */
    "NMatchingResultGetId () failed",                       /* 127 */
    "NMatchingResultGetScore () failed",                    /* 128 */
    "NSubjectGetMatchingResults () failed",                 /* 129 */
    "NSubjectSetId () failed",                              /* 130 */
    "NSubjectSetTemplateBuffer () failed",                  /* 131 */
    "NTemplateCreate () failed",                            /* 132 */
    "NTemplateCreateFromMemoryN () failed",                 /* 133 */
    "NTemplateGetFingersEx () failed",                      /* 134 */
    "NTemplateSetFaces () failed",                          /* 135 */
    "NFRecordCreateFromMemoryN() failed",                   /* 136 */
    "failed to create extractor",                           /* 137 */
    "failed to generalize templates",                       /* 138 */
    "Time out",                                             /* 139 */
    "tidak selesai",                                        /* 140 */
    "NBiometricStatus () failed",                           /* 141 */
    "NBiometricSetFilename () failed",                      /* 142 */
    "NCallbackCreate () failed",                            /* 143 */
    "NObjectAddPropertyChanged () failed",                  /* 144 */
    "NdeviceGetDeviceType () failed",                       /* 145 */
    "NdeviceGetId () failed",                               /* 146 */
    "NdeviceGetMake () failed",                             /* 147 */
    "NdeviceGetModel () failed",                            /* 148 */
    "NdeviceGetSerialNumber () failed",                     /* 149 */
    "NfrictionRidgeSetImage () failed",                     /* 150 */
};

#define NUM_FSIERRSTR100 (sizeof (fsiErrStr100) / sizeof (fsiErrStr100 [0]))


/*----------------------------------------------------------------------*/

static char     fsiTmpErrMsg [32];

static NResult  fsiLastErr;
static char     fsiMsg [256];


/************************************************************************/
/*                                                                      */
/************************************************************************/

const char *fsiGetErrStr (int msgNum)
{
const char *msg;


    msg = NULL;

    if (msgNum < 100) {
        if (msgNum < NUM_FSIERRSTR0) {
            msg = fsiErrStr0 [msgNum];
        }
    }
    else if (msgNum < 200)
    {
        if (msgNum - 100 < NUM_FSIERRSTR100) {
            msg = fsiErrStr100 [msgNum - 100];
        }
    }

    if (msg == NULL) {
        sprintf_s (fsiTmpErrMsg, sizeof (fsiTmpErrMsg), fsiErrStr0 [1], msgNum);
        msg = fsiTmpErrMsg;
    }

    return msg;
}


/*----------------------------------------------------------------------*/

NResult fsiGetLastErr ()
{
    return fsiLastErr;
}


/*----------------------------------------------------------------------*/

void fsiSetLastErr (int lastErr)
{
    fsiLastErr = lastErr;
}


/*----------------------------------------------------------------------*/

const char *fsiGetLastMsg ()
{
    return fsiMsg;
}


/*----------------------------------------------------------------------*/

void fsiSetLastMsg (const char *msg)
{
int maxSz;


    maxSz = strlen (msg) + 1;
    if (maxSz > sizeof (fsiMsg)) {
        maxSz = sizeof (fsiMsg);
        *(fsiMsg + maxSz - 1) = 0;
    }

    if (maxSz == 1)
        *fsiMsg = 0;
    else
        strncpy_s (fsiMsg, maxSz, msg, maxSz - 1);
}


/*----------------------------------------------------------------------*/

void fsiGetLastErrMsgStr (int *lastErr, const char **msg)
{
    *lastErr = fsiGetLastErr ();
    *msg = fsiGetLastMsg ();
}


/*----------------------------------------------------------------------*/

void fsiSetLastErrMsgStr (int lastErr, const char *msg)
{
    fsiSetLastErr (lastErr);
    fsiSetLastMsg (msg);

    TRACE_PRINT4 ("%s error %08x(%d)", msg, lastErr, lastErr);
}


/*----------------------------------------------------------------------*/

void fsiSetLastErrMsgNum (int lastErr, int msgNum)
{
    fsiSetLastErr (lastErr);
    fsiSetLastMsg (fsiGetErrStr (msgNum));

    TRACE_PRINT4 ("(%d)%s error %08x(%d)", msgNum, fsiGetErrStr (msgNum), lastErr, lastErr);
}


/*----------------------------------------------------------------------*/

