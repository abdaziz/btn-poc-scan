#include <windows.h>

#include <stdint.h>
#include <stdio.h>
#include <string>
#include "glb_defs.h"
#include "consts.h"
#include "glbvars.h"
#include "glb_defs.h"
#include "appguid.h"
#include "fscommon.h"
#include "baglb.h"
#include "bacglb.h"
#include "fserrba.h"
#include "fserr.h"
#include "fsierr.h"
#include "pgrep.h"
#include "FINGERBTN_STC.h"
#include "errmsg.h"
#include "BAOPS.h"
#include "FINGER_BTN.h"

using namespace std;


HWND        mainWndHdl;


#define ERROR_INFO				"Error Info"
#define VERIFY_VAILED_MESSAGE	"Verification finger failed (Error Code: %d)"
#define VERIFY_NOTMATCHED_MESSAGE	"Verification finger failed (finger not match found [Error Code: %d])"


static int          glbTimeOut;

static char         glbLastReqId[9];
static char			glbReqUid[37];
static BA_REQ_HDL   glbReqHdl;


const char* NewGuid()
{
	GUID guid;
	CoCreateGuid(&guid);
	char guidStr[37];
	sprintf_s(
		guidStr,
		"%08lX-%04hX-%04hX-%02hhX%02hhX-%02hhX%02hhX%02hhX%02hhX%02hhX%02hhX",
		guid.Data1, guid.Data2, guid.Data3,
		guid.Data4[0], guid.Data4[1], guid.Data4[2], guid.Data4[3],
		guid.Data4[4], guid.Data4[5], guid.Data4[6], guid.Data4[7]);

	return guidStr;
}

int __stdcall fs_send(char *message)
{
	int         err;
	char* errorMessage = NULL;

	if ((message != NULL) && (message[0] == '\0')) {
		return RC_VERIFY_REQID_CANNOT_EMPTY;
	}

	FINGER_REQ   *fsReq;

	string str(message);

	std::string varbdsids = str.substr(0, 8);

	char BDSID[9];

	strcpy_s(BDSID, varbdsids.c_str());

	err = NO_ERR;
	glbTimeOut = TIME_OUT_REV;
	fsReq = (FINGER_REQ *)BDSID;

	progressReport(PGD_LOG, PG_PRINTF, "Starting verification finger");	

	memset(glbLastReqId, 0, sizeof(glbLastReqId));
	strncpy_s(glbLastReqId, sizeof(glbLastReqId), fsReq->reqId, 9);
	*(glbLastReqId + sizeof(glbLastReqId) - 1) = 0;

	memset(glbReqUid, 0, sizeof(glbReqUid));
	strcpy(glbReqUid, NewGuid());

	BA_STATUS baStatus;
	BA_RESULT baResult;

	progressReport(PGD_LOG, PG_PRINTF, "Starting close request handle");
	baCloseHandle(&glbReqHdl);
	progressReport(PGD_LOG, PG_PRINTF, "Close request handle ");


	if ((err = baSendRequest(glbTimeOut, &glbReqHdl, BA_REQ_VERIFY, glbReqUid, glbLastReqId)) == 0) {
		if (glbReqHdl.evtSendResponse != NULL)
		{
			if ((err = baWaitResponse(glbTimeOut, &glbReqHdl, &baStatus, &baResult)) == NO_ERR) {
				if (baStatus.errCode == FSE_DEVICE_BUSY) {
					progressReport(PGD_LOG, PG_PRINTF, "Verification finger error: DEVICE BUSY");
					err = FSE_DEVICE_BUSY;
				}
				else if (baStatus.errCode == FSE_INPROGRESS)
				{
					progressReport(PGD_LOG, PG_PRINTF, "Finger Scanning in progress");
					err = FSE_OK;
				}
			}
		}
	}

	return err;
}

int __stdcall fs_receive(int *iRespCode)
{
	int err;
	err = NO_ERR;
	//FINGER_RESP *fResp;

	BA_STATUS baStatus;
	BA_RESULT baResult;

	//fResp = *((FINGER_RESP **)respResult);
	int respCode;

	progressReport(PGD_LOG, PG_PRINTF, "Starting receive verification result");
	if (strlen(glbLastReqId) != 0)
	{
		if ((err = baWaitResponse(glbTimeOut, &glbReqHdl, &baStatus, &baResult)) == 0) {
			if (baStatus.errCode == 0) {
				respCode = RC_SUCCESS;

				progressReport(PGD_LOG, PG_PRINTF, "Verification finger success (Error Code: %d, UserId: %s, Score: %d)", baStatus.errCode, baResult.matchInfo->reqId, baResult.matchInfo->score);

			}
			else if ((baStatus.errCode == 53) && (baStatus.fsiLastErr == 139))
			{
				mapBioAgentError(baStatus.errCode, &respCode);
				progressReport(PGD_LOG, PG_PRINTF, VERIFY_VAILED_MESSAGE, respCode);
			}
			else if ((baStatus.errCode == 53) && (baStatus.fsiLastErr == -33))
			{
				mapBioAgentError(FSE_VERIFY_BDS_CANNOT_OPEN_SCANNER_DEVICE, &respCode);
				progressReport(PGD_LOG, PG_PRINTF, VERIFY_VAILED_MESSAGE, respCode);
			}
			else if (baStatus.errCode == FSE_VERIFY_BDS_NOT_MATCH)
			{
				mapBioAgentError(FSE_VERIFY_BDS_NOT_MATCH, &respCode);
				progressReport(PGD_LOG, PG_PRINTF, VERIFY_NOTMATCHED_MESSAGE, respCode);

			}
			else {
				mapBioAgentError(baStatus.errCode, &respCode);
				progressReport(PGD_LOG, PG_PRINTF, VERIFY_VAILED_MESSAGE, respCode);
			}
		}

		baCloseHandle(&glbReqHdl);

	}
	else
	{
		baStatus.errCode = 0;
		respCode = RC_VERIFY_REQID_NOT_FOUND;
	}

	*iRespCode = respCode;

	progressReport(PGD_LOG, PG_PRINTF, "Verification finger process completed");

	return err;
}


//int __stdcall fs_receive(char **result)
//{
//	int         err;
//	err = NO_ERR;
//
//
//	FINGER_RESP *fsResp;
//
//	BA_STATUS baStatus;
//	BA_RESULT baResult;
//
//	 err = NO_ERR;
//
//	fsResp = *((FINGER_RESP **)result);
//
//	memset(fsResp, 0, sizeof(FINGER_RESP));
//
//	progressReport(PGD_LOG, PG_PRINTF, "Starting receive verification result");
//	if (strlen(glbLastReqId) != 0)
//	{
//		if ((err = baWaitResponse(glbTimeOut, &glbReqHdl, &baStatus, &baResult)) == 0) {
//			strcpy_s(fsResp->reqId, sizeof(fsResp->reqId), glbLastReqId);
//			if (baStatus.errCode == 0) {
//				fsResp->respCode = RC_SUCCESS;
//
//				progressReport(PGD_LOG, PG_PRINTF, "Verification finger success (Error Code: %d, UserId: %s, Score: %d)", baStatus.errCode, baResult.matchInfo->reqId, baResult.matchInfo->score);
//
//			}
//			else if ((baStatus.errCode == 53) && (baStatus.fsiLastErr == 139))
//			{
//			    mapBioAgentError(baStatus.errCode, &fsResp->respCode);
//				progressReport(PGD_LOG, PG_PRINTF, VERIFY_VAILED_MESSAGE, fsResp->respCode);
//			}
//			else if ((baStatus.errCode == 53) && (baStatus.fsiLastErr == -33))
//			{
//				mapBioAgentError(FSE_VERIFY_BDS_CANNOT_OPEN_SCANNER_DEVICE, &fsResp->respCode);
//				progressReport(PGD_LOG, PG_PRINTF, VERIFY_VAILED_MESSAGE, fsResp->respCode);
//			}
//			else if (baStatus.errCode == FSE_VERIFY_BDS_NOT_MATCH)
//			{
//				mapBioAgentError(FSE_VERIFY_BDS_NOT_MATCH, &fsResp->respCode);
//				progressReport(PGD_LOG, PG_PRINTF, VERIFY_NOTMATCHED_MESSAGE, baStatus.errCode);
//
//			}
//			else {
//				mapBioAgentError(baStatus.errCode, &fsResp->respCode);
//				progressReport(PGD_LOG, PG_PRINTF, VERIFY_VAILED_MESSAGE, fsResp->respCode);
//			}
//		}
//
//		baCloseHandle(&glbReqHdl);
//
//	}
//	else
//	{
//		baStatus.errCode = 0;
//		strcpy_s(fsResp->reqId, sizeof(fsResp->reqId), glbLastReqId);
//
//		fsResp->respCode = RC_VERIFY_REQID_NOT_FOUND;
//	}
//
//	progressReport(PGD_LOG, PG_PRINTF, "Verification finger process completed");
//
//	return err;
//}

int mapBioAgentError(int err, int *respCode)
{
	switch (err)
	{
	case FSE_VERIFY_BDS_SYSTEM_ERROR:
		*respCode = RC_UNSPECIFIED_ERROR;
		break;
	case FSE_VERIFY_BDS_TIMEOUT:
	case FSE_TIME_OUT:
	case FSE_VERIFY_BDS_SCANNER_TIMEOUT:
		*respCode = RC_TIME_OUT;
		break;
	case FSE_VERIFY_BDS_LICENSE_ERROR:
		*respCode = RC_LICENSING_ERROR;
		break;
	case FSE_VERIFY_BDS_ID_NOT_FOUND:
		*respCode = RC_VERIFY_REQID_NOT_FOUND;
		break;
	case FSE_VERIFY_BDS_NOT_MATCH:
		*respCode = RC_VERIFY_FAIL;
		break;
	case FSE_VERIFY_BDS_CONNECTION_DATABASE_CENTRAL_ERROR:
		*respCode = RC_CONNECTION_DATABASE_CENTRAL_ERROR;
		break;
	case FSE_VERIFY_BDS_CANNOT_OPEN_SCANNER_DEVICE:
		*respCode = RC_CANNOT_OPEN_SCANNER_DEVICE;
		break;
	case FSE_VERIFY_BDS_CANNOT_OPEN_ADDITIONAL_CFG:
		*respCode = RC_CONFIG_FILE_NOT_FOUND;
		break;
	case FSE_VERIFY_BDS_CENTRAL_SERVICE_ERROR:
		*respCode = RC_CENTRAL_SERVICE_PROBLEM;
		break;
	case FSE_DEVICE_BUSY:
		*respCode = FSE_DEVICE_BUSY;
		break;
	default:
		*respCode = RC_UNSPECIFIED_ERROR;
	}

	return err;
}
