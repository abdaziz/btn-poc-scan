#include <windows.h>
#include <stdio.h>
#include <process.h>
#include <stdint.h>
#include <stdio.h>

#include "glb_defs.h"
#include "consts.h"
#include "glbvars.h"
#include "glb_defs.h"
#include "appguid.h"
#include "fscommon.h"
#include "baglb.h"
#include "bacglb.h"
#include "fserrba.h"
#include "fserr.h"
#include "fsierr.h"
#include "pgrep.h"
#include "FINGERBTN_STC.h"
#include "errmsg.h"
#include "BAOPS.h"
#include "AGENTTH.h"

using namespace std;

#define BIOAGENT_WAIT_TIMEOUT           5000 //in millisecond = 5 seconds
#define EMPTY_STRING			""


static int initialize();


int main()
{
	int err = NO_ERR;
	printf("============================================================================== \n");
	printf("************************* FINGER AGENT-BTN (POC)*******************************\n");
	printf("============================================================================== \n");

	printf("#Start monitoring agent \n");

	HANDLE hThread = (HANDLE)_beginthread(agentThread, 0, NULL);

	//int timeWait = BIOAGENT_WAIT_TIMEOUT / 500; //give 6 seconds
	WaitForSingleObject(hThread, BIOAGENT_WAIT_TIMEOUT);     // wait until the thread has finished
	err = initialize();
	if (err != NO_ERR)
	{
		if (err == 2)
		{
			printf("#Agent error: license error, please close application and re-run \n");
			system("pause");
			CloseHandle(hThread);
			ExitProcess(1);
			return 0;
		}
		else if (err == 6)
		{
			printf("#Agent error: Cannot open scanner client/device , please close application and re-run \n");
			system("pause");
			CloseHandle(hThread);
			ExitProcess(1);
			return 0;
		}
		
	}
	printf("#Agent event was activated \n");
	printf("#Agent event was initialized\n");

	system("pause");
	CloseHandle(hThread);
	ExitProcess(1);

	return 0;


}

int initialize()
{
	int err = 0;
	BA_REQ_HDL reqHdl;
	BA_STATUS baStatus;
	BA_RESULT baResult;

	if ((err = baSendRequest(BIOAGENT_WAIT_TIMEOUT, &reqHdl, BA_REQ_INITIALIZE, EMPTY_STRING, EMPTY_STRING)) == 0) {
		if ((err = baWaitResponse(BIOAGENT_WAIT_TIMEOUT * 6, &reqHdl, &baStatus, &baResult)) == 0) {
			err = baStatus.errCode;
		}
	}

	baCloseHandle(&reqHdl);

	return err;

}