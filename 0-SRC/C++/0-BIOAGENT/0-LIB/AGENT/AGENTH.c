#include <windows.h>

#include <stdint.h>
#include <stdlib.h>

#include "glb_defs.h"
#include "glbtrace.h"
#include "appguid.h"

#include "pgrep.h"
#include "fscommon.h"
#include "fsierr.h"

#include "baglb.h"
#include "syncops.h"
#include "AGENTTH.h"

/******************************************************************/

#define EVT_CANCEL_SCAN_IDX     0
#define EVT_TERMINATE_IDX       1
#define EVT_REQUEST_IDX         2
#define NUM_WAIT_OBJS           3

/******************************************************************/

void agentThread(void *parm)
{
	HANDLE waitThArr[NUM_WAIT_OBJS];
	int waitRes;

	int quitThread;

	int err;


	fileShareMap = NULL;
	fmMemData = NULL;

	mtxBioAgent = NULL;
	mtxPgStatus = NULL;
	mtxPgImage = NULL;

	evtRequest = NULL;
	evtCancelScan = NULL;

	evtResponse = NULL;
	evtPgStatus = NULL;
	evtPgImage = NULL;

	evtScanEnd = NULL;
	evtTerminate = NULL;

	err = createFileShareMap(&fileShareMap, &fmMemData, BA_SHARE_MEM);

	if (err == 0) err = createMutex(&mtxBioAgent, NULL, FALSE, BA_MTX_BIOAGENT);
	if (err == 0) err = createMutex(&mtxPgStatus, NULL, FALSE, BA_MTX_PG_STATUS);
	if (err == 0) err = createMutex(&mtxPgImage, NULL, FALSE, BA_MTX_PG_IMAGE);

	if (err == 0) err = createEvent(&evtRequest, NULL, FALSE, FALSE, BA_EVT_REQUEST);
	if (err == 0) err = createEvent(&evtCancelScan, NULL, FALSE, FALSE, BA_EVT_CANCEL_SCAN);

	if (err == 0) err = createEvent(&evtPgStatus, NULL, FALSE, FALSE, BA_EVT_PG_STATUS);
	if (err == 0) err = createEvent(&evtPgImage, NULL, FALSE, FALSE, BA_EVT_PG_IMAGE);

	//if (err == 0) err = createEvent(&evtScanEnd, NULL, FALSE, FALSE, NULL);
	if (err == 0) err = createEvent(&evtTerminate, NULL, TRUE, FALSE, NULL);

	// Creation of evtResponse must be the last one, because other process will be waiting for these event as an indication of initialization finish
	if (err == 0) err = createEvent(&evtResponse, NULL, FALSE, FALSE, BA_EVT_RESPONSE);

	if (err == 0) {
		fsiSetLastErr(0);
		fsiSetLastMsg("");

		quitThread = 0;

		waitThArr[EVT_CANCEL_SCAN_IDX] = evtCancelScan;
		waitThArr[EVT_TERMINATE_IDX] = evtTerminate;
		waitThArr[EVT_REQUEST_IDX] = evtRequest;

		while (quitThread == 0) {
			waitRes = WaitForMultipleObjects(NUM_WAIT_OBJS, waitThArr, FALSE, INFINITE);

			switch (waitRes)
			{
			case WAIT_OBJECT_0 + EVT_CANCEL_SCAN_IDX:
				processBioCancelScan();
				break;

			case WAIT_OBJECT_0 + EVT_REQUEST_IDX:
				processBioRequest();
				break;

			case WAIT_OBJECT_0 + EVT_TERMINATE_IDX:
				processBioCancelScan();

				quitThread = 1;
				break;

			case WAIT_ABANDONED_0 + EVT_TERMINATE_IDX:
			case WAIT_ABANDONED_0 + EVT_REQUEST_IDX:
				progressReport(PGD_ALL, PG_BA_WAIT_FAILED, "abandoned", GetLastError());
				processBioCancelScan();

				quitThread = 1;
				break;

			case WAIT_TIMEOUT:
				break;

			default:
				progressReport(PGD_ALL, PG_BA_WAIT_FAILED, "unknown response", GetLastError());

				quitThread = 1;
				break;
			}
		}

		closeAgent();
	}

	if (mtxBioAgent != NULL) CloseHandle(mtxBioAgent);
	if (mtxPgStatus != NULL) CloseHandle(mtxPgStatus);
	if (mtxPgImage != NULL) CloseHandle(mtxPgImage);

	if (evtRequest != NULL) CloseHandle(evtRequest);
	if (evtCancelScan != NULL) CloseHandle(evtCancelScan);

	if (evtResponse != NULL) CloseHandle(evtResponse);
	if (evtPgStatus != NULL) CloseHandle(evtPgStatus);
	if (evtPgImage != NULL) CloseHandle(evtPgImage);

	if (evtScanEnd != NULL) CloseHandle(evtScanEnd);
	if (evtTerminate != NULL) CloseHandle(evtTerminate);

	closeFileShareMap(fileShareMap, fmMemData);

}