#include <windows.h>

#include <stdlib.h>
#include <stdint.h>

#include <NCore.h>
//#include <NBiometrics.h>
#include <NBiometricClient.h>

#include "glb_defs.h"
#include "consts.h"
#include "TBLSTRUC.H"
#include "glbvars.h"
#include "appinfo.h"


#include "pgrep.h"
//#include "fsierr.h"
#include "ipreview.h"

#include "fsinit.h"
#include "fsclient.h"

#include "INITAGENT.h"


HWND        mainWndHdl;
FSCONFIG    fsConfig;

/*----------------------------------------------------------------*/

int initAgent(void)
{
	int err;
	getAppStartUpInfo();


	if (initFs() != 0) {
		progressReport(PGD_ALL, PG_FSCAN_INIT_ERR);
		return 2;
	}

	err = fsClientOpen();
	if (err != NO_ERROR)
	{
		progressReport(PGD_ALL, PG_PRINTF, "Scanner Error : %s", "Cannot open fingerscan client/device");
		err = 6;
	}

	return err;

}



/*----------------------------------------------------------------*/

int closeAgent(void)
{
	int err;


	err = NO_ERR;

	err = fsClientClose();

	closeFs();

	return err;
}


/*----------------------------------------------------------------*/


