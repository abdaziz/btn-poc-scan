#include <windows.h>

#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#include <process.h>

#include <NCore.h>
#include <NBiometrics.h>

#include "glb_defs.h"
#include "consts.h"
#include "glbvars.h"
#include "fscommon.h"
#include "fserr.h"
#include "fsierr.h"
#include "TBLSTRUC.H"
#include "FSUTIL.H"

#include "ipreview.h"
#include "pgrep.h"
#include "fsclient.h"
#include "baglb.h"
#include "appinfo.h"
#include "acqsgl.h"
#include "initagent.h"
#include "FSSERVER.H"
#include "TIMEUTIL.H"
#include "CONFGITEM.h"
#include "SVCCOMMAND.h"
#include "base64.h"
#include "AGENTPROC.h"

extern "C"
{
	CFGITEMS		glbConfigItem;
}

/******************************************************************/

static int          glbIsInited;
static LONG         glbScanInProgress;
static BA_REQUEST   glbRequest;

clock_t end_clock;
clock_t start_clock;

/******************************************************************/

static int  doInit();
static void baImgPreview(void *pVi, void *img);
static void doFingerOp(void *parm);
static int setResultData(time_t matchTime, int matchInfoCnt, FSMATCHINGINFO *matchInfo);
static int verifyFinger(char* serviceURL, const char* reqUniqueId, const char* reqId, int reqCode, HNBuffer hTemplate);
/******************************************************************/

#define FINGER_MAX_REQUEST               1
#define FINGER_MIN_QUALITY               70
#define FINGER_MIN_FAR                   0.0010000000


int doInit()
{
	

	int err = N_OK;

	if (glbIsInited == 0) {
		err = initAgent();
		processBioResult(err);
		if (err == 0)
			glbIsInited = 1;
	}
	else
		processBioResult(0);

	return err;

}

//int verifyFinger(const char* templatePath, const char* reqId, int reqCode, HNBuffer hTemplate)
//{
//
//	int err;
//	time_t          matchTime;
//	int             matchInfoCnt;
//	FSMATCHINGINFO  matchInfo[1000];       // Same as default Matching.MaximalResultCount
//	NBiometricStatus bioStatus;
//
//	int         bestScore;
//	int         bestIndex;
//	int         loop;
//
//
//	getAdjTime(&matchTime);
//
//	memset(matchInfo, 0, sizeof (matchInfo));
//	matchInfoCnt = sizeof (matchInfo) / sizeof (matchInfo[0]);
//
//	if (reqCode == BA_REQ_VERIFY) {
//
//		int minFAR = FINGER_MIN_FAR;
//
//		err = VerifyByFile(templatePath, reqId, hTemplate, minFAR, matchInfo, &matchInfoCnt, &bioStatus);
//
//		if (err != N_OK) {
//			progressReport(PGD_ALL, PG_PRINTF, "Verification template error %d", err);
//
//			err = FSE_VERIFY_ON_SERVER;
//		}
//		else{
//
//			bestIndex = 0;
//			bestScore = 0;
//
//			for (loop = 0; loop < matchInfoCnt; loop++) {
//				if (reqId == matchInfo[loop].reqId) {
//					if (bestScore < matchInfo[loop].score) {
//						bestIndex = loop;
//						bestScore = matchInfo[loop].score;
//					}
//				}
//			}
//
//			if (bestScore == 0)
//				matchInfoCnt = 0;
//			else {
//				matchInfoCnt = 1;
//				strcpy_s(matchInfo[0].reqId, matchInfo[bestIndex].reqId);
//				matchInfo[0].score = matchInfo[bestIndex].score;
//				matchInfo[0].index = matchInfo[bestIndex].index;
//			}
//
//
//
//		}
//
//	}
//	return err;
//
//}

int verifyFinger(char* serviceURL, const char* reqUniqueId, const char* reqId, int reqCode, HNBuffer hTemplate)
{

	//set timeout
	end_clock = clock();
	ULONG diffClock = ((double)(end_clock - start_clock));
	ULONG diffTimeOut = (double)glbRequest.timeOut - (double)diffClock;

	time_t vDate;
	time(&vDate);
	struct tm  ts;
	char       reqVerifyDate[80];
	ts = *localtime(&vDate);
	strftime(reqVerifyDate, sizeof(reqVerifyDate), "%d-%m-%Y %H:%M:%S", &ts);

	FSMATCHINGINFO matchingInfo;
	time_t matchingTime;
	int matchCount = 0;

	int err = VerifyFingerSystem(serviceURL, diffTimeOut, reqUniqueId, reqId, hTemplate, &matchingInfo, &matchCount, &matchingTime, reqVerifyDate);
	if (err == N_OK)
	{
		setResultData(matchingTime, matchCount, &matchingInfo);
	}

	return err;
}

void baImgPreview(void *pVi, void *img)
{
	UNREFERENCED_PARAMETER(pVi);

	FPIMAGE_DT imgRec;
	HNImage hImg;


	hImg = (HNImage)img;

	if (fsGetImageData(hImg, &imgRec) == NO_ERR) {
		if (imgRec.buffSz <= MAX_IMAGE_SZ) {
			if (WaitForSingleObject(mtxPgImage, DEF_WAIT_FOR_OBJECT) == WAIT_OBJECT_0) {
				fmMemData->imgSize = imgRec.buffSz;
				memcpy(fmMemData->imgData, imgRec.buff, imgRec.buffSz);

				ReleaseMutex(mtxPgImage);
				SetEvent(evtPgImage);
			}
		}
	}
}

int setResultData(time_t matchTime, int matchInfoCnt, FSMATCHINGINFO *matchInfo)
{
	int loop;


	fmMemData->result.matchTime = matchTime;
	fmMemData->result.matchInfoCnt = matchInfoCnt;

	for (loop = 0; loop < matchInfoCnt; loop++) {
		progressReport(PGD_LOG, PG_PRINTF, "ID: %s Score: %d MatchIndex: %d", matchInfo[loop].reqId, matchInfo[loop].score, matchInfo[loop].index);
		strcpy_s(fmMemData->result.matchInfo[loop].reqId, matchInfo[loop].reqId);
		fmMemData->result.matchInfo[loop].score = matchInfo[loop].score;
		fmMemData->result.matchInfo[loop].index = matchInfo[loop].index;
	}

	return 0;
}

void doFingerOp(void *parm)
{
	BA_REQUEST *reqData;
	FP_DATA_SINGLE fps;
	IMGPREVIEW_INFO previewInfo;

	int useGeneralization;
	int numRequest;

	int err;

	progressReport(PGD_LOG, PG_PRINTF, "Starting read config file");
	err = readConfigFile(appConfigFile, &glbConfigItem);
	if (err == N_OK)
	{

		reqData = (BA_REQUEST *)parm;

		previewInfo.previewF = baImgPreview;
		previewInfo.viewerInfo = NULL;

		if ((err = fpAcqSglInit(&fps)) == NO_ERR) {
			if ((err = fpAcqSglScan(FINGER_MIN_QUALITY, reqData->timeOut, &previewInfo, &fps, FINGER_MAX_REQUEST, -1)) == NO_ERR) {

				err = verifyFinger(glbConfigItem.ServiceURL, reqData->reqUniqueId, reqData->reqId, reqData->reqCode, fps.hTemplate);
			}
		}

		processBioResult(err);

		fpAcqSglCleanUp(&fps);

		glbScanInProgress = 0;
	}
	else{

		fsiSetLastErrMsgStr(FSE_VERIFY_BDS_CANNOT_OPEN_ADDITIONAL_CFG, "Additional config file problem");
		processBioResult(FSE_VERIFY_BDS_CANNOT_OPEN_ADDITIONAL_CFG);
	}


}


int processBioRequest(void)
{
	int err;
	int reqCode;

	err = N_OK;

	fsiSetLastErr(0);
	fsiSetLastMsg("");

	reqCode = fmMemData->request.reqCode;
	switch (reqCode)
	{

	case BA_REQ_NULL:
		processBioResult(err);
		break;
	case BA_REQ_INITIALIZE:
		err = doInit();
		break;
	case BA_REQ_VERIFY:
		start_clock = clock();
		if (glbIsInited == 0) {
			err = doInit();
		}

		if (glbIsInited != 0) {
			if (InterlockedCompareExchange(&glbScanInProgress, 1, 0) == 0) {
				// Reset some events
				ResetEvent(evtCancelScan);
				ResetEvent(evtResponse);
				ResetEvent(evtPgStatus);
				ResetEvent(evtPgImage);

				glbRequest.reqCode = reqCode;
				strcpy_s(glbRequest.reqId, fmMemData->request.reqId);
				strcpy_s(glbRequest.reqUniqueId, fmMemData->request.reqUniqueId);
				glbRequest.timeOut = fmMemData->request.timeOut;

				_beginthread(doFingerOp, 0, &glbRequest);
			}
			else {
				processBioResult(FSE_DEVICE_BUSY);
			}
		}
		break;
	}

	return err;
}


int processBioCancelScan(void)
{
	/*if (WaitForSingleObject(evtCancelScan, 0) != WAIT_OBJECT_0)
		progressReport(PGD_ALL, PG_BA_CANCEL_SCAN);*/

	fmMemData->request.numHImage = 1; // force scanning

	fsClientCancel();
	fsiSetLastErr(0);
	fsiSetLastMsg("");
	processBioResult(FSE_USER_CANCELLED);

	return 0;
}


int processBioResult(int err)
{
	fmMemData->status.errCode = err;
	fmMemData->status.fsiLastErr = fsiGetLastErr();
	strncpy_s(fmMemData->status.fsiMsg, sizeof (fmMemData->status.fsiMsg), fsiGetLastMsg(), sizeof (fmMemData->status.fsiMsg) - 1);
	*(fmMemData->status.fsiMsg + sizeof (fmMemData->status.fsiMsg) - 1) = 0;

	SetEvent(evtResponse);

	progressReport(PGD_ALL, PG_BA_RESULT, err, fsiGetLastErr(), fsiGetLastMsg());

	return 0;
}