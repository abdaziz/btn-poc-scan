#pragma warning( disable : 4789 ) 
#include <windows.h>
#include "errmsg.h"
#include "CONFGITEM.h"

#define CFG_APP_NAME            "ADDITIONAL"
#define CFG_KEY_FILEPATH         "SERVICEURL"

static void fill_BDSWEB_cfg(CFGITEMS *confInfo, char *cfg_key, char *cfg_val)
{
	if (_stricmp(cfg_key, CFG_KEY_FILEPATH) == 0)
	{
		strcpy_s(confInfo->ServiceURL, sizeof (confInfo->ServiceURL), cfg_val);
	}
}

int readConfigFile(char *fn, CFGITEMS *configItem)
{

	char tmpVar[1024];
	unsigned char binStr[64];
	size_t rSize;
	int rv;


	memset(configItem, 0, sizeof (CFGITEMS));

	rv = GetPrivateProfileString(CFG_APP_NAME, CFG_KEY_FILEPATH, "**INVALID**", tmpVar, sizeof (tmpVar), fn);
	if ((rv == sizeof (tmpVar)-1) || (strcmp(tmpVar, "**INVALID**") == 0)) {
		displayError("ERROR", "Reading config file path");
		return 1;
	}

	fill_BDSWEB_cfg(configItem, CFG_KEY_FILEPATH, tmpVar);

	return 0;
}