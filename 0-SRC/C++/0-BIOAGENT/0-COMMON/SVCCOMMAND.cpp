#include <windows.h>
#include <stdint.h>
#include <stdlib.h>
#include <NCore.h>
#include <NBiometrics.h>
#include "glb_defs.h"
#include "glbtrace.h"
#include "consts.h"
#include "tblstruc.h"
#include "FSCOMMON.H"
#include "FSERR.H"
#include "fsutil.h"
#include "TIMEUTIL.H"
#include "base64.h"
#include "pgrep.h"
#include "curl\curl.h"
#include "json\json.h"
#include "COMMON.h"
#include "SVCCOMMAND.h"

#define BTN_FINGER					"BTN FINGER"
//=======================================FINGER_KBD2==================================================================================

#define __MK_VERSTR(maj,min,bld,blddt)  #maj "." #min "." #bld "." #blddt "\0"
#define _MK_VERSTR(maj,min,bld,blddt)   __MK_VERSTR(maj,min,bld,blddt)

#define RCVER_MAJOR_BTN_FINGER					1
#define RCVER_MINOR_BTN_FINGER					0
#define RCVER_REVISION_BTN_FINGER				0
#define RCVER_BUILD_BTN_FINGER					1115

#define RCVER_FILEVERSION_BTN_FINGER				RCVER_MAJOR_BTN_FINGER, RCVER_MINOR_BTN_FINGER, RCVER_REVISION_BTN_FINGER, RCVER_BUILD_BTN_FINGER
#define RCVER_FILEVERSIONSTR_BTN_FINGER			_MK_VERSTR (RCVER_MAJOR_BTN_FINGER, RCVER_MINOR_BTN_FINGER, RCVER_REVISION_BTN_FINGER, RCVER_BUILD_BTN_FINGER)

#define RCVER_PRODUCTVERSION_BTN_FINGER			RCVER_MAJOR_BTN_FINGER, RCVER_MINOR_BTN_FINGER, RCVER_REVISION_BTN_FINGER, RCVER_BUILD_BTN_FINGER
#define RCVER_PRODUCTVERSIONSTR_BTN_FINGER		_MK_VERSTR (RCVER_MAJOR_BTN_FINGER, RCVER_MINOR_BTN_FINGER, RCVER_REVISION_BTN_FINGER, RCVER_BUILD_BTN_FINGER)


int VerifyFingerSystem(char *fingerServicURL, int timeOut, const char* reqUniqueId, const char* reqId, HNBuffer hTemplate, FSMATCHINGINFO* matchingInfo, int* matchCount, time_t* matchingTime, char* verifyDate)
{
	int httpCode = NO_ERR;
	int respCode = NO_ERR;
	int err = NO_ERR;

	std::string respMessage;
	std::string strResponse;
	std::string strTemporary;
	Json::Value jsonDataBdsWeb;

	char* response;
	char* raw_json_body;

	FSMATCHINGINFO matchInfo;
	matchInfo = { 0 };

	FPTEMPLATEBUFFER_DT templateBuffer;
	fsGetBufferData(hTemplate, &templateBuffer);

	unsigned char* data = (unsigned char*)templateBuffer.buff;
	std::string encoded = base64_encode(data, templateBuffer.buffSz);
	char *cTemplate = (char*)encoded.c_str();

	int matchInfoCount = 0;

	//serialize json data
	Json::Value requestBody;
	requestBody["ApplicationGuid"] = reqUniqueId;
	requestBody["ApplicationName"] = BTN_FINGER;
	requestBody["ApplicationVersion"] = RCVER_PRODUCTVERSIONSTR_BTN_FINGER;
	requestBody["BdsId"] = reqId;
	requestBody["Template"] = cTemplate;
	requestBody["VerifyDate"] = verifyDate;


	Json::StreamWriterBuilder wBuilder;
	std::string json_body;
	json_body.clear();
	json_body = Json::writeString(wBuilder, requestBody);
	raw_json_body = '\0';
	raw_json_body = const_cast<char*>(json_body.c_str());

	strTemporary.assign(GetUrlRoute(fingerServicURL, "/finger/verificationBTN"));
	char *url = new char[strTemporary.length() + 1];
	strcpy(url, strTemporary.c_str());
	strTemporary.clear();

	//progressReport(PGD_LOG, PG_PRINTF, "Bioagent client timeout : %d ", timeOut);

	progressReport(PGD_LOG, PG_ZSTR, "Starting verification finger services");
	httpCode = RestAPIRequest(raw_json_body, url, APPLICATION_JSON, APPLICATION_JSON, UTF_8, "", HTTP_POST, timeOut, &response);
	strResponse.clear();
	strResponse.assign(response);

	Json::Value jsonData;
	if (httpCode == HTTP_STATUS_OK){
		jsonData.empty();
		Json::Reader reader;
		if (!reader.parse(strResponse, jsonData))
		{
			progressReport(PGD_LOG, PG_PRINTF, "Response data failed (error parsing json data verifylogin)");
			err = FSE_VERIFY_BDS_SYSTEM_ERROR;
		}

		if (err == NO_ERR)
		{

			//get json data
			respCode = jsonData["statusCode"].asInt();
			respMessage.clear();
			respMessage = jsonData["statusMessage"].asString();

			if (respCode == NO_ERR)
			{

				std::string strResponseData = jsonData["responseData"].asString();
				Json::Value jsonResponseData;
				if (!reader.parse(strResponseData, jsonResponseData))
				{
					progressReport(PGD_LOG, PG_PRINTF, "Verification finger services failed (Response error parsing json data)");
					progressReport(PGD_LOG, PG_PRINTF, "Verification finger services  (Error: 999 - %s) - %d", response, httpCode);
					err = FSE_VERIFY_BDS_SYSTEM_ERROR;
				}

				if (err == NO_ERR)
				{
					const Json::Value verifyData = jsonResponseData;
					matchInfoCount = verifyData["fingerPrintResult"]["MatchCount"].asInt();
					if (matchInfoCount > 0)
					{
						strTemporary.assign(verifyData["fingerPrintResult"]["UserId"].asString().c_str());
						char *cUserId = new char[strTemporary.length() + 1];
						cUserId = new char[strTemporary.length() + 1];
						strcpy(cUserId, strTemporary.c_str());
						strTemporary.clear();
						strcpy(matchInfo.reqId, cUserId);

						strTemporary.assign(verifyData["fingerPrintResult"]["Name"].asString().c_str());
						char *cName = new char[strTemporary.length() + 1];
						cName = new char[strTemporary.length() + 1];
						strcpy(cName, strTemporary.c_str());
						strTemporary.clear();
						strcpy(matchInfo.name, cName);

						strTemporary.assign(verifyData["fingerPrintResult"]["FingerName"].asString().c_str());
						char *cFingerName = new char[strTemporary.length() + 1];
						cFingerName = new char[strTemporary.length() + 1];
						strcpy(cFingerName, strTemporary.c_str());
						strTemporary.clear();
						strcpy_s(matchInfo.fingerName, cFingerName);

						matchInfo.score = verifyData["fingerPrintResult"]["Score"].asInt();
						*matchCount = matchInfoCount;

						time_t currTime;
						getAdjTime(&currTime);
						*matchingTime = currTime;
						progressReport(PGD_ALL, PG_PRINTF, "%s - %d", (char*)respMessage.c_str(), httpCode);
					}
					else
					{

						getAdjTime(matchingTime);

						progressReport(PGD_ALL, PG_PRINTF, "%s - %d", (char*)respMessage.c_str(), httpCode);
						err = FSE_VERIFY_BDS_NOT_MATCH;
					}

					*matchingInfo = matchInfo;
				}
			}
			else
			{
				getAdjTime(matchingTime);

				progressReport(PGD_ALL, PG_PRINTF, "%s - %d", (char*)respMessage.c_str(), httpCode);
				err = FSE_VERIFY_BDS_CENTRAL_SERVICE_ERROR;


			}

		}

	}
	else{

		/*progressReport(PGD_ALL, PG_ZSTR, "%s - %d", "Central service problem", httpCode);
		err = FSE_VERIFY_BDS_CENTRAL_SERVICE_ERROR;*/

		jsonData.empty();
		Json::Reader reader;
		if (!reader.parse(strResponse, jsonData))
		{
			progressReport(PGD_LOG, PG_PRINTF, "Response data failed (error parsing json data verifylogin)");
			err = FSE_VERIFY_BDS_SYSTEM_ERROR;
			return err;
		}

		//get json data
		respCode = jsonData["statusCode"].asInt();
		respMessage.clear();
		respMessage = jsonData["statusMessage"].asString();

		progressReport(PGD_ALL, PG_PRINTF, "%s response code: %d, message: %s", "Central service problem:", respCode, respMessage);
		err = respCode;
	}
	progressReport(PGD_LOG, PG_ZSTR, "Verification finger services finished");
	return err;
}
