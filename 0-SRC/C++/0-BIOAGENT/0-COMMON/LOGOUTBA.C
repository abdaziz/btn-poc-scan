#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

//#include "global.h"
#include "DIRPATH.H"
#include "LOGMGMT.H"
#include "LOGOUTP.H"


/****************************************************************************/

#define PROGRESS_LOG_FILENAME       "FingerAgent.log"


/****************************************************************************/

int statOutCon;


/****************************************************************************/
/*                                                                          */
/****************************************************************************/

int logFileOutput (char *msg, int addFlag)
{
static time_t logCreateTime = 0;
static char path [_MAX_PATH] = { 0 };


    if (addFlag != 1) {
        if (*path == 0) {
            getLogDir (path, sizeof (path));
            strcat_s (path, sizeof (path), PROGRESS_LOG_FILENAME);
        }
        logRecycle (&logCreateTime, path);
        logOutput (path, NULL, msg);
    }

    return 0;
}


