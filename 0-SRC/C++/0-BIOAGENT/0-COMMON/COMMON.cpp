#include <windows.h>
#include "FSERR.H"
#include "curl\curl.h"
#include "json\json.h"
#include "COMMON.h"

static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp);


static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
	((std::string*)userp)->append((char*)contents, size * nmemb);
	return size * nmemb;
}

const char* GetUrlRoute(char* endpoint, char* route)
{
	std::string strTemporary;
	strTemporary.assign(endpoint);
	strTemporary.append(route);
	char *urlRouting = new char[strTemporary.length() + 1];
	strcpy(urlRouting, strTemporary.c_str());
	strTemporary.clear();
	return urlRouting;
}

int RestAPIRequest(char* jsonData, char* sUrl, char* sAcceptHeader, char* sContentType, char* sCharset, char* sCustomHeader, char* sMethod, int sTimeOut, char** response)
{

	int http_code = 0;
	std::string result;

	char strAcceptHeader[125];
	memset(strAcceptHeader, 0, sizeof(strAcceptHeader));
	strcpy(strAcceptHeader, "Accept: ");
	char strContentType[125];
	memset(strContentType, 0, sizeof(strContentType));
	strcpy(strContentType, "Content-Type: ");
	char strCharset[125];
	memset(strCharset, 0, sizeof(strCharset));
	strcpy(strCharset, "charsets: ");

	strcat(strAcceptHeader, sAcceptHeader);
	strcat(strContentType, sContentType);
	strcat(strCharset, sCharset);

	//=================== begin curl ===================//
	CURL *curl;
	CURLcode res;
	std::string readBuffer;
	curl_global_init(CURL_GLOBAL_ALL);

	/* set header option*/
	struct curl_slist *headers = NULL;
	headers = curl_slist_append(headers, strAcceptHeader);
	headers = curl_slist_append(headers, strContentType);
	headers = curl_slist_append(headers, strCharset);
	//headers = curl_slist_append(headers, strAuthorization);
	if (strlen(sCustomHeader) != 0)
	{
		headers = curl_slist_append(headers, sCustomHeader);
	}

	/* init the curl session */
	curl = curl_easy_init();
	if (curl) {

		/* pass in a pointer to the data - libcurl will not copy */
		if (strlen(jsonData) != 0)
		{
			curl_easy_setopt(curl, CURLOPT_POSTFIELDS, jsonData);
		}
		else
		{
			curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "{}");
		}

		curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, sMethod);
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
		/* handle https connection */
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1L);
		/* specify URL to get */
		curl_easy_setopt(curl, CURLOPT_URL, sUrl);
		/* get verbose debug output please */
		curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
		/* Now specify we want to POST data */
		curl_easy_setopt(curl, CURLOPT_POST, 1L);
		/* complete within n seconds */
		curl_easy_setopt(curl, CURLOPT_TIMEOUT, sTimeOut);
		/* send all data to this function  */
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
		/* we pass our 'chunk' struct to the callback function */
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);

		/* Perform the request, res will get the return code */
		res = curl_easy_perform(curl);

		/* check http statuscode */
		curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &http_code);

		/* check for errors */
		if (res != CURLE_OK){
			switch (res)
			{
			case CURLE_COULDNT_RESOLVE_HOST:
				http_code = HTTP_COULDNT_RESOLVE_SERVICE_HOST_NAME;
				break;
			case CURLE_COULDNT_CONNECT:
				http_code = HTTP_COULDNT_CONNECT_SERVER;
				break;
			case CURLE_SSL_CONNECT_ERROR:
				http_code = HTTP_SSL_CONNECT_ERROR;
				break;
			case CURLE_OPERATION_TIMEDOUT:
				http_code = HTTP_OPERATION_TIMEOUT;
				break;
			case CURLE_RECV_ERROR:
				http_code = HTTP_FAILURE_RECEIVE;
				break;
			default:
				http_code = FSE_SYSTEM_ERROR;
				break;
			}

			char const *errorMessage = curl_easy_strerror(res);

			result.clear();
			result.assign(errorMessage);
		}
		else{

			result.clear();
			result.assign(readBuffer.c_str());
		}

		/* cleanup curl stuff */
		curl_easy_cleanup(curl);
	}

	/* we're done with libcurl, so clean it up */
	curl_global_cleanup();


	char *cstr = new char[result.length() + 1];
	strcpy(cstr, result.c_str());
	result.clear();
	*response = cstr;

	return http_code;
	//=================== end curl ===================//
}